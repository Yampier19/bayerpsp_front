import {useEffect} from 'react';

import {endpoint} from 'secrets';

import {connect} from 'react-redux';
import {helpers_get_request} from 'redux/actions/helpersActions';

const Dependencies = props => {

    const { dispatchDep } = props;

    const helpersReducer = props.helpersReducer;

    const load_dependencies = async () => {
        // console.log('Acá en este load dependencies GLOBAL');
        const dep = [
            {
                url: `${endpoint}/api/location/country`,
                req_name: 'GET_COUNTRIES',
                req_body: null
            },{
                url: `${endpoint}/api/location/department`,
                req_name: 'GET_DEPARTAMENTS',
                req_body: null
            },{
                url: `${endpoint}/api/location/city`,
                req_name: 'GET_CITIES',
                req_body: null
            },{
                url: `${endpoint}/api/operator`,
                req_name: 'GET_LOGISTIC_OPERATOR',
                req_body: null
            },
            // {
            //     url: `${endpoint}/api/insurance`,
            //     req_name: 'GET_INSURANCE',
            //     req_body: null
            // }
        ];

        dep.map( async d => {
            await props.helpers_get_request(d.url, d.req_name, d.req_body);
        });
    }

    useEffect(
        () => {

            if(props.authenticationReducer.isLoggedIn)
                load_dependencies();

        }, [props.authenticationReducer.isLoggedIn]
    );

    useEffect(
        () => {
            const data = helpersReducer['GET_COUNTRIES'].data;

            let _data = {
                countries: []
            };

            if(data != null && helpersReducer['GET_COUNTRIES'].success){

                _data = {
                    countries: data.map(c => ({
                        id: c.id,
                        name: c.name
                    }))
                }

                dispatchDep({
                    type: 'set_countries',
                    payload: _data.countries
                });
            }

        }, [helpersReducer['GET_COUNTRIES']]
    );

    useEffect(
        () => {
            const data = helpersReducer['GET_DEPARTAMENTS'].data;

            let _data = {
                departments: []
            };
            if(data != null && helpersReducer['GET_DEPARTAMENTS'].success){

                _data = {
                    departments: data.map(d => ({
                        id: d.id,
                        name: d.name,
                        country: d.country
                    }))
                }

                dispatchDep({
                    type: 'set_departments',
                    payload: _data.departments
                });
            }

        }, [helpersReducer['GET_DEPARTAMENTS']]
    );

    useEffect(
        () => {
            const data = helpersReducer['GET_CITIES'].data;

            let _data = {
                cities: []
            };
            if(data != null && helpersReducer['GET_CITIES'].success){

                _data = {
                    cities: data.map(c => ({
                        id: c.id,
                        name: c.name,
                        department: c.department,
                        country: c.country
                    }))
                }

                dispatchDep({
                    type: 'set_cities',
                    payload: _data.cities
                });
            }

        }, [helpersReducer['GET_CITIES']]
    );

    useEffect(
        () => {
            const data = helpersReducer['GET_LOGISTIC_OPERATOR'].data;

            let _data = {
                logistic_operators: []
            };
            if(data != null && helpersReducer['GET_LOGISTIC_OPERATOR'].success){

                _data = {
                    logistic_operators: data.map(lo => ({
                        id: lo.id,
                        name: lo.name
                    }))
                }

                dispatchDep({
                    type: 'set_logistic_operators',
                    payload: _data.logistic_operators
                });
            }

        }, [helpersReducer['GET_LOGISTIC_OPERATOR']]
    );

    useEffect(
        () => {
            const data = helpersReducer['GET_INSURANCE'].data;

            let _data = {
                insurances: []
            };
            if(data != null && helpersReducer['GET_INSURANCE'].success){

                _data = {
                    insurances: data.map(i => ({
                        id: i.id,
                        name: i.name,
                        logistic_operator: i.logistic_operator
                    }))
                }

                dispatchDep({
                    type: 'set_insurances',
                    payload: _data.insurances
                });
            }

        }, [helpersReducer['GET_INSURANCE']]
    );

    useEffect(
        () => {


            const dep1 = helpersReducer['GET_COUNTRIES'];
            const dep2 = helpersReducer['GET_DEPARTAMENTS'];
            const dep3 = helpersReducer['GET_CITIES'];
            const dep4 = helpersReducer['GET_LOGISTIC_OPERATOR'];
            const dep5 = helpersReducer['GET_INSURANCE'];



            dispatchDep({
                type: 'req_status',
                loading: dep1.loading && dep2.loading && dep3.loading && dep4.loading && dep5.loading,
                success: dep1.success && dep2.success && dep3.success && dep4.success && dep5.success,
                error: dep1.error || dep2.error || dep3.error || dep4.error || dep5.error
            });

        }, [
            helpersReducer['GET_COUNTRIES'],
            helpersReducer['GET_DEPARTAMENTS'],
            helpersReducer['GET_CITIES'],
            helpersReducer['GET_LOGISTIC_OPERATOR'],
            helpersReducer['GET_INSURANCE']
        ]
    );

    return null;
}

const mapStateToPros = state => ({
    helpersReducer: state.helpersReducer,
    authenticationReducer: state.authenticationReducer
});

export default connect(
    mapStateToPros,
    {
        helpers_get_request
    }
)(Dependencies);
