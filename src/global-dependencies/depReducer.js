export const dependencies_initState = {
    loading: false,
    success: false,
    error: false,
    countries: [],
    departments: [],
    cities: [],
    logistic_operators: [],
    insurances: []
};

export const depReducer = (state, action) => {
    switch (action.type) {
        case 'req_status':
            return{...state, loading: action.loading, success: action.success, error: action.error};
        case 'set_countries': return{...state, countries: action.payload};
        case 'set_departments': return{...state, departments: action.payload};
        case 'set_cities': return{...state, cities: action.payload};
        case 'set_logistic_operators': return{...state, logistic_operators: action.payload};
        case 'set_insurances': return{...state, insurances: action.payload};
        default: return{...state}
    }
}
