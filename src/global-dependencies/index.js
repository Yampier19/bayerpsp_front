import {useReducer} from 'react';

import {dependencies_initState, depReducer} from './depReducer';
import Dependencies from './dependencies';

const GlobalDependencies = props => {

    const [dependencies, dispatchDep] =
        useReducer(depReducer, dependencies_initState);

    return(
        <Dependencies dependencies={dependencies} dispatchDep={dispatchDep}/>
    );
}

export default GlobalDependencies;
//<Dependencies dependencies={dependencies} dispatchDep={dispatchDep}/>
