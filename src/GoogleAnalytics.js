import React, {useEffect} from 'react';
import { withRouter } from 'react-router-dom';
import ReactGA from 'react-ga';

const GoogleAnalytics = () => {
    useEffect(() => {
        ReactGA.initialize('G-TWMF6DY06J');
      }, []);

      return(null)
}

export default withRouter(GoogleAnalytics);