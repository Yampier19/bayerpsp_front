const getCredentials = () => {
  const credentials = localStorage.getItem("session") || sessionStorage.getItem("session");
  if(!credentials) return undefined;
  return JSON.parse(credentials)
};

const setCredentials = (credentials, remember) => {
  const storage = remember ? localStorage : sessionStorage;
  storage.setItem("session", JSON.stringify(credentials))
};

const clearCredentials = () => {
  localStorage.removeItem("session");
  sessionStorage.removeItem("session");
}

export const useLogin = () => {
  const credentials = getCredentials();
  const login = setCredentials;
  const logout = clearCredentials;
  const isLogin = Boolean(credentials);
  return { isLogin, credentials, login, logout };
}
