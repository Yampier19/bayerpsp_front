const storageKey = "country"
const getCountry = () => {
  const country = localStorage.getItem(storageKey) || sessionStorage.getItem(storageKey);
  if (!country) return 1;
  return JSON.parse(country)
};

const setCountry = (country, remember) => {
  const storage = remember ? localStorage : sessionStorage;
  storage.setItem(storageKey, JSON.stringify(country))
};

const clearCountry = () => {
  localStorage.removeItem(storageKey);
  sessionStorage.removeItem(storageKey);
}

export const useCountry = () => {
  const country = getCountry();
  const set = setCountry;
  const clear = clearCountry;
  return { country, set, clear };
}
