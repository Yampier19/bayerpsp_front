//react router

import { BrowserRouter as Router } from "react-router-dom";
import { QueryClientProvider } from "react-query";
import { QueryClient } from "react-query";


//styles
import "./sty.scss";
import "./newColors.scss";
import "./scroll.scss";
import "./utils.scss";
import "react-toastify/dist/ReactToastify.css";

import GlobalDependencies from "./global-dependencies";

import LoginSystem from "components/base/login-system";
import ModalSystem from "components/base/modal-system";

import EntryPoint from "components/pages/entry-point";
import HomePage from "components/pages/psp/home-page";
import CreatePage from "components/pages/psp/create-page";
import NewsPage from "components/pages/psp/news-page";
import TrackingPage from "components/pages/psp/tracking-page";
import ProductsPage from "components/pages/psp/products-page";
import ReportsPage from "components/pages/psp/reports-page";

import CRSHomePage from "components/pages/crs/home-page";
import CRSCreatePage from "components/pages/crs/create-page";
import CRSTrackingPage from "components/pages/crs/tracking-page";

import APHomePage from "components/pages/support/home-page";
import APCreatePage from "components/pages/support/create-page";
import APTrackingPage from "components/pages/support/tracking-page";
import APReportsPage from "components/pages/support/reports-page";
import ConfigurationPage from "components/pages/configuration/configuration-page";

import UserProvider from "providers/user/userProvider";
import { AuthorizationSystemProvider } from "providers/authorization/system/authorizationSystemProvider";
import { AuthorizationActionProvider } from "providers/authorization/action/authorizationActionProvider";
import ToastContainer from "components/base/toast/ToastContainer";

import GoogleAnalytics from "GoogleAnalytics";


// import Pagination from 'pagination';
const App = () => {
  const queryClient = new QueryClient();


  return (
    <QueryClientProvider client={queryClient}>
      <Router>
      <GoogleAnalytics />
        <ToastContainer />
        <GlobalDependencies />
        <LoginSystem />
        <ModalSystem />
        <EntryPoint />
        <UserProvider>
          <AuthorizationSystemProvider>
            <AuthorizationActionProvider>
              <HomePage />
              <CreatePage />
              <NewsPage />
              <TrackingPage />
              <ProductsPage />
              <ReportsPage />

              <CRSHomePage />
              <CRSCreatePage />
              <CRSTrackingPage />

              <APHomePage />
              <APCreatePage />
              <APTrackingPage />
              <APReportsPage />

              <ConfigurationPage />
            </AuthorizationActionProvider>
          </AuthorizationSystemProvider>
        </UserProvider>
      </Router>
    </QueryClientProvider>
  );
};

export default App;
