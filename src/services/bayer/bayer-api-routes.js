export const bayerApiRoutes = {
  auth: {
    login: () => `/api/login`,
    logout: () => `/api/logout`,
    signUp: () => `/api/register`,
    forgotPasswordEmail: () => `/api/password/email`,
    forgotPasswordCode: () => `/api/password/code`,
    forgorPasswordReset: () => `/api/password/reset`,
    verifyToken: () => `/api/auth`,
  },
  user: {
    update: (idUser) => `/api/user/update/${idUser}`,
    delete: (idUser) => `/api/user/delete/${idUser}`,
    listAll: () => `/api/users`,
    create: () => `/api/register`,
    findOne: (idUser) => `/api/user/show/${idUser}`,
  },
  patient: {
    register: () => `/api/patient`,
    edit: (idPatient) => `/api/patient/${idPatient}`,
    listAll: () => `/api/patient`,
    listOne: (idPatient) => `/api/patient/${idPatient}`,
    listByDocumentNumber: (doc) => `/api/patient/document/${doc}`,
    statusPatient: () => `/api/statu/patient`,
    pathologicalClassification: (id) =>
      `/api/classification/pathological/${id}`,
    listOperators: () => `/api/operator`,
    claimNoCause: () => `/api/claim/cause`,
    reasonEducation: () => `/api/education/reason`,
    themeEducation: () => `/api/education/theme`,
    treatment: () => `/api/treatment`,
    // TODO: change this routes below
    dose: (id) => `/api/doses/product/${id}`,
    listEnsuranceByDepartment: (department = 0) =>
      `/api/insurance/${department}`,
    listLogisticOperatorByInsurance: (insuranceId) =>
      `/api/operator/insurance/${insuranceId}`,
  },
  treatment: {
    create: () => `/api/treatment`,
    edit: (idTreatment) => `/api/treatment/${idTreatment}`,
    listAll: () => `/api/treatment`,
    listOne: (idTreatment) => `/api/treatment/${idTreatment}`,
    trackingCreate: (idTreatment) => `/api/follows/treatment/${idTreatment}`,
    trackingCreateComunication: (idTreatment) =>
      `/api/follows/treatment/comunication/${idTreatment}`,
    status: () => `/api/statu/treatment/`,
    previous: () => `/api/previou/treatment`,
    statusByProduct: (id) => `/api/statu/treatment/${id}`,
    historyTreatment: (id) => `/api/claim/history/treatment/${id}`,
  },
  location: {
    countrys: () => `/api/location/country`,
    listAllDepartments: () => `/api/location/department`,
    listOneDeparment: (idDepartment) =>
      `/api/location/department/${idDepartment}`,
    listAllCities: () => `/api/location/city`,
    listCitiesByDepartment: (idDepartment = 0) =>
      `/api/location/city/${idDepartment}`,
    zones: (id) => `/api/location/zone/${id}`,
    citiesByZone: (id) => `/api/location/city/zone/${id}`,
  },
  eps: {
    listAll: () => `/api/eps`,
    listOneByInsurance: (idInsurance) => `/api/handle/eps/${idInsurance}`,
  },
  insurance: {
    listAll: () => `/api/insurance`,
  },
  role: {
    listAll: () => `/api/role`,
    listOne: (idRole) => `/api/role/${idRole}`,
  },
  novelty: {
    create: () => `/api/novelty`,
    listAll: () => `/api/novelty`,
    update: (idNovelty) => `/api/novelty/${idNovelty}`,
    listOne: (idNovelty) => `/api/novelty/${idNovelty}`,
    history: (idNovelty) => `/api/history/novelty/${idNovelty}`,
    listComments: (idNovelty) => `/api/comment/novelty/${idNovelty}`,
    createComment: (idNovelty) => `/api/comment/novelty/${idNovelty}`,
    responseComment: (idNovelty) =>
      `/api/comment/novelty/response/${idNovelty}`,
    status: () => `/api/statu/novelty`,
  },
  medical: {
    create: () => `/api/medical/user`,
    edit: (idMedico) => `/api/medical/user/${idMedico}`,
    listOne: (idMedico) => `/api/medical/user/${idMedico}`,
    listAll: () => `/api/medical/user`,
    delete: (idMedico) => `/api/medical/user/${idMedico}`,
  },
  replacement: {
    create: () => `/api/replacement`,
    listAll: () => `/api/replacement`,
    listOne: (idReplacement) => `/api/replacement/${idReplacement}`,
    edit: (idReplacement) => `/api/replacement/${idReplacement}`,
    createComment: () => `/api/replacement/comment`,
    listOneComment: (idComment) => `/api/replacement/comment/${idComment}`,
    editComment: (idComment) => `/api/replacement/comment/${idComment}`,
    deleteComment: (idComment) => `/api/replacement/comment/${idComment}`,
  },
  product: {
    listAll: () => `/api/product`,
    doses: (idProduct) => `/api/doses/product/${idProduct}`,
    listProductByMedicalProduct: (idProduct) =>
      `/api/supplie/incomes/product/${idProduct}`,
  },
  doctor: {
    listAll: () => `/api/doctor`,
  },
  provider: {
    listAll: () => `/api/provider`,
  },
  follow: {
    listOne: (id) => `/api/follows/treatment/${id}`,
  },
  diagnosticSupport: {
    create: () => `/api/diagnostic/support`,
    edit: (id) => `/api/diagnostic/support/${id}`,
    listAll: () => `/api/diagnostic/support`,
    listOneById: (id) => `/api/diagnostic/support/${id}`,
    deleteOneById: (id) => `/api/diagnostic/support/${id}`,
    listByTreatment: (id) => `/api/diagnostic/support/treatment/${id}`,
  },
};
