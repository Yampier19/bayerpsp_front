import axios from "axios";

import { useLogin } from "../../hooks/useLogin";
import Configuration from "../../config/configuration";
import { useCountry } from "hooks/useCountry";

export const BayerPSPAxios = axios.create();

BayerPSPAxios.interceptors.request.use((config) => {
  const { isLogin, credentials } = useLogin();
  if (isLogin) {
    config.headers.Authorization = `${credentials.token_type} ${credentials.access_token}`;
  }
  return config;
});

BayerPSPAxios.interceptors.request.use((config) => {
  const { country } = useCountry()
  config.headers.country = country
  return config;
})

BayerPSPAxios.interceptors.request.use((config) => {
  const baseURL = Configuration.api.hostname;
  return { baseURL, ...config };
});