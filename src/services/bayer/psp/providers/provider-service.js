import { BayerPSPAxios } from "../../bayer-api-axios";
import { bayerApiRoutes } from "../../bayer-api-routes";

export const listAllProviders = () => {
  const url = bayerApiRoutes.provider.listAll();
  return BayerPSPAxios.get(url).then((res) => res.data);
};
