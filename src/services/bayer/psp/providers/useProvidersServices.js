import { useQuery } from "react-query";
import { listAllProviders } from "./provider-service";

export const useListAllProviders = (options = {}) => {
  return useQuery(["providers/list"], listAllProviders, options);
};
