import { useQuery } from "react-query";
import { listAllDepartments } from "./departments-service";

export const useListAllDepartements = (options = {}) => {
  return useQuery(["location/departments"], listAllDepartments, options);
};
