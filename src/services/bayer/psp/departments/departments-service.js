import { BayerPSPAxios } from "../../bayer-api-axios";
import { bayerApiRoutes } from "../../bayer-api-routes";

export const listAllDepartments = () => {
  const url = bayerApiRoutes.location.listAllDepartments();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listAllCountries = () => {
  const url = bayerApiRoutes.location.countrys();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listOneDeparment = () => {
  const url = bayerApiRoutes.location.listOneDeparment();
  return BayerPSPAxios.get(url).then((res) => res.data);
};
