import { useQuery } from "react-query";
import { listAll } from "./product-service";
import { listByMedicalProduct } from "./product-service";

export const useListAllProducts = (config = {}) => {
  return useQuery(["products/all"], listAll, config);
};

export const useListProductByMedicalProduct = (id, config = {}) => {
  return useQuery(
    ["producst/oneByProductMedical"],
    () => listByMedicalProduct(id),
    config
  );
};
