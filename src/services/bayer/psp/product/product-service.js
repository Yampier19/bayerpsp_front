import { BayerPSPAxios } from "../../bayer-api-axios";
import { bayerApiRoutes } from "../../bayer-api-routes";

export const listAll = () => {
  const url = bayerApiRoutes.product.listAll();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listByMedicalProduct = (id) => {
  const url = bayerApiRoutes.product.listProductByMedicalProduct(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};
