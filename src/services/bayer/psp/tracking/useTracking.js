import { useQuery } from "react-query";
import { useMutation } from "react-query";
import { getOne } from "./tracking-service";
import { getAll } from "./tracking-service";
import { createDiagnosticSupport } from "./tracking-service";
import { editDiagnosticSupport } from "./tracking-service";
import { getAllDiagnosticSupport } from "./tracking-service";
import { getDiagnosticSupportByTreatment } from "./tracking-service";

export const useGetTreatmentTracking = (id = 0, config) => {
  return useQuery(["follows/treatment"], () => getOne(id), config);
};

export const useListAll = (config) => {
  return useQuery(["treatment"], getAll, config);
};

export const useListAllDiagnoticsSupport = (config = {}) => {
  return useQuery(
    ["diagnosticSupport/listAll"],
    getAllDiagnosticSupport,
    config
  );
};

export const useCreateDiagnosticSupport = (config = {}) => {
  return useMutation(
    (data) => createDiagnosticSupport(data).then((res) => res),
    config
  );
};

export const useEditDiagnosticSupport = (config = {}) => {
  return useMutation(
    ({ id, data }) => editDiagnosticSupport(id, data).then((res) => res.data),
    config
  );
};

export const useListDiagnosticByTreatment = (id, config = {}) => {
  return useQuery(
    ["diagnosticSupport/listByTreatment"],
    () => getDiagnosticSupportByTreatment(id),
    config
  );
};
