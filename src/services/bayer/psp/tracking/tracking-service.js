import { BayerPSPAxios } from "../../bayer-api-axios";
import { bayerApiRoutes } from "../../bayer-api-routes";

export const getOne = (id = 0) => {
  const url = bayerApiRoutes.follow.listOne(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const getAll = () => {
  const url = bayerApiRoutes.patient.listAll();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const createDiagnosticSupport = (data) => {
  const url = bayerApiRoutes.diagnosticSupport.create();
  console.log("data", data);
  return BayerPSPAxios.post(url, data, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  }).then((res) => res.data);
};

export const editDiagnosticSupport = (id, data) => {
  const url = bayerApiRoutes.diagnosticSupport.edit(id);
  return BayerPSPAxios.put(url, data).then((res) => res.data);
};

export const getAllDiagnosticSupport = () => {
  const url = bayerApiRoutes.diagnosticSupport.listAll();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const getDiagnosticSupportByTreatment = (id) => {
  const url = bayerApiRoutes.diagnosticSupport.listByTreatment(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};
