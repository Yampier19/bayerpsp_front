import { useQuery } from "react-query";
import { listAllCountries } from "./countries-service";

export const useListAllCountries = (options = {}) => {
  return useQuery(["location/countries"], listAllCountries, options);
};
