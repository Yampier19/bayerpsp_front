import { BayerPSPAxios } from "../../bayer-api-axios";
import { bayerApiRoutes } from "../../bayer-api-routes";

export const listAllCountries = () => {
  const url = bayerApiRoutes.location.countrys();
  return BayerPSPAxios.get(url).then((res) => res.data);
};
