import { useQuery } from "react-query";
import { useMutation } from "react-query";
import { getOneById } from "./treatment-service";
import { editTreatmentById } from "./treatment-service";
import { historyClaimTreatment } from "./treatment-service";
import { createTreatment } from "./treatment-service";

export const useListOneById = (id, config = {}) => {
  return useQuery(["treatment/listOne"], () => getOneById(id), config);
};

export const useEditTreatmentById = (config = {}) => {
  return useMutation(
    ({ id, data }) => editTreatmentById(id, data).then((res) => res),
    config
  );
};

export const useHistoryClaimByTreatment = (id, config = {}) => {
  return useQuery(
    ["treatment/histroyClaim"],
    () => historyClaimTreatment(id),
    config
  );
};

export const useCreateTreatment = (config = {}) => {
  return useMutation(
    (data) => createTreatment(data).then((res) => res),
    config
  );
};
