import { BayerPSPAxios } from "../../bayer-api-axios";
import { bayerApiRoutes } from "../../bayer-api-routes";

export const getOneById = (id) => {
  const url = bayerApiRoutes.treatment.listOne(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const editTreatmentById = (id, data) => {
  const url = bayerApiRoutes.treatment.edit(id);
  return BayerPSPAxios.put(url, data).then((res) => res.data);
};

export const historyClaimTreatment = (id) => {
  const url = bayerApiRoutes.treatment.historyTreatment(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const createTreatment = (data) =>{
  const url = bayerApiRoutes.treatment.create();
  return BayerPSPAxios.post(url, data).then((res) => res.data);
}
