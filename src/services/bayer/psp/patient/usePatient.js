import { useQuery, useMutation } from "react-query";
import {
  listStatus,
  listAll,
  listClaims,
  listNoEducationReason,
  listPathologicalClassifications,
  listDose,
  listTreatments,
  listInsuranceByDeparmentId,
  listLogisticOperatorByInsurance,
  listAllDoctors,
  listPreviousTreatments,
  createPatient,
  listEducationThemes,
  listTreatmentStatusByProductId,
  listPathologicalClassificationsByProductId,
  editPatient,
  listZones,
  listCitiesByZone,
  listPatientsByDocument
} from "./patient-service";

export const useListStatus = (config = {}) => {
  return useQuery(["patient/status"], listStatus, config);
};

export const useListAllPatients = (config = {}) => {
  return useQuery(["patient/all"], listAll, config);
};

export const useListClaims = (config = {}) => {
  return useQuery(["claim/all"], listClaims, config);
};

export const useNoEducationReason = (config = {}) => {
  return useQuery(["education/reason"], listNoEducationReason, config);
};

export const usePathologicalClassification = (config = {}) => {
  return useQuery(
    ["classification/pathological"],
    listPathologicalClassifications,
    config
  );
};

export const usePathologicalClassificationByProduct = (id = 0, config = {}) => {
  return useQuery(
    ["classification/pathologicalproduct"],
    () => listPathologicalClassificationsByProductId(id),
    config
  );
};

export const useDose = (id = 0, config = {}) => {
  return useQuery(["dose/all"], () => listDose(id), config);
};

export const useAllTreatments = (config = {}) => {
  return useQuery(["treatments/user"], listTreatments, config);
};

export const useListInsuranceByDepartmentId = (id = 0, config = {}) => {
  return useQuery(
    ["insurance/department"],
    () => listInsuranceByDeparmentId(id),
    config
  );
};

export const useListLogisticOperatorsByEnsurance = (id = 0, config = {}) => {
  return useQuery(
    ["operator/ensurance"],
    () => listLogisticOperatorByInsurance(id),
    config
  );
};

export const useListAllDoctors = (config = {}) => {
  return useQuery(["doctor/all"], listAllDoctors, config);
};

export const usePreviusTreatments = (config = {}) => {
  return useQuery(["previou/treatment"], listPreviousTreatments, config);
};

export const useCreatePatient = (config = {}) => {
  return useMutation(
    (patient = {}) => createPatient(patient).then((res) => res),
    config
  );
};

export const useEditPatient = (config = {}) => {
  return useMutation(
    (patient = {}) => editPatient(patient.id, patient.data).then((res) => res),
    config
  );
};

export const useEducationThemes = (config = {}) => {
  return useQuery(["education/theme"], listEducationThemes, config);
};

export const useTreatmentStatus = (metadata = {}, config) => {
  return useQuery(
    ["treatment/status"],
    () => listTreatmentStatusByProductId(metadata),
    config
  );
};

export const useListZones = (country_id, config = {}) => {
  return useQuery(["zones"], () => listZones(country_id), config);
};

export const useListCitiesByZone = (zone_id, config = {}) => {
  return useQuery(["citi/zone"], () => listCitiesByZone(zone_id), config);
}

export const useGetPatientByDocumentNumber = (document_number, config = {}) => {
  return useQuery(["patient/document"], () => listPatientsByDocument(document_number), config);
}