import { BayerPSPAxios } from "../../bayer-api-axios";
import { bayerApiRoutes } from "../../bayer-api-routes";

export const listStatus = () => {
  const url = bayerApiRoutes.patient.statusPatient();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listAll = () => {
  const url = bayerApiRoutes.patient.listAll();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listClaims = () => {
  const url = bayerApiRoutes.patient.claimNoCause();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listNoEducationReason = () => {
  const url = bayerApiRoutes.patient.reasonEducation();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listPathologicalClassifications = () => {
  const url = bayerApiRoutes.patient.pathologicalClassification();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listPathologicalClassificationsByProductId = (id) => {
  const url = bayerApiRoutes.patient.pathologicalClassification(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listDose = (id) => {
  const url = bayerApiRoutes.patient.dose(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listTreatments = () => {
  const url = bayerApiRoutes.patient.treatment();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listInsuranceByDeparmentId = (id = 0) => {
  const url = bayerApiRoutes.patient.listEnsuranceByDepartment(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listLogisticOperatorByInsurance = (id) => {
  const url = bayerApiRoutes.patient.listLogisticOperatorByInsurance(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listAllDoctors = () => {
  const url = bayerApiRoutes.doctor.listAll();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listPreviousTreatments = () => {
  const url = bayerApiRoutes.treatment.previous();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const createPatient = (patient = {}) => {
  const url = bayerApiRoutes.patient.register();
  return BayerPSPAxios.post(url, patient).then((res) => res.data);
};

export const editPatient = (id, patient = {}) => {
  const url = bayerApiRoutes.patient.edit(id);
  return BayerPSPAxios.put(url, patient).then((res) => res.data);
};

export const listEducationThemes = () => {
  const url = bayerApiRoutes.patient.themeEducation();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listTreatmentStatusByProductId = (id = 0) => {
  const url = bayerApiRoutes.treatment.statusByProduct(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listZones = (id) => {
  const url = bayerApiRoutes.location.zones(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listCitiesByZone = (id) => {
  const url = bayerApiRoutes.location.citiesByZone(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listPatientsByDocument = (doc) => {
  const url = bayerApiRoutes.patient.listByDocumentNumber(doc);
  return BayerPSPAxios.get(url).then((res) => res.data);
}