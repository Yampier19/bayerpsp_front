import { useQuery } from "react-query";
import { useMutation } from "react-query";
import { listAllUsers } from "./users-services";
import { createUser } from "./users-services";
import { deleteUser } from "./users-services";
import { findOneUser } from "./users-services";
import { editUser } from "./users-services";

export const useListAllUsers = (config = {}) => {
  return useQuery(["users/list"], listAllUsers, config);
};

export const useCreateUser = (config = {}) => {
  return useMutation((data) => createUser(data).then((res) => res), config);
};

export const useDeleteUser = (config = {}) => {
  return useMutation((id) => deleteUser(id).then((res) => res.data), config);
};

export const useFindOneUser = (id, config = {}) => {
  return useQuery(["users/findOne", id], () => findOneUser(id), config);
};

export const useEditUser = (config = {}) => {
  return useMutation(
    ({ id, data }) => editUser(id, data).then((res) => res.data),
    config
  );
};
