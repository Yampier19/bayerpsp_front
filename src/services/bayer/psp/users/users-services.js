import { BayerPSPAxios } from "../../bayer-api-axios";
import { bayerApiRoutes } from "../../bayer-api-routes";

export const listAllUsers = () => {
  const url = bayerApiRoutes.user.listAll();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const createUser = (data) => {
  const url = bayerApiRoutes.user.create();
  return BayerPSPAxios.post(url, data).then((res) => res.data);
};

export const deleteUser = (id) => {
  const url = bayerApiRoutes.user.delete(id);
  return BayerPSPAxios.post(url).then((res) => res);
};

export const findOneUser = (id) => {
  const url = bayerApiRoutes.user.findOne(id);
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const editUser = (id, data) => {
  const url = bayerApiRoutes.user.update(id);
  return BayerPSPAxios.post(url, data).then((res) => res);
};
