import { useQuery } from "react-query";
import { listCitiesByDepartment } from "./cities-service";

export const useListCitiesByDepartment = (departmentId = 0, options = {}) => {
  return useQuery(["location/cityes"], () => listCitiesByDepartment(departmentId), options);
};
