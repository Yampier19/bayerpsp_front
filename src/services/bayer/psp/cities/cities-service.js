import { BayerPSPAxios } from "../../bayer-api-axios";
import { bayerApiRoutes } from "../../bayer-api-routes";

export const listCitiesByDepartment = (departmentId = 0) => {
  const url = bayerApiRoutes.location.listCitiesByDepartment(departmentId);
  return BayerPSPAxios.get(url).then((res) => res.data);
};
