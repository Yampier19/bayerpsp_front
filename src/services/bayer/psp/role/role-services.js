import { BayerPSPAxios } from "../../bayer-api-axios";
import { bayerApiRoutes } from "../../bayer-api-routes";

export const listAllRoles = () => {
  const url = bayerApiRoutes.role.listAll();
  return BayerPSPAxios.get(url).then((res) => res.data);
};

export const listOneRole = (idRole) => {
  const url = bayerApiRoutes.role.listOne(idRole);
  return BayerPSPAxios.get(url).then((res) => res.data);
};
