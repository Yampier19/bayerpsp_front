import { useQuery } from "react-query";
import { listAllRoles } from "./role-services";
import { listOneRole } from "./role-services";

export const useListAllRoles = (config = {}) => {
  return useQuery(["role/list"], listAllRoles, config);
};

export const useListOneRole = (idRole, options = {}) => {
  return useQuery(["role/one"], () => listOneRole(idRole), options);
};
