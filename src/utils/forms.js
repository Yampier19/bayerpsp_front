export const areAllFieldFilled = (form, fields) => {

    for(let i = 0; i < fields; i++)
        if(!form[`check_field${i+1}`].checked)
            return false;

    return true;

}

export const getRequired = schema => {
    const required = [];
    const fieldskeys = Object.keys(schema.fields);
    const fieldsValues = Object.values(schema.fields);

    fieldsValues.map( (f, i) => f.name = fieldskeys[i] );
    fieldsValues.forEach((f, i) => {
        if(f.spec.presence === 'required')
            required.push(f.name);
    });

    return required;
}
