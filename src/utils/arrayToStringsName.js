export const arrayToStringsName = (data, name) => {
  let string = "";
  data.forEach((d, index) => {
    string += `${index % 2 === 1 ? ", " : ""}${d[name]}`;
  });
  if (string !== "") {
    return string;
  } else {
    return "";
  }
};
