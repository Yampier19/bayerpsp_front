export const ConvertToFormData = (values) => {
  const form = new FormData();
  if (values instanceof Object) {
    Object.keys(values).forEach((key) => {
      const data = values[key];
      if (Array.isArray(data)) {
        data.forEach((val) => {
          if (val instanceof Object) {
            form.append(`${key}[]`, JSON.stringify(val));
          } else {
            form.append(`${key}[]`, val);
          }
        });
      } else {
        form.append(key, data);
      }
    });
  }
  return form;
};
