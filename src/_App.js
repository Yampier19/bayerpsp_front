//react router
import {
  BrowserRouter as Router,
  Redirect,
  Switch,
  Route,
} from "react-router-dom";


//common components
import Sidebar from "./components/commons/sidebar";
import Sidebar2 from "./components/commons/sidebar/crs-sidebar";
import Sidebar3 from "./components/commons/sidebar/support-sidebar";

import Main from "./components/commons/main";
import ModalSystem from "./components/base/modal-system";

//routes
import LoginEnter from "./components/routes/psp/login/enter";
import LoginPassword from "./components/routes/psp/login/password";
import LoginCode from "./components/routes/psp/login/code";
import LoginRecover from "./components/routes/psp/login/recover";

import Home from "./components/routes/psp/home";

import CreateNew from "./components/routes/create/new";
import CreatePatient from "./components/routes/create/patient";
import CreateEPS from "./components/routes/create/eps-opl";

import Form from "./components/routes/create/eps-opl/form";

import Consult from "./components/routes/news/consult";
import History from "./components/routes/news/history";
import Update from "./components/routes/news/update";
import Comments from "./components/routes/news/comments";
import Assigment from "./components/routes/news/assignment";

import Tracking from "./components/routes/tracking/main";
import TrackingUpdatePatient from "./components/routes/tracking/update";
import TrackingHistory from "./components/routes/tracking/history";
import TrackingHistory2 from "./components/routes/tracking/history2";

import Products from "./components/routes/psp/products/filters";
import ProductsEditIncomings from "./components/routes/psp/products/edit/incomings";
import ProductsEditOutcomings from "./components/routes/psp/products/edit/outcomings";
import ProductsEditSuppliers from "./components/routes/psp/products/edit/suppliers";

import Reports from "./components/routes/reports/main";
import ReportsCreate from "./components/routes/reports/create";
import DataList from "./components/routes/reports/data-list";
import ReportsGraphics from "./components/routes/reports/graphics";

import ConfigurationFundem from "./components/routes/configuration/fundem";
import ConfiguracionDate from "./components/routes/configuration/date";
import ConfigurationUsers from "./components/routes/configuration/users";
import ConfigurationCreate from "./components/routes/configuration/create";
import ConfigurationTemplates from "./components/routes/configuration/templates";
import ConfigurationHistory from "./components/routes/configuration/history";
import ConfigurationEmail from "./components/routes/configuration/email";
import ConfigurationTemplatesEmail from "./components/routes/configuration/templatesEmail";
import ConfigurationEditUser from "./components/routes/configuration/users/edit";

//base components
import ProgressIcon from "./components/base/progress-icon";
import ProgressBar from "./components/base/progress-bar";
import TextEditor from "./components/base/text-editor";
import Inputfile from "./components/base/inputfile";
import Table from "./components/base/table";
import Sections from "./components/base/sections";

import Home2 from "./components/routes/crs/home";
import CreateMedic from "./components/routes/crs/create/medic";
import CreateProvider from "./components/routes/crs/create/provider";
import Tracking2 from "./components/routes/crs/tracking/main";
import TrackingEdit2 from "./components/routes/crs/tracking/edit";

import Home3 from "./components/routes/support/home";
import CreatePatient3 from "./components/routes/support/create/patient";
import Tracking3 from "./components/routes/support/tracking/main";
import TrackingEdit3 from "./components/routes/support/tracking/edit";
import Reports3 from "./components/routes/support/reports/main";
import ReportsCreate3 from "./components/routes/support/reports/create";

import Pagination from "./pagination.js";

//styles
import "./sty.scss";
import "./newColors.scss";
import "./scroll.scss";
import "./utils.scss";

// localStorage.removeItem('token');
// console.log(localStorage.getItem('token'));
function isLoggedIn() {
  const token = localStorage.getItem("token");
  console.log(token == null ? false : true);
  return token == null ? false : true;
}



function App() {
  return (
    <Router>
      <ModalSystem />
      <Switch>
        <Redirect from="/" to="/login" exact />
        <Route exact path="/login">
          <LoginEnter />
        </Route>
        <Route exact path="/forgot-password">
          <LoginPassword />
        </Route>
        <Route exact path="/code">
          <LoginCode />
        </Route>
        <Route exact path="/recover">
          <LoginRecover />
        </Route>
        <Route
          exact
          path="/home"
          render={() =>
            isLoggedIn() ? (
              <div>
                <Sidebar type={2} />
                <Main>
                  <Home />
                </Main>
              </div>
            ) : (
              <Redirect to="/login" />
            )
          }
        />
        <Route exact path="/create/new">
          <Sidebar />
          <Main>
            <CreateNew />
          </Main>
        </Route>
        <Route exact path="/tabs"></Route>
        <Route exact path="/create/patient">
          <Sidebar />
          <Main>
            <CreatePatient />
          </Main>
        </Route>
        <Route exact path="/create/eps-opl">
          <Sidebar />
          <Main>
            <CreateEPS />
          </Main>
        </Route>
        <Route exact path="/news/consult">
          <Sidebar activeSite="2" />
          <Main>
            <Consult />
          </Main>
        </Route>
        <Route exact path="/news/history">
          <Sidebar activeSite="2" />
          <Main>
            <History />
          </Main>
        </Route>
        <Route exact path="/news/update">
          <Sidebar activeSite="2" />
          <Main>
            <Update />
          </Main>
        </Route>
        <Route exact path="/news/comments">
          <Sidebar activeSite="2" />
          <Main>
            <Comments />
          </Main>
        </Route>
        <Route exact path="/news/assignment">
          <Sidebar activeSite="2" />
          <Main>
            <Assigment />
          </Main>
        </Route>
        <Route exact path="/tracking">
          <Sidebar activeSite="3" />
          <Main>
            <Tracking />
          </Main>
        </Route>
        <Route exact path="/tracking/update">
          <Sidebar activeSite="3" />
          <Main>
            <TrackingUpdatePatient />
          </Main>
        </Route>
        <Route exact path="/tracking/history">
          <Sidebar activeSite="3" />
          <Main>
            <TrackingHistory />
          </Main>
        </Route>
        <Route exact path="/tracking/history2">
          <Sidebar activeSite="3" />
          <Main>
            <TrackingHistory2 />
          </Main>
        </Route>
        <Route exact path="/products">
          <Sidebar activeSite="4" />
          <Main>
            <Products />
          </Main>
        </Route>
        <Route exact path="/products/create/incomings">
          <Sidebar activeSite="4" />
          <Main>
            <ProductsEditIncomings />
          </Main>
        </Route>
        <Route exact path="/products/create/outcomings">
          <Sidebar activeSite="4" />
          <Main>
            <ProductsEditOutcomings />
          </Main>
        </Route>
        <Route exact path="/products/create/suppliers">
          <Sidebar activeSite="4" />
          <Main>
            <ProductsEditSuppliers />
          </Main>
        </Route>
        <Route exact path="/reports">
          <Sidebar activeSite="5" />
          <Main>
            <Reports />
          </Main>
        </Route>
        <Route exact path="/reports/create">
          <Sidebar activeSite="5" />
          <Main>
            <ReportsCreate />
          </Main>
        </Route>
        <Route exact path="/reports/data-list">
          <Sidebar activeSite="5" />
          <Main>
            <DataList />
          </Main>
        </Route>
        <Route exact path="/reports/graphics">
          <Sidebar activeSite="5" />
          <Main>
            <ReportsGraphics />
          </Main>
        </Route>
        <Route exact path="/configuration/fundem">
          <Sidebar activeSite="6" />
          <Main>
            <ConfigurationFundem />
          </Main>
        </Route>
        <Route exact path="/configuration/date">
          <Sidebar activeSite="6" />
          <Main>
            <ConfiguracionDate />
          </Main>
        </Route>
        <Route exact path="/configuration/users">
          <Sidebar activeSite="6" />
          <Main>
            <ConfigurationUsers />
          </Main>
        </Route>
        <Route exact path="/configuration/users/edit/:id">
          <Sidebar activeSite="6" />
          <Main>
            <ConfigurationEditUser />
          </Main>
        </Route>
        <Route exact path="/configuration/create">
          <Sidebar activeSite="6" />
          <Main>
            <ConfigurationCreate />
          </Main>
        </Route>
        <Route exact path="/configuration/templates">
          <Sidebar activeSite="6" />
          <Main>
            <ConfigurationTemplates />
          </Main>
        </Route>
        <Route exact path="/configuration/history">
          <Sidebar activeSite="6" />
          <Main>
            <ConfigurationHistory />
          </Main>
        </Route>
        <Route exact path="/configuration/email">
          <Sidebar activeSite="6" />
          <Main>
            <ConfigurationEmail />
          </Main>
        </Route>
        <Route exact path="/configuration/templatesemail">
          <Sidebar activeSite="6" />
          <Main>
            <ConfigurationTemplatesEmail />
          </Main>
        </Route>
        <Route exact path="/crs/home">
          <Sidebar2 type={2} />
          <Main>
            <Home2 />
          </Main>
        </Route>
        <Route exact path="/crs/create/medic">
          <Sidebar2 activeSite={2} />
          <Main>
            <CreateMedic />
          </Main>
        </Route>
        <Route exact path="/crs/create/provider">
          <Sidebar2 activeSite={2} />
          <Main>
            <CreateProvider />
          </Main>
        </Route>
        <Route exact path="/crs/tracking">
          <Sidebar2 activeSite={3} />
          <Main>
            <Tracking2 />
          </Main>
        </Route>
        <Route exact path="/crs/tracking/edit">
          <Sidebar2 activeSite={3} />
          <Main>
            <TrackingEdit2 />
          </Main>
        </Route>
        <Route exact path="/support/home">
          <Sidebar3 type={2} />
          <Main>
            <Home3 />
          </Main>
        </Route>
        <Route exact path="/support/create/patient">
          <Sidebar3 activeSite="2" />
          <Main>
            <CreatePatient3 />
          </Main>
        </Route>
        <Route exact path="/support/tracking">
          <Sidebar3 activeSite="3" />
          <Main>
            <Tracking3 />
          </Main>
        </Route>
        <Route exact path="/support/tracking/edit">
          <Sidebar3 activeSite="3" />
          <Main>
            <TrackingEdit3 />
          </Main>
        </Route>
        <Route exact path="/support/reports">
          <Sidebar3 activeSite="4" />
          <Main>
            <Reports3 />
          </Main>
        </Route>
        <Route exact path="/support/reports/create">
          <Sidebar type={2} />
          <Main>
            <ReportsCreate3 />
          </Main>
        </Route>
        <Route exact path="/comment">
          <Sidebar />
          <Main>
            <TextEditor id="1" placeholder="escribe un comentario..." />
          </Main>
        </Route>
        <Route exact path="/filee">
          <Sidebar />
          <Main>
            <Inputfile />
          </Main>
        </Route>
        <Route exact path="/tests">
          <div className="App">
            <div className="container py-6">
              <ProgressIcon
                progress={100}
                className="has-background-primary has-text-white"
              />
              <br />
              <ProgressBar className="is-primary" />
              <Form />
              {/*<Modal>
                                </Modal>*/}
            </div>
          </div>
        </Route>
        <Route exact path="/sidebar">
          <Sidebar />
          <Main>
            <h1 className="title has-text-primary">Example</h1>
          </Main>
        </Route>
        <Route exact path="/table">
          <Table />
        </Route>
        <Route exact path="/pagination">
          <Pagination />
        </Route>
        <Route exact path="/sections">
          <Sections />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
