import LoadingComponent from 'components/base/loading-component';
// import Sidebar from 'components/commons/sidebar';

import {Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';

const _PrivateRoute = props => {

    const {isLoggedIn, isLoadingFromLocal} = props.authenticationReducer;

    return(
        <Route exact={props.exact} path={props.path}>
            {
                isLoadingFromLocal ?
                    <div style={{height: '100%'}}>
                        <LoadingComponent loadingText="Validando sesión"/>
                    </div>
                :
                isLoggedIn ?
                    props.children
                :
                    <Redirect to="/login"/>
            }
        </Route>
    );
}

const mapStateToProps = state => ({
    authenticationReducer: state.authenticationReducer
});

export const PrivateRoute = connect(
    mapStateToProps,
    null
)(_PrivateRoute);
