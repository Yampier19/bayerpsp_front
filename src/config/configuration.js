const Configuration = {
  api: {
    hostname: process.env.REACT_APP_BASE_BAYER_PSP_URL,
  },
};

export default Configuration;
