import { Ability, AbilityBuilder } from "@casl/ability";

export const getAbilityByPermission = (permission) => {
  const ability = new Ability();
  const { can, rules } = new AbilityBuilder(ability);
  if (permission) {
    permission.forEach((d) => {
      if (d.theme === "NOVEDADES") {
        switch (d.name) {
          case "Registro de novedad":
            if (d.edit === "SI")
              can("bayer/actions/noveltys/register.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/noveltys/register.view", "all");
            break;
          case "Consulta de novedad":
            if (d.edit === "SI")
              can("bayer/actions/noveltys/consult.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/noveltys/consult.view", "all");
            break;
          case "Comunicación Novedad":
            if (d.edit === "SI")
              can("bayer/actions/noveltys/comunicate.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/noveltys/comunicate.view", "all");
            break;
          default:
            return;
        }
      } else if (d.theme === "PACIENTE NUEVO") {
        switch (d.name) {
          case "Creación":
            if (d.edit === "SI")
              can("bayer/actions/patient/create.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/patient/create.view", "all");
            break;
          case "Listado":
            if (d.edit === "SI") can("bayer/actions/patient/list.edit", "all");
            if (d.view === "SI") can("bayer/actions/patient/list.view", "all");
            break;
          default:
            return;
        }
      } else if (d.theme === "SEGUIMIENTO") {
        switch (d.name) {
          case "Listado seguimiento":
            if (d.edit === "SI") can("bayer/actions/tracking/list.edit", "all");
            if (d.view === "SI") can("bayer/actions/tracking/list.view", "all");
            break;
          case "Buscar":
            if (d.edit === "SI")
              can("bayer/actions/tracking/search.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/tracking/search.view", "all");
            break;
          case "Gestionar":
            if (d.edit === "SI")
              can("bayer/actions/tracking/manage.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/tracking/manage.view", "all");
            break;
          default:
            return;
        }
      } else if (d.theme === "PRODUCTOS") {
        switch (d.name) {
          case "Registro material":
            if (d.edit === "SI")
              can("bayer/actions/products/register.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/products/register.view", "all");
            break;
          case "Inventario":
            if (d.edit === "SI")
              can("bayer/actions/products/inventory.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/products/inventory.view", "all");
            break;
          default:
            return;
        }
      } else if (d.theme === "REPORTES") {
        switch (d.name) {
          case "Listado reportes":
            if (d.edit === "SI") can("bayer/actions/reports/list.edit", "all");
            if (d.view === "SI") can("bayer/actions/reports/list.view", "all");

            break;
          case "Crear/editar":
            if (d.edit === "SI")
              can("bayer/actions/reports/create.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/reports/create.view", "all");
            break;
          default:
            return;
        }
      } else if (d.theme === "CONFIGURACIÓN") {
        switch (d.name) {
          case "Creación EPS/OPL":
            if (d.edit === "SI")
              can("bayer/actions/settings/createEPS.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/settings/createEPS.view", "all");
            break;
          case "Gestiones COORDINADOR":
            if (d.edit === "SI")
              can("bayer/actions/settings/manageCoordinator.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/settings/manageCoordinator.view", "all");

            break;
          case "Cambio de fecha":
            if (d.edit === "SI") can("bayer/actions/settings/date.edit", "all");
            if (d.view === "SI") can("bayer/actions/settings/date.view", "all");
            break;
          case "Asignación":
            if (d.edit === "SI")
              can("bayer/actions/settings/assign.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/settings/assign.view", "all");
            break;
          case "Usuarios":
            if (d.edit === "SI")
              can("bayer/actions/settings/users.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/settings/users.view", "all");
            break;
          case "Mi cuenta":
            if (d.edit === "SI")
              can("bayer/actions/settings/account.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/settings/account.view", "all");
            break;
          default:
            return;
        }
      } else if (d.theme === "SOLICTUDES ADICIONALES") {
        switch (d.name) {
          case "Asignar novedades":
            if (d.edit === "SI")
              can("bayer/actions/aditional/assign.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/aditional/assign.view", "all");
            break;
          case "Tipificador":
            if (d.edit === "SI")
              can("bayer/actions/aditional/categorize.edit", "all");
            if (d.view === "SI")
              can("bayer/actions/aditional/categorize.view", "all");
            break;
          default:
            return;
        }
      } else if (d.theme === "todos los permisos") {
        can("bayer/actions/noveltys/register.edit", "all");
        can("bayer/actions/noveltys/register.view", "all");
        can("bayer/actions/noveltys/consult.edit", "all");
        can("bayer/actions/noveltys/consult.view", "all");
        can("bayer/actions/noveltys/comunicate.edit", "all");
        can("bayer/actions/noveltys/comunicate.view", "all");
        can("bayer/actions/patient/create.edit", "all");
        can("bayer/actions/patient/create.view", "all");
        can("bayer/actions/patient/list.edit", "all");
        can("bayer/actions/patient/list.view", "all");
        can("bayer/actions/tracking/list.edit", "all");
        can("bayer/actions/tracking/list.view", "all");
        can("bayer/actions/tracking/search.edit", "all");
        can("bayer/actions/tracking/search.view", "all");
        can("bayer/actions/tracking/manage.edit", "all");
        can("bayer/actions/tracking/manage.view", "all");
        can("bayer/actions/products/register.edit", "all");
        can("bayer/actions/products/register.view", "all");
        can("bayer/actions/products/inventory.edit", "all");
        can("bayer/actions/products/inventory.view", "all");
        can("bayer/actions/reports/list.edit", "all");
        can("bayer/actions/reports/list.view", "all");
        can("bayer/actions/reports/create.edit", "all");
        can("bayer/actions/reports/create.view", "all");
        can("bayer/actions/settings/createEPS.edit", "all");
        can("bayer/actions/settings/createEPS.view", "all");
        can("bayer/actions/settings/manageCoordinator.edit", "all");
        can("bayer/actions/settings/manageCoordinator.view", "all");
        can("bayer/actions/settings/date.edit", "all");
        can("bayer/actions/settings/date.view", "all");
        can("bayer/actions/settings/assign.edit", "all");
        can("bayer/actions/settings/assign.view", "all");
        can("bayer/actions/settings/users.edit", "all");
        can("bayer/actions/settings/users.view", "all");
        can("bayer/actions/settings/account.edit", "all");
        can("bayer/actions/settings/account.view", "all");
        can("bayer/actions/aditional/assign.edit", "all");
        can("bayer/actions/aditional/assign.view", "all");
        can("bayer/actions/aditional/categorize.edit", "all");
        can("bayer/actions/aditional/categorize.view", "all");
      }
    });
  }
  ability.update(rules);
  return ability;
};

export const getAuthorizationPermissionByAbility = (ability) => {
  const policies = {
    canNoveltysRegisterEdit: ability.can(
      "bayer/actions/noveltys/register.edit",
      "all"
    ),
    canNoveltysRegisterView: ability.can(
      "bayer/actions/noveltys/register.view",
      "all"
    ),
    canNoveltysConsultEdit: ability.can(
      "bayer/actions/noveltys/consult.edit",
      "all"
    ),
    canNoveltysConsultView: ability.can(
      "bayer/actions/noveltys/consult.view",
      "all"
    ),
    canNoveltysComunicateEdit: ability.can(
      "bayer/actions/noveltys/comunicate.edit",
      "all"
    ),
    canNoveltysComunicateView: ability.can(
      "bayer/actions/noveltys/comunicate.view",
      "all"
    ),
    canPatientCreateEdit: ability.can(
      "bayer/actions/patient/create.edit",
      "all"
    ),
    canPatientCreateView: ability.can(
      "bayer/actions/patient/create.view",
      "all"
    ),
    canPatientListEdit: ability.can("bayer/actions/patient/list.edit", "all"),
    canPatientListView: ability.can("bayer/actions/patient/list.view", "all"),
    canTrackingListEdit: ability.can("bayer/actions/tracking/list.edit", "all"),
    canTrackingListView: ability.can("bayer/actions/tracking/list.view", "all"),
    canTrackingSearchEdit: ability.can(
      "bayer/actions/tracking/search.edit",
      "all"
    ),
    canTrackingSearchView: ability.can(
      "bayer/actions/tracking/search.view",
      "all"
    ),
    canTrackingManageEdit: ability.can(
      "bayer/actions/tracking/manage.edit",
      "all"
    ),
    canTrackingManageView: ability.can(
      "bayer/actions/tracking/manage.view",
      "all"
    ),
    canProductsRegisterEdit: ability.can(
      "bayer/actions/products/register.edit",
      "all"
    ),
    canProductsRegisterView: ability.can(
      "bayer/actions/products/register.view",
      "all"
    ),
    canProductsInventoryEdit: ability.can(
      "bayer/actions/products/inventory.edit",
      "all"
    ),
    canProductsInventoryView: ability.can(
      "bayer/actions/products/inventory.view",
      "all"
    ),
    canReportsListEdit: ability.can("bayer/actions/reports/list.edit", "all"),
    canReportsListView: ability.can("bayer/actions/reports/list.view", "all"),
    canReportsCreateEdit: ability.can(
      "bayer/actions/reports/create.edit",
      "all"
    ),
    canReportsCreateView: ability.can(
      "bayer/actions/reports/create.view",
      "all"
    ),
    canSettingCreateEPSEdit: ability.can(
      "bayer/actions/settings/createEPS.edit",
      "all"
    ),
    canSettingCreateEPSView: ability.can(
      "bayer/actions/settings/createEPS.view",
      "all"
    ),
    canSettingManageCoordinatorEdit: ability.can(
      "bayer/actions/settings/manageCoordinator.edit",
      "all"
    ),
    canSettingManageCoordinatorView: ability.can(
      "bayer/actions/settings/manageCoordinator.view",
      "all"
    ),
    canSettingDateEdit: ability.can("bayer/actions/settings/date.edit", "all"),
    canSettingDateView: ability.can("bayer/actions/settings/date.view", "all"),
    canSettingAssingEdit: ability.can(
      "bayer/actions/settings/assign.edit",
      "all"
    ),
    canSettingAssingView: ability.can(
      "bayer/actions/settings/assign.view",
      "all"
    ),
    canSettingUsersEdit: ability.can(
      "bayer/actions/settings/users.edit",
      "all"
    ),
    canSettingUsersView: ability.can(
      "bayer/actions/settings/users.view",
      "all"
    ),
    canSettingAccountEdit: ability.can(
      "bayer/actions/settings/account.edit",
      "all"
    ),
    canSettingAccountView: ability.can(
      "bayer/actions/settings/account.view",
      "all"
    ),
    canAditionalAssignEdit: ability.can(
      "bayer/actions/aditional/assign.edit",
      "all"
    ),
    canAditionalAssingView: ability.can(
      "bayer/actions/aditional/assign.view",
      "all"
    ),
    canAditionalCategorizeEdit: ability.can(
      "bayer/actions/aditional/categorize.edit",
      "all"
    ),
    canAditionalCategorizeView: ability.can(
      "bayer/actions/aditional/categorize.view",
      "all"
    ),
  };
  return policies;
};
