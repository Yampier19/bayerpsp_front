import React from "react";
import { useContext } from "react";
import { createContext } from "react";

import { getAbilityByPermission } from "./authorizationAction";
import { getAuthorizationPermissionByAbility } from "./authorizationAction";
import { useUser } from "../../user/userProvider";

const AuthorizationActionContext = createContext();

export const AuthorizationActionProvider = ({ children }) => {
  const user = useUser();
  const permissions = user?.permissions;
  const abilities = getAbilityByPermission(permissions);
  const policies = getAuthorizationPermissionByAbility(abilities);
  return (
    <AuthorizationActionContext.Provider children={children} value={policies} />
  );
};

export const useAuthorizationAction = () => {
  const policies = useContext(AuthorizationActionContext);
  if (!policies) {
    return null;
  }
  return policies;
};
