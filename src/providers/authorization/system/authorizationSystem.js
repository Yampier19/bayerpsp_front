import { Ability, AbilityBuilder } from "@casl/ability";

export const getAbilityBySystem = (system) => {
  const ability = new Ability();
  const { can, rules } = new AbilityBuilder(ability);
  if (system) {
    system.forEach((d) => {
      switch (d.name) {
        case "PSP":
          can("bayer/system/psp.open", "all");
          break;
        case "CRS":
          can("bayer/system/crs.open", "all");
          break;
        case "AD":
          can("bayer/system/ad.open", "all");
          break;
        default:
          return;
      }
    });
  }
  ability.update(rules);
  return ability;
};

export const getAuthorizationSystemByAbility = (ability) => {
  const policies = {
    canSystemPSP: ability.can("bayer/system/psp.open", "all"),
    canSystemCRS: ability.can("bayer/system/crs.open", "all"),
    canSystemAD: ability.can("bayer/system/ad.open", "all"),
  };
  return policies;
};
