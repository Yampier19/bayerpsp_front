import React from "react";
import { Fragment } from "react";
import { useHistory } from "react-router-dom";

export const AuthorizationSystemComponent = (props) => {
  const history = useHistory();

  const { isAuthorized, autoRedirect, redirect, children } = props;

  if (!isAuthorized) {
    if (autoRedirect) history.push("403");
    if (redirect) history.push(redirect);
    return null;
  }
  return <Fragment>{children}</Fragment>;
};
