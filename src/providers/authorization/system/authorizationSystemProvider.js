import React from "react";
import { useContext } from "react";
import { createContext } from "react";
import { getAbilityBySystem } from "./authorizationSystem";
import { getAuthorizationSystemByAbility } from "./authorizationSystem";
import { useUser } from "../../user/userProvider";

const AuthorizationSystemContext = createContext();

export const AuthorizationSystemProvider = ({ children }) => {
  const user = useUser();
  const systems = user?.systems;
  const abilities = getAbilityBySystem(systems);
  const policies = getAuthorizationSystemByAbility(abilities);
  return (
    <AuthorizationSystemContext.Provider value={policies} children={children} />
  );
};

export const useAuthorizationSystem = () => {
  const policies = useContext(AuthorizationSystemContext);
  if (!policies) {
    return null;
  }
  return policies;
};
