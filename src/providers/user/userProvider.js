import { useContext } from "react";
import { createContext } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

const AuthenticationContext = createContext();

const UserProvider = ({ children, authenticationReducer }) => {
  const { sessionData, isLoggedIn } = authenticationReducer;
  if (!isLoggedIn) {
    <Redirect to={"/login"} />;
  }
  const user = sessionData?.user;
  return <AuthenticationContext.Provider value={user} children={children} />;
};

const mapStateToProps = (state) => ({
  authenticationReducer: state.authenticationReducer,
});

export default connect(mapStateToProps, {})(UserProvider);

export const useUser = () => {
  const user = useContext(AuthenticationContext);
  if (typeof user === "undefined") {
    return null;
  }
  return user;
};
