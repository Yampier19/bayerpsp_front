import {
    CLOSE_SIDEBAR,
    OPEN_SIDEBAR
} from 'redux/constants';

const switch_off = () => {
    return{
        type: CLOSE_SIDEBAR
    }
}

const switch_on = () => {
    return{
        type: OPEN_SIDEBAR
    }
}

export const open_sidebar = () => {

    return dispatch => {

        dispatch( switch_on() );
        const main = document.getElementById('main');
        if(main == null) return;
        main.classList.add('main-open');

    }
}

export const close_sidebar = () => {

    return dispatch => {

        dispatch( switch_off() );
        const main = document.getElementById('main');
        if(main == null) return;
        main.classList.remove('main-open');
    }
}
