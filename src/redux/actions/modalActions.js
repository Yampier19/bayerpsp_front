import {
  SET_MODAL_ON,
  SET_MODAL_OFF,
  SET_MODAL_LOADING,
} from "redux/constants";

export const set_modal_on = (modalData) => {
  return {
    type: SET_MODAL_ON,
    payload: modalData,
  };
};

export const set_modal_off = () => {
  return {
    type: SET_MODAL_OFF,
  };
};

export const set_loading = (state) => {
  return {
    type: SET_MODAL_LOADING,
    payload: state,
  };
};
