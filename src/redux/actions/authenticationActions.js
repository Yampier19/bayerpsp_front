import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  LOGOUT_FAILURE,
  SET_IS_LOADING_FROM_LOCAL,
  EMAIL_REQUEST,
  EMAIL_SUCCESS,
  EMAIL_FAILURE,
  CODE_REQUEST,
  CODE_SUCCESS,
  CODE_FAILURE,
  FORGOT_PASSWORD_REQUEST,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
} from "redux/constants";

import { BayerPSPAxios } from "../../services/bayer/bayer-api-axios";
import { useLogin } from "../../hooks/useLogin";

const _login_request = () => {
  return {
    type: LOGIN_REQUEST,
    payload: null,
  };
};

const login_failure = (errorData) => {
  return {
    type: LOGIN_FAILURE,
    payload: errorData,
  };
};

const login_success = (session_data) => {
  return {
    type: LOGIN_SUCCESS,
    payload: session_data,
  };
};

/**
 * @param {authentication object} userData - username & password
 */
export const login_request = (userData) => {
  return async (dispatch, getState) => {
    const { login } = useLogin();
    dispatch(_login_request());
    try {
      const res = await BayerPSPAxios.post(`/api/login`, userData);
      const data = res.data;
      if (data.error) {
        throw data;
      }
      dispatch(login_success(data));
      dispatch(start_token_validation_loop());
      login(data, true);
      return true;
    } catch (err) {
      let data = "No ha sido posible establecer conexion con el servidor";
      if (err.response) {
        // Request made and server responded
        console.log(err.response.data);
        console.log(err.response.status);
        console.log(err.response.headers);
        data = "No ha sido posible establecer conexion con el servidor";
      } else if (err.request) {
        // The request was made but no response was received
        // console.log(err);
        // console.log(err.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        data = err.error;
      }
      dispatch(login_failure(data));
      return false;
    }
  };
};

const _logout_request = () => {
  return {
    type: LOGOUT_REQUEST,
    payload: null,
  };
};
//eslint-disable-next-line
const logout_failure = (errorData) => {
  return {
    type: LOGOUT_FAILURE,
    payload: errorData,
  };
};

const logout_success = () => {
  return {
    type: LOGOUT_SUCCESS,
    payload: null,
  };
};

export const logout_request = () => {
  return async (dispatch, getState) => {
    const { logout } = useLogin();
    dispatch(_logout_request());
    try {
      const res = await BayerPSPAxios.post(`/api/logout`);
      const data = res.data;
      if (data.status) {
        throw data;
      }
      dispatch(logout_success());
      logout();
      return true;
    } catch (err) {
      //let data = 'unkown error';
      if (err.response) {
        // Request made and server responded
        console.log(err.response.data);
        console.log(err.response.status);
        console.log(err.response.headers);
        //data = 'No ha sido posible establecer conexion con el servidor'
      } else if (err.request) {
        // The request was made but no response was received
        //data = 'No se ha obtenido respuesta por parte del servidor';
        // console.log(err);
        // console.log(err.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        //data = err.status;
      }
      logout();
      dispatch(logout_success());
      return true;
    } finally {
      //const l = getState().authenticationReducer.isLoggedIn;
    }
  };
};

/* *~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~ */

const set_is_loadingFromLocal = (status) => {
  return {
    type: SET_IS_LOADING_FROM_LOCAL,
    payload: status,
  };
};

//loads the login session stored in the local storage
export const load_session = () => {
  return async (dispatch, getState) => {
    dispatch(set_is_loadingFromLocal(true));

    const sessionData = JSON.parse(localStorage.getItem("session"));
    if (sessionData == null) {
      dispatch(set_is_loadingFromLocal(false));
      return;
    }

    dispatch(login_success(sessionData));
    dispatch(start_token_validation_loop());
    dispatch(set_is_loadingFromLocal(false));
  };
};

//creates a loop in which calls the validate token function every 10 mins
export const start_token_validation_loop = () => {
  return async (dispatch, getState) => {
    dispatch(validate_token());

    const mins = 10;
    setTimeout(() => {
      dispatch(start_token_validation_loop());
    }, 1000 * 60 * mins);
  };
};

// sends a req to the server to check if the token stills valid
export const validate_token = () => {
  return async (dispatch, getState) => {
    const sessionData = getState().authenticationReducer.sessionData;
    try {
      if (sessionData == null) return null;
      const res = await BayerPSPAxios.get(`/api/auth`);
      if (res.data.response) {
        //token still active
      } else throw res.data;
    } catch (err) {
      if (getState().authenticationReducer.isLoggedIn) {
        dispatch(logout_request());
      }
    } finally {
    }
  };
};

const _email_request = () => {
  return {
    type: EMAIL_REQUEST,
    payload: null,
  };
};

const _email_failure = (errorData) => {
  return {
    type: EMAIL_FAILURE,
    payload: errorData,
  };
};

const _email_success = (data) => {
  return {
    type: EMAIL_SUCCESS,
    payload: data,
  };
};

export const email_request = (userData) => {
  return async (dispatch, getState) => {
    dispatch(_email_request());
    try {
      const res = await BayerPSPAxios.post(`/api/password/email`, userData);
      const data = res.data;
      if (data.error || data.code === 500) {
        throw data;
      }
      dispatch(_email_success(userData));
      return true;
    } catch (err) {
      let data = "No ha sido posible establecer conexion con el servidor";
      if (err.response) {
        // Request made and server responded
        console.log(err.response.data);
        console.log(err.response.status);
        console.log(err.response.headers);
        data = "No ha sido posible establecer conexion con el servidor";
      } else if (err.message) {
        // The request was made but no response was received
        // console.log(err);
        // console.log(err.request);
        data = err.message;
      } else {
        // Something happened in setting up the request that triggered an Error
        data = err.error;
      }
      dispatch(_email_failure(data));
      return false;
    }
  };
};

const _code_request = () => {
  return {
    type: CODE_REQUEST,
    payload: null,
  };
};

const _code_failure = (errorData) => {
  return {
    type: CODE_FAILURE,
    payload: errorData,
  };
};

const _code_success = (data) => {
  return {
    type: CODE_SUCCESS,
    payload: data,
  };
};

export const code_request = (userData) => {
  return async (dispatch, getState) => {
    dispatch(_code_request());
    try {
      let form = {
        code: userData.code,
        email: getState().authenticationReducer.emailData.email,
      };
      const res = await BayerPSPAxios.post(`/api/password/code`, form);
      const data = res.data;
      if (data.error || data.code === 500) {
        throw data;
      }
      dispatch(_code_success(userData));
      return true;
    } catch (err) {
      let data = "No ha sido posible establecer conexion con el servidor";
      if (err.response) {
        // Request made and server responded
        console.log(err.response.data);
        console.log(err.response.status);
        console.log(err.response.headers);
        data = "No ha sido posible establecer conexion con el servidor";
      } else if (err.message) {
        // The request was made but no response was received
        // console.log(err);
        // console.log(err.request);
        data = err.message;
      } else {
        // Something happened in setting up the request that triggered an Error
        data = err.error;
      }
      dispatch(_code_failure(data));
      return false;
    }
  };
};

const _forgot_password_request = () => {
  return {
    type: FORGOT_PASSWORD_REQUEST,
    payload: null,
  };
};

const _forgot_password_failure = (errorData) => {
  return {
    type: FORGOT_PASSWORD_FAILURE,
    payload: errorData,
  };
};

const _forgot_password_success = (data) => {
  return {
    type: FORGOT_PASSWORD_SUCCESS,
    payload: data,
  };
};

export const forgot_password_request = (userData) => {
  return async (dispatch, getState) => {
    dispatch(_forgot_password_request);
    try {
      let form = {
        code: getState().authenticationReducer.codeData.code,
        email: getState().authenticationReducer.emailData.email,
        ...userData,
      };
      const res = await BayerPSPAxios.post(`/api/password/reset`, form);
      const data = res.data;
      if (data.error || data.code === 500) {
        throw data;
      }
      dispatch(_forgot_password_success(userData));
      return true;
    } catch (err) {
      let data = "No ha sido posible establecer conexion con el servidor";
      if (err.response) {
        // Request made and server responded
        console.log(err.response.data);
        console.log(err.response.status);
        console.log(err.response.headers);
        data = "No ha sido posible establecer conexion con el servidor";
      } else if (err.message) {
        // The request was made but no response was received
        // console.log(err);
        // console.log(err.request);
        data = err.message;
      } else {
        // Something happened in setting up the request that triggered an Error
        data = err.error;
      }
      dispatch(_forgot_password_failure(data));
      return false;
    }
  };
};
