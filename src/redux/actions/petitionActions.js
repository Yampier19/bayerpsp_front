import {
  SMALL_PETITION_LOADING,
  SMALL_PETITION_ERROR,
  SMALL_PETITION_SUCCESS,
} from "redux/constants";

import { BayerPSPAxios } from "../../services/bayer/bayer-api-axios";

const petition_loading = (petitionName) => {
  return {
    type: SMALL_PETITION_LOADING,
    petitionName: petitionName,
    data: null,
  };
};

const petition_error = (petitionName, errorData) => {
  return {
    type: SMALL_PETITION_ERROR,
    petitionName: petitionName,
    data: errorData,
  };
};

const petition_success = (petitionName, responseData) => {
  return {
    type: SMALL_PETITION_SUCCESS,
    petitionName: petitionName,
    data: responseData,
  };
};

export const get_data_request = (url, petitionName) => {
  return async (dispatch, getState) => {
    dispatch(petition_loading());
    try {
      const res = await BayerPSPAxios.get(url);
      // console.log(res);
      if (res.data.response) {
        dispatch(petition_success(petitionName, res.data.data));
        return true;
      } else throw res.data;
    } catch (e) {
      dispatch(petition_error(petitionName, e));
      return false;
    }
  };
};
