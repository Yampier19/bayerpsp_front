import {
  TRACKING_GET_REQ_LOADING,
  TRACKING_GET_REQ_ERROR,
  TRACKING_GET_REQ_SUCCESS,
  TRACKING_POST_REQ_LOADING,
  TRACKING_POST_REQ_ERROR,
  TRACKING_POST_REQ_SUCCESS,
  TRACKING_PUT_REQ_LOADING,
  TRACKING_PUT_REQ_ERROR,
  TRACKING_PUT_REQ_SUCCESS,
} from "redux/constants";

import { BayerPSPAxios } from "../../services/bayer/bayer-api-axios";

const tracking_get_req_loading = (req_name) => {
  return {
    type: TRACKING_GET_REQ_LOADING,
    req_name: req_name,
    data: null,
  };
};
const tracking_get_req_error = (req_name, errorData) => {
  return {
    type: TRACKING_GET_REQ_ERROR,
    req_name: req_name,
    data: errorData,
  };
};
const tracking_get_req_success = (req_name, responseData) => {
  return {
    type: TRACKING_GET_REQ_SUCCESS,
    req_name: req_name,
    data: responseData,
  };
};

const tracking_post_req_loading = (req_name) => {
  return {
    type: TRACKING_POST_REQ_LOADING,
    req_name: req_name,
    data: null,
  };
};

const tracking_post_req_error = (req_name, errorData) => {
  return {
    type: TRACKING_POST_REQ_ERROR,
    req_name: req_name,
    data: errorData,
  };
};
const tracking_post_req_success = (req_name, responseData) => {
  return {
    type: TRACKING_POST_REQ_SUCCESS,
    req_name: req_name,
    data: responseData,
  };
};

const tracking_put_req_loading = (req_name) => {
  return {
    type: TRACKING_PUT_REQ_LOADING,
    req_name: req_name,
    data: null,
  };
};

const tracking_put_req_error = (req_name, errorData) => {
  return {
    type: TRACKING_PUT_REQ_ERROR,
    req_name: req_name,
    data: errorData,
  };
};
const tracking_put_req_success = (req_name, responseData) => {
  return {
    type: TRACKING_PUT_REQ_SUCCESS,
    req_name: req_name,
    data: responseData,
  };
};

export const tracking_get_request = (url, req_name, req_body) => {
  return async (dispatch, getState) => {
    dispatch(tracking_get_req_loading(req_name));

    try {
      const res = await BayerPSPAxios.get(url, req_body);
      // console.log(res);
      if (res.data.response) {
        // const _data = Array.from([...res.data.data]);
        // console.log(_data);
        dispatch(tracking_get_req_success(req_name, res.data.data));
        return true;
      } else throw res.data;
    } catch (e) {
      dispatch(tracking_get_req_error(req_name, e));
      return false;
    }
  };
};

export const tracking_post_request = (url, req_name, req_body) => {
  return async (dispatch, getState) => {
    dispatch(tracking_post_req_loading(req_name));
    try {
      const res = await BayerPSPAxios.post(url, req_body);
      console.log(res);
      if (res.data.response) {
        dispatch(tracking_post_req_success(req_name, res.data.data));
        return true;
      } else throw res.data;
    } catch (e) {
      dispatch(tracking_post_req_error(req_name, e));
      return false;
    }
  };
};

export const tracking_put_request = (url, req_name, req_body) => {
  return async (dispatch, getState) => {
    dispatch(tracking_put_req_loading(req_name));
    try {
      const res = await BayerPSPAxios.put(url, req_body);
      // console.log(res);
      if (res.data.response) {
        dispatch(tracking_put_req_success(req_name, res.data.data));
        return true;
      } else throw res.data;
    } catch (e) {
      dispatch(tracking_put_req_error(req_name, e));
      return false;
    }
  };
};
