import {
    CONSULT_LOADING,
    CONSULT_ERROR,
    CONSULT_SUCCESS
} from 'redux/constants.js';

import axios from 'axios';
import consults from 'consults';

/* *~~*~~*~~*~~*~~*~~* CONSULT PLAIN ACTIONS *~~*~~*~~*~~*~~*~~* */
const consult_loading = consultName => {
    return{
        type: CONSULT_LOADING,
        consultName: consultName
    };
}

const consult_error = (consultName, errorData) => {
    return{
        type: CONSULT_ERROR,
        consultName: consultName,
        errorData: errorData
    };
}

const consult_success = (consultName, data) => {
    return{
        type: CONSULT_SUCCESS,
        consultName: consultName,
        data: data
    };
}

/* *~~*~~*~~*~~*~~*~~* CONSULT THUNK ACTIONS *~~*~~*~~*~~*~~*~~* */
// {
//     endpoint: string -> 'endpoint url'
//     currentPage:
// }

export const consult_request = (endpoint, currentPage) => {
    return async (dispatch, getState) => {

        dispatch( consult_loading(consults.NEWS) );

        let res;

        try {
            res = await axios.get(`${endpoint}/posts?_page=${currentPage}`);
            dispatch( consult_success(consults.NEWS, {...res}) );

        } catch (e) {
            console.log('error cought while tryng axios get consult' + consults.NEWS);
            console.log(e);
            dispatch( consult_error(consults.NEWS, {...e}) );
            res = e;
        }
        finally{
            // console.log('finished');
            // console.log(getState().consultReducer['NEWS']);
            // console.log('--------------------');
            return res;
        }
    }
}
