import {
    HELPERS_GET_REQ_LOADING,
    HELPERS_GET_REQ_ERROR,
    HELPERS_GET_REQ_SUCCESS,

    HELPERS_POST_REQ_LOADING,
    // HELPERS_POST_REQ_ERROR,
    HELPERS_POST_REQ_SUCCESS,

    HELPERS_PUT_REQ_LOADING,
    // HELPERS_PUT_REQ_ERROR,
    HELPERS_PUT_REQ_SUCCESS
} from 'redux/constants';


import axios from 'axios';

const helpers_get_req_loading = req_name => {
    return {
        type: HELPERS_GET_REQ_LOADING,
        req_name: req_name,
        data: null
    };
}
const helpers_get_req_error = (req_name, errorData) => {
    return {
        type: HELPERS_GET_REQ_ERROR,
        req_name: req_name,
        data: errorData
    };
}
const helpers_get_req_success = (req_name, responseData) => {
    return {
        type: HELPERS_GET_REQ_SUCCESS,
        req_name: req_name,
        data: responseData
    };
}

const helpers_post_req_loading = req_name => {
    return {
        type: HELPERS_POST_REQ_LOADING,
        req_name: req_name,
        data: null
    };
}
// const helpers_post_req_error = (req_name, errorData) => {
//     return {
//         type: HELPERS_POST_REQ_ERROR,
//         req_name: req_name,
//         data: errorData
//     };
// }
const helpers_post_req_success = (req_name, responseData) => {
    return {
        type: HELPERS_POST_REQ_SUCCESS,
        req_name: req_name,
        data: responseData
    };
}

const helpers_put_req_loading = req_name => {
    return {
        type: HELPERS_PUT_REQ_LOADING,
        req_name: req_name,
        data: null
    };
}
// const helpers_put_req_error = (req_name, errorData) => {
//     return {
//         type: HELPERS_PUT_REQ_ERROR,
//         req_name: req_name,
//         data: errorData
//     };
// }
const helpers_put_req_success = (req_name, responseData) => {
    return {
        type: HELPERS_PUT_REQ_SUCCESS,
        req_name: req_name,
        data: responseData
    };
}

export const helpers_get_request = (url, req_name, req_body)  => {
    return async (dispatch, getState) => {

        dispatch( helpers_get_req_loading( req_name ) );

        const {sessionData} = getState().authenticationReducer;

        const headers = {
            'Authorization': sessionData.token_type + ' ' + sessionData.access_token
        }
        // console.log( sessionData );
        try {
            const res = await axios({
                method: 'get',
                url: url,
                headers: headers,
                data: req_body
            });

            if(res.data.response){
                dispatch( helpers_get_req_success(req_name, res.data.data) );
                return true;
            }
            else
                throw res.data;


        } catch (e) {
            dispatch( helpers_get_req_error(req_name, e) );
            return false;
        }
    }
}

export const helpers_post_request = (url, req_name, req_body)  => {
    return async (dispatch, getState) => {

        dispatch( helpers_post_req_loading( req_name ) );

        const {sessionData} = getState().authenticationReducer;

        const headers = {
            'Authorization': sessionData.token_type + ' ' + sessionData.access_token
        }

        try {
            const res = await axios({
                method: 'post',
                url: url,
                headers: headers,
                data: req_body
            });

            if(res.data.response){
                dispatch( helpers_post_req_success(req_name, res.data.data) );
                return true;
            }

            else
                throw res.data;


        } catch (e) {
            dispatch( helpers_post_req_success(req_name, e) );
            return false;
        }
    }
}

export const helpers_put_request = (url, req_name, req_body)  => {
    return async (dispatch, getState) => {

        dispatch( helpers_put_req_loading( req_name ) );

        const {sessionData} = getState().authenticationReducer;

        const headers = {
            'Authorization': sessionData.token_type + ' ' + sessionData.access_token
        }

        try {
            const res = await axios({
                method: 'put',
                url: url,
                headers: headers,
                data: req_body
            });
            // console.log(res);
            if(res.data.response){
                dispatch( helpers_put_req_success(req_name, res.data.data) );
                return true;
            }

            else
                throw res.data;

        } catch (e) {
            dispatch( helpers_put_req_success(req_name, e) );
            return false;
        }
    }
}
