import {
  NEWS_GET_REQ_LOADING,
  NEWS_GET_REQ_ERROR,
  NEWS_GET_REQ_SUCCESS,
  NEWS_POST_REQ_LOADING,
  NEWS_POST_REQ_ERROR,
  NEWS_POST_REQ_SUCCESS,
  NEWS_PUT_REQ_LOADING,
  NEWS_PUT_REQ_ERROR,
  NEWS_PUT_REQ_SUCCESS,
} from "redux/constants";

import { BayerPSPAxios } from "../../services/bayer/bayer-api-axios";

const news_get_req_loading = (req_name) => {
  return {
    type: NEWS_GET_REQ_LOADING,
    req_name: req_name,
    data: null,
  };
};
const news_get_req_error = (req_name, errorData) => {
  return {
    type: NEWS_GET_REQ_ERROR,
    req_name: req_name,
    data: errorData,
  };
};
const news_get_req_success = (req_name, responseData) => {
  return {
    type: NEWS_GET_REQ_SUCCESS,
    req_name: req_name,
    data: responseData,
  };
};

const news_post_req_loading = (req_name) => {
  return {
    type: NEWS_POST_REQ_LOADING,
    req_name: req_name,
    data: null,
  };
};
const news_post_req_error = (req_name, errorData) => {
  return {
    type: NEWS_POST_REQ_ERROR,
    req_name: req_name,
    data: errorData,
  };
};
const news_post_req_success = (req_name, responseData) => {
  return {
    type: NEWS_POST_REQ_SUCCESS,
    req_name: req_name,
    data: responseData,
  };
};

const news_put_req_loading = (req_name) => {
  return {
    type: NEWS_PUT_REQ_LOADING,
    req_name: req_name,
    data: null,
  };
};
const news_put_req_error = (req_name, errorData) => {
  return {
    type: NEWS_PUT_REQ_ERROR,
    req_name: req_name,
    data: errorData,
  };
};
const news_put_req_success = (req_name, responseData) => {
  return {
    type: NEWS_PUT_REQ_SUCCESS,
    req_name: req_name,
    data: responseData,
  };
};

export const news_get_request = (url, req_name, req_body) => {
  return async (dispatch, getState) => {
    dispatch(news_get_req_loading(req_name));
    try {
      const res = await BayerPSPAxios.get(url, req_body);
      if (res.data.response) {
        dispatch(news_get_req_success(req_name, res.data.data));
        return true;
      } else throw res.data;
    } catch (e) {
      dispatch(news_get_req_error(req_name, e));
      return false;
    }
  };
};

export const news_post_request = (url, req_name, req_body) => {
  return async (dispatch, getState) => {
    dispatch(news_post_req_loading(req_name));
    try {
      const res = await BayerPSPAxios.post(url, req_body);
      if (res.data.response) {
        dispatch(news_post_req_success(req_name, res.data.data));
        return true;
      } else throw res.data;
    } catch (e) {
      dispatch(news_post_req_error(req_name, e));
      return false;
    }
  };
};

export const news_put_request = (url, req_name, req_body) => {
  return async (dispatch, getState) => {
    dispatch(news_put_req_loading(req_name));
    try {
      const res = await BayerPSPAxios.put(url, req_body);
      if (res.data.response) {
        dispatch(news_put_req_success(req_name, res.data.data));
        return true;
      } else throw res.data;
    } catch (e) {
      dispatch(news_put_req_error(req_name, e));
      return false;
    }
  };
};
