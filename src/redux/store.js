import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import sidebarReducer from './reducers/sidebarReducer';
import modalReducer from './reducers/modalReducer';
import authenticationReducer from './reducers/authenticationReducer';
import consultReducer from './reducers/consultReducer';
import petitionReducer from './reducers/petitionReducer';
import newsReducer from './reducers/newsReducer';
import trackingReducer from './reducers/trackingReducer';
import helpersReducer from './reducers/helpersReducer';

// import {InfoModal, ConfirmModal} from '../components/base/modal-system/classes/modal';

const reducer = combineReducers({
    sidebarReducer,
    modalReducer,
    authenticationReducer,
    consultReducer,
    petitionReducer,
    newsReducer,
    trackingReducer,
    helpersReducer
});

const store = createStore(reducer, applyMiddleware(thunk));

window.addEventListener('keydown', e => {
    if (e.which === 32)
        console.log(store.getState().consultReducer);
});

// const createPrimary = document.createElement('button', {className: 'button'});
// createPrimary.classList.add('button');
// createPrimary.addEventListener('click', () => {
//     const {_types, _icons} = store.getState().modalReducer;
//     store.dispatch( set_modal_on({type: _types.INFO,msg: 'test verde',icon: _icons.CHECK_PRIMARY}));
// });
// createPrimary.innerHTML = 'click';
// document.body.appendChild(createPrimary);
//
// const create2 = document.createElement('button', {className: 'button'});
// create2.classList.add('button');
// create2.addEventListener('click', () => {
//     const {_types, _icons} = store.getState().modalReducer;
//     store.dispatch( set_modal_on({type: _types.INFO,msg: 'test naranja',icon: _icons.CHECK_HORANGE}));
// });
// create2.innerHTML = 'click 2';
// document.body.appendChild(create2);
//
// const create3 = document.createElement('button', {className: 'button'});
// create3.classList.add('button');
// create3.addEventListener('click', () => {
//     const {_types, _icons} = store.getState().modalReducer;
//     store.dispatch( set_modal_on({type: _types.INFO,msg: 'test rojo',icon: _icons.CHECK_HRED}));
// });
// create3.innerHTML = 'click 3';
// document.body.appendChild(create3);
//
// const create4 = document.createElement('button', {className: 'button'});
// create4.classList.add('button');
// create4.addEventListener('click', () => {
//     const {_types, _icons} = store.getState().modalReducer;
//     store.dispatch( set_modal_on({type: _types.INFO,msg: 'test negro',icon: _icons.CHECK_HBLACK}));
// });
// create4.innerHTML = 'click 4';
// document.body.appendChild(create4);
//
// const create5 = document.createElement('button', {className: 'button'});
// create5.classList.add('button');
// create5.addEventListener('click', () => {
//     const {_types, _icons} = store.getState().modalReducer;
//     store.dispatch( set_modal_on({type: _types.CONFIRM, msg: 'test confirmar warning', icon: _icons.WARNING_HRED, onConfirm: () => console.log('confirmado')}));
// });
// create5.innerHTML = 'click 5';
// document.body.appendChild(create5);
//
// const create6 = document.createElement('button', {className: 'button'});
// create6.classList.add('button');
// create6.addEventListener('click', () => {
//     const {_types, _icons} = store.getState().modalReducer;
//
//     // const modal = new ConfirmModal('Test confirmar borrar', _icons.TRASH_CAN_HBLACK, () => console.log('borrar'));
//
//     store.dispatch( set_modal_on({type: _types.CONFIRM, msg: 'Test borrar', icon: _icons.TRASH_CAN_HBLACK, onConfirm: () => console.log('eliminado')}) );
// });
// create6.innerHTML = 'click 6';
// document.body.appendChild(create6);
//
// const create7 = document.createElement('button', {className: 'button'});
// create7.classList.add('button');
// create7.addEventListener('click', () => {
//     const {_types, _icons} = store.getState().modalReducer;
//
//     // const modal = new ConfirmModal('Test confirmar borrar', _icons.TRASH_CAN_HBLACK, () => console.log('borrar'));
//     const msg = 'Buenos días {{FirstName}} {{LastName}}, Se ha asignado un nuevo Seguimiento el cual debe culminarse la fecha DD-MM-AAAA. Atentamente, Nombre de la coordinadora.';
//     store.dispatch( set_modal_on({type: _types.EMAIL, msg: msg, icon: _icons.TRASH_CAN_HBLACK, onConfirm: () => console.log('eliminado')}) );
// });
// create7.innerHTML = 'click 7';
// document.body.appendChild(create7);

export default store;
