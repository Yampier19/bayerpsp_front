import {
  SET_MODAL_ON,
  SET_MODAL_OFF,
  SET_MODAL_LOADING,
} from "redux/constants";

const icons = {
  CHECK_PRIMARY: "CHECK_PRIMARY",
  CHECK_HORANGE: "CHECK_HORANGE",
  CHECK_HRED: "CHECK_HRED",
  CHECK_HBLACK: "CHECK_HBLACK",
  WARNING_HRED: "WARNING_HRED",
  TRASH_CAN_HBLACK: "TRASH_CAN_HBLACK",
};

const types = {
  INFO: "INFO",
  CONFIRM: "CONFIRM",
  EMAIL: "EMAIL",
};

const defaultState = {
  modalActive: false,
  modalData: null,
  isLoading: false,
  _types: types,
  _icons: icons,
};

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case SET_MODAL_ON:
      return {
        ...state,
        modalActive: true,
        modalData: action.payload,
        isLoading: false,
      };

    case SET_MODAL_OFF:
      return {
        ...state,
        modalActive: false,
        modalData: null,
        isLoading: false,
      };
    case SET_MODAL_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };
    default:
      return { ...state };
  }
};

export default reducer;
