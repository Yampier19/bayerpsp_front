import {
  NEWS_GET_REQ_LOADING,
  NEWS_GET_REQ_ERROR,
  NEWS_GET_REQ_SUCCESS,
  NEWS_POST_REQ_LOADING,
  NEWS_POST_REQ_ERROR,
  NEWS_POST_REQ_SUCCESS,
  NEWS_PUT_REQ_LOADING,
  NEWS_PUT_REQ_ERROR,
  NEWS_PUT_REQ_SUCCESS,
} from "redux/constants";

const news_req_names = {
  CREATE_NEW: "CREATE_NEW",
  UPDATE_NEW: "UPDATE_NEW",
  GET_ALL_NEWS: "GET_ALL_NEWS",
  GET_NEW: "GET_NEW",
  NEW_HISTORY: "NEW_HISTORY",
  GET_NEW_COMMENT: "GET_NEW_COMMENT",
  NEW_POST_COMMENT: "NEW_POST_COMMENT",
  NEW_POST_COMMENT_REPLY: "NEW_POST_COMMENT_REPLY",
};

const getDefaultState = (req_names) => {
  let state = null;
  const keys = Object.keys(req_names);
  for (let key of keys) {
    state = {
      ...state,
      [key]: {
        loading: false,
        error: false,
        success: false,
        data: null,
      },
    };
  }
  return state;
};

const defaultState = getDefaultState(news_req_names);

const reducer = (state = defaultState, action) => {
  // console.log(action);
  switch (action.type) {
    case NEWS_GET_REQ_LOADING:
      return {
        ...state,
        [action.req_name]: {
          loading: true,
          error: false,
          success: false,
          data: null,
        },
      };

    case NEWS_GET_REQ_ERROR:
      return {
        ...state,
        [action.req_name]: {
          loading: false,
          error: true,
          success: false,
          data: action.data,
        },
      };

    case NEWS_GET_REQ_SUCCESS:
      return {
        ...state,
        [action.req_name]: {
          loading: false,
          error: false,
          success: true,
          data: action.data,
        },
      };

    case NEWS_POST_REQ_LOADING:
      return {
        ...state,
        [action.req_name]: {
          loading: true,
          error: false,
          success: false,
          data: null,
        },
      };

    case NEWS_POST_REQ_ERROR:
      return {
        ...state,
        [action.req_name]: {
          loading: false,
          error: true,
          success: false,
          data: action.data,
        },
      };

    case NEWS_POST_REQ_SUCCESS:
      return {
        ...state,
        [action.req_name]: {
          loading: false,
          error: false,
          success: true,
          data: action.data,
        },
      };

    case NEWS_PUT_REQ_LOADING:
      return {
        ...state,
        [action.req_name]: {
          loading: true,
          error: false,
          success: false,
          data: null,
        },
      };

    case NEWS_PUT_REQ_ERROR:
      return {
        ...state,
        [action.req_name]: {
          loading: false,
          error: true,
          success: false,
          data: action.data,
        },
      };

    case NEWS_PUT_REQ_SUCCESS:
      return {
        ...state,
        [action.req_name]: {
          loading: false,
          error: false,
          success: true,
          data: action.data,
        },
      };

    default:
      return { ...state };
  }
};

export default reducer;
