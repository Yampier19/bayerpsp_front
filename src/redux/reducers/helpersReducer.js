import {
    HELPERS_GET_REQ_LOADING,
    HELPERS_GET_REQ_ERROR,
    HELPERS_GET_REQ_SUCCESS,

    HELPERS_POST_REQ_LOADING,
    HELPERS_POST_REQ_ERROR,
    HELPERS_POST_REQ_SUCCESS,

    HELPERS_PUT_REQ_LOADING,
    HELPERS_PUT_REQ_ERROR,
    HELPERS_PUT_REQ_SUCCESS
} from 'redux/constants';


const news_req_names = {
    GET_COUNTRIES: 'GET_COUNTRIES',
    GET_CITIES: 'GET_CITIES',
    GET_DEPARTAMENTS: 'GET_DEPARTAMENTS',
    GET_LOGISTIC_OPERATOR: 'GET_LOGISTIC_OPERATOR',
    GET_INSURANCE: 'GET_INSURANCE'
}

const getDefaultState = req_names => {
    let state = null;
    const keys = Object.keys(req_names);
    for(let key of keys){
        state = {
            ...state,
            [key]: {
                loading: false,
                error: false,
                success: false,
                data: null
            }
        }
    }
    return state;
}

const defaultState = getDefaultState(news_req_names);

const reducer = (state = defaultState, action) => {
    // console.log(action);
    switch (action.type) {
        case HELPERS_GET_REQ_LOADING:
            return{
                ...state,
                [action.req_name]: {
                    loading: true,
                    error: false,
                    success: false,
                    data: null
                }
            };

        case HELPERS_GET_REQ_ERROR:
            return{
                ...state,
                [action.req_name]: {
                    loading: false,
                    error: true,
                    success: false,
                    data: action.data
                }
            };

        case HELPERS_GET_REQ_SUCCESS:

            return{
                ...state,
                [action.req_name]: {
                    loading: false,
                    error: false,
                    success: true,
                    data: action.data
                }
            };

        case HELPERS_POST_REQ_LOADING:
            return{
                ...state,
                [action.req_name]: {
                    loading: true,
                    error: false,
                    success: false,
                    data: null
                }
            };

        case HELPERS_POST_REQ_ERROR:
            return{
                ...state,
                [action.req_name]: {
                    loading: false,
                    error: true,
                    success: false,
                    data: action.data
                }
            };

        case HELPERS_POST_REQ_SUCCESS:
            return{
                ...state,
                [action.req_name]: {
                    loading: false,
                    error: false,
                    success: true,
                    data: action.data
                }
            };

        case HELPERS_PUT_REQ_LOADING:
            return{
                ...state,
                [action.req_name]: {
                    loading: true,
                    error: false,
                    success: false,
                    data: null
                }
            };

        case HELPERS_PUT_REQ_ERROR:
            return{
                ...state,
                [action.req_name]: {
                    loading: false,
                    error: true,
                    success: false,
                    data: action.data
                }
            };

        case HELPERS_PUT_REQ_SUCCESS:
            return{
                ...state,
                [action.req_name]: {
                    loading: false,
                    error: false,
                    success: true,
                    data: action.data
                }
            };

        default:
            return {...state};
    }
}

export default reducer;
