import {
    CLOSE_SIDEBAR,
    OPEN_SIDEBAR
} from 'redux/constants.js';

const defaultState = {
    isOpen: true
};

const reducer = (state = defaultState, action) => {

    switch (action.type) {

        case CLOSE_SIDEBAR:
            return{
                ...state,
                isOpen: false
            };

        case OPEN_SIDEBAR:
            return{
                ...state,
                isOpen: true
            };

        default:
            return {...state};
    }
}

export default reducer;
