import {
  SMALL_PETITION_LOADING,
  SMALL_PETITION_ERROR,
  SMALL_PETITION_SUCCESS,
} from "redux/constants";

const defaultState = {
  genericPetition: {
    loading: false,
    error: false,
    success: false,
    data: null,
  },
  PRODUCT: {
    loading: false,
    error: false,
    success: false,
    data: null,
  },
  NOVELTIES: {
    loading: false,
    error: false,
    success: false,
    data: null,
  },
  USERS: {
    loading: false,
    error: false,
    success: false,
    data: null,
  },
};

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case SMALL_PETITION_LOADING:
      return {
        ...state,
        [action.petitionName]: {
          loading: true,
          error: false,
          success: false,
          data: null,
        },
      };

    case SMALL_PETITION_ERROR:
      return {
        ...state,
        [action.petitionName]: {
          loading: false,
          error: true,
          success: false,
          data: action.data,
        },
      };

    case SMALL_PETITION_SUCCESS:
      return {
        ...state,
        [action.petitionName]: {
          loading: false,
          error: false,
          success: true,
          data: action.data,
        },
      };

    default:
      return { ...state };
  }
};

export default reducer;
