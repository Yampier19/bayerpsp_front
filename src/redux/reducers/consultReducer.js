import {
    CONSULT_LOADING,
    CONSULT_ERROR,
    CONSULT_SUCCESS
} from 'redux/constants.js';

import consults from 'consults';

const getDefaultState = () => {
    let state = null;
    const keys = Object.keys(consults);
    for(let key of keys){
        state = {
            ...state,
            [key]: {
                loading: false,
                error: false,
                success: false,
                data: null
            }
        }
    }
    return state;
}
const defaultState = getDefaultState();


const reducer = (state = defaultState, action) => {

    switch (action.type) {

        case CONSULT_LOADING:

            return{
                ...state,
                [action.consultName]: {
                    loading: true,
                    error: false,
                    success: false,
                    data: null
                }
            };


        case CONSULT_ERROR:
            return{
                ...state,
                [action.consultName]: {
                    loading: false,
                    error: true,
                    success: false,
                    data: {...action.errorData}
                }
            };

        case CONSULT_SUCCESS:
            return{
                ...state,
                [action.consultName]: {
                    loading: false,
                    error: false,
                    success: true,
                    data: {...action.data}
                }
            };

        default:
            return {...state};
    }
}

export default reducer;
