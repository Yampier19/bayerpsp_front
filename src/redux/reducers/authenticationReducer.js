import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  SET_IS_LOADING_FROM_LOCAL,
  EMAIL_REQUEST,
  EMAIL_SUCCESS,
  EMAIL_FAILURE,
  CODE_REQUEST,
  CODE_SUCCESS,
  CODE_FAILURE,
  FORGOT_PASSWORD_REQUEST,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
} from "redux/constants";

const defaultState = {
  isLoadingFromLocal: true,
  sessionData: null,
  emailData: null,
  codeData: null,
  isLoggedIn: false,
  isSendEmail: false,
  isSendCode: false,
  isSendForgot: false,
  login: {
    isLoading: false,
    error: false,
    errorData: {},
  },
  logout: {
    isLoading: false,
    error: false,
    errorData: {},
  },
  email: {
    isLoading: false,
    error: false,
    errorData: {},
  },
  code: {
    isLoading: false,
    error: false,
    errorData: {},
  },
  forgot_password: {
    isLoading: false,
    error: false,
    errorData: {},
  },
};

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        login: {
          isLoading: true,
          error: false,
          errorData: {},
        },
      };

    case LOGIN_SUCCESS:
      return {
        ...state,
        sessionData: action.payload,
        isLoggedIn: true,
        login: {
          isLoading: false,
          error: false,
          errorData: {},
        },
      };

    case LOGIN_FAILURE:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        login: {
          isLoading: false,
          error: true,
          errorData: action.payload,
        },
      };

    case LOGOUT_REQUEST:
      return {
        ...state,
        logout: {
          isLoading: true,
          error: false,
          errorData: {},
        },
      };

    case LOGOUT_SUCCESS:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        logout: {
          isLoading: false,
          error: false,
          errorData: {},
        },
      };
    case EMAIL_REQUEST:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        isSendEmail: false,
        emailData: null,
        email: {
          isLoading: true,
          error: false,
          errorData: {},
        },
      };
    case EMAIL_SUCCESS:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        isSendEmail: true,
        emailData: action.payload,
        email: {
          isLoading: false,
          error: false,
          errorData: {},
        },
      };
    case EMAIL_FAILURE:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        isSendEmail: false,
        emailData: null,
        email: {
          isLoading: false,
          error: true,
          errorData: action.payload,
        },
      };
    case CODE_REQUEST:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        isSendEmail: false,
        codeData: null,
        code: {
          isLoading: true,
          error: false,
          errorData: {},
        },
      };
    case CODE_SUCCESS:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        isSendEmail: false,
        isSendCode: true,
        codeData: action.payload,
        code: {
          isLoading: false,
          error: false,
          errorData: {},
        },
      };
    case CODE_FAILURE:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        isSendEmail: false,
        codeData: null,
        code: {
          isLoading: false,
          error: true,
          errorData: action.payload,
        },
      };
    case FORGOT_PASSWORD_REQUEST:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        isSendEmail: false,
        isSendForgot: false,
        forgot_password: {
          isLoading: true,
          error: false,
          errorData: {},
        },
      };
    case FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        isSendEmail: false,
        isSendForgot: true,
        forgot_password: {
          isLoading: false,
          error: false,
          errorData: {},
        },
      };
    case FORGOT_PASSWORD_FAILURE:
      return {
        ...state,
        sessionData: null,
        isLoggedIn: false,
        isSendEmail: false,
        isSendForgot: false,
        forgot_password: {
          isLoading: false,
          error: true,
          errorData: action.payload,
        },
      };
    case SET_IS_LOADING_FROM_LOCAL:
      return {
        ...state,
        isLoadingFromLocal: action.payload,
      };

    default:
      return { ...state };
  }
};

export default reducer;
