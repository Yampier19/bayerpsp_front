import * as React from "react";
import { Link } from "react-router-dom";
import Box from "@mui/material/Box";

import Modal from "@mui/material/Modal";
import FormField2Modal from "components/form/modal/formfield2_modal";
import FormField3Modal from "components/form/modal/formfield3_modal";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  borderRadius: 10,
  boxShadow: 24,
  p: 4,
};

export default function BasicModal2() {
  const [open, setOpen] = React.useState(false);
  const handleOpen2 = () => setOpen(true);
  const handleClose2 = () => setOpen(false);

  return (
    <div>
      <Link onClick={handleOpen2} className="dropdown-item">
        EGRESOS
      </Link>
      <Modal
        open={open}
        onClose={handleClose2}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div className="row">
            <div className="col-2">
              <i
                class="far fa-question-circle fa-3x"
                style={{ color: "#E7967A" }}
              ></i>
            </div>
            <div className="col">
              <p className="h5">
                Antes de generar el egreso del producto responda las siguientes
                preguntas:
              </p>
            </div>
          </div>
          <div className="row-reverse">
            <form id="form">
              <div className="row-reverse py-5">
                <div className="col">
                  <FormField2Modal
                    label="Nombre del Coordinador que autoriza la salida del producto"
                    number="1"
                    name="codigoUsuario"
                  />
                </div>
                <div className="col">
                  <FormField3Modal
                    label="Fecha de autorización"
                    number="2"
                    name="codigoUsuario"
                  />
                </div>
              </div>
            </form>
          </div>
          <div className="row mx-auto pl-5">
            <div className="col d-flex justify-content-end align-items-center">
              <Link
                to="/products/create/outcomings"
                style={{ backgroundColor: "#E7967A", fontSize: 15 }}
                className="py-3 px-5 rounded text-white"
              >
                CONTINUAR
              </Link>
            </div>
          </div>
          <div className="row d-flex justify-content-center mt-3">
            <div className="col-10 ">
              <p className="text-center text-muted" style={{ fontSize: 14 }}>
                <i>
                  Una vez continue se notificará al coordinador que comenzo un
                  proceso de egreso autorizado por el
                </i>
              </p>
            </div>
          </div>
        </Box>
      </Modal>
    </div>
  );
}
