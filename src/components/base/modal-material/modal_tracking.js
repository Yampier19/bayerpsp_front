import React, { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "75%",
  bgcolor: "background.paper",
  borderRadius: 10,
  boxShadow: 24,
  p: 4,
};

/* ESTE MODAL ES PARA EL TIPO DE ENVIO EN SEGUIMIENTO */

export default function BasicModal(props) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const [state, setState] = useState({
    address: {},
  });

  useEffect(() => {
    console.log(state);
  }, [state]);

  const checkboxChange = (e) => {
    const { name } = e.target;
    setState((prevState) => ({
      ...prevState,
      address: {
        ...prevState.address,
        [name]: !prevState.address[name],
      },
    }));
  };

  return (
    <div>
      <button onClick={handleOpen}>
        <i class="fas fa-plus-circle fa-2x my-3"></i>
      </button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <button onClick={handleClose}>
            <i
              class="fas fa-times fa-2x text-white"
              style={{ position: "absolute", marginLeft: "100%", top: "-20%" }}
            ></i>
          </button>
          <div className="row my-5">
            <div className="col">
              <label style={{ color: "#6992D6", fontWeight: "bold" }}>
                PRODUCTOS PARA ENVIAR
              </label>
            </div>
          </div>
          <div className="row-reverse my-5">
            <div className="col">
              <table class="table table-bordered">
                <thead style={{ backgroundColor: "#6992D6", opacity: 0.8 }}>
                  <tr>
                    <th
                      scope="col"
                      className="text-white p-4"
                      style={{ borderRadius: 20 }}
                    >
                      NOMBRE DEL MATERIAL
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {list.map((it, index) => (
                    <tr key={it.id}>
                      <td>
                        <input
                          className="mr-5"
                          checked={state.address.index}
                          key={it.id}
                          name={it.id}
                          value={state.address[it.id]}
                          onChange={checkboxChange}
                          type="checkbox"
                        />{" "}
                        No. APLICACIÓN
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <div className="col d-flex justify-content-end">
              <button
                className="btn text-white"
                style={{ backgroundColor: "#6992D6" }}
              >
                CONTINUAR
              </button>
            </div>
          </div>
        </Box>
      </Modal>
    </div>
  );
}

const list = [
  {
    name: "first",
    id: "1",
  },
  {
    name: "first",
    id: "2",
  },
  {
    name: "first",
    id: "3",
  },
];
