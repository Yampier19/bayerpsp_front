import React from "react";

import "./progress-bar.scss";

const ProgressBar = (props) => {
  const progress = props.value || 0;

  return (
    <div className="progress-container is-flex is-justify-content-center is-align-items-center py-2">
      <div className="progress-display-container">
        <div
          className={`progress-circle-indicator is-flex is-justify-content-center is-align-items-center ${props.className}`}
          style={{ left: progress + "%" }}
        >
          <span className="has-text-white is-size-5">
            {Math.round(progress)}
            <span className="is-size-6">%</span>
          </span>
        </div>
      </div>
      <progress
        class={`progress progress-bar ${props.className}`}
        value={progress}
        max="100"
      ></progress>
    </div>
  );
};

export default ProgressBar;
