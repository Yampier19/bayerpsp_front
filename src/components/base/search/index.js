import _ from "lodash";
import Stack from "@mui/material/Stack";
import { useDebounce } from "use-debounce";
import TextField from "@mui/material/TextField";
import React, { useEffect, useState } from "react";
import { Autocomplete } from "@mui/material";

import { useGetPatientByDocumentNumber } from "services/bayer/psp/patient/usePatient";
import { useSearchedPatientContext } from "components/routes/psp/create/patient/context/searched-patient-context";

export const Search = ({}) => {
  const {setData} = useSearchedPatientContext();
  const reFetchQueryConfig = {
    enabled: false,
  }
  const [localPatients, setLocalPatients] = useState([]);
  const [localLoading, setLocalLoading] = useState(false);
  const [search, setSearch] = useState("");
  const [debouncedSearch] = useDebounce(search, 500);
  const {
    refetch: refetchPatient,
    data: patients,
    isLoading: isLoadingPatients,
  } = useGetPatientByDocumentNumber(debouncedSearch, reFetchQueryConfig);
  useEffect(() => {
    if (_.isEmpty(debouncedSearch) /* && search < 3 */) {
      setLocalPatients([]);
      return;
    }
    refetchPatient();
  }, [debouncedSearch, refetchPatient]);

  useEffect(() => {
    const candidates = patients?.data || [];
    setLocalPatients(candidates);
  }, [setLocalPatients, patients]);

  return (
    <Stack spacing={2} sx={{ width: 300 }}>
      <Autocomplete
        id="size-small-standard"
        size="small"
        options={localPatients}
        loading={isLoadingPatients}
        filterOptions={(opt) => opt}
        getOptionLabel={(option) => option? option.name + " " + option.last_name + " - " + String(option.document_number) : ""}
        defaultValue={""}
        renderInput={(params) => (
          <TextField
            variant="standard"
            label="Buscar paciente..."
            placeholder="Número de cedula"
            {...params}
          />
        )}
        onChange={(_,value) => {if(value){setData(value)}}}
        onInputChange={(_, value) => {setSearch(value); setLocalLoading(true)}}
        noOptionsText="No se han encontrado resultados"
        loadingText="Cargando búsqueda..."
      />
    </Stack>
  );
};
