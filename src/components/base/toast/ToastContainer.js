import React from "react";
import { ToastContainer as TC, toast } from "react-toastify";

const ToastContainer = (children) => {
  return (
    <TC
      position="top-right"
      autoClose={3000}
      hideProgressBar={false}
      pauseOnHover
      draggable
      theme="colored"
    >
      {children}
    </TC>
  );
};

export default ToastContainer;

export const Toast = ({ msg, category, callback, timeout = 3000 }) => {
  switch (category) {
    case "error":
      return toast.error(msg, {
        onClose: () => callback(),
        autoClose: timeout,
      });
    case "success":
      return toast.success(msg, {
        onClose: () => callback(),
        autoClose: timeout,
      });
    case "info":
    default:
      return toast.info(msg, {
        onClose: () => callback(),
        autoClose: timeout,
      });
  }
};
