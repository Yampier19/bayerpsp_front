export const ToastUtil = (msg, category) => {
  let message = msg;
  const callback = () => {
    message = " ";
  };
  return {
    msg: message,
    category,
    callback,
  };
};
