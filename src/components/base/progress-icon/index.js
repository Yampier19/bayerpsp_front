import { useEffect, useRef } from "react";

import "./progress-icon.scss";

function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
  var angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;

  return {
    x: centerX + radius * Math.cos(angleInRadians),
    y: centerY + radius * Math.sin(angleInRadians),
  };
}

function describeArc(x, y, radius, startAngle, endAngle) {
  var start = polarToCartesian(x, y, radius, endAngle);
  var end = polarToCartesian(x, y, radius, startAngle);

  var largeArcFlag = endAngle - startAngle <= 180 ? "0" : "1";

  var d = [
    "M",
    start.x,
    start.y,
    "A",
    radius,
    radius,
    0,
    largeArcFlag,
    0,
    end.x,
    end.y,
  ].join(" ");

  return d;
}

const ProgressIcon = (props) => {
  const progress = props.value || 0;

  const angle = (progress * 359) / 100;

  const diameter = 60;
  const rad = diameter / 2;
  const stroke = 2;

  const arc1 = useRef(null);

  useEffect(() => {
    arc1.current.setAttribute(
      "d",
      describeArc(rad + stroke / 2, rad + stroke / 2, rad, 0, angle)
    );
    //eslint-disable-next-line
  }, [progress]);

  return (
    <div
      className="picon is-flex is-justify-content-center is-align-items-center"
      style={{
        position: "relative",
        width: diameter + stroke,
        height: diameter + stroke,
      }}
    >
      <svg
        className="progress-outline"
        width={diameter + stroke}
        height={diameter + stroke}
      >
        <path
          fill="none"
          stroke={progress < 99 ? "#999999" : props.color}
          strokeWidth={stroke}
          ref={arc1}
        />
      </svg>

      <div
        className={`${props.className} progress-icon is-flex is-justify-content-center is-align-items-center`}
        style={{ width: diameter * 0.8, height: diameter * 0.8 }}
      >
        <span className="icon is-size-4 has-text-white">
          {progress < 100 ? props.icon : <i class="fas fa-check"></i>}
        </span>
      </div>
    </div>
  );
};

export default ProgressIcon;
