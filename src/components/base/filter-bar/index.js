import "./filter-bar.scss";

const FilterBar = (props) => {
  return (
    <div>
      <div className="is-flex is-flex-direction-row is-flex-wrap-wrap">
        {
          //filter dropdowns
          props.filters.map((filter, i) => (
            <div className="dropdown is-hoverable level-item filter mr-3">
              <div className="dropdown-trigger">
                <button
                  className="button has-text-light2"
                  aria-haspopup="true"
                  aria-controls="dropdown-menu"
                >
                  <span>{filter.name}</span>
                  <span className={`icon is-small ${props.textColorClass}`}>
                    <i className="fas fa-angle-down" aria-hidden="true"></i>
                  </span>
                </button>
              </div>
              <div
                className="dropdown-menu py-0"
                id="dropdown-menu"
                role="menu"
              >
                <div className="dropdown-content">
                  {filter.options.map((op, j) => (
                    <a className="dropdown-item">{op.name}</a>
                  ))}
                </div>
              </div>
            </div>
          ))
        }
        {
          //more filters
          props.moreFiltersOn ? (
            <a className={`pt-3 ${props.textColorClass}`}>
              <span className="icon">
                <i class="fas fa-ellipsis-v"></i>
              </span>
              <span>
                <u>Otros filtros</u>
              </span>
            </a>
          ) : null
        }
      </div>

      {
        //tags
        props.tags ? (
          <div
            className="is-flex is-flex-direction-row"
            style={{ "overflow-x": "scroll" }}
          >
            {props.tags.map((tag, i) => (
              <div class="block" key={i}>
                <span class="tag is-white filter-tag">
                  <button class="delete is-small"></button> &nbsp; &nbsp;
                  {tag.name}
                </span>
              </div>
            ))}
          </div>
        ) : (
          <div className="mb-6" />
        )
      }
    </div>
  );
};

export default FilterBar;
