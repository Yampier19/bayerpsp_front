import React, { Component } from "react";
import MUIDataTable from "mui-datatables";
import { createTheme, MuiThemeProvider } from "@material-ui/core/styles";

class datatables extends Component {
  getMuiTheme = () =>
    createTheme({
      overrides: {
        MUIDataTableToolbar: {
          root: {
            backgroundColor: this.props.color,
            color: "white",
            borderRadius: 10,
          },
        },
        MuiIconButton: {
          root: {
            color: "white",
          },
        },
        MUIDataTableHeadCell: {
          root: {
            fontWeight: "bold",
          },
        },
        MUIDataTableBodyCell: {
          root: {
            boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.1)",
            borderRadius: 7,
            padding: 20,
          },
        },
        MUIDataTablePagination: {
          root: {
            float: "left",
          },
        },
      },
    });

  render() {
    const options = {
      searchPlaceholder: "Busqueda rapida",
      filterType: "dropdown",
      responsive: "scroll",
      fixedHeader: "true",
      textLabels: {
        body: {
          noMatch: "Lo siento, no hay datos registrados",
          toolTip: "Sort",
          columnHeaderTooltip: (column) => `Sort for ${column.label}`,
        },
        pagination: {
          next: "Siguiente pagina",
          previous: "Anterior pagina",
          rowsPerPage: "Filas por pargina:",
          displayRows: "of",
          backgroundColor: "red",
        },
        toolbar: {
          search: "Buscar",
          downloadCsv: "Descargar Excel",
          print: "Imprimir",
          viewColumns: "Ver columnas",
          filterTable: "Filtrar tabla",
        },
        filter: {
          all: "Todos",
          title: "Filtros",
          reset: "Resetear",
        },
        viewColumns: {
          title: "Ver columnas",
          titleAria: "Ver/Ocultar Columnas de la tabla",
        },
        selectedRows: {
          text: "Fila(s) seleccionadas",
          delete: "Eliminado",
          deleteAria: "Filas seleccionadas eliminadas",
        },
      },
    };

    return (
      <MuiThemeProvider theme={this.getMuiTheme()}>
        <MUIDataTable
          title={this.props.title}
          data={this.props.data}
          columns={this.props.columns}
          rowHover
          options={options}
        />
      </MuiThemeProvider>
    );
  }
}
export default datatables;
