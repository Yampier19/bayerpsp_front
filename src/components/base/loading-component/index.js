import "./loading-component.scss";

const LoadingComponent = ({ loadingText=""}) => {
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        display: "grid",
        placeItems: "center",
      }}
    >
      <section className="has-text-centered">
        <div className="lds-roller">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
        <div className="subtitle has-text-primary">{loadingText}</div>
      </section>
    </div>
  );
};

export default LoadingComponent;
