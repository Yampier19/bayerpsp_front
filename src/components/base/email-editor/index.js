import {useEffect, useState, useRef} from 'react';

import Inputfile from '../inputfile';

import ReactQuill from 'react-quill';
// import ReactHtml from 'raw-html-react';

import 'react-quill/dist/quill.snow.css';
import './email-editor.scss';

const CustomToolbar = props => {

    const inputElm = useRef(null);

    return(
        <div id={`toolbar${props.id}`} className="email is-pulled-right">
            <span class="ql-formats is-hidden-touch email">
                <select className="ql-header email " defaultValue={""} onChange={e => e.persist()}>
                    <option value="1" />
                    <option value="2" />
                    <option value="3" />
                    <option value="4" />
                    <option value="5" />
                    <option value="6" />
                    <option value="normal" selected />
                </select>
            </span>

            <span class="ql-formats email">
                <button className="ql-bold email" />
                <button className="ql-italic email" />
                <button className="ql-strike email" />
            </span>

            <span class="ql-formats no-border">

                <button className="ql-file email">
                    <input id="inputfile" className="is-hidden" type="file" name="resume" ref={inputElm}/>
                    <span className="icon has-text-hblack"><i class="fas fa-paperclip"></i></span>
                </button>
            </span>

      </div>
    )
}



const EmailEditor = props => {

    //*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~ quill instance

    let quillRef = useRef(null);
    const [quillInstance, setQuillInstance] = useState(null);

    useEffect(
        () => {

            if(quillRef.current != null){
                const quillInstance = quillRef.current;
                setQuillInstance(quillInstance);
            }
        },
        [quillRef]
    );

    //*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~ content

    // const onSendPressed = () => {
    //     if(quillRef == null) return;
    //     const editor = quillRef.getEditor();

    //     const html = editor.container.firstChild.innerHTML;

    //     //console.log(html);

    // }

    //*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~ custom button handlers

    const insertAt = () => {

        if(quillRef == null) return;
        const editor = quillRef.getEditor();

        const  range = editor.getSelection();
        const position = range ? range.index : 0;
        editor.insertText(position, '@');

    }

    const handleFile = () => {

        const input = document.getElementById('fileinput'+props.id);
        input.click();
    }


    const modules = {
        toolbar: {
            container: "#toolbar"+props.id,
            handlers: {
                insertAt: insertAt,
                file: handleFile
            }
        }
    };

    return(
        <div className="email-editor" style={{maxWidth: '100%'}}>




            <ReactQuill
                onChange={null}
                placeholder={props.placeholder}
                modules={modules}
                ref={(el) => { quillRef = el }}
            />

            <CustomToolbar id={props.id}/>

            <Inputfile id={props.id}/>

    {/*        <div className>
                <ReactHtml html={content}  />
            </div> */}

      </div>
    );
}

export default EmailEditor;
