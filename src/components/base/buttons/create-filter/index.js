import React from "react";

const CreateFilter = (props) => {
  return (
    <div className="dropdown is-hoverable is-right" style={{ width: "100%" }}>
      <button className="button is-hyellow is-fullwidth is-size-5 has-text-left btn2">
        <span className="icon mr-1">
          <i className="fas fa-plus-circle"></i>
        </span>{" "}
        &nbsp; Crear Filtro
      </button>
    </div>
  );
};

export default CreateFilter;
