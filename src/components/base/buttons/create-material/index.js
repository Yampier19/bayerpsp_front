import React from "react";
import { Link } from "react-router-dom";

const CreateMaterial = (props) => {
  return (
    <div className="dropdown is-hoverable is-right" style={{ width: "100%" }}>
      <div className="dropdown-trigger" style={{ width: "100%" }}>
        <button className="button is-horange is-fullwidth is-size-5">
          <span className="icon mr-1">
            <i className="fas fa-plus-circle"></i>
          </span>
          &nbsp; Crear nuevo material
        </button>
      </div>
      <div className="dropdown-menu py-0" role="menu" style={{ width: "100%" }}>
        <div className="dropdown-content">
          <Link to="/products/create/incomings" className="dropdown-item">
            INGRESO
          </Link>
          <Link to="/products/create/outcomings" className="dropdown-item">
            EGRESOS
          </Link>
          <Link to="/products/create/suppliers" className="dropdown-item">
            DEVOLUCIONES
          </Link>
        </div>
      </div>
    </div>
  );
};

export default CreateMaterial;
