import React from "react";

const CreateUserBtn = (props) => {
  return (
    <button className="button is-hblack is-fullwidth is-size-5 has-text-left btn2">
      <span className="icon mr-1">
        <i className="fas fa-plus-circle"></i>
      </span>{" "}
      &nbsp; Crear usuario
    </button>
  );
};

export default CreateUserBtn;
