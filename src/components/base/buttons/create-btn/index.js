import React from "react";
import { Link } from "react-router-dom";
import { When } from "react-if";

import { useAuthorizationAction } from "providers/authorization/action/authorizationActionProvider";
import "./create-btn.scss";

const CreateBtn = (props) => {
  const {
    canNoveltysRegisterEdit,
    canPatientCreateEdit,
    canSettingCreateEPSEdit,
  } = useAuthorizationAction();
  return (
    <div className="dropdown is-hoverable is-right" style={{ width: "100%" }}>
      <div className="dropdown-trigger" style={{ width: "100%" }}>
        <button className="button is-primary is-fullwidth is-size-6 has-text-left btn">
          <span className="icon mr-1">
            <i className="fas fa-plus-circle"></i>
          </span>
          &nbsp; CREAR
        </button>
      </div>
      <div className="dropdown-menu py-0" role="menu" style={{ width: "100%" }}>
        <div className="dropdown-content">
          <When condition={canNoveltysRegisterEdit}>
            <Link
              to="/create/new"
              className={`dropdown-item ${
                Number(props.active) === 1 ? "is-active" : ""
              }`}
            >
              Novedad
            </Link>
          </When>
          <When condition={canPatientCreateEdit}>
            <Link
              to="/create/patient"
              className={`dropdown-item ${
                Number(props.active) === 2 ? "is-active" : ""
              }`}
            >
              Paciente nuevo
            </Link>
          </When>
          <When condition={canSettingCreateEPSEdit}>
            <Link
              to="/create/eps-opl"
              className={`dropdown-item ${
                Number(props.active) === 3 ? "is-active" : ""
              }`}
            >
              EPS / OPL
            </Link>
          </When>
        </div>
      </div>
    </div>
  );
};

export default CreateBtn;
