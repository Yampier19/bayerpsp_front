const CreateFilter = props => {
    return(
        <div className={`box has-background-${props.color} py-2 has-text-centered`} style={{minWidth:"250px"}}>

            <span>
                Descargar
            </span>  &nbsp;

            <a className="circle-bg p-1">
                <span className={`icon has-text-${props.color}`}><i class="fas fa-print"></i></span>
            </a> &nbsp;

            <a className="circle-bg p-1">
                <span className={`icon has-text-${props.color}`}><i class="fas fa-file-pdf"></i></span>
            </a> &nbsp;

            <a className="circle-bg p-1">
                <span className={`icon has-text-${props.color}`}><i class="fas fa-file-excel"></i></span>
            </a>

        </div>
    );
}

export default CreateFilter;
