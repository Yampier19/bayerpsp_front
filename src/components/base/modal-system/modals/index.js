import {
  check_primary,
  check_horange,
  check_hred,
  check_hblack,
  warning_hred,
  trash_can_hblack,
} from "./icons";

import store from "../../../../redux/store";
import "./modal.scss";

const _icons = store.getState().modalReducer._icons;
const _types = store.getState().modalReducer._types;
const isLoading = store.getState().modalReducer.isLoading;

const icons = {};
const modals = {};

icons[_icons.CHECK_PRIMARY] = check_primary;
icons[_icons.CHECK_HORANGE] = check_horange;
icons[_icons.CHECK_HRED] = check_hred;
icons[_icons.CHECK_HBLACK] = check_hblack;

icons[_icons.WARNING_HRED] = warning_hred;

icons[_icons.TRASH_CAN_HBLACK] = trash_can_hblack;

const ModalCreate = (props) => {
  return (
    <div id="modal" className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-content custom-modal">
        <div className="box py-6 ">
          <div className="columns is-vcentered">
            <div className="column is-4 is-flex  is-justify-content-center">
              <figure className="image is-128x128">
                <img src={icons[props.iconConst]} alt="" />
              </figure>
            </div>

            <div className="column">
              <h1 className="subtitle is-4">{props.msg}</h1>
            </div>
          </div>
        </div>
      </div>
      <button
        className="modal-close is-large"
        aria-label="close"
        onClick={props.onClose}
      ></button>
    </div>
  );
};
const ModalConfirm = (props) => {
  return (
    <div id="modal" className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-content custom-modal">
        <div className="box py-6 ">
          <div className="columns is-vcentered">
            <div className="column is-4 is-flex  is-justify-content-center">
              <figure className="image is-128x128">
                <img src={icons[props.iconConst]} alt="" />
              </figure>
            </div>
            <br />
            <div className="column is-flex is-flex-direction-column">
              <h1 className="subtitle is-4">{props.msg}</h1>
              <div className="has-text-right is-align-self-flex-end">
                <button
                  className="button"
                  onClick={props.onConfirm}
                  disabled={isLoading}
                >
                  Confirmar
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <button
        className="modal-close is-large"
        aria-label="close"
        onClick={props.onClose}
      ></button>
    </div>
  );
};
const ModalEmail = (props) => {
  return (
    <div id="modal" className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-card custom-modal">
        <header class="modal-card-head">
          <p class="modal-card-title">
            <span className="icon">
              <i class="far fa-eye"></i>
            </span>{" "}
            &nbsp; Previsualización del correo
          </p>
        </header>

        <section class="modal-card-body">
          <p>{props.msg}</p>
        </section>

        <footer class="modal-card-foot ">
          <button class="button is-success">Guardar</button>
          <button class="button">editar</button>
        </footer>
      </div>
      <button
        className="modal-close is-large"
        aria-label="close"
        onClick={props.onClose}
      ></button>
    </div>
  );
};

modals[_types.INFO] = (msg, iconConst, onClose = null) => (
  <ModalCreate msg={msg} onClose={onClose} iconConst={iconConst} />
);
modals[_types.CONFIRM] = (msg, iconConst, onClose = null, onConfirm = null) => (
  <ModalConfirm
    msg={msg}
    iconConst={iconConst}
    onClose={onClose}
    onConfirm={onConfirm}
  />
);
modals[_types.EMAIL] = (msg, onClose) => (
  <ModalEmail msg={msg} iconConst={"CHECK_PRIMARY"} onClose={onClose} />
);

export default modals;
