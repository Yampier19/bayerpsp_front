class AbstractModal {}

export class InfoModal extends AbstractModal {
  constructor(msg, icon) {
    super();
    this.type = "INFO";
    this.msg = msg;
    this.icon = icon;
  }
}

export class ConfirmModal extends AbstractModal {
  constructor(msg, icon, onConfirm) {
    super();
    this.type = "CONFIRM";
    this.msg = msg;
    this.icon = icon;
    this.onConfirm = onConfirm; //onConfirm Callback
  }
}

// class ModalFactory {
//     createModal(){}
// }
//
// const modalType = {
//     INFO: 'INFO',
//     CONFIRM: 'CONFIRM'
// }

// class GeneralModalFactory extends ModalFactory {
//     createModal(modalType){
//
//         switch (modalType) {
//             case modalType.INFO:
//                 return new
//
//             default:
//
//         }
//
//     }
// }
//
// export default new GeneralModalFactory();
