import React from "react";

import { connect } from "react-redux";
import {
  set_modal_on,
  set_modal_off,
} from "../../../redux/actions/modalActions";

import modals from "./modals";

const ModalSystem = (props) => {
  const { modalActive, modalData, _types } = props.modalState;

  const close = (e) => {
    props.set_modal_off();
  };

  return (
    <div>
      {modalActive
        ? modalData.type === _types.INFO
          ? modals[modalData.type](modalData.msg, modalData.icon, close)
          : modalData.type === _types.CONFIRM
          ? modals[modalData.type](modalData.msg, modalData.icon, close, () => {
              modalData.onConfirm();
            })
          : modalData.type === _types.EMAIL
          ? modals[modalData.type](modalData.msg, close)
          : null
        : null}
    </div>
  );
};

const mapStateToProps = (state) => ({
  modalState: state.modalReducer,
});

export default connect(mapStateToProps, {
  set_modal_on,
  set_modal_off,
})(ModalSystem);
