import {useState, useEffect} from 'react';
import {useLocation, useHistory} from 'react-router-dom';

import {connect} from 'react-redux';
import {consult_request} from 'redux/actions/consultActions';

const Pagination = props => {

    const {endpoint, itemsPerPage, data_key} = props;             //component props

    let consultData = props.consultReducer[data_key];               //redux state

    const {search} = useLocation();                     //get current page
    let page = new URLSearchParams(search).get('page');

    const [data, setData] = useState([]);

    const [lastPage, setLastPage] = useState(0);
    const [nextPage, setNextPage] = useState(0);
    const [previousPage, setPreviousPage] = useState(0);
    const [currentPage, setCurrentPage] = useState(page);
    const [total, setTotal] = useState(0);


    const getPaginationData = () => {

        if(consultData.error || consultData.data == null) return;

        const {data, headers} = consultData.data;

        //get total & last
        const _total = headers['x-total-count'];
        const _lastPage = Math.ceil(_total / itemsPerPage);

        //get next
        const _nextHipotetical = Number(currentPage) + 1;
        const _nextPage = _nextHipotetical <= _lastPage ? _nextHipotetical : null;

        //get previous
        const _previousHipotetical = Number(currentPage) - 1;
        const _previousPage = _previousHipotetical >= 1 ? _previousHipotetical : null;

        setTotal(_total);
        setLastPage(_lastPage);
        setNextPage(_nextPage);
        setPreviousPage(_previousPage);
        setData(data);
    }

    useEffect(
        () => {
            page = new URLSearchParams(search).get('page');
            setCurrentPage(page);
        },
        [search]
    );

    useEffect(
        () => {
            const getPage = async () => await props.consult_request(endpoint, currentPage);
            getPage();
        }, [currentPage]
    );

    useEffect(
        () => {
            getPaginationData();
        },
        [props.consultReducer[data_key].data]
    );

    const history = useHistory();
    const previosClick = e => {
        history.push(`${history.location.pathname}?page=${previousPage}`);
    }
    const nextClick = e => history.push(`${history.location.pathname}?page=${nextPage}`);

    return(
        <div>
            {
                consultData.success ?
                    <nav class="pagination is-small is-centered" role="navigation" aria-label="pagination">
                       <button class="pagination-previous" disabled={previousPage == null} onClick={previosClick}>{'<'}</button>
                       <button class="pagination-next" disabled={nextPage == null} onClick={nextClick} >{'>'}</button>
                       <ul class="pagination-list">
                           <li><button class={`pagination-link ${previousPage ? '' : 'is-hidden'}`} onClick={previosClick}>{previousPage}</button></li>
                           <li><a class="pagination-link is-current">{currentPage}</a></li>
                           <li><button class={`pagination-link ${nextPage ? '' : 'is-hidden'}`} onClick={nextClick}>{nextPage}</button></li>
                       </ul>
                   </nav>
               : null
            }
        </div>
    );

}

const mapStateToProps = state => ({
    consultReducer: state.consultReducer
});

export default connect(
    mapStateToProps,
    {
        consult_request
    }
)(Pagination);
