import {useEffect} from 'react';

import {connect} from 'react-redux';
import {load_session, validate_token, login_request, logout_request} from 'redux/actions/authenticationActions';

import {useLocation} from 'react-router-dom';
import store from 'redux/store';

window.addEventListener('keydown', e => {
    if(e.which===32 && e.ctrlKey){
        store.dispatch(
            logout_request()
        );
    }
    if(e.which===73 && e.ctrlKey){

        store.dispatch(
            login_request({
                user: 'admin',
                password: '$adminPsp*@'
            })
        );
        console.log('iniciando sesion');
    }
    if(e.which===81){
        console.log("isLoggedIn: " + store.getState().authenticationReducer.isLoggedIn);
        console.log(store.getState().authenticationReducer.sessionData);
    }
});

const LoginSystem = props => {
    const {pathname} = useLocation();
    useEffect(
        () => {
            (async () => {
                await props.load_session();
            })();
        },
        //eslint-disable-next-line
        []
    );

    useEffect(
        () => {
            (async () => {
                // await props.validate_token();
            })();
        },
        [pathname]
    );
    return(null);
}

const mapStateToProps = state => ({
    authenticationReducer: state.authenticationReducer
});
export default connect(
    mapStateToProps,
    {
        load_session,
        validate_token
    }
)(LoginSystem);
