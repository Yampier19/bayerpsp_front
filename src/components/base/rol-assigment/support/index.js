import {useEffect, useRef} from 'react';
//import './sections.scss';

const Support = props => {

    const form = useRef(null);

    useEffect(
        () => {

            window.addEventListener('keyup', e => {
                if(e.which == 13)
                    submit();
            });

        }, []
    );

    const submit = e => {

        const elements = Array.from(form.current.elements);

        //get which elements are selected
        for (let i = 0; i < elements.length; i++) {
            if(elements[i].type == "checkbox"){
                if(elements[i].checked){
                    console.log(elements[i].name);
                }
            }
        }
    }

    const onChange = e => {

        const checkbox = e.currentTarget;
        const targetDivSelector = checkbox.dataset.target;

        const $targetDiv = document.querySelector(targetDivSelector);

        if(checkbox.checked)
            $targetDiv.classList.add('is-active');
        else
            $targetDiv.classList.remove('is-active');
    }


    return(
        <div className="sections p-3">
            <form id="form.rolAssigmentCrs" ref={form}>
                <ul>
                    <label>
                        <input className="checkbox-config" type="checkbox" name="crs" data-target="#support" onChange={onChange}/> &nbsp;
                        Apoyo diagnostico
                    </label>
                    <ul className="ml-5 sec-list" id="support">

                        <li>
                            <label>
                                <input className="checkbox-config" type="checkbox" name="newPatient"/> &nbsp;
                                paciente nuevo
                            </label>
                        </li>

                        <li>
                            <label>
                                <input className="checkbox-config" type="checkbox" name="tracking" data-target="#tracking3" onChange={onChange}/> &nbsp;
                                Seguimiento
                            </label>

                            <ul className="ml-5 sec-list" id="tracking3">
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="tracking.patient"/> &nbsp;
                                        Paciente
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="products.tratment"/> &nbsp;
                                        Tratamiento
                                    </label>
                                </li>
                                <li>
                                    <label>
                                        <input className="checkbox-config" type="checkbox" name="products.comunications"/> &nbsp;
                                        Comunicaciones
                                    </label>
                                </li>

                            </ul>
                        </li>

                        <li>
                            <label>
                                <input className="checkbox-config" type="checkbox" name="reports"/> &nbsp;
                                Reportes
                            </label>
                        </li>

                        <li>
                            <label>
                                <input className="checkbox-config" type="checkbox" name="configuration"/> &nbsp;
                                Configuración
                            </label>
                        </li>
                    </ul>
                </ul>
            </form>
        </div>
    );
}

export default Support;
