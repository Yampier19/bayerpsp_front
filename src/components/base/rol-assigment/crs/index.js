import React from "react";
import { useEffect } from "react";

import Form1 from "./../../../routes/configuration/create/form1";
import Form2 from "./../../../routes/configuration/create/form2";
import Form3 from "./../../../routes/configuration/create/form3";
import { useRoleContext } from "../../../routes/configuration/create/provider/rolesProvider";
//import './sections.scss';

import Data from "./prueba.json";

const RolAssigmentCrs = ({ data, setField, nameRole, idRole, isLoading }) => {
  const { arrayRoles, orderRoles, setArrayRoles, setOrderRoles } =
    useRoleContext();
  const orderData = () => {
    let count = 0;
    let noveltys = {
      name: "Novedades",
      data: [],
    };
    let patientNew = {
      name: "Paciente",
      data: [],
    };
    let tracking = {
      name: "Seguimiento",
      data: [],
    };
    let products = {
      name: "Producto",
      data: [],
    };
    let reports = {
      name: "Reportes",
      data: [],
    };
    let configuration = {
      name: "Configuraciones",
      data: [],
    };
    let aditional = {
      name: "Adicional",
      data: [],
    };
    if (data.length > 1) {
      data.forEach((permi) => {
        if (permi.theme === "NOVEDADES") {
          noveltys.data.push(permi);
        } else if (permi.theme === "PACIENTE NUEVO") {
          patientNew.data.push(permi);
        } else if (permi.theme === "SEGUIMIENTO") {
          tracking.data.push(permi);
        } else if (permi.theme === "PRODUCTOS") {
          products.data.push(permi);
        } else if (permi.theme === "REPORTES") {
          reports.data.push(permi);
        } else if (permi.theme === "CONFIGURACIÓN") {
          configuration.data.push(permi);
        } else if (permi.them === "SOLICTUDES ADICIONALES") {
          aditional.data.push(permi);
        }
        count++;
      });
      if (count === data.length) {
        setOrderRoles({
          noveltys,
          reports,
          patientNew,
          configuration,
          tracking,
          products,
        });
      }
    } else {
      Data.forEach((permi) => {
        if (permi.theme === "NOVEDADES") {
          noveltys.data.push(permi);
        } else if (permi.theme === "PACIENTE NUEVO") {
          patientNew.data.push(permi);
        } else if (permi.theme === "SEGUIMIENTO") {
          tracking.data.push(permi);
        } else if (permi.theme === "PRODUCTOS") {
          products.data.push(permi);
        } else if (permi.theme === "REPORTES") {
          reports.data.push(permi);
        } else if (permi.theme === "CONFIGURACIÓN") {
          configuration.data.push(permi);
        } else if (permi.them === "SOLICTUDES ADICIONALES") {
          aditional.data.push(permi);
        }
        count++;
      });
      if (count === Data.length) {
        setOrderRoles({
          noveltys,
          reports,
          patientNew,
          configuration,
          tracking,
          products,
        });
      }
    }
  };
  useEffect(() => {
    if (data) {
      orderData();
      if (data.length > 1) {
        setArrayRoles(Data);
      } else {
        setArrayRoles(data);
      }
    }
    //eslint-disable-next-line
  }, [data, isLoading]);

  useEffect(() => {
    if (arrayRoles) {
      setField("role_object", {
        id: Number(idRole),
        name: nameRole,
        permissions: arrayRoles,
      });
    }
    //eslint-disable-next-line
  }, [arrayRoles, isLoading]);

  return (
    <div className="sections p-3">
      <div className="row mb-5">
        <div className="col">
          <label style={{ fontWeight: "bold" }}>{nameRole}</label>
        </div>
      </div>
      {console.log("orderRoles", orderRoles)}
      {/* FILA 1 (PERMISOS PARA NOVEDADES Y REPORTES) */}
      <Form1 />

      {/* FILA 2 (PERMISOS PARA PACIENTE Y CONFIGURACIONES)*/}
      <Form2 />

      {/* FILA 3 (PERMISOS PARA SEGUIMIENTO Y PRODUCTOS)*/}
      <Form3 />
    </div>
  );
};

export default RolAssigmentCrs;
