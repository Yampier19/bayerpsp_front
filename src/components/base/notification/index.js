import "./temp.scss";

const Notification = (props) => {
  return (
    <div className="box box2">
      <section className="section p-0 m-0">
        <h2 className="subtitle has-text-primary is-6">
          Notificaciones nuevas
        </h2>

        <div className="columns is-mobile is-vcentered">
          <div className="column is-4">
            <figure className="image is-48x48">
              <img
                src="https://bulma.io/images/placeholders/128x128.png"
                alt="img_notify"
              />
            </figure>
          </div>

          <div className="column">
            <h1 className="subtitle is-6">
              Te han asignado la novedad
              <br />
              Codigo ###
            </h1>
          </div>
        </div>

        <div className="columns is-mobile is-vcentered">
          <div className="column is-4">
            <figure className="image is-48x48">
              <img
                src="https://bulma.io/images/placeholders/128x128.png"
                alt="img_notify"
              />
            </figure>
          </div>

          <div className="column">
            <h1 className="subtitle is-6">
              Te han asignado la novedad
              <br />
              Codigo ###
            </h1>
          </div>
        </div>
      </section>
      <hr />
      <section className="section p-0">
        <h2 className="subtitle has-text-light2 is-6">
          Notificaciones antiguas
        </h2>

        <div className="columns is-mobile is-vcentered">
          <div className="column is-4">
            <figure className="image is-48x48">
              <img
                src="https://bulma.io/images/placeholders/128x128.png"
                alt="img_notify"
              />
            </figure>
          </div>

          <div className="column">
            <h1 className="subtitle is-6 has-text-light2">
              Te han asignado la novedad
              <br />
              Codigo ###
            </h1>
          </div>
        </div>

        <div className="columns is-mobile is-vcentered">
          <div className="column is-4">
            <figure className="image is-48x48">
              <img
                src="https://bulma.io/images/placeholders/128x128.png"
                alt="img_notify"
              />
            </figure>
          </div>

          <div className="column">
            <h1 className="subtitle is-6 has-text-light2">
              Te han asignado la novedad
              <br />
              Codigo ###
            </h1>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Notification;
