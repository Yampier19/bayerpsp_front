import React from "react";
import "./notification.scss";

const NotificationBtn = (props) => {
  return (
    <span className="icon is-size-4 icon2">
      <div className={`notifi-ball ${props.news ? "" : "not-visible"}`}></div>
      <i className="far fa-bell"></i>
    </span>
  );
};

export default NotificationBtn;
