import { useState, useRef } from "react";

import Inputfile from "../inputfile";

import ReactQuill from "react-quill";

import "react-quill/dist/quill.snow.css";
import "./editor.scss";

const CustomToolbar = (props) => {
  const inputElm = useRef(null);

  return (
    <div id={`toolbar${props.id}`}>
      <span className="ql-formats is-hidden-touch">
        <select
          className="ql-header"
          defaultValue="normal"
          onChange={(e) => e.persist()}
        >
          <option value="normal" />
          <option value="1" />
          <option value="2" />
          <option value="3" />
          <option value="4" />
          <option value="5" />
          <option value="6" />
        </select>
      </span>

      <span className="ql-formats">
        <button className="ql-bold" />
        <button className="ql-italic" />
        <button className="ql-strike " />
      </span>

      <span className="ql-formats is-hidden-touch">
        <select className="ql-color">
          <option value="#333333" />
          <option value="#999999" />
          <option value="#74B1B4" />
          <option value="#F2D8A8" />
          <option value="#E7967A" />
          <option value="#D7908F" />
          <option value="#6992D6" />
          <option value="#887DD0" />
        </select>
      </span>

      <span className="ql-formats">
        <button className="ql-list" value="bullet"></button>
        <button className="ql-list" value="ordered"></button>
      </span>

      <span className="ql-formats no-border">
        <button className="ql-file">
          <input
            id="inputfile"
            className="is-hidden"
            type="file"
            name="resume"
            ref={inputElm}
          />
          <span className="icon has-text-hgreen">
            <i className="fas fa-paperclip"></i>
          </span>
        </button>

        <button className="ql-image" />

        <button className="ql-insertAt">
          <span className="icon has-text-hgreen">
            <i className="fas fa-at"></i>
          </span>
        </button>
      </span>
    </div>
  );
};

const TextEditor = (props) => {
  //*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~ dependencies
  const { onSendCallback } = props;
  const [files, setFiles] = useState([]);

  //*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~ quill ref

  let quillRef = useRef(null);

  //*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~ event handler

  const onSendPressed = () => {
    if (quillRef == null) return;
    const editor = quillRef.getEditor();

    const html = editor.container.firstChild.innerHTML;
    onSendCallback(html, files, props.additionalData);
  };

  const handleChange = (html) => {
    //
  };

  //*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~ custom button handlers

  const insertAt = () => {
    if (quillRef == null) return;
    const editor = quillRef.getEditor();
    const range = editor.getSelection();
    const position = range ? range.index : 0;
    editor.insertText(position, "@");
  };
  //
  const handleFile = () => {
    const input = document.getElementById("fileinput" + props.id);
    input.click();
  };

  //*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~ modules
  const [modules] = useState({
    toolbar: {
      container: "#toolbar" + props.id,
      handlers: {
        insertAt: insertAt,
        file: handleFile,
      },
    },
  });

  return (
    <div className="text-editor" style={{ maxWidth: "100%" }}>
      <div className="" style={{ width: "100%" }}>
        <button
          className={`button send-btn has-text-hgreen has-background-transparent has-no-border has-no-box-shadow ${
            props.isLoading ? "is-loading" : ""
          }`}
          onClick={onSendPressed}
        >
          <span className="icon is-size-5">
            <i className="far fa-paper-plane"></i>
          </span>
        </button>
      </div>

      <ReactQuill
        onChange={handleChange}
        placeholder={props.placeholder}
        modules={modules}
        ref={(el) => {
          quillRef = el;
        }}
      />

      <CustomToolbar id={props.id} />

      <Inputfile id={props.id} oFiles={files} oSetFiles={setFiles} />
    </div>
  );
};

export default TextEditor;
