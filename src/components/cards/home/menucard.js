import {Link} from 'react-router-dom';

const MenuCard = props => {
    return(
        <Link className="box pt-0" to={{pathname: props.to, search: props.search}}>
            <figure className={`image is-32x32 has-background-${props.color}`}>
                <div className="col d-flex justify-content-center">
                    <i class="fas fa-plus-circle fa-lg mt-2"></i>
                </div>
            </figure>
            <br/>
            <h1 className="subtitle has-text-black is-6" >{props.title}</h1>
        </Link>
    );
}

export default MenuCard;
