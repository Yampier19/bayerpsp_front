import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar/crs-sidebar";
import Main from "components/commons/main";

import Tracking from "components/routes/crs/tracking/main";
import EditTracking from "components/routes/crs/tracking/edit";

const CreatePage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/crs/tracking/main">
        <Sidebar activeSite="2" />
        <Main>
          <Tracking />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/crs/tracking/edit">
        <Sidebar activeSite="2" />
        <Main>
          <EditTracking />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default CreatePage;
