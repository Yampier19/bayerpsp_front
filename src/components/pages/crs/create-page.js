import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar/crs-sidebar";
import Main from "components/commons/main";

import CreateMedic from "components/routes/crs/create/medic";
import CreateReplacement from "components/routes/crs/create/replacement";

const CreatePage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/crs/create/medic">
        <Sidebar activeSite="2" />
        <Main>
          <CreateMedic />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/crs/create/replacement">
        <Sidebar />
        <Main>
          <CreateReplacement />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default CreatePage;
