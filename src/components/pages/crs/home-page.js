import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar/crs-sidebar";
import Main from "components/commons/main";

import Home from "components/routes/crs/home";

const HomePage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/crs/home">
        <Sidebar type={4} />
        <Main>
          <Home />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default HomePage;
