import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar";
import Main from "components/commons/main";

import ConfigurationFundem from "components/routes/configuration/fundem";
import ConfiguracionDate from "components/routes/configuration/date";
import CreateUserComponent from "components/routes/configuration/users/create/create-user-component";
import EditUserComponent from "components/routes/configuration/users/edit/edit-user-component";
import ListUserComponent from "components/routes/configuration/users/list/list-users-component";
import ConfigurationTemplates from "components/routes/configuration/templates";
import ConfigurationHistory from "components/routes/configuration/history";
import ConfigurationEmail from "components/routes/configuration/email";
import ConfigurationTemplatesEmail from "components/routes/configuration/templatesEmail";

const ConfigurationPage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/configuration/fundem">
        <Sidebar activeSite="6" />
        <Main>
          <ConfigurationFundem />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/configuration/date">
        <Sidebar activeSite="6" />
        <Main>
          <ConfiguracionDate />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/configuration/users">
        <Sidebar activeSite="6" />
        <Main>
          <ListUserComponent />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/configuration/users/edit/:id">
        <Sidebar activeSite="6" />
        <Main>
          <EditUserComponent />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/configuration/create">
        <Sidebar activeSite="6" />
        <Main>
          <CreateUserComponent />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/configuration/templates">
        <Sidebar activeSite="6" />
        <Main>
          <ConfigurationTemplates />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/configuration/history">
        <Sidebar activeSite="6" />
        <Main>
          <ConfigurationHistory />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/configuration/email">
        <Sidebar activeSite="6" />
        <Main>
          <ConfigurationEmail />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/configuration/templatesemail">
        <Sidebar activeSite="6" />
        <Main>
          <ConfigurationTemplatesEmail />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default ConfigurationPage;
