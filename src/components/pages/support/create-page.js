import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar/support-sidebar";
import Main from "components/commons/main";

import CreatePatient from "components/routes/support/create/patient";

const CreatePage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/support/create/patient">
        <Sidebar activeSite="2" />
        <Main>
          <CreatePatient />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default CreatePage;
