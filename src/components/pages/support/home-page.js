import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar/support-sidebar";
import Main from "components/commons/main";

import Home from "components/routes/support/home";

const HomePage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/support/home">
        <Sidebar type={2} />
        <Main>
          <Home />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default HomePage;
