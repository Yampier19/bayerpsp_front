import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar/support-sidebar";
import Main from "components/commons/main";

import Reports from "components/routes/support/reports/main";
import ReportsCreate from "components/routes/support/reports/create";
import ReportsGraphics from "components/routes/support/reports/graphics";

const ProductsPage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/support/reports">
        <Sidebar activeSite="4" />
        <Main>
          <Reports />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/support/reports/create">
        <Sidebar activeSite="4" />
        <Main>
          <ReportsCreate />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/support/reports/graphics">
        <Sidebar activeSite="4" />
        <Main>
          <ReportsGraphics />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default ProductsPage;
