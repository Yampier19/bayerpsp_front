import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar/support-sidebar";
import Main from "components/commons/main";

import Tracking from "components/routes/support/tracking/main";
import TrackingUpdatePatient from "components/routes/support/tracking/update";
import TrackingHistory from "components/routes/support/tracking/history";
import TrackingHistory2 from "components/routes/support/tracking/history2";

const TrackingPage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/support/tracking">
        <Sidebar activeSite="3" />
        <Main>
          <Tracking />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/support/tracking/update/:id">
        <Sidebar activeSite="3" />
        <Main>
          <TrackingUpdatePatient />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/support/tracking/history">
        <Sidebar activeSite="3" />
        <Main>
          <TrackingHistory />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/support/tracking/history2">
        <Sidebar activeSite="3" />
        <Main>
          <TrackingHistory2 />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default TrackingPage;
