//react router
import { Redirect, Route, Switch } from "react-router-dom";

import LoginEnter from "components/routes/psp/login/enter";
import LoginPassword from "components/routes/psp/login/password";
import LoginCode from "components/routes/psp/login/code";
import LoginRecover from "components/routes/psp/login/recover";

const EntryPoint = (props) => {
  return (
    <Switch>
      <Route exact path="/">
        <Redirect to="/login" />
      </Route>
      <Route exact path="/login">
        <LoginEnter />
      </Route>
      <Route exact path="/forgot-password">
        <LoginPassword />
      </Route>
      <Route exact path="/code">
        <LoginCode />
      </Route>
      <Route exact path="/recover">
        <LoginRecover />
      </Route>
    </Switch>
  );
};

export default EntryPoint;
