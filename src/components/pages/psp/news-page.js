import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar";
import Main from "components/commons/main";

import Consult from "components/routes/psp/news/consult";
import History from "components/routes/psp/news/history";
import Edit from "components/routes/psp/news/edit";
import Comments from "components/routes/psp/news/comments";

const NewsPage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/news/consult">
        <Sidebar activeSite="2" />
        <Main>
          <Consult />
        </Main>
      </PrivateRoute>

      <PrivateRoute path="/news/history/:id">
        <Sidebar activeSite="2" />
        <Main>
          <History />
        </Main>
      </PrivateRoute>

      <PrivateRoute path="/news/update/:id">
        <Sidebar activeSite="2" />
        <Main>
          <Edit />
        </Main>
      </PrivateRoute>

      <PrivateRoute path="/news/comments/:id">
        <Sidebar activeSite="2" />
        <Main>
          <Comments />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default NewsPage;
