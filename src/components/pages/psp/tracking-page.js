import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar";
import Main from "components/commons/main";

import Tracking from "components/routes/psp/tracking/main";
import TrackingUpdatePatient from "components/routes/psp/tracking/update";
import TrackingHistory from "components/routes/psp/tracking/history";
import TrackingHistory2 from "components/routes/psp/tracking/history2";
import TrackingEditGestion from "components/routes/psp/tracking/update/apoyoD/edit";

const TrackingPage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/tracking">
        <Sidebar activeSite="3" />
        <Main>
          <Tracking />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/tracking/update/:id">
        <Sidebar activeSite="3" />
        <Main>
          <TrackingUpdatePatient />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/tracking/history">
        <Sidebar activeSite="3" />
        <Main>
          <TrackingHistory />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/tracking/history2">
        <Sidebar activeSite="3" />
        <Main>
          <TrackingHistory2 />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/tracking/update/apoyoD/edit">
        <Sidebar activeSite="3" />
        <Main>
          <TrackingEditGestion />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default TrackingPage;
