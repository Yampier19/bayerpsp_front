import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar";
import Main from "components/commons/main";

import Products from "components/routes/psp/products/filters";
import ProductsEditIncomings from "components/routes/psp/products/edit/incomings";
import ProductsEditOutcomings from "components/routes/psp/products/edit/outcomings";
import ProductsEditSuppliers from "components/routes/psp/products/edit/suppliers";

const ProductsPage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/products">
        <Sidebar activeSite="4" />
        <Main>
          <Products />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/products/create/incomings">
        <Sidebar activeSite="4" />
        <Main>
          <ProductsEditIncomings />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/products/create/outcomings">
        <Sidebar activeSite="4" />
        <Main>
          <ProductsEditOutcomings />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/products/create/suppliers">
        <Sidebar activeSite="4" />
        <Main>
          <ProductsEditSuppliers />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default ProductsPage;
