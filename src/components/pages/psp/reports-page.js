import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar";
import Main from "components/commons/main";

import Reports from "components/routes/psp/reports/main";
import ReportsCreate from "components/routes/psp/reports/create";
import DataList from "components/routes/psp/reports/data-list";
import ReportsGraphics from "components/routes/psp/reports/graphics";

const ProductsPage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/reports">
        <Sidebar activeSite="5" />
        <Main>
          <Reports />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/reports/create">
        <Sidebar activeSite="5" />
        <Main>
          <ReportsCreate />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/reports/data-list">
        <Sidebar activeSite="5" />
        <Main>
          <DataList />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/reports/graphics">
        <Sidebar activeSite="5" />
        <Main>
          <ReportsGraphics />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default ProductsPage;
