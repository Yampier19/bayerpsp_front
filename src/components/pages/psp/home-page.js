import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar";
import Main from "components/commons/main";

import Home from "components/routes/psp/home";
import WhatsApp from "components/base/whatsapp";

const HomePage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/home">
        <Sidebar type={2} />
        <Main>
          <Home />
          <WhatsApp />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default HomePage;
