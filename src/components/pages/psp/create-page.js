import { Switch } from "react-router-dom";
import { PrivateRoute } from "react-router-dom-extension";

import Sidebar from "components/commons/sidebar";
import Main from "components/commons/main";

import CreateNew from "components/routes/psp/create/new";
import CreatePatient from "components/routes/psp/create/patient";
import CreateEPS from "components/routes/psp/create/eps-opl";
import CreatePatientTreatment from 'components/routes/psp/create/patient/treatments/'

const CreatePage = (props) => {
  return (
    <Switch>
      <PrivateRoute exact path="/create/new">
        <Sidebar />
        <Main>
          <CreateNew />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/create/patient">
        <Sidebar />
        <Main>
          <CreatePatient />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/create/patient/info/treatments">
        <Sidebar />
        <Main>
          <CreatePatientTreatment />
        </Main>
      </PrivateRoute>

      <PrivateRoute exact path="/create/eps-opl">
        <Sidebar />
        <Main>
          <CreateEPS />
        </Main>
      </PrivateRoute>
    </Switch>
  );
};

export default CreatePage;
