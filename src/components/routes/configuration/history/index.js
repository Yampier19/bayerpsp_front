import {useState, useEffect} from 'react';

import FilterBar from 'components/base/filter-bar';
import Table from 'components/base/table';

import axios from 'axios';

import {data} from './data';

const ConfigurationHistory = props => {

    const url = 'https://jsonplaceholder.typicode.com/posts';

    const [news, setNews] = useState([]);

    useEffect(
        () => {

            axios.get(url)
            .then(res => {

                setNews(res.data.splice(0, 50));

            })
            .then(err => console.log(err));

        }, []
    );

    const [appliedFilters, setAppliedFilters] = useState([
        {
            name: 'Motivo comunicación'
        },
        {
            name: 'Logro comunicación'
        }
    ]);



    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 py-0">

                <div className="columns">
                    <div className="column">
                        <h1 className="title is-3 mb-5"><strong className="has-text-hblack">CONFIGURACIÓN</strong></h1>
                        <h2 className="subtitle has-text-hblack-light">Usuario <strong>- Creación y asignación de roles</strong></h2>
                    </div>
                    <div className="column has-text-centered is-2">

                        <div className="box has-background-hblack py-2">

                            <span>
                                Descargar
                            </span>  &nbsp;

                            <a className="circle-bg p-1">
                                <span className="icon has-text-hblack"><i class="fas fa-print"></i></span>
                            </a> &nbsp;

                            <a className="circle-bg p-1">
                                <span className="icon has-text-hblack"><i class="fas fa-file-pdf"></i></span>
                            </a> &nbsp;

                            <a className="circle-bg p-1">
                                <span className="icon has-text-hblack"><i class="fas fa-file-excel"></i></span>
                            </a>

                        </div>
                    </div>

                </div>

            </section>



            <br/>
            <br/>

            {/* filter bar */}
            <section className="section p-0 pb-1 ">
                <div className="columns">
                    <div className="column">
                        <FilterBar
                        textColorClass="has-text-hblack"
                        filters={[
                            {
                                name: "Nombre",
                                options: [{name: "op1"}, {name: "op2"}]
                            },
                            {
                                name: "Acción realizada",
                                options: []
                            },
                            {
                                name: "Fecha de inicio",
                                options: []
                            },
                            {
                                name: "Fecha de finalización",
                                options: []
                            }
                        ]}
                        moreFiltersOn={false}
                        tags={null}
                    />
                    </div>
                    <div className="column is-2-desktop has-text-right has-text-left-mobile">
                    </div>
                </div>

            </section>


            {/* second level - table */}
            <section className="is-flex-grow-1" style={{minHeight: '500px', overflow: 'hidden'}}>
                <div className="is-inline-flex is-flex-direction-row coolscroll hblack is-flex-wrap-nowrap" style={{overflow: 'auto', height:"100%", width: "100%"}} >
                    <div className="mr-3" style={{width: '100%'}}>
                        {/*}<Table data={data} ignore={[]} color="hblack" bgColor="light"/>*/}
                    </div>
                </div>
            </section>

            {/* data count */}
            <section className="section px-0 pb-0">

                <h1 className="subtitle has-text-hblack">
                    se encontraron {news.length} registros
                </h1>

            </section>

        </div>

    );
}

export default ConfigurationHistory;
