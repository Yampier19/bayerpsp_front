import { useState } from "react";
import { useEffect } from "react";
import { When } from "react-if";
import { Unless } from "react-if";

import DraggablePanel from "./draggable-panel";
import LoadingComponent from "components/base/loading-component";

import { useListAllRoles } from "../../../../services/bayer/psp/role/useRole";

const ConfigurationTemplates = (props) => {
  const { data: Roles, isLoading } = useListAllRoles();

  return (
    <div
      className="is-flex is-flex-direction-column "
      style={{ height: "100%" }}
    >
      {/* header */}
      <section className="section p-0">
        <h1 className="title is-3 mb-5">
          <strong className="has-text-hblack">CONFIGURACIÓN</strong>
        </h1>
        <h2 className="subtitle has-text-hblack-light">
          Historial de plantillas
        </h2>
      </section>

      <br />
      <br />
      <When condition={!isLoading}>
        {/* forms */}
        <section
          className="section px-0 is-flex-grow-1 py-4 coolscroll"
          style={{ height: "1px", overflow: "hidden", minHeight: "500px" }}
        >
          <div
            className="py-4 coolscroll"
            style={{ height: "100%", overflowY: "scroll", overflowX: "hidden" }}
          >
            <DraggablePanel items={Roles?.data} />
          </div>
        </section>
      </When>
      <Unless condition={!isLoading}>
        <LoadingComponent loadingText="Cargando datos" />
      </Unless>
    </div>
  );
};

export default ConfigurationTemplates;
