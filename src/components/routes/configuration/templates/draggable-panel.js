import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { useState } from "react";
// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const DraggablePanel = (props) => {
  const [items, setItems] = useState(props.items);

  const onDragEnd = (result) => {
    const { source, destination } = result;

    if (!destination || source.index === destination.index) return;

    setItems((prevFilters) =>
      reorder(prevFilters, source.index, destination.index)
    );
  };

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="patient">
        {(droppableProvided) => (
          <ul
            {...droppableProvided.droppableProps}
            ref={droppableProvided.innerRef}
          >
            {items.map((item, i) => (
              <Draggable
                key={item.id}
                draggableId={item.id.toString()}
                index={i}
              >
                {(draggableProvided) => (
                  <li
                    {...draggableProvided.draggableProps}
                    ref={draggableProvided.innerRef}
                    {...draggableProvided.dragHandleProps}
                    className="filter-item mb-0"
                    key={i}
                  >
                    <div className="columns is-vcentered">
                      <div className="column is-7 mb-3">
                        <div className="box mb-0">
                          <span className="icon has-text-hblack">
                            <i class="fas fa-bars"></i>
                          </span>
                          Plantilla {item.name}
                        </div>
                      </div>

                      <div className="column has-text-right pr-1 has-text-centered-mobile">
                        <div className="columns is-mobile">
                          <div className="column is-2-desktop is-offset-5-desktop">
                            <span>
                              <span className="icon has-text-hred is-size-2">
                                <i class="far fa-eye"></i>
                              </span>
                            </span>
                          </div>

                          <div className="column is-2-desktop">
                            <span>
                              <span className="icon has-text-hyellow is-size-2">
                                <i class="far fa-edit"></i>
                              </span>
                            </span>
                          </div>

                          <div className="column is-2-desktop">
                            <span>
                              <span className="icon has-text-light2 is-size-2">
                                <i class="fas fa-trash-alt"></i>
                              </span>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                )}
              </Draggable>
            ))}
            {droppableProvided.placeholder}
          </ul>
        )}
      </Droppable>
    </DragDropContext>
  );
};

export default DraggablePanel;
