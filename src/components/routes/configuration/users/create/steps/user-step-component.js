import React from "react";
import { Fragment } from "react";

import { useUserStepContext } from "./user-step-provider";
import Tabs from "../components/components/tabs";
import FormCreateUser from "../components/user/form-create-user";
import FormCreateRole from "../components/role/form-create-role";

const UserStepComponent = () => {
  const { currentStep } = useUserStepContext();

  const firstStep = <FormCreateUser />;
  const secondStep = <FormCreateRole />;

  return (
    <Fragment>
      <Tabs />
      {currentStep === 0 && firstStep}
      {currentStep === 1 && secondStep}
    </Fragment>
  );
};

export default UserStepComponent;
