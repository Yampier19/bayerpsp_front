import React from "react";
import { useEffect } from "react";
import { Fragment } from "react";
import _ from "lodash";
import { When } from "react-if";
import { Unless } from "react-if";
import { useFormikContext } from "formik";

import { useUserStepContext } from "../../steps/user-step-provider";
import { useCreateUserForm } from "../../useCreateUserForm";
import FormNoveltysReports from "./noveltys-reports-form";
import FormPatientConfiguration from "./patient-configuration-form";
import FormTrackingProducts from "./tracking-producst-form";
import LoadingComponent from "components/base/loading-component";

const RoleField = () => {
  const {
    user,
    currentRole: data,
    arrayRoles,
    setArrayRoles,
    setOrderRoles,
  } = useUserStepContext();
  const { roles, isLoadingRoles: isLoading } = useCreateUserForm();
  const { values, setFieldValue } = useFormikContext();

  const orderData = () => {
    let count = 0;
    let noveltys = {
      name: "Novedades",
      data: [],
    };
    let patientNew = {
      name: "Paciente",
      data: [],
    };
    let tracking = {
      name: "Seguimiento",
      data: [],
    };
    let products = {
      name: "Producto",
      data: [],
    };
    let reports = {
      name: "Reportes",
      data: [],
    };
    let configuration = {
      name: "Configuraciones",
      data: [],
    };
    let aditional = {
      name: "Adicional",
      data: [],
    };

    data.permissions.forEach((permi) => {
      if (permi.theme === "NOVEDADES") {
        noveltys.data.push(permi);
      } else if (permi.theme === "PACIENTE NUEVO") {
        patientNew.data.push(permi);
      } else if (permi.theme === "SEGUIMIENTO") {
        tracking.data.push(permi);
      } else if (permi.theme === "PRODUCTOS") {
        products.data.push(permi);
      } else if (permi.theme === "REPORTES") {
        reports.data.push(permi);
      } else if (permi.theme === "CONFIGURACIÓN") {
        configuration.data.push(permi);
      } else if (permi.them === "SOLICTUDES ADICIONALES") {
        aditional.data.push(permi);
      }
      count++;
    });
    if (count === data.permissions.length) {
      setOrderRoles({
        noveltys,
        reports,
        patientNew,
        configuration,
        tracking,
        products,
      });
    }
  };
  useEffect(() => {
    if (data) {
      orderData();
      setArrayRoles(data.permissions);
    }
    //eslint-disable-next-line
  }, [data, isLoading]);

  useEffect(() => {
    if (arrayRoles) {
      setFieldValue("role_object", {
        id: Number(data.id),
        name: data.name,
        permissions: arrayRoles,
      });
    }
    //eslint-disable-next-line
  }, [arrayRoles, isLoading]);

  return (
    <Fragment>
      {console.log("values", values)}
      <When
        condition={
          !_.isUndefined(roles.find((d) => d.id === Number(user.role)))
        }
      >
        <div className="sections p-3">
          <div className="row mb-5">
            <div className="col">
              <label style={{ fontWeight: "bold" }}>{data?.name}</label>
            </div>
          </div>
          {/* FILA 1 (PERMISOS PARA NOVEDADES Y REPORTES) */}
          <FormNoveltysReports />

          {/* FILA 2 (PERMISOS PARA PACIENTE Y CONFIGURACIONES)*/}
          <FormPatientConfiguration />

          {/* FILA 3 (PERMISOS PARA SEGUIMIENTO Y PRODUCTOS)*/}
          <FormTrackingProducts />
        </div>
      </When>
      <Unless
        condition={
          !_.isUndefined(roles.find((d) => d.id === Number(user.role)))
        }
      >
        <LoadingComponent loadingText="Cargando datos ...." />
      </Unless>
    </Fragment>
  );
};

export default RoleField;
