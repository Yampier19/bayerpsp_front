import React from "react";
import { useState } from "react";
import { useEffect } from "react";

import Field from "../components/field-role";
import { useUserStepContext } from "../../steps/user-step-provider";

const FormNoveltysReports = () => {
  const [localForm, setLocalForm] = useState([]);
  const { orderRoles, remplaceRole } = useUserStepContext();
  const handleChange = (e) => {
    let newPermission = [];
    remplaceRole(e);
    localForm.forEach((d) => {
      if (e.theme === d.theme && e.name === d.name) {
        newPermission.push(e);
      } else {
        newPermission.push(d);
      }
    });
    if (newPermission.length > 0) {
      setLocalForm(newPermission);
    }
  };

  useEffect(() => {
    if (orderRoles !== null && orderRoles.noveltys) {
      let permission = [];
      orderRoles.noveltys.data.forEach((d) => {
        permission.push(d);
      });
      if (permission.length > 0) {
        setLocalForm(permission);
      }
    }
    //eslint-disable-next-line
  }, [orderRoles]);
  useEffect(() => {
    if (orderRoles !== null && orderRoles.reports) {
      let permission = [];
      orderRoles.reports.data.forEach((d) => {
        permission.push(d);
      });
      if (permission.length > 0) {
        setLocalForm((prevState) => [...prevState, ...permission]);
      }
    }
    //eslint-disable-next-line
  }, [orderRoles]);
  return (
    <div>
      <div className="row my-5">
        <div className="col">
          {/* <form> */}
          <ul>
            <label style={{ fontWeight: "bold" }}>
              <input className="checkbox-config" type="checkbox" name="crs" />
              &nbsp; {orderRoles !== null && orderRoles.noveltys.name}
            </label>
            <ul className="ml-5 sec-list mt-3" id="crs">
              {orderRoles !== null &&
                orderRoles.noveltys.data.map((d, index) => (
                  <Field
                    data={d}
                    key={`noveltys_field_${index}`}
                    setField={handleChange}
                  />
                ))}
            </ul>
          </ul>
          {/* </form> */}
        </div>
        <div className="col ">
          {/* <form> */}
          <ul>
            <label style={{ fontWeight: "bold" }}>
              <input className="checkbox-config" type="checkbox" name="crs" />
              &nbsp; {orderRoles !== null && orderRoles.reports.name}
            </label>
            <ul className="ml-5 sec-list mt-3" id="crs">
              {orderRoles !== null &&
                orderRoles.reports.data.map((d, index) => (
                  <Field
                    data={d}
                    key={`reports_field_${index}`}
                    setField={handleChange}
                  />
                ))}
            </ul>
          </ul>
          {/*  </form> */}
        </div>
      </div>
    </div>
  );
};

export default FormNoveltysReports;
