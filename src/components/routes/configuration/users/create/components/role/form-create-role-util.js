import * as Yup from "yup";
import { useState } from "react";
import { useHistory } from "react-router-dom";

import { useUserStepContext } from "../../steps/user-step-provider";
import { useCreateUser } from "services/bayer/psp/users/useUsers";
import { ToastUtil } from "components/base/toast/toast-util";
import { Toast } from "components/base/toast/ToastContainer";
import { set_modal_on } from "redux/actions/modalActions";
import store from "redux/store";
import { InfoModal } from "components/base/modal-system/models/modal";

const useCreateRoleFormUtil = () => {
  const { user } = useUserStepContext();
  const [loading, setLoading] = useState(false);
  const { mutateAsync: createUser } = useCreateUser();
  const { push } = useHistory();

  const initialValues = {
    role_object: user.role_object || {},
  };

  const formSchema = Yup.object().shape({
    role_object: Yup.object(),
  });

  const onSubmit = (values) => {
    const data = {
      user: {
        ...user,
        country_id: Number(user.country_id),
        phone: Number(user.phone),
        role: values.role_object,
        system: [1],
      },
    };
    setLoading(true);
    createUser(data)
      .then((res) => {
        if (res.code === 200 && res.response === true) {
          const modalReducer = store.getState().modalReducer;
          const { _icons } = modalReducer;
          const modal = new InfoModal(
            `El usario ha sido creado éxitosamente`,
            _icons.CHECK_PRIMARY
          );
          store.dispatch(set_modal_on(modal));
          push("/configuration/users");
        } else {
          const message = ToastUtil(res.message, "error");
          Toast(message);
        }
      })
      .catch((err) => {
        console.log(err, "error");
        const message = ToastUtil("Lo sentimos, ocurrio un error", "error");
        Toast(message);
      })
      .finally(() => setLoading(false));
  };

  return { initialValues, formSchema, onSubmit, loading };
};

export default useCreateRoleFormUtil;
