import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { Fragment } from "react";

const FieldRole = ({ data, setField }) => {
  const { edit, view, name, theme } = data;
  const [form, setForm] = useState({ name, edit, view, theme });

  const handleChange = (value, key) => {
    if (value === "SI") {
      setForm({ ...form, [key]: "NO" });
    } else if (value === "NO") {
      setForm({ ...form, [key]: "SI" });
    }
  };

  useEffect(() => {
    setField(form);
    //eslint-disable-next-line
  }, [form]);
  return (
    <Fragment>
      <li>
        <div className="row">
          <div className="col-4">
            <label>
              <input
                className="checkbox-config"
                type="checkbox"
                name="newUser"
                data-target="#newuser"
              />
              &nbsp; {form.name}
            </label>
          </div>
          <div className="col-3">
            <div className="row">
              <div className="col d-flex align-items-center">
                <button
                  className="btn btn-sm"
                  style={
                    form.view === "SI"
                      ? { backgroundColor: "#D7908F" }
                      : { backgroundColor: "#FFF" }
                  }
                  onClick={() => handleChange(form.view, "view")}
                >
                  <i
                    class="fas fa-eye"
                    style={
                      form.view === "SI"
                        ? { color: "#FFF" }
                        : { color: "#D7908F" }
                    }
                  ></i>
                </button>
              </div>
              <div className="col d-flex align-items-center">
                <button
                  className="btn btn-sm"
                  style={
                    form.edit === "SI"
                      ? { backgroundColor: "#F1D6A4" }
                      : { backgroundColor: "#FFF" }
                  }
                  onClick={() => handleChange(form.edit, "edit")}
                >
                  <i
                    class="fas fa-edit"
                    style={
                      form.edit === "SI"
                        ? { color: "#FFF" }
                        : { color: "#F1D6A4" }
                    }
                  ></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </li>
    </Fragment>
  );
};

export default FieldRole;
