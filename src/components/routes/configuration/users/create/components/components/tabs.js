import React from "react";

import ProgressIcon from "components/base/progress-icon";
import ProgressBar from "components/base/progress-bar";
import { useUserStepContext } from "../../steps/user-step-provider";

const Tabs = () => {
  const { currentStep, progress, setCurrentStep } = useUserStepContext();

  return (
    <section className="section p-0 m-0">
      <div className="columns is-vcentered">
        <div className="column">
          <div className="tabs is-boxed rolAssignmentTabs ">
            <ul>
              <li
                className={`formTab is-active`}
                onClick={() => setCurrentStep(0)}
                data-tab="tab1"
              >
                {/* eslint-disable-next-line */}
                <a>
                  <ProgressIcon
                    className="has-background-hblack has-text-white"
                    value={progress}
                    icon={
                      progress === 100 ? (
                        <i className="fas fa-check"></i>
                      ) : (
                        <i class="fas fa-user"></i>
                      )
                    }
                  ></ProgressIcon>
                  &nbsp; GENERAL
                </a>
              </li>
              <li className={`formTab`} onClick={() => setCurrentStep(1)}>
                {/* eslint-disable-next-line */}
                <a>
                  <ProgressIcon
                    className={`${
                      progress === 100 && currentStep === 1
                        ? "has-background-hblack"
                        : "has-background-grey-lighter"
                    }  has-text-white`}
                    value={progress === 100 && currentStep === 1 ? 100 : 0}
                    icon={<i className="fas fa-pills"></i>}
                  ></ProgressIcon>{" "}
                  &nbsp; EDICIÓN DEL ROL
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="column is-3">
          <ProgressBar
            className="has-background-hblack"
            value={progress}
          ></ProgressBar>
        </div>
      </div>
    </section>
  );
};

export default Tabs;
