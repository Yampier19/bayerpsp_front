import * as Yup from "yup";

import { useUserStepContext } from "../../steps/user-step-provider";

const useCreateUserFormUtil = () => {
  const { user, setCurrentStep, setUser } = useUserStepContext();

  const initialValues = {
    name: user.name || "",
    password: user.password || "",
    user_name: user.user_name || "",
    type: user.type || "",
    role: user.role || "",
    provider: user.provider || "",
    phone: user.phone || "",
    country_id: user.country_id || "",
  };

  const requiredFieldType = "Este campo es obligatorio";

  const formSchema = Yup.object().shape({
    name: Yup.string()
      .required(requiredFieldType)
      .min(8, "El nombre debe tener minimo 8 caracteres"),
    password: Yup.string()
      .required(requiredFieldType)
      .min(8, "La contrasena debe tener minimo 8 caracteres")
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]/,
        "La contrasena debe tener una mayuscula, minuscula y un numero"
      ),
    user_name: Yup.string()
      .required(requiredFieldType)
      .min(8, "El User Name debe tener minimo 8 caracteres"),
    type: Yup.string().required(requiredFieldType),
    role: Yup.string().required(requiredFieldType),
    provider: Yup.string().required(requiredFieldType),
    phone: Yup.string().required(requiredFieldType),
    country_id: Yup.string().required(requiredFieldType),
    role_object: Yup.object(),
  });

  const onSubmit = (values) => {
    setCurrentStep(1);
    setUser({ ...values });
  };

  return { initialValues, formSchema, onSubmit };
};

export default useCreateUserFormUtil;
