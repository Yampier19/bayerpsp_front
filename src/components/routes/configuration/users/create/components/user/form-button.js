import React from "react";

const FormButton = () => {
  return (
    <div className="field has-text-right has-text-centered-mobile">
      <div className="control">
        <button
          type="submit"
          className="button is-hblack"
          style={{ width: "150px" }}
          disabled={false}
        >
          SIGUIENTE
        </button>
      </div>
    </div>
  );
};

export default FormButton;
