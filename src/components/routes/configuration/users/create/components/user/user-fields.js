import React from "react";
import { Fragment } from "react";
import { useEffect } from "react";
import { Field } from "formik";
import { useFormikContext } from "formik";
import { TextField } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";

import FormField from "components/form/formfield";
import FormField2 from "components/form/formfield2";
import Radio from "../components/radio";
import Dropdown from "components/form/dropdown";
import { useCreateUserForm } from "../../useCreateUserForm";
import { useUserStepContext } from "../../steps/user-step-provider";

const UserFields = () => {
  const { values } = useFormikContext();

  const {
    roles,
    countries,
    providers,
    isLoadingRoles,
    isLoadingProviders,
    isLoadingCountries,
  } = useCreateUserForm();

  const { setProgress } = useUserStepContext();

  const setProgressCount = () => {
    let count = 0;
    let lengthFields = Object.entries(values).length;
    Object.entries(values).forEach((d) => {
      if (d[1] !== null && d[1] !== undefined && d[1] !== "") {
        count++;
      }
    });
    if (count !== 0) {
      setProgress((count * 100) / lengthFields);
    }
  };

  useEffect(() => {
    setProgressCount();
    //eslint-disable-next-line
  }, [values]);

  return (
    <Fragment>
      <FormField label="Nombres y Apellidos *" number="1" checked={values.name}>
        <Field name="name">
          {({ field, form: { touched, errors } }) => (
            <Fragment>
              <input type={"text"} className="input cool-input" {...field} />
              {touched.name && errors.name ? (
                <div className="help is-danger">{errors.name}</div>
              ) : null}
            </Fragment>
          )}
        </Field>
      </FormField>
      <FormField label="Contraseña *" number="2" checked={values.password}>
        <Field name="password">
          {({ field, form: { touched, errors } }) => (
            <Fragment>
              <input type={"text"} className="input cool-input" {...field} />
              {touched.password && errors.password ? (
                <div className="help is-danger">{errors.password}</div>
              ) : null}
            </Fragment>
          )}
        </Field>
      </FormField>
      <FormField label="User Name *" number="3" checked={values.user_name}>
        <Field name="user_name">
          {({ field, form: { touched, errors } }) => (
            <Fragment>
              <input type={"text"} className="input cool-input" {...field} />
              {touched.user_name && errors.user_name ? (
                <div className="help is-danger">{errors.user_name}</div>
              ) : null}
            </Fragment>
          )}
        </Field>
      </FormField>
      <FormField2
        number1="4"
        number2="4.1"
        checked1={values.type !== "" && values.role !== ""}
        label1="Tipo *"
        label2="Rol *"
        input1={
          <Field name="type">
            {({ field, form: { errors, touched } }) => (
              <div>
                <Radio {...field} value={values.type} />
                {touched.type && errors.type ? (
                  <div className="help is-danger">{errors.type}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="role">
            {({ field, form: { errors, touched } }) => (
              <div>
                <Dropdown
                  options={roles}
                  {...field}
                  arrowColor="has-text-primary"
                  value={Number(values.role)}
                  disabled={isLoadingRoles}
                />
                {touched.role && errors.role ? (
                  <div className="help is-danger">{errors.role}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormField
        label="Proveedor *"
        number="5"
        checked={values.provider !== ""}
      >
        <Field name="provider">
          {({ field, form: { errors, touched } }) => (
            <Fragment>
              <Autocomplete
                {...field}
                freeSolo
                autoSelect
                value={values.provider}
                options={providers}
                renderInput={(params) => (
                  <TextField name="provider" id="provider" {...params} />
                )}
                disabled={isLoadingProviders}
              />
              {touched.provider && errors.provider ? (
                <div className="help is-danger">{errors.provider}</div>
              ) : null}
            </Fragment>
          )}
        </Field>
      </FormField>
      <FormField
        label="Numero de contacto *"
        number="6"
        checked={values.phone !== ""}
      >
        <Field name="phone">
          {({ field, form: { errors, touched } }) => (
            <Fragment>
              <input
                id="phone"
                className="input cool-input"
                type="text"
                name="phone"
                {...field}
              />
              {touched.phone && errors.phone ? (
                <div className="help is-danger">{errors.phone}</div>
              ) : null}
            </Fragment>
          )}
        </Field>
      </FormField>
      <FormField
        label="Pais *"
        number="7"
        noline
        checked={values.country_id !== ""}
      >
        <Field name="country_id">
          {({ field, form: { errors, touched } }) => (
            <Fragment>
              <Dropdown
                {...field}
                value={Number(values.country_id)}
                options={countries}
                arrowColor="has-text-primary"
                disabled={isLoadingCountries}
              />
              {touched.country_id && errors.country_id ? (
                <div className="help is-danger">{errors.country_id}</div>
              ) : null}
            </Fragment>
          )}
        </Field>
      </FormField>
    </Fragment>
  );
};

export default UserFields;
