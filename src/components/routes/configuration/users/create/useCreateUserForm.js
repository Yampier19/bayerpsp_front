import { useState } from "react";

import { useUserStepContext } from "./steps/user-step-provider";
import { useListAllRoles } from "services/bayer/psp/role/useRole";
import { useListAllCountries } from "services/bayer/psp/countries/useCountries";
import { useListAllProviders } from "services/bayer/psp/providers/useProvidersServices";

export const useCreateUserForm = () => {
  const { setRoles: setRolesStepper } = useUserStepContext();
  const [roles, setRoles] = useState([]);
  const [countries, setCountries] = useState([]);
  const [providers, setProviders] = useState([]);

  const queryConfig = {
    refetchOnWindowFocus: false,
    refetchOnmount: false,
    refetchOnReconnect: true,
    retry: false,
    enabled: false,
  };

  const { isLoading: isLoadingRoles } = useListAllRoles({
    onSuccess: (data) => {
      setRolesStepper(data.data.filter((opt) => opt.name !== "Super Admin"));
      setRoles(data.data.filter((opt) => opt.name !== "Super Admin"));
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });

  const { isLoading: isLoadingProviders } = useListAllProviders({
    onSuccess: (data) => {
      setProviders(data.data.map((opt) => opt.name));
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });

  const { isLoading: isLoadingCountries } = useListAllCountries({
    onSuccess: (data) => {
      setCountries(data.data);
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });

  return {
    roles,
    isLoadingRoles,
    countries,
    isLoadingCountries,
    providers,
    isLoadingProviders,
  };
};
