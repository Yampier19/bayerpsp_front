import { useState } from "react";
import { useParams } from "react-router-dom";

import { useFindOneUser } from "services/bayer/psp/users/useUsers";
import { useUserStepContext } from "./steps/user-step-provider";
import { useListAllRoles } from "services/bayer/psp/role/useRole";
import { useListAllCountries } from "services/bayer/psp/countries/useCountries";
import { useListAllProviders } from "services/bayer/psp/providers/useProvidersServices";

export const useEditUserForm = () => {
  const {
    setRoles: setRolesStepper,
    setUser,
    setCurrentRole,
  } = useUserStepContext();
  const [roles, setRoles] = useState([]);
  const [countries, setCountries] = useState([]);
  const [providers, setProviders] = useState([]);
  const [loading, setLoading] = useState(true);
  const { id } = useParams();

  const queryConfig = {
    refetchOnWindowFocus: false,
    refetchOnmount: true,
    refetchOnReconnect: true,
    retry: 2,
  };

  const { isLoading: isLoadingUser, refetch: refetchUser } = useFindOneUser(
    id,
    {
      onSuccess: ({ data }) => {
        setUser({
          name: data?.name,
          password: data?.password,
          user_name: data?.user_name,
          type: data?.type,
          phone: data?.phone,
          country_id: data?.country?.id,
          provider: data?.provider?.name,
          role: roles.filter((d) => d.name === data?.role?.name.trim())[0]?.id,
        });
        setCurrentRole({
          id: data?.role?.id,
          name: data?.role?.name.trim(),
          permissions: data?.permissions,
        });
        setLoading(false);
      },
      onError: (err) => {
        console.log("Here on error");
      },
      enabled: false,
      ...queryConfig,
    }
  );

  const { isLoading: isLoadingRoles } = useListAllRoles({
    onSuccess: (data) => {
      setRolesStepper(data.data.filter((opt) => opt.name !== "Super Admin"));
      setRoles(data.data.filter((opt) => opt.name !== "Super Admin"));
    },
    onError: (err) => {
      console.log("Here on error");
    },
    ...queryConfig,
  });

  const { isLoading: isLoadingProviders } = useListAllProviders({
    onSuccess: (data) => {
      setProviders(data.data.map((opt) => opt.name));
    },
    onError: (err) => {
      console.log("Here on error");
    },
    ...queryConfig,
  });

  const { isLoading: isLoadingCountries } = useListAllCountries({
    onSuccess: (data) => {
      setCountries(data.data);
    },
    onError: (err) => {
      console.log("Here on error");
    },
    ...queryConfig,
  });

  return {
    roles,
    isLoadingRoles,
    countries,
    isLoadingCountries,
    providers,
    isLoadingProviders,
    isLoadingUser,
    refetchUser,
    loading,
  };
};
