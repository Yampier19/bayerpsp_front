import React from "react";
import _ from "lodash";
import { Fragment } from "react";
import { useEffect } from "react";
import { When } from "react-if";
import { Unless } from "react-if";

import { useUserStepContext } from "./user-step-provider";
import { useEditUserForm } from "../useEditUserForm";
import Tabs from "../components/components/tabs";
import FormCreateUser from "../components/user/form-edit-user";
import FormCreateRole from "../components/role/form-create-role";
import LoadingComponent from "components/base/loading-component";

const UserStepComponent = () => {
  const { currentStep, user } = useUserStepContext();
  const { refetchUser, loading } = useEditUserForm();

  const firstStep = <FormCreateUser />;
  const secondStep = <FormCreateRole />;

  useEffect(() => {
    if (_.values(user).every(_.isEmpty)) {
      refetchUser();
    }
    //eslint-disable-next-line
  }, []);

  return (
    <Fragment>
      <Tabs />
      <When condition={!loading}>
        {currentStep === 0 && firstStep}
        {currentStep === 1 && secondStep}
      </When>
      <Unless condition={!loading}>
        <LoadingComponent loadingText="Cargando datos ..." />
      </Unless>
    </Fragment>
  );
};

export default UserStepComponent;
