import React, { useEffect } from "react";
import _ from "lodash";
import { createContext } from "react";
import { useContext } from "react";
import { useState } from "react";

export const UserStepContext = createContext();
export const useUserStepContext = () => useContext(UserStepContext);

const UserStepProvider = ({ children }) => {
  const [currentStep, setCurrentStep] = useState(0);
  const [progress, setProgress] = useState(0);
  const [user, setUser] = useState({
    name: "",
    password: "",
    user_name: "",
    type: "",
    role: "",
    role_object: {},
    provider: "",
    phone: "",
    country_id: "",
  });
  const [roles, setRoles] = useState([]);
  const [currentRole, setCurrentRole] = useState(null);
  const [orderRoles, setOrderRoles] = useState(null);
  const [arrayRoles, setArrayRoles] = useState(null);

  useEffect(() => {
    if (!_.values(user).every(_.isEmpty)) {
      setCurrentRole(roles.find((d) => d.id === Number(user.role)));
    }
    //eslint-disable-next-line
  }, [user]);

  const remplaceRole = (e) => {
    setArrayRoles(
      arrayRoles.map((d) => (e.theme === d.theme && e.name === d.name ? e : d))
    );
  };

  const context = {
    currentStep,
    setCurrentStep,
    progress,
    setProgress,
    user,
    setUser,
    arrayRoles,
    orderRoles,
    roles,
    setRoles,
    setOrderRoles,
    setArrayRoles,
    remplaceRole,
    currentRole,
    setCurrentRole,
  };

  return <UserStepContext.Provider value={context} children={children} />;
};

export default UserStepProvider;
