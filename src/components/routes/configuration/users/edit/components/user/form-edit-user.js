import React from "react";
import { Formik } from "formik";
import { Form } from "formik";

import useCreateUserFormUtil from "./form-edit-user-util";
import UserField from "./user-fields";
import UserButton from "./form-button";

const FormCreateUser = () => {
  const { initialValues, formSchema, onSubmit } = useCreateUserFormUtil();
  return (
    <section
      className="section px-0 is-flex-grow-1 py-4 coolscroll"
      style={{ height: "1px", overflow: "hidden", minHeight: "500px" }}
    >
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={formSchema}
        onSubmit={onSubmit}
      >
        <Form style={{ height: "100%", overflowX: "scroll" }}>
          <UserField />
          <UserButton />
        </Form>
      </Formik>
    </section>
  );
};

export default FormCreateUser;
