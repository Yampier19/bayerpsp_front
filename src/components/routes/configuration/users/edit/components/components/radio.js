const Radio = (props) => {
  return (
    <div class="control">
      <label class="radio">
        <input
          type="radio"
          name={props.name}
          onChange={props.onChange}
          value={props.option1 || "Educador"}
          checked={props.value === props.option1 || props.value === "Educador"}
        />
        &nbsp;
        {props.option1 || "Educador"}
      </label>{" "}
      &nbsp;&nbsp;&nbsp;
      <label class="radio">
        <input
          type="radio"
          name={props.name}
          onChange={props.onChange}
          value={props.option2 || "Edugestor"}
          checked={props.value === props.option2 || props.value === "Edugestor"}
        />
        &nbsp;
        {props.option2 || "Edugestor"}
      </label>
    </div>
  );
};

export default Radio;
