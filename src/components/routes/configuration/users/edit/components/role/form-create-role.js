import React from "react";
import { Formik } from "formik";
import { Form } from "formik";

import useCreateRoleForm from "./form-create-role-util";
import RoleField from "./role-field";
import FormButton from "./form-button";

const FormCreateRole = () => {
  const { initialValues, formSchema, onSubmit, loading } = useCreateRoleForm();
  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      validationSchema={formSchema}
      onSubmit={onSubmit}
    >
      <Form>
        <div style={{ height: "100%" }}>
          <div className="columns">
            <hr />
            <div className="column">
              <RoleField />
            </div>
          </div>
          <br />
          <FormButton loading={loading} />
        </div>
      </Form>
    </Formik>
  );
};

export default FormCreateRole;
