import React from "react";
import { LoadingButton } from "@mui/lab";

const FormButton = ({ loading }) => {
  return (
    <div className="field has-text-right has-text-centered-mobile">
      <div className="control">
        <LoadingButton
          type="submit"
          className="button is-hblack"
          style={{ width: "150px" }}
          disabled={loading}
        >
          EDITAR ROL
        </LoadingButton>
      </div>
    </div>
  );
};

export default FormButton;
