import React from "react";

import "./rol-assignment.scss";
import UserEditSteps from "./steps/user-step-component";
import UserEditProvider from "./steps/user-step-provider";
import { useAuthorizationAction } from "providers/authorization/action/authorizationActionProvider";
import { AuthorizationActionComponent } from "providers/authorization/action/authorizationactionComponent";

const CreateUserComponent = () => {
  const { canSettingUsersEdit } = useAuthorizationAction();
  return (
    <AuthorizationActionComponent
      isAuthorized={canSettingUsersEdit}
      redirect={"/home"}
      children={
        <UserEditProvider>
          <div
            className="is-flex is-flex-direction-column "
            style={{ height: "100%" }}
          >
            <section className="section p-0">
              <h1 className="title is-3 mb-5">
                <strong className="has-text-hblack">CONFIGURACIÓN</strong>
              </h1>
              <h2 className="subtitle has-text-hblack-light">
                Creación y asignación de roles
              </h2>
            </section>
            <hr />
            <UserEditSteps />
          </div>
        </UserEditProvider>
      }
    />
  );
};

export default CreateUserComponent;
