import { When } from "react-if";
import { Unless } from "react-if";

import LoadingComponent from "components/base/loading-component/index.js";
import FilterBar from "components/base/filter-bar";
import CreateUserBtn from "components/base/buttons/create-user";
import Table from "./components/table";
import { useListAllUsers } from "services/bayer/psp/users/useUsers";
import { useAuthorizationAction } from "providers/authorization/action/authorizationActionProvider";
import { AuthorizationActionComponent } from "providers/authorization/action/authorizationactionComponent";
import "./users.scss";

const ListUsersComponent = (props) => {
  const { data: Clients, isLoading, refetch } = useListAllUsers();
  const { canSettingUsersView } = useAuthorizationAction();

  return (
    <AuthorizationActionComponent
      isAuthorized={canSettingUsersView}
      redirect={"/home"}
      children={
        <div
          className="is-flex is-flex-direction-column "
          style={{ height: "100%" }}
        >
          {/* header */}
          <section className="section px-0 pt-0 pb-3 ">
            <div className="columns ">
              <div className="column is-3-desktop ">
                <h1
                  className="subtitle has-text-hred is-3 mb-1"
                  style={{ whiteSpace: "nowrap" }}
                >
                  <strong className="has-text-hblack">Configuración</strong>
                </h1>
                <h1 className="subtitle has-text-hblack-light">Usuarios</h1>
              </div>
              <div className="column is-3-desktop is-offset-6-desktop">
                <CreateUserBtn />
              </div>
            </div>
          </section>

          {/* filter bar */}
          <section className="section p-0 pb-1 ">
            <div className="columns">
              <div className="column">
                <FilterBar
                  textColorClass="has-text-hblack"
                  filters={[
                    {
                      name: "Nombre de usuario",
                      options: [{ name: "op1" }, { name: "op2" }],
                    },
                    {
                      name: "Perfil",
                      options: [],
                    },
                    {
                      name: "Acción",
                      options: [],
                    },
                  ]}
                  moreFiltersOn={false}
                  tags={null}
                />
              </div>
              {/*}<div className="column is-2-desktop has-text-right has-text-left-mobile">

                    </div> */}
            </div>
          </section>
          {/* form */}
          <When condition={isLoading}>
            <LoadingComponent loadingText="Cargando datos" />
          </When>
          <Unless condition={isLoading}>
            <section
              className="section px-0 is-flex-grow-1 pt-0 coolscroll"
              style={{ minHeight: "500px", overflowY: "scroll" }}
            >
              <div
                className=""
                style={{ minWidth: "1450px", overflow: "scroll" }}
              >
                <Table data={Clients?.data} refetch={refetch} />
              </div>
            </section>

            {/* <section className="section px-0 pb-0 has-text-right has-text-centered-mobile">
          <p className="subtitle has-text-hblack">
            <span className="icon has-text-hblack is-size-4">
              <i class="far fa-eye"></i>
            </span>
            &nbsp;
            <span>
              <u>
                <strong>Ver plantillas</strong>
              </u>
            </span>
          </p>
        </section> */}
          </Unless>
        </div>
      }
    />
  );
};

export default ListUsersComponent;
