import { useEffect } from "react";
import { When } from "react-if";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import store from "redux/store";
import DataTable from "components/base/datatable";
import {
  set_modal_off,
  set_modal_on,
  set_loading,
} from "redux/actions/modalActions";
import { ConfirmModal } from "components/base/modal-system/models/modal";
import { useDeleteUser } from "services/bayer/psp/users/useUsers";
import { useAuthorizationAction } from "providers/authorization/action/authorizationActionProvider";

const Table = ({ data = [], refetch }) => {
  const { mutateAsync: deleteUser, isLoading } = useDeleteUser();

  useEffect(() => {
    store.dispatch(set_loading(isLoading));
  }, [isLoading]);

  const { canSettingUsersEdit } = useAuthorizationAction();

  const show = (value) => {
    const modalReducer = store.getState().modalReducer;
    const { _icons } = modalReducer;
    const modal = new ConfirmModal(
      `¿Esta seguro de eliminar el rol ${value.rol}?`,
      _icons.TRASH_CAN_HBLACK,
      () => confirm(value.id)
    );
    store.dispatch(set_modal_on(modal));
  };

  const confirm = (id) => {
    deleteUser(id).then((res) => {
      if (res.code === 200 && res.response === true) {
        store.dispatch(set_modal_off());
        refetch();
      }
    });
  };

  const columns = [
    {
      name: "foto",
      label: "AVATAR",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => (
          <div className="column has-no-border py-0 pr-0 p-2">
            <div
              className="columns is-mobile my-0"
              style={{ height: "100%", background: "" }}
            >
              <div className="row ">
                <div className="col-2">
                  <img
                    alt="avatar-user"
                    className="w-100 rounded-circle"
                    src="https://static2.elnortedecastilla.es/www/pre2017/multimedia/noticias/201501/12/media/cortadas/facebook-profile-picture-no-pic-avatar--575x323.jpg"
                  />
                </div>
                <div className="col-10">
                  <div className="row-reverse">
                    <div className="col">
                      <label>{value.name}</label>
                    </div>
                    <div className="col">
                      <label>{value.user}</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ),
      },
    },
    { name: "estado", label: "ESTADO", options: { filter: true, sort: true } },
    {
      name: "razon",
      label: "RAZÓN INACTIVO",
      options: { filter: true, sort: true },
    },
    { name: "rol", label: "ROL", options: { filter: true, sort: true } },
    ...(canSettingUsersEdit
      ? [
          {
            name: "acciones",
            label: "OPCIONES",
            options: {
              filter: true,
              sort: true,
              customBodyRender: (value) => (
                <When condition={canSettingUsersEdit}>
                  <div className="column has-no-border py-0 pr-0 p-2">
                    <div
                      className="columns is-mobile my-0"
                      style={{ height: "100%", background: "" }}
                    >
                      <div className="row ">
                        <div className="col ">
                          <Link
                            to={`/configuration/users/edit/${value.id}`}
                            className="border border-white rounded-circle"
                          >
                            <i class="fas fa-edit "></i>
                          </Link>
                        </div>
                        <div className="col ">
                          <button
                            className="border border-white rounded-circle"
                            onClick={() => show(value)}
                          >
                            <i class="fas fa-trash-alt text-danger"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </When>
              ),
            },
          },
        ]
      : []),
  ];

  const dataLocal = data.map((d) => ({
    foto: { name: d.name, user: d.user },
    estado: d.status,
    razon: d.reason_inactivity ? d.reason_inactivity : "No hay razon",
    rol: d.role.name,
    acciones: { id: d.id, name: d.name, rol: d.role.name },
  }));

  return (
    <div className="columns is-mobile">
      <div
        className="column is-1 is-hidden-tablet has-text-centered"
        style={{ minWidth: "10px" }}
      ></div>
      <div className="column">
        <div className="columns is-mobile">
          <div
            className="column is-1 is-hidden-tablet has-text-centered"
            style={{ minWidth: "10px" }}
          ></div>
          <div className="column">
            <DataTable
              data={dataLocal}
              columns={columns}
              title={"Listado de Usuarios"}
            />
          </div>
          <div
            className="column is-1 is-hidden-mobile"
            style={{ maxWidth: "10px" }}
          ></div>
        </div>
      </div>
      <div
        className="column is-1 is-hidden-mobile"
        style={{ maxWidth: "10px" }}
      ></div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  modalState: state.modalReducer,
});

export default connect(mapStateToProps, {
  set_modal_on,
  set_modal_off,
  set_loading,
})(Table);
