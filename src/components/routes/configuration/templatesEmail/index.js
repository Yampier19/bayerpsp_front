import {useState, useEffect} from 'react';

import CreateFilter from 'components/base/buttons/create-filter';

import DraggablePanel from './draggable-panel';

import {tabClicked} from 'components/commons/tabs/tabClicked';

import {
    templatesData
} from './userTemplates';


const ConfigurationTemplatesEmail = props => {

    const [templates, setTemplates] = useState(templatesData);
    const [templates2, setTemplates2] = useState(templatesData);
    const [templates3, setTemplates3] = useState(templatesData);


    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section p-0">
                <h1 className="title is-3 mb-5"><strong className="has-text-hblack">CONFIGURACIÓN</strong></h1>
                <h2 className="subtitle has-text-hblack-light">Usuario <strong>- Plantillas</strong></h2>
            </section>



            <br/>
            <br/>

            {/* tabs */}
            <section className="section p-0 m-0">
                <div className="columns is-vcentered">

                    <div className="column">
                        <div className="tabs is-boxed emailTab is-pulled-right">
                            <ul>
                                <li className={`formTab is-active mx-2`} onClick={tabClicked} data-tab="tab1" style={{minWidth: '120px'}}>
                                    <a>
                                        PSP
                                    </a>
                                </li>
                                <li className={`formTab mx-2`} onClick={tabClicked} data-tab="tab2" style={{minWidth: '120px'}}>
                                    <a>
                                        CRS
                                    </a>
                                </li>
                                <li className={`formTab mx-2`} onClick={tabClicked} data-tab="tab3" style={{minWidth: '120px'}}>
                                    <a>
                                        Apoyo diagnostico
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>



            {/* forms */}
            <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                <div id="tab1" className="tab is-active py-4 coolscroll" style={{height: '100%', overflowY: 'scroll', overflowX: 'hidden'}}>
                    <DraggablePanel items={templates} setItems={setTemplates}/>
                </div>
                <div id="tab2" className="tab py-4 coolscroll" style={{height: '100%', overflowY: 'scroll', overflowX: 'hidden'}}>
                    <DraggablePanel items={templates2} setItems={setTemplates2}/>
                </div>
                <div id="tab3" className="tab py-4 coolscroll" style={{height: '100%', overflowY: 'scroll', overflowX: 'hidden'}}>
                    <DraggablePanel items={templates3} setItems={setTemplates3}/>
                </div>
            </section>



        </div>
    );
}

export default ConfigurationTemplatesEmail;
