import {tabClicked} from 'components/commons/tabs/tabClicked';

import './email.scss';

import EmailEditor from 'components/base/email-editor';

const ConfigurationEmail = () => {
    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section p-0">
                <h1 className="title is-3 mb-5"><strong className="has-text-hblack">CONFIGURACIÓN</strong></h1>
                <h2 className="subtitle has-text-hblack-light">Plantilla correo electrónico</h2>
            </section>

            <br/>

            {/* tabs */}
            <section className="section p-0 m-0">
                <div className="columns is-vcentered">

                    <div className="column">
                        <div className="tabs is-boxed emailTab is-pulled-right">
                            <ul>
                                <li className={`formTab is-active mx-2`} onClick={tabClicked} data-tab="tab1" style={{minWidth: '120px'}}>
                                    <a>
                                        PSP
                                    </a>
                                </li>
                                <li className={`formTab mx-2`} onClick={tabClicked} data-tab="tab2" style={{minWidth: '120px'}}>
                                    <a>
                                        CRS
                                    </a>
                                </li>
                                <li className={`formTab mx-2`} onClick={tabClicked} data-tab="tab3" style={{minWidth: '120px'}}>
                                    <a>
                                        Apoyo diagnostico
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>

            <hr className="has-background-dark"/>

            {/* forms */}
            <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>

                <div id="tab1"/><div id="tab2"/><div id="tab3"/>
                <div className="py-4 px-3 coolscroll is-flex is-flex-direction-column " style={{height: '100%', overflowX: 'hidden', maxWidth: '100%'}}>

                    <form id="form.email">

                        <div class="field">
                            <div className="columns is-vcentered">
                                <div className="column is-1">
                                    <label class="label">Para</label>
                                </div>
                                <div className="column">
                                    <div class="control">
                                        <input class="input cool-input" type="text" placeholder=""/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field">
                            <div className="columns is-vcentered">
                                <div className="column is-1">
                                    <label class="label">CC</label>
                                </div>
                                <div className="column">
                                    <div class="control">
                                        <input class="input cool-input" type="text" placeholder=""/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="field">
                            <div className="columns is-vcentered">
                                <div className="column is-1">
                                    <label class="label">Asunto</label>
                                </div>
                                <div className="column">
                                    <div class="control">
                                        <input class="input cool-input" type="text" placeholder=""/>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>

                    <div className=" is-flex-grow-1">
                        <EmailEditor id="email"/>
                    </div>

                    <hr className="has-background-dark"/>

                    <div className="level">

                        <div className="level-left">
                            <div className="control">
                                <button
                                    className="button is-normal"
                                    type="button"
                                    style={{width: '150px'}}

                                >
                                    <span className="icon">
                                        <i class="fas fa-eye"></i>
                                    </span>
                                    <span>
                                        Previsualizar
                                    </span>

                                </button>
                            </div>
                        </div>



                        <div className="level-right has-text-right">

                            <a className="has-text-hblack"><u>Guardar plantilla de correo electrónico</u></a>

                            &nbsp;&nbsp;&nbsp;

                            <div className="control">
                                <button
                                    className="button is-hblack"
                                    type="button"
                                    style={{width: '150px'}}

                                >
                                    Enviar
                                </button>
                            </div>
                        </div>

                    </div>



                </div>
            </section>


        </div>
    );
}

export default ConfigurationEmail;
