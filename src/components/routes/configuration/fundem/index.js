import {useState} from 'react';

import FormField from 'components/form/formfield';

import {Link} from 'react-router-dom';

const areAllFieldFilled = (form, fields) => {

    for(let i = 0; i < fields; i++)
        if(!form[`check_field${i+1}`].checked)
            return false;

    return true;
}


const ConfiguracionFundem = props => {

    const [submitDisabled, setSubmitDisabled] = useState(true);

    const handleSubmit = e => {
        e.preventDefault();
    }

    const onChange = e => {



        const form = document.getElementById('form');

        //get the name of current field
        const name = e.target.name;

        //look for the linked check
        const check = form[`check_${name}`];

        check.checked = e.target.value != '' ? true : false;

        const allFilled = areAllFieldFilled(form, 8);

        setSubmitDisabled( allFilled ? false : true );
    }



    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 pt-0">
                <h1 className="title has-text-hblack">Configuración</h1>
                <h2 className="subtitle has-text-hblack-light">Gestiónes</h2>
            </section>

            {/* form */}
            <section className="section px-0 is-flex-grow-1 pt-0" style={{height: '300px'}}>

                <form id="form" onSubmit={handleSubmit} className="coolscroll pr-3 py-3">
                    <FormField label="Usuario" number="1" name="field1">
                        <input className="input cool-input" type="text" name="field1"  onChange={onChange}/>
                    </FormField>

                    <FormField label="Seleccionar archivo" number="2" name="field2" noline>
                        <button className="button is-light is-rounded" type="text" name="field2" onChange={onChange}>Seleccionar archivo</button>
                    </FormField>


                    <br/>

                    <div className="field">
                        <div className="control has-text-right has-text-centered-mobile">
                            <button
                                className="button is-hblack"
                                type="submit"
                                style={{width: '150px'}}
                                disabled={submitDisabled}
                            >
                                Subir
                            </button>
                        </div>
                    </div>



                </form>

            </section>

        </div>
    );
}

export default ConfiguracionFundem;
