import React from "react";
import { Link } from "react-router-dom";
import { Switch } from "react-if";
import { Case } from "react-if";
import { Default } from "react-if";
import { When } from "react-if";

import DataTable from "components/base/datatable";
import { useAuthorizationAction } from "providers/authorization/action/authorizationActionProvider";

//IMAGENES PAISES
import IMGColombia from "./../../../../../utils/img/flags/colombia.png";
import IMGPeru from "./../../../../../utils/img/flags/peru.png";
import IMGEcuador from "./../../../../../utils/img/flags/ecuador.png";

const Table = ({ data = [], refetch }) => {
  const { canTrackingListEdit } = useAuthorizationAction();

  const columns = [
    {
      name: "id",
      label: "PAP",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => (
          <div>PAP{value.toString().padStart(5, 0)}</div>
        ),
      },
    },
    {
      name: "document_number",
      label: "COD. USUARIO",
      options: { filter: true, sort: true },
    },
    {
      name: "treatments",
      label: "ID TRAT.",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) =>
          value.map((d, index) => (
            <div
              className={`${
                index % 2 === 0 ? "subcol" : ""
              } column has-no-border py-0 pr-0 p-2`}
            >
              <div
                className="columns is-mobile my-0"
                style={{ height: "100%", background: "" }}
              >
                <div className="column">{d.id}</div>
              </div>
            </div>
          )),
      },
    },
    {
      name: "treatments",
      label: "PRODUCTO",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) =>
          value.map((d, index) => (
            <div
              className={`${
                index % 2 === 0 ? "subcol" : ""
              } column has-no-border py-0 pr-0 p-2`}
            >
              <div
                className="columns is-mobile my-0"
                style={{ height: "100%", background: "" }}
              >
                <div className="column">{d.product}</div>
              </div>
            </div>
          )),
      },
    },
    {
      name: "treatments",
      label: "GESTIÓN",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) =>
          value.map((d, index) => (
            <div
              className={`${
                index % 2 === 0 ? "subcol" : ""
              } column has-no-border py-0 pr-0 p-2`}
            >
              <div
                className="columns is-mobile my-0"
                style={{ height: "100%", background: "" }}
              >
                <div className="column">{d.therapy_start_date}</div>
              </div>
            </div>
          )),
      },
    },
    {
      name: "treatments",
      label: "PRÓX. CONTACTO",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) =>
          value.map((d, index) => (
            <div
              className={`${
                index % 2 === 0 ? "subcol" : ""
              } column has-no-border py-0 pr-0 p-2`}
            >
              <div
                className="columns is-mobile my-0"
                style={{ height: "100%", background: "" }}
              >
                <div className="column">{d.date_next_call}</div>
              </div>
            </div>
          )),
      },
    },
    {
      name: "treatments",
      label: "RESPONSABLE",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) =>
          value.map((d, index) => (
            <div
              className={`${
                index % 2 === 0 ? "subcol" : ""
              } column has-no-border py-0 pr-0 p-2`}
            >
              <div
                className="columns is-mobile my-0"
                style={{ height: "100%", background: "" }}
              >
                <div className="column">{d.user}</div>
                <div
                  className={`column is-1 p-0 py-2 has-text-centered ${"has-background-hblue"}`}
                  style={{ minWidth: "30px" }}
                >
                  <figure class="image is-16x16" style={{ left: "5px" }}>
                    <Switch>
                      <Case condition={d.patient.country === "Peru"}>
                        <img src={IMGPeru} alt="img_peru_icon" />
                      </Case>
                      <Case condition={d.patient.country === "Ecuador"}>
                        <img src={IMGEcuador} alt="img_ecuador_icon" />
                      </Case>
                      <Default>
                        <img src={IMGColombia} alt="img_colombia_icon" />
                      </Default>
                    </Switch>
                  </figure>
                  <br />
                  <Switch>
                    <Case condition={d.patient.country === "Peru"}>
                      <span className="has-text-white">P</span>
                    </Case>
                    <Case condition={d.patient.country === "Ecuador"}>
                      <span className="has-text-white">E</span>
                    </Case>
                    <Default>
                      <span className="has-text-white">C</span>
                    </Default>
                  </Switch>
                </div>
              </div>
            </div>
          )),
      },
    },
    ...(canTrackingListEdit
      ? [
          {
            name: "treatments",
            label: "ACCIONES",
            options: {
              filter: true,
              sort: true,
              customBodyRender: (value) =>
                value.map((d, index) => (
                  <When condition={canTrackingListEdit}>
                    <div
                      className={`${
                        index % 2 === 0 ? "subcol" : ""
                      } column has-no-border py-0 pr-0 p-2`}
                      style={{ width: "100%" }}
                    >
                      <div
                        className="columns is-mobile my-0"
                        style={{ height: "100%", background: "" }}
                      >
                        <div className="row">
                          <div className="col ml-3">
                            <Link
                              className="btn"
                              to={`tracking/update/${d.id}`}
                            >
                              <i class="fas fa-edit"></i>
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </When>
                )),
            },
          },
        ]
      : []),
  ];

  return (
    <div className="columns is-mobile">
      <div
        className="column is-1 is-hidden-tablet has-text-centered"
        style={{ minWidth: "10px" }}
      ></div>
      <div className="column">
        <div className="columns is-mobile">
          <div
            className="column is-1 is-hidden-tablet has-text-centered"
            style={{ minWidth: "10px" }}
          ></div>
          <div className="column">
            <DataTable
              data={data}
              columns={columns}
              title={"Listado de seguimiento"}
              color="#6992D6"
            />
          </div>
          <div
            className="column is-1 is-hidden-mobile"
            style={{ maxWidth: "10px" }}
          ></div>
        </div>
      </div>
      <div
        className="column is-1 is-hidden-mobile"
        style={{ maxWidth: "10px" }}
      ></div>
    </div>
  );
};

export default Table;
