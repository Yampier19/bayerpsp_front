import { When } from "react-if";
import { Unless } from "react-if";

import LoadingComponent from "components/base/loading-component";
import Table from "./table";
import { useListAll } from "services/bayer/psp/tracking/useTracking";
import { useAuthorizationAction } from "providers/authorization/action/authorizationActionProvider";
import { AuthorizationActionComponent } from "providers/authorization/action/authorizationactionComponent";

import "./main.scss";

const Tracking = () => {
  const { data: Patients, isLoading, refetch } = useListAll();
  const { canTrackingListView } = useAuthorizationAction();
  return (
    <AuthorizationActionComponent
      redirect={"/home"}
      isAuthorized={canTrackingListView}
      children={
        <div
          className="is-flex is-flex-direction-column "
          style={{ height: "100%" }}
        >
          {/* header */}
          <section className="pb-3 mt-5" style={{ marginBottom: 50 }}>
            <h1 className="title has-text-hblue">Seguimiento</h1>
          </section>

          {/* DataTable */}
          <When condition={!isLoading}>
            <section
              className="is-flex-grow-1"
              style={{ minHeight: "500px", overflowY: "hidden" }}
            >
              <div
                className="coolscroll hblue"
                style={{ overflowX: "scroll", height: "100%", width: "100%" }}
              >
                <Table data={Patients?.data} refetch={refetch} />
              </div>
            </section>
          </When>
          <Unless condition={!isLoading}>
            <LoadingComponent loadingText="Cargando datos" />
          </Unless>
        </div>
      }
    />
  );
};

export default Tracking;
