export const dependencies_initState = {
    loading: false,
    success: false,
    error: false,
    patients: []
};

export const depReducer = (state, action) => {
    switch (action.type) {
        case 'req_status':
            return{...state, loading: action.loading, success: action.success, error: action.error};
        case 'set_patients': return{...state, patients: action.payload};
        default: return{...state}
    }
}
