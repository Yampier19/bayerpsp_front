import { Link, useParams } from "react-router-dom";

import { tabClicked } from "components/commons/tabs/tabClicked";
import DataTable from "components/base/datatable";
const Table = (props) => {
  const columns = [
    {
      name: "diagnostico",
      label: "ID DIAGNOSTICO",
      options: { filter: true, sort: true },
    },
    { name: "pap", label: "PAP", options: { filter: true, sort: true } },
    {
      name: "TERAPIA",
      label: "TERAPIA",
      options: { filter: true, sort: true },
    },
    {
      name: "EXAMENES",
      label: "EXAMENES CANTIDAD",
      options: { filter: true, sort: true },
    },
    {
      name: "VAUCHER",
      label: "NÚMERO VAUCHER",
      options: { filter: true, sort: true },
    },
    {
      name: "centro",
      label: "CENTRO MÉDICO",
      options: { filter: true, sort: true },
    },
    {
      name: "ARCHIVO",
      label: "ARCHIVO",
      options: { filter: true, sort: true },
    },
    {
      name: "GESTIONAR",
      label: "GESTIONAR",
      options: {
        customBodyRender: () => {
          return (
            <Link
              to="/tracking/update/apoyoD/edit"
              className="d-flex justify-content-center"
            >
              <i class="far fa-edit fa-lg"></i>
            </Link>
          );
        },
      },
    },
  ];

  const data = [
    {
      diagnostico: "0001",
      pap: "0001",
      TERAPIA: "XARELTO",
      EXAMENES: "02",
      VAUCHER: "doctor",
      centro: "bayer",
      ARCHIVO: "bogota",
      GESTIONAR: "04/11/2021",
    },
    {
      remision: "0001",
      referencia: "0001",
      producto: "XARELTO",
      cantidad: "02",
      responsable: "doctor",
      bodega: "bayer",
      ubicacion: "bogota",
      ingreso: "04/11/2021",
      proveedor: "dental",
      img_producto: "Sin imagen",
      observaciones: "Ninguno",
    },
    {
      remision: "0001",
      referencia: "0001",
      producto: "XARELTO",
      cantidad: "02",
      responsable: "doctor",
      bodega: "bayer",
      ubicacion: "bogota",
      ingreso: "04/11/2021",
      proveedor: "dental",
      img_producto: "Sin imagen",
      observaciones: "Ninguno",
    },
    {
      remision: "0001",
      referencia: "0001",
      producto: "XARELTO",
      cantidad: "02",
      responsable: "doctor",
      bodega: "bayer",
      ubicacion: "bogota",
      ingreso: "04/11/2021",
      proveedor: "dental",
      img_producto: "Sin imagen",
      observaciones: "Ninguno",
    },
    {
      remision: "0001",
      referencia: "0001",
      producto: "XARELTO",
      cantidad: "02",
      responsable: "doctor",
      bodega: "bayer",
      ubicacion: "bogota",
      ingreso: "04/11/2021",
      proveedor: "dental",
      img_producto: "Sin imagen",
      observaciones: "Ninguno",
    },
  ];

  return (
    <div className="columns is-mobile">
      <div
        className="column is-1 is-hidden-tablet has-text-centered"
        style={{ minWidth: "10px" }}
      ></div>
      <div className="column">
        <div className="columns is-mobile">
          <div
            className="column is-1 is-hidden-tablet has-text-centered"
            style={{ minWidth: "10px" }}
          ></div>
          <div className="column">
            <DataTable
              data={data}
              columns={columns}
              title={"Listado de ingresos"}
            />
          </div>
          <div
            className="column is-1 is-hidden-mobile"
            style={{ maxWidth: "10px" }}
          ></div>
        </div>
      </div>
      <div
        className="column is-1 is-hidden-mobile"
        style={{ maxWidth: "10px" }}
      ></div>
    </div>
  );
};

export default Table;
