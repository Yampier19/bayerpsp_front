import {useState, useEffect, useReducer} from 'react';

import ProgressIcon from 'components/base/progress-icon';
import ProgressBar from 'components/base/progress-bar';

import {serverURL} from 'secrets';
import {Link, useParams} from 'react-router-dom';

import {tabClicked} from 'components/commons/tabs/tabClicked';

import FormEdit from './formedit';

import {dependencies_initState, depReducer} from './../depReducer';
import LoadingComponent from 'components/base/loading-component';
import Dependencies from './../dependencies';


import styles from './../trackingUpdate.scss';

const TrackingUpdatePatient = props => {

    {/* 
    const [dependencies, dispatchDep] = useReducer(depReducer, dependencies_initState);
    */}
    const [progress, setProgress] = useState(0);
    const [currentTab, setCurrentTab] = useState(1);

{/*
    const patientId = useParams().id;
    const [currentTreatment, setCurrentTreatment] = useState( 0 );
    
    const onTreatmentBtnClicked = e => {
        const target = e.currentTarget;
        const currentActive = document.querySelector('.treatment-btn.is-active');
        currentActive.classList.toggle('is-active');
        target.classList.toggle('is-active');
    }

*/}

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>
                {/*
                    <Dependencies dependencies={dependencies} dispatchDep={dispatchDep} inputData={{patientId}}/>
                */}

                {/* header */}
                <section className="section p-0">
                    <h1 className="subtitle is-3 mb-3"><strong className="has-text-hblue">SEGUIMIENTO</strong></h1>
                    <h2 className="subtitle has-text-hblue">EDITAR</h2>
                </section>

                <hr/>

                {/* tabs */}
                <section className="section p-0 m-0">
                    <div className="columns is-vcentered">

                        <div className="column">
                            <div className="tabs is-boxed trackingTabs">
                                <ul>
                                    <li className={`formTab is-active ${styles.tabs}`} onClick={e => {tabClicked(e); setCurrentTab(1)}} data-tab="tab1">
                                        <a >
                                            <ProgressIcon className="has-background-hblue has-text-white" value={progress} icon={<i className="fas fa-check"></i>}></ProgressIcon> &nbsp;
                                            Paciente
                                        </a>
                                    </li>
                                    <li className={`formTab ${styles.tabs}`} onClick={e => {tabClicked(e); setCurrentTab(2)}} data-tab="tab2">
                                        <a>
                                            <ProgressIcon className="has-background-hblue has-text-white" value={progress} icon={<i className="fas fa-pills"></i>}></ProgressIcon> &nbsp;
                                            Tratamiento
                                        </a>
                                    </li>
                                    <li className={`formTab ${styles.tabs}`} onClick={e => {tabClicked(e); setCurrentTab(3)}}  data-tab="tab3">
                                        <a>
                                            <ProgressIcon className="has-background-hblue has-text-white" value={progress} icon={<i className="far fa-comment-dots"></i>}></ProgressIcon> &nbsp;
                                            Comunicaciones
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="column is-3">

                            <ProgressBar
                                className="has-background-hblue"
                                value={progress}
                            ></ProgressBar>
                        </div>
                    </div>
                </section>

                <section className="my-5">
                    <Link to="./../index" className="button is-hblue has-text-white is-rounded">Historico reclamaciones</Link>&nbsp;
                    <Link to="./../index" className="button is-hblue has-text-white is-rounded">Mis pedidos</Link>&nbsp;

                    {/* ESTE BOTON SE DEBE HABILITAR SOLO CUANDO ESCOGAN EL TRATAMIENTO XOFIGO */}
                    <Link to="./../index" className="button is-hblue has-text-white is-rounded" >Apoyo Diagnostico</Link>&nbsp;

                     {/* ESTE BOTON SE DEBE HABILITAR SOLO CUANDO ESCOGAN EL TRATAMIENTO XOFIGO */}
                     <Link to="./../index" className="button is-hblue has-text-white is-rounded"  >Gestión Apoyo Diagnostico</Link>&nbsp;

                </section>

                <section  style={{overflow: 'scroll'}}>
                    <ul className="is-inline-flex">
                        {/*
                            !dependencies.loading && dependencies.success ?
                        dependencies.paciente.tratamientos.map((t,i) =>  */}
                                    <li className={'mr-6 has-text-light2 treatment-btn'}  >
                                        <a > Prueba&nbsp;</a>
                                    </li>
                           {/*     )
                            : null
                        } */}
                    </ul>
                </section>



                {/* forms */}
                <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                    {/*
                        dependencies.loading ?
                            <LoadingComponent loadingText="Cargando datos..."/>
                        :
                    dependencies.success ?  */}
                            <>
                                <div  style={{height: '100%'}}>
                                    <div className="coolscroll hblue" style={{height: '100%', overflow: 'auto'}}>
                                     <FormEdit />
                                    </div>
                                </div>
                            </>
                   {/*     :
                        dependencies.error ?
                            <div>Error cargando las dependencias del formulario...</div>
                        :
                            null

                    }
                */}


                </section>


                 
                     



        </div>
    );
}

export default TrackingUpdatePatient;










