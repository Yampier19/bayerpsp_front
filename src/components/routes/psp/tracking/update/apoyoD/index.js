import React from "react";

import FormField2 from "components/form/formfield2";

import CheckForm from "./checkForm";
const Form = (props) => {
  return (
    <form id="form" className="pr-3 py-3">
      <FormField2
        number1="1"
        number2="1.1"
        name1="fechaActivacion"
        name2="solicitudCambioEstadoPaciente"
        label1="PAP"
        label2="Terapia"
        input1={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="fechaActivacion"
            />
          </div>
        }
        input2={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="fechaActivacion"
            />
          </div>
        }
      />

      <CheckForm />

      <FormField2
        number1="3"
        number2="3.1"
        name1="fechaActivacion"
        name2="solicitudCambioEstadoPaciente"
        label1="Cantidad de examenes"
        label2="Número de voucher"
        input1={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="fechaActivacion"
            />
          </div>
        }
        input2={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="fechaActivacion"
            />
          </div>
        }
      />

      <FormField2
        number1="4"
        number2="4.1"
        name1="fechaActivacion"
        name2="solicitudCambioEstadoPaciente"
        label1="Centro médico diagnostico"
        label2="Gestión"
        input1={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="fechaActivacion"
            />
          </div>
        }
        input2={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="fechaActivacion"
            />
          </div>
        }
      />
    </form>
  );
};

export default Form;
