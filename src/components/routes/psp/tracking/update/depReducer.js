export const dependencies_initState = {
    loading: false,
    success: false,
    error: false,
    paciente: {
        tratamientos: []
    }
};

export const depReducer = (state, action) => {
    switch (action.type) {
        case 'req_status':
            return{...state, loading: action.loading, success: action.success, error: action.error};
        case 'set_paciente': return{...state, paciente: action.payload};
        default: return{...state}
    }
}
