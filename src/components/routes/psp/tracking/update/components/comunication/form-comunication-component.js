import React from "react";
import { Fragment } from "react";
import { Formik } from "formik";
import { Form } from "formik";

import FormFields from "./components/form-fields";

import { useTrackingContext } from "../../steps/trackingSteps/tracking-step-provider";
import useFormComunicationUtil from "./components/form-comunication-util";
import FormComunication from "./components/form-comunication";

const FormComunicationComponent = (props) => {
  const { comunications } = useTrackingContext();
  const { initialValues, validationSchema, submitForm } =
    useFormComunicationUtil({});
  return (
    <Fragment>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={submitForm}
        validateOnMount={true}
      >
        <Form>
          <FormFields />
          {/* <FormButton loading={loading} /> */}
        </Form>
      </Formik>
      {comunications.map((d) => (
        <FormComunication comunication={d} />
      ))}
    </Fragment>
  );
};

export default FormComunicationComponent;
