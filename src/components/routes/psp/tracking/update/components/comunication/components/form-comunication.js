import React from "react";
import { Formik } from "formik";
import { Form } from "formik";

import FormFields from "./form-fields";

import useFormComunicationUtil from "./form-comunication-util";

const FormComunication = ({ comunication }) => {
  const { initialValues, validationSchema, submitForm } =
    useFormComunicationUtil(comunication);
  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={submitForm}
      validateOnMount={true}
    >
      <Form>
        <FormFields />
        {/* <FormButton loading={loading} /> */}
      </Form>
    </Formik>
  );
};

export default FormComunication;
