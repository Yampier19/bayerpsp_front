import * as Yup from "yup";
import { useState } from "react";
import { useQueryClient } from "react-query";

import { useTrackingContext } from "../../../steps/trackingSteps/tracking-step-provider";

import { ToastUtil } from "components/base/toast/toast-util";
import { Toast } from "components/base/toast/ToastContainer";

const useComunicationFormUtil = (props) => {
  const [loading, setLoading] = useState(false);
  const queryClient = useQueryClient();
  const { comunications } = useTrackingContext();

  const initialValues = {
    asd: props.asd || "",
    description: props.description || "",
    next_contact: props.next_contact || "",
    autor: props.autor || "",
    last_collection: props.last_collection || "",
    next_collection: props.next_collection || "",
    start_paap: props.start_paap || "",
    end_paap: props.end_paap || "",
    code_argus: props.code_argus || "",
  };

  const requiredFieldType = "Este campo es obligatorio";

  const validationSchema = Yup.object().shape({
    asd: Yup.string(),
    description: Yup.string().required(requiredFieldType),
    next_contact: Yup.string().required(requiredFieldType),
    autor: Yup.string().required(requiredFieldType),
    last_collection: Yup.string().required(requiredFieldType),
    next_collection: Yup.string().required(requiredFieldType),
    start_paap: Yup.string().required(requiredFieldType),
    end_paap: Yup.string().required(requiredFieldType),
    code_argus: Yup.string().required(requiredFieldType),
  });

  const submitForm = (values, { resetForm }) => {
    console.log("valies", values);
  };

  return { initialValues, validationSchema, submitForm };
};

export default useComunicationFormUtil;
