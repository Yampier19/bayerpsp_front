import React from "react";
import Date from "components/form/date";
import { Field } from "formik";
import { useFormikContext } from "formik";

const FormFields = () => {
  const { submitForm } = useFormikContext();
  return (
    <div>
      <div className="columns mx-0">
        <div className="row py-5">
          <div className="col d-flex align-items-center">
            <Field name="asd">
              {({ field }) => <Date {...field} placeholder="DD-MM-AAAA" />}
            </Field>
          </div>
          <div className="col d-flex align-items-center">
            <span
              class="fa-stack fa-lg"
              style={{ cursor: "pointer" }}
              onClick={submitForm}
            >
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-edit fa-stack-1x fa-inverse"></i>
            </span>
          </div>
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <Field name="description">
            {({ field }) => (
              <textarea
                {...field}
                className="textarea areacool"
                placeholder="Descripción"
              ></textarea>
            )}
          </Field>
        </div>
        <div className="column">
          <Field name="next_contact">
            {({ field }) => <Date {...field} placeholder="Próximo contacto" />}
          </Field>
          <br />
          <br />
          <Field name="autor">
            {({ field }) => (
              <input
                {...field}
                className="input cool-input"
                type="text"
                placeholder="Autor"
              />
            )}
          </Field>
        </div>
        <div className="column">
          <Field name="last_collection">
            {({ field }) => (
              <Date {...field} placeholder="Ultima recolección" />
            )}
          </Field>
          <br />
          <br />
          <Field name="next_collection">
            {({ field }) => (
              <Date {...field} placeholder="Proxima recolección" />
            )}
          </Field>
        </div>
        <div className="column">
          <Field name="start_paap">
            {({ field }) => <Date {...field} placeholder="Inicio PAAP" />}
          </Field>
          <br />
          <br />
          <Field name="end_paap">
            {({ field }) => <Date {...field} placeholder="Fin PAAP" />}
          </Field>
        </div>
        <div className="column">
          <Field name="code_argus">
            {({ field }) => (
              <input
                className="input cool-input"
                type="text"
                placeholder="CÓDIGO ARGUS"
                {...field}
              />
            )}
          </Field>
          <br />
          <br />
          <button className="button is-hblue">
            <i class="fas fa-eye mr-2"></i> VER ADJUNTO
          </button>
        </div>
      </div>
    </div>
  );
};

export default FormFields;
