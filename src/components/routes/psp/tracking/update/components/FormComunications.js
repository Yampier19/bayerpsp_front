import Date from "components/form/date";

const FormComunication = (props) => (
  <div>
    <div className="columns mx-0">
      <div className="row py-5">
        <div className="col d-flex align-items-center">
          <Date name="asd" onChange={null} placeholder="DD-MM-AAAA" />
        </div>
        <div className="col d-flex align-items-center">
          <span class="fa-stack fa-lg" style={{ cursor: "pointer" }}>
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class="fa fa-edit fa-stack-1x fa-inverse"></i>
          </span>
        </div>
      </div>
    </div>
    <div className="columns">
      <div className="column">
        <textarea
          className="textarea areacool"
          placeholder="Descripción"
        ></textarea>
      </div>
      <div className="column">
        <Date name="asd" onChange={null} placeholder="Próximo contacto" />
        <br />
        <br />
        <input className="input cool-input" type="text" placeholder="Autor" />
      </div>
      <div className="column">
        <Date name="asd" onChange={null} placeholder="Ultima recolección" />
        <br />
        <br />
        <Date name="asd" onChange={null} placeholder="Proxima recolección" />
      </div>
      <div className="column">
        <Date name="asd" onChange={null} placeholder="Inicio PAAP" />
        <br />
        <br />
        <Date name="asd" onChange={null} placeholder="Fin PAAP" />
      </div>
      <div className="column">
        <input
          className="input cool-input"
          type="text"
          placeholder="CÓDIGO ARGUS"
        />
        <br />
        <br />
        <a href="javascript:;" className="button is-hblue">
          <i class="fas fa-eye mr-2"></i> VER ADJUNTO
        </a>
      </div>
    </div>

    <div className="columns mx-3" style={{ marginTop: "15%" }}>
      <div className="column">
        <nav aria-label="Page navigation example">
          <ul class="pagination justify-content-end">
            <li class="page-item disabled">
              <a class="page-link" href="#" tabindex="-1">
                Previous
              </a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">
                1
              </a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">
                2
              </a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">
                3
              </a>
            </li>
            <li class="page-item">
              <a class="page-link" href="#">
                Next
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
);

export default FormComunication;
