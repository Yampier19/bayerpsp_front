import { useState } from "react";
import { useFormikContext } from "formik";

import { useTrackingContext } from "../../steps/trackingSteps/tracking-step-provider";

import { useListClaims } from "services/bayer/psp/patient/usePatient";
import { useNoEducationReason } from "services/bayer/psp/patient/usePatient";
import { useEducationThemes } from "services/bayer/psp/patient/usePatient";
import { useListLogisticOperatorsByEnsurance } from "services/bayer/psp/patient/usePatient";
import { useListInsuranceByDepartmentId } from "services/bayer/psp/patient/usePatient";
import { useListAllDoctors } from "services/bayer/psp/patient/usePatient";
import { usePreviusTreatments } from "services/bayer/psp/patient/usePatient";
import { useDose } from "services/bayer/psp/patient/usePatient";
import { useListCitiesByDepartment } from "services/bayer/psp/cities/useCities";
import { useListProductByMedicalProduct } from "services/bayer/psp/product/useProduct";

export const useTreatmentForm = () => {
  const { values } = useFormikContext();
  const { product, treatment } = useTrackingContext();
  const [causeNoReclamation, setCauseNoReclamation] = useState([]);
  const [educationNoReason, setEducationNoReason] = useState([]);
  const [educationTheme, setEducationThemes] = useState([]);
  const [logisticOperator, setLogisticOperators] = useState([]);
  const [insurrance, setInsurrance] = useState([]);
  const [listDoctors, setListDoctors] = useState([]);
  const [cities, setCities] = useState([]);
  const [previusTreatment, setPreviousTreatment] = useState([]);
  const [listDose, setListDose] = useState([]);
  const [productsByMedicalProducts, setProductByMedicalProducts] = useState({
    response: false,
    data: [],
  });

  const queryConfig = {
    refetchOnWindowFocus: false,
    refetchOnmount: false,
    refetchOnReconnect: true,
    retry: false,
  };

  const { isLoading: isLoadingClaims } = useListClaims({
    onSuccess: (data) => {
      setCauseNoReclamation(data.data);
    },
    onError: (err) => {
      console.log("Here on error");
    },
    ...queryConfig,
  });
  const { isLoading: isLoadingNoEducationReason } = useNoEducationReason({
    onSuccess: (data) => {
      setEducationNoReason(data.data);
    },
    onError: (err) => {
      console.log("Here on error");
    },
    ...queryConfig,
  });
  const { isLoading: isLoadingEducationThemes } = useEducationThemes({
    onSuccess: (data) => {
      setEducationThemes(data.data.map((d) => ({ id: d.id, name: d.name })));
    },
    onError: (err) => {
      console.log("Here on error");
    },
    ...queryConfig,
  });

  const { isLoading: isLoadingListAllDoctors } = useListAllDoctors({
    onSuccess: (data) => {
      setListDoctors(data.data);
    },
    onError: (err) => {
      console.log("Here on error");
    },
    ...queryConfig,
  });

  const { isLoading: isLoadingPreviusTreatments } = usePreviusTreatments({
    onSuccess: (data) => {
      setPreviousTreatment(data.data);
    },
    onError: (err) => {
      console.log("Here on error");
    },
    ...queryConfig,
  });

  const { isLoading: isLoadingDose } = useDose(product.id, {
    onSuccess: (data) => {
      setListDose(data.data.map((d) => ({ id: d.id, name: d.dose })));
    },
    onError: (err) => {
      console.log("Here on error");
    },
    ...queryConfig,
  });

  const { isLoading: isLoadingInsurance } = useListInsuranceByDepartmentId(
    treatment.department_id,
    {
      onSuccess: (data) => {
        setInsurrance(data.data.map((d) => ({ id: d.id, name: d.name })));
      },
      onError: (err) => {
        console.log("Here on error");
      },
      ...queryConfig,
    }
  );

  const { isLoading: isLoadingCities } = useListCitiesByDepartment(
    treatment.department_id,
    {
      onSuccess: (data) => {
        setCities(data.data);
      },
      onError: (err) => {
        console.log("Here on error");
      },
      ...queryConfig,
    }
  );

  const { isLoading: isLoadingOperator } = useListLogisticOperatorsByEnsurance(
    values.insurance,
    {
      onSuccess: (data) => {
        setLogisticOperators(data.data);
      },
      onError: (err) => {
        console.log("Here on error");
      },
      ...queryConfig,
    }
  );

  const { isLoading: isLoadingProductsByMedicalProducts } =
    useListProductByMedicalProduct(product.id, {
      onSuccess: (data) => {
        setProductByMedicalProducts({
          response: data.response,
          data: data.data || [],
        });
      },
      onError: (err) => {
        console.log("Here on error");
      },
      ...queryConfig,
    });

  return {
    causeNoReclamation,
    isLoadingClaims,
    educationNoReason,
    isLoadingNoEducationReason,
    educationTheme,
    isLoadingEducationThemes,
    listDoctors,
    isLoadingListAllDoctors,
    previusTreatment,
    isLoadingPreviusTreatments,
    listDose,
    isLoadingDose,
    insurrance,
    isLoadingInsurance,
    cities,
    isLoadingCities,
    logisticOperator,
    isLoadingOperator,
    productsByMedicalProducts,
    isLoadingProductsByMedicalProducts,
  };
};
