import React from "react";
import _ from "lodash";
import { useState } from "react";
import { useEffect } from "react";
import { Switch } from "react-if";
import { Case } from "react-if";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import { FieldArray } from "formik";
import { useFormikContext } from "formik";

import LoadingComponent from "components/base/loading-component";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "75%",
  bgcolor: "background.paper",
  borderRadius: 10,
  boxShadow: 24,
  p: 4,
};

/* ESTE MODAL ES PARA EL TIPO DE ENVIO EN SEGUIMIENTO */

const ModalTypeShipping = ({ data, isLoading }) => {
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [products, setProducts] = useState([]);
  const { values, setFieldValue } = useFormikContext();

  useEffect(() => {
    setProducts(values.product_send);
  }, [values.product_send]);

  const checkboxChange = (e, product) => {
    const { checked } = e.target;
    if (checked) {
      setProducts((prevState) => [...prevState, product]);
    } else {
      setProducts((prevState) => prevState.filter((d) => d !== product));
    }
  };

  const handleSubmitModal = () => {
    setFieldValue("product_send", products);
    handleClose();
  };

  return (
    <div>
      <button type="button" onClick={handleOpen}>
        <i class="fas fa-plus-circle fa-2x my-3"></i>
      </button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <button onClick={handleClose}>
            <i
              class="fas fa-times fa-2x text-white"
              style={{ position: "absolute", marginLeft: "100%", top: "-20%" }}
            ></i>
          </button>
          <div className="row my-5">
            <div className="col">
              <label style={{ color: "#6992D6", fontWeight: "bold" }}>
                PRODUCTOS PARA ENVIAR
              </label>
            </div>
          </div>
          <Switch>
            <Case condition={isLoading}>
              <LoadingComponent loadingText="Cargando datos..." />
            </Case>
            <Case condition={_.isEmpty(data)}>
              <div>No hay datos para mostrar</div>
            </Case>
            <Case condition={!_.isEmpty(data)}>
              <div className="row-reverse my-5">
                <div className="col">
                  <table class="table table-bordered">
                    <thead style={{ backgroundColor: "#6992D6", opacity: 0.8 }}>
                      <tr>
                        <th
                          scope="col"
                          className="text-white p-4"
                          style={{ borderRadius: 20 }}
                        >
                          NOMBRE DEL MATERIAL
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <FieldArray
                        name="product_send"
                        render={(arrayHelpers) =>
                          data.map((it, index) => (
                            <tr key={`product_by_medical_product_${index}`}>
                              <td>
                                <input
                                  className="mr-5"
                                  checked={products.find((d) => d.id === it.id)}
                                  key={it.id}
                                  name={it.id}
                                  onChange={(e) => checkboxChange(e, it)}
                                  type="checkbox"
                                />
                                {it.supplier}
                              </td>
                            </tr>
                          ))
                        }
                      />
                    </tbody>
                  </table>
                </div>
                <div className="col d-flex justify-content-end">
                  <button
                    type="button"
                    className="btn text-white"
                    style={{ backgroundColor: "#6992D6" }}
                    onClick={handleSubmitModal}
                  >
                    CONTINUAR
                  </button>
                </div>
              </div>
            </Case>
          </Switch>
        </Box>
      </Modal>
    </div>
  );
};

export default ModalTypeShipping;
