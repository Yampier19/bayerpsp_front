import * as Yup from "yup";
import { useState } from "react";
import { useQueryClient } from "react-query";
//import { serialize } from "object-to-formdata";

import { useTrackingContext } from "../../../steps/trackingSteps/tracking-step-provider";
import { capitalizeFirstLetter } from "utils/capitalizeFirstLetter";

import { useEditTreatmentById } from "services/bayer/psp/treatment/useTreatment";
import { ToastUtil } from "components/base/toast/toast-util";
import { Toast } from "components/base/toast/ToastContainer";

const useTreatmentFormUtil = () => {
  const { treatment, user, setCurrentStep } = useTrackingContext();
  const { mutateAsync: editTreatment } = useEditTreatmentById();
  const [loading, setLoading] = useState(false);
  const queryClient = useQueryClient();

  const initialValues = {
    initial_visit_schedule:
      treatment.initial_visit_schedule || "" /* Programacion Visita Inicial */,
    effective_initial_visit:
      capitalizeFirstLetter(treatment.effective_initial_visit) ||
      "" /* visita inicial efectiva */,
    date_visit_started:
      treatment.date_visit_started || "" /* Fecha Visita Inicial */,
    cause_no_visit: treatment.cause_no_visit || "" /* causa no Visita */,
    formulation_date: treatment.formulation_date || "" /* Fecha formulacion */,
    claim: capitalizeFirstLetter(treatment.claim) || "" /* Reclamo */,
    claim_date: treatment.claim_date || "" /* Fecha reclamacion */,
    cause_no_claim:
      treatment.cause_no_claim?.id || "" /* Causa no reclamacion */,
    active_for_change:
      treatment.active_for_change || "" /* Active para cambio */,
    education:
      capitalizeFirstLetter(treatment.education) || "" /* Se brindo eduacion */,
    education_theme: treatment.education_theme?.id || "" /* Educacion tema */,
    education_date:
      treatment.education_date || "" /* Fecha reclamacion eduacion */,
    education_reason:
      treatment.education_reason?.id || "" /* Motivo No educacion */,
    number_box: treatment.number_box || "" /* Numero de cajas */,
    add_application_information:
      treatment.add_application_information ||
      "" /* Agregar informacion de aplicacion */,
    medication_date: treatment.medication_date || "" /* medicamento hasta */,
    comunication:
      capitalizeFirstLetter(treatment.comunication) ||
      "" /* se logro comunicacion */,
    reason_communication:
      treatment.reason_communication || "" /* motivo de la comunicacion */,
    reason_not_communication:
      treatment.reason_not_communication ||
      "" /* motivo de la no comunicacion */,
    number_attemps: treatment.number_attemps || "" /* numero de intentos */,
    call_type: treatment.call_type || "" /* Tipo llamada */,
    contact_medium: treatment.contact_medium || "" /* Medio de contacto */,
    insurance: treatment.insurance?.id || "" /* Asegurador */,
    logistic_operator:
      treatment.logistic_operator?.id || "" /* Operador logistico */,
    doctor_id: treatment.doctor_id || "" /* Medico */,
    ips: treatment.ips || "" /* Ips */,
    delivery_point: treatment.delivery_point || "" /* Punto entrega */,
    city: treatment.city?.id || "" /* Ciudad entrega */,
    authorization_number:
      treatment.authorization_number || "" /* Numero de autorizacion */,
    doctor_formulator:
      treatment.doctor_formulator || "" /* Doctor formulador */,
    difficulty_access:
      capitalizeFirstLetter(treatment.difficulty_access) ||
      "" /* Dificultad de acceso */,
    type_difficulty: treatment.type_difficulty || "" /* Tipo de dificultad */,
    autor: treatment.user || "",
    generate_request:
      capitalizeFirstLetter(treatment.generate_request) ||
      "" /* Genera solicitud */,
    adverse_event:
      capitalizeFirstLetter(treatment.adverse_event) || "" /* Evento adverso */,
    type_adverse_event: treatment.type_adverse_event || "",
    date_next_call: treatment.date_next_call || "" /* Fecha proxima llamada */,
    reason_next_call:
      treatment.reason_next_call || "" /* Motivo proxima llamada */,
    observation_next_call:
      treatment.observation_next_call || "" /* Observaciones proxima llamada */,
    consecutive: treatment.consecutive || "" /* Consecutivo */,
    generate_requirement:
      capitalizeFirstLetter(treatment.generate_requirement) ||
      "" /* Genera requerimiento */,
    require_support: treatment.require_support || "" /* Requiere apoyo */,
    transfer_type: treatment.transfer_type || "" /* Tipo de Transferencia */,
    previous_treatment:
      treatment.previous_treatment?.id || "" /* Tratamiento Previo */,
    product: treatment.product || "" /* Producto */,
    product_dose_id: treatment.product_dose?.id || "" /* Dosis producto */,
    Shipping_request:
      capitalizeFirstLetter(treatment.Shipping_request) ||
      "" /* solicitud Envio */,
    product_send: treatment.product_send || [],
    communication_description:
      treatment.communication_description ||
      "" /* Descipcion de comunicacion */,
    note: treatment.note || "" /* Notas */,
    archivo: treatment.archivo || "",
  };

  const requiredFieldType = "Este campo es obligatorio";

  const validationSchema = Yup.object().shape({
    initial_visit_schedule: Yup.string(),
    effective_initial_visit: Yup.string().required(requiredFieldType),
    date_visit_started: Yup.string().when("effective_initial_visit", {
      is: (value) => value === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    cause_no_visit: Yup.string().when("effective_initial_visit", {
      is: (value) => value === "No",
      then: Yup.string().required(requiredFieldType),
    }),
    formulation_date: Yup.string(),
    claim: Yup.string().required(requiredFieldType),
    claim_date: Yup.string().when("claim", {
      is: (value) => value === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    cause_no_claim: Yup.string().when(requiredFieldType, {
      is: (value) => value === "No",
      then: Yup.string().required(requiredFieldType),
    }),
    active_for_change: Yup.string(),
    education: Yup.string().required(requiredFieldType),
    education_theme: Yup.string().when("education", {
      is: (value) => value === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    education_date: Yup.string().when("seBrinEducacion", {
      is: (value) => value === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    education_reason: Yup.string().when("seBrindoEduacion", {
      is: (value) => value === "No",
      then: Yup.string().required(requiredFieldType),
    }),
    number_box: Yup.string(),
    add_application_information: Yup.string(),
    medication_date: Yup.string().required(requiredFieldType),
    comunication: Yup.string().required(requiredFieldType),
    reason_communication: Yup.string().when("comunication", {
      is: (value) => value === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    reason_not_communication: Yup.string().when("comunication", {
      is: (value) => value === "No",
      then: Yup.string().required(requiredFieldType),
    }),
    number_attemps: Yup.number()
      .typeError("se debe especificar un valor numerico")
      .required(requiredFieldType),
    call_type: Yup.string().required(requiredFieldType),
    contact_medium: Yup.string().required(requiredFieldType),
    insurance: Yup.string().required(requiredFieldType),
    logistic_operator: Yup.string().when("insurance", {
      is: (value) => value !== "",
      then: Yup.string().required(requiredFieldType),
    }),
    doctor_id: Yup.string().required(requiredFieldType),
    ips: Yup.string().required(requiredFieldType),
    delivery_point: Yup.string(),
    city: Yup.string().required(requiredFieldType),
    authorization_number: Yup.number()
      .typeError("se debe especificar un valor numerico")
      .required(requiredFieldType),
    doctor_formulator: Yup.string(),
    difficulty_access: Yup.string(),
    type_difficulty: Yup.string(),
    autor: Yup.string(),
    generate_request: Yup.string().required(requiredFieldType),
    adverse_event: Yup.string().required(requiredFieldType),
    type_adverse_event: Yup.string().when("adverse_event", {
      is: (value) => value === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    date_next_call: Yup.string().required(requiredFieldType),
    reason_next_call: Yup.string().required(requiredFieldType),
    observation_next_call: Yup.string(),
    consecutive: Yup.string(),
    generate_requirement: Yup.string().required(requiredFieldType),
    require_support: Yup.string().when("generate_requirement", {
      is: (value) => value === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    transfer_type: Yup.string().when("generate_requirement", {
      is: (value) => value === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    previous_treatment: Yup.string(),
    product: Yup.string().required(requiredFieldType),
    product_dose_id: Yup.string().required(requiredFieldType),
    Shipping_request: Yup.string(),
    product_send: Yup.array().when("Shipping_request", {
      is: (value) => value === "Si",
      then: Yup.array()
        .of(
          Yup.object().shape({
            referral_number: Yup.string(),
            id: Yup.string(),
            supplie_expenses_data: Yup.string(),
          })
        )
        .required(requiredFieldType),
    }),
    communication_description: Yup.string(),
    note: Yup.string(),
    archivo: Yup.mixed(),
  });

  const submitForm = (values, { resetForm }) => {
    if (values.archivo && values.archivo.size / 1024 > 5000) {
      return;
    }
    /* const data = serialize({ treatment: { patient_id: user.id, ...values } }); */
    const data = { treatment: { patient_id: user.id, ...values } };
    setLoading(true);
    editTreatment({
      id: treatment.id,
      data,
    })
      .then((res) => {
        if (res.code === 200 && res.response === true) {
          const message = ToastUtil(res.message, "success");
          Toast(message);
          setCurrentStep(2);
          resetForm();
          queryClient.removeQueries("treatment/listOne");
        } else {
          if (res.message) {
            const message = ToastUtil(res.message, "error");
            Toast(message);
          } else {
            const message = ToastUtil(res.error, "error");
            Toast(message);
          }
        }
      })
      .catch((error) => {
        console.log(error, "error");
        const message = ToastUtil(error.message, "error");
        Toast(message);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return { initialValues, validationSchema, submitForm, loading };
};

export default useTreatmentFormUtil;
