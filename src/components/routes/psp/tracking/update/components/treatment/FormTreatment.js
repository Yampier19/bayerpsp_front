import FormField from "components/form/formfield";
import FormField2 from "components/form/formfield2";
import Dropdown from "components/form/dropdown";
import Radio from "components/form/radio";
import ModalTipoEnvio from "components/base/modal-material/modal_tracking";

import "../../formfield.scss";
import Reclamo from "../../reclamo";
const FormTreatment = (props) => {
  return (
    <form id="form" className="pr-3 py-3">
      <div className="field21 mb-6">
        <div className="columns " style={{ minHeight: "100px" }}>
          <div className="column">
            <div className="columns  is-mobile">
              <div className="column is-1" style={{ width: "50px" }}>
                <div className="control">
                  <input
                    className="def_checkbox"
                    type="checkbox"
                    name={`check_${props.name1}`}
                    checked={props.checked1}
                    readOnly
                  />
                  <div
                    className={`newcheck3 d-flex justify-content-center align-items-center ${
                      props.noline ? "no-line-desktop" : ""
                    }`}
                  >
                    <strong className="num">1</strong>
                  </div>
                </div>
              </div>

              <div className="column">
                <div className="columns ">
                  <div className="column is-4">
                    <label className="label">Programación Visita Inicial</label>
                  </div>
                  <div className="column">
                    <div className="control">
                      <input
                        className="input cool-input"
                        type="date"
                        name="medicamentoHasta"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="column mt-2">
            <div className="columns is-vcentered-desktop is-mobile">
              <div
                className="column is-1 is-hidden-tablet"
                style={{ width: "50px" }}
              >
                <div className="control">
                  <input
                    className="def_checkbox"
                    type="checkbox"
                    name={`check_${props.name2}`}
                    checked={props.checked2}
                    readOnly
                  />
                  <div
                    className={`newcheck3 no-line-desktop d-flex justify-content-center align-items-center ${
                      props.noline ? "no-line" : ""
                    }`}
                  >
                    <strong className="num">1.1</strong>
                  </div>
                </div>
              </div>

              <div className="column">
                <div className="columns">
                  <div className="column is-4">
                    <label className="label">Visita Inicial Efectiva*</label>
                  </div>
                  <div className="column">
                    <div className="control">
                      <Radio name="reclamo" />
                    </div>
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <div className="columns">
                      <div className="column is-4 d-flex align-items-center">
                        <label className="label">Fecha Visita Inicial</label>
                      </div>
                      <div className="column">
                        <div className="control">
                          <input
                            className="input cool-input"
                            type="date"
                            name="medicamentoHasta"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <FormField
        label="Fecha de formulación"
        number="2"
        name="medicamentoHasta"
      >
        <input
          className="input cool-input"
          type="date"
          name="medicamentoHasta"
        />
      </FormField>

      <Reclamo
        label="Reclamo*"
        label2="Fecha de reclamación"
        label3="Active para cambio"
        label4="Se brindo educación"
        label5="Tema"
        label6="Fecha de reclamación"
        number="3"
        number2="3.1"
        input1={
          <div>
            <Radio name="seLogroComunicacion" />
          </div>
        }
        input2={
          <div>
            <input
              className="input cool-input"
              type="date"
              name="medicamentoHasta"
            />
          </div>
        }
        input3={
          <div>
            <Radio name="seLogroComunicacion" />
          </div>
        }
        input4={
          <div>
            <Radio name="seLogroComunicacion" />
          </div>
        }
        input5={
          <div>
            <Dropdown
              name="motivoComunicacion"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
          </div>
        }
        input6={
          <div>
            <input
              className="input cool-input"
              type="date"
              name="medicamentoHasta"
            />
          </div>
        }
      />

      <FormField
        label="Número de cajas / unidades"
        number="4"
        name="medicamentoHasta"
      >
        <input
          className="input cool-input"
          type="number"
          name="medicamentoHasta"
        />
      </FormField>

      <FormField label="Medicamento hasta*" number="5" name="medicamentoHasta">
        <input
          className="input cool-input"
          type="text"
          name="medicamentoHasta"
        />
      </FormField>

      <div className="field21 mb-6">
        <div className="columns " style={{ minHeight: "100px" }}>
          <div className="column">
            <div className="columns  is-mobile">
              <div className="column is-1" style={{ width: "50px" }}>
                <div className="control">
                  <input
                    className="def_checkbox"
                    type="checkbox"
                    name={`check_${props.name1}`}
                    checked={props.checked1}
                    readOnly
                  />
                  <div
                    className={`newcheck3 d-flex justify-content-center align-items-center ${
                      props.noline ? "no-line-desktop" : ""
                    }`}
                  >
                    <strong className="num">6</strong>
                  </div>
                </div>
              </div>

              <div className="column">
                <div className="columns ">
                  <div className="column is-4">
                    <label className="label">Se logro la comunicación*</label>
                  </div>
                  <div className="column">
                    <div className="control">
                      <Radio name="seLogroComunicacion" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="column mt-2">
            <div className="columns is-vcentered-desktop is-mobile">
              <div
                className="column is-1 is-hidden-tablet"
                style={{ width: "50px" }}
              >
                <div className="control">
                  <input
                    className="def_checkbox"
                    type="checkbox"
                    name={`check_${props.name2}`}
                    checked={props.checked2}
                    readOnly
                  />
                  <div
                    className={`newcheck3 no-line-desktop d-flex justify-content-center align-items-center ${
                      props.noline ? "no-line" : ""
                    }`}
                  >
                    <strong className="num">6.1</strong>
                  </div>
                </div>
              </div>

              <div className="column">
                <div className="columns">
                  <div className="column is-4">
                    <label className="label">Motivo de la comunicación*</label>
                  </div>
                  <div className="column">
                    <div className="control">
                      <Dropdown
                        name="motivoComunicacion"
                        options={["asd"]}
                        arrowColor="has-text-hblue"
                      />
                    </div>
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <div className="columns">
                      <div className="column is-4 d-flex align-items-center">
                        <label className="label">Número de intentos</label>
                      </div>
                      <div className="column">
                        <div className="control">
                          <Dropdown
                            name="motivoComunicacion"
                            options={["asd"]}
                            arrowColor="has-text-hblue"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <FormField2
        number1="7"
        number2="7.1"
        name1="motivoDeNoComunicacion"
        name2="numeroIntentos"
        label1="Tipo de contacto*"
        label2="Medio de contacto*"
        input1={
          <div>
            <Dropdown
              name="motivoDeNoComunicacion"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
          </div>
        }
        input2={
          <div>
            <div className="row ">
              <div className="col">
                <a href="javascript:;" className="btn has-background-hblue">
                  <i class="fas fa-envelope"></i>
                </a>
              </div>
              <div className="col">
                <a href="javascript:;" className="btn has-background-hblue">
                  <i class="fas fa-phone-volume"></i>
                </a>
              </div>
              <div className="col">
                <a href="javascript:;" className="btn has-background-hblue">
                  <i class="fas fa-user"></i>
                </a>
              </div>
            </div>
          </div>
        }
      />

      <FormField2
        number1="8"
        number2="8.1"
        name1="asegurador"
        name2="ipsQueAtiende"
        label1="Asegurador*"
        label2="Operador logistico*"
        input1={
          <div>
            <Dropdown
              name="asegurador"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
          </div>
        }
        input2={
          <div>
            <Dropdown
              name="operadorLogistico"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
          </div>
        }
      />

      <FormField2
        number1="9"
        number2="9.1"
        name1="medico"
        name2="operadorLogistico"
        label1="Médico*"
        label2="IPS que atiende*"
        input1={
          <div>
            <Dropdown
              name="medico"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
          </div>
        }
        input2={
          <div>
            <Dropdown
              name="ipsQueAtiende"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
          </div>
        }
      />

      <FormField2
        number1="10"
        number2="10.1"
        name1="puntoEntrega"
        name2="ciudadEntrega"
        label1="Punto de entrega"
        label2="Ciudad de entrega*"
        input1={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="puntoEntrega"
            />
          </div>
        }
        input2={
          <div>
            <Dropdown
              name="ciudadEntrega"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
          </div>
        }
      />

      <FormField
        label="Número de autorización*"
        number="11"
        name="numeroAutorizacion"
      >
        <Dropdown
          name="numeroAutorizacion"
          options={["asd"]}
          arrowColor="has-text-hblue"
        />
      </FormField>

      <div className="field21 mb-6">
        <div className="columns " style={{ minHeight: "100px" }}>
          <div className="column">
            <div className="columns  is-mobile">
              <div className="column is-1" style={{ width: "50px" }}>
                <div className="control">
                  <input
                    className="def_checkbox"
                    type="checkbox"
                    name={`check_${props.name1}`}
                    checked={props.checked1}
                    readOnly
                  />
                  <div
                    className={`newcheck3 d-flex justify-content-center align-items-center ${
                      props.noline ? "no-line-desktop" : ""
                    }`}
                  >
                    <strong className="num">12</strong>
                  </div>
                </div>
              </div>

              <div className="column">
                <div className="columns ">
                  <div className="column is-4">
                    <label className="label">Médico Formulador</label>
                  </div>
                  <div className="column">
                    <div className="control">
                      <Dropdown
                        name="motivoComunicacion"
                        options={["asd"]}
                        arrowColor="has-text-hblue"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="column mt-2">
            <div className="columns is-vcentered-desktop is-mobile">
              <div
                className="column is-1 is-hidden-tablet"
                style={{ width: "50px" }}
              >
                <div className="control">
                  <input
                    className="def_checkbox"
                    type="checkbox"
                    name={`check_${props.name2}`}
                    checked={props.checked2}
                    readOnly
                  />
                  <div
                    className={`newcheck3 no-line-desktop d-flex justify-content-center align-items-center ${
                      props.noline ? "no-line" : ""
                    }`}
                  >
                    <strong className="num">12.1</strong>
                  </div>
                </div>
              </div>

              <div className="column">
                <div className="columns">
                  <div className="column is-4">
                    <label className="label">Dificultad en el acceso</label>
                  </div>
                  <div className="column">
                    <div className="control">
                      <Radio name="seLogroComunicacion" />
                    </div>
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <div className="columns">
                      <div className="column is-4 d-flex align-items-center">
                        <label className="label">Tipo de dificultad</label>
                      </div>
                      <div className="column">
                        <div className="control">
                          <input
                            className="input cool-input"
                            type="text"
                            name="tipoDificultad"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <FormField2
        number1="13"
        number2="13.1"
        name1="autor"
        name2="generaSolicitud"
        label1="Autor"
        label2="Genera solicitud*"
        input1={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="autor"
              value="Autor"
              style={{ fontWeight: "bold" }}
              readOnly
            />
          </div>
        }
        input2={
          <div>
            <Radio name="generaSolicitud" />
          </div>
        }
      />

      <FormField2
        number1="14"
        number2="14.1"
        name1="autor"
        name2="generaSolicitud"
        label1="Evento Adverso*"
        label2="Tipo de evento adverso"
        input1={
          <div>
            <Radio name="generaSolicitud" />
          </div>
        }
        input2={
          <div>
            <Dropdown
              name="motivoComunicacion"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
          </div>
        }
      />

      <FormField2
        number1="15"
        number2="15.1"
        name1="autor"
        name2="generaSolicitud"
        label1="Fecha de la
                Próxima llamada*"
        label2="Motivo de la
                Próxima llamada*"
        input1={
          <div>
            <input
              className="input cool-input"
              type="date"
              name="medicamentoHasta"
            />
          </div>
        }
        input2={
          <div>
            <Dropdown
              name="motivoComunicacion"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
          </div>
        }
      />

      <FormField
        label="Observaciones próxima llamada"
        number="16"
        name="numeroAutorizacion"
      >
        <input className="input cool-input" type="text" name="autor" />
      </FormField>

      <FormField label="Consecutivo" number="17" name="numeroAutorizacion">
        <input className="input cool-input" type="text" name="autor" />
      </FormField>

      <div className="field21 mb-6">
        <div className="columns " style={{ minHeight: "100px" }}>
          <div className="column">
            <div className="columns  is-mobile">
              <div className="column is-1" style={{ width: "50px" }}>
                <div className="control">
                  <input
                    className="def_checkbox"
                    type="checkbox"
                    name={`check_${props.name1}`}
                    checked={props.checked1}
                    readOnly
                  />
                  <div
                    className={`newcheck3 d-flex justify-content-center align-items-center ${
                      props.noline ? "no-line-desktop" : ""
                    }`}
                  >
                    <strong className="num">18</strong>
                  </div>
                </div>
              </div>

              <div className="column">
                <div className="columns">
                  <div className="column">
                    <div className="columns">
                      <div className="column is-4 d-flex align-items-center">
                        <label className="label">
                          Paciente hace parte del PAAP
                        </label>
                      </div>
                      <div className="column">
                        <div className="control">
                          <Radio name="generaSolicitud" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <div className="columns">
                      <div className="column is-4 d-flex align-items-center">
                        <label className="label">Requiere apoyo del PAAP</label>
                      </div>
                      <div className="column">
                        <div className="control">
                          <Dropdown
                            name="motivoComunicacion"
                            options={["asd"]}
                            arrowColor="has-text-hblue"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <div className="columns">
                      <div className="column is-4 d-flex align-items-center">
                        <label className="label">Tipo de transferencia*</label>
                      </div>
                      <div className="column">
                        <div className="control">
                          <input
                            className="input cool-input"
                            type="text"
                            name="autor"
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <FormField
        label="Tratamiento previo"
        number="19"
        name="numeroAutorizacion"
      >
        <Dropdown
          name="motivoComunicacion"
          options={["asd"]}
          arrowColor="has-text-hblue"
        />
      </FormField>

      <FormField2
        number1="20"
        number2="20.1"
        name1="autor"
        name2="generaSolicitud"
        label1="Medicamento"
        label2="Dosis Tratamiento*"
        input1={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="autor"
              value="XOFIGO"
              style={{ fontWeight: "bold" }}
              readOnly
            />
          </div>
        }
        input2={
          <div>
            <Dropdown
              name="motivoComunicacion"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
          </div>
        }
      />

      <FormField2
        number1="21"
        number2="21.1"
        name1="autor"
        name2="generaSolicitud"
        label1="Solicitud de Envio"
        label2="Tipo de envio"
        input1={
          <div>
            <Radio name="generaSolicitud" />
          </div>
        }
        input2={
          <div>
            <Dropdown
              name="motivoComunicacion"
              options={["asd"]}
              arrowColor="has-text-hblue"
            />
            <ModalTipoEnvio />
          </div>
        }
      />

      <FormField
        label="Descripción de Comunicación"
        number="22"
        name="numeroAutorizacion"
      >
        <input className="input cool-input" type="text" name="autor" />
      </FormField>

      <FormField label="Notas" number="23" name="numeroAutorizacion">
        <input className="input cool-input" type="text" name="autor" />
      </FormField>

      <FormField label="Adjuntar archivo" number="24" name="numeroAutorizacion">
        <a href="javascript:;" className="button is-rounded is-primary">
          Seleccionar archivo
        </a>
      </FormField>

      <br />

      <div className="field">
        <div className="control has-text-right has-text-centered-mobile">
          <button
            className="button is-hblue"
            type="submit"
            style={{ width: "150px" }}
          >
            CONTINUAR
          </button>
        </div>
      </div>
    </form>
  );
};

export default FormTreatment;
