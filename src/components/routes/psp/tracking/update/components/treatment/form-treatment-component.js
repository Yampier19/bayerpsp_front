import React from "react";
import { Formik } from "formik";
import { Form } from "formik";

import useFormTreatmentUtil from "./components/form-treatment-util";
import FormFields from "./components/form-fields";
import FormButton from "./components/form-button";

const TreatmentComponent = () => {
  const { initialValues, validationSchema, submitForm, loading } =
    useFormTreatmentUtil();
  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={submitForm}
      validateOnMount={true}
    >
      <Form>
        <FormFields />
        <FormButton loading={loading} />
      </Form>
    </Formik>
  );
};

export default TreatmentComponent;
