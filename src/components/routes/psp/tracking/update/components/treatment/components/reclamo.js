import "../../../formfield.scss";

const Reclamo = (props) => {
  return (
    <div className="field22 mb-6">
      <div className="columns " style={{ minHeight: "100px" }}>
        <div className="column">
          <div className="columns  is-mobile">
            <div className="column is-1" style={{ width: "50px" }}>
              <div className="control">
                <input
                  className="def_checkbox"
                  type="checkbox"
                  name={`check_${props.name}`}
                  checked={props.checked1}
                  readOnly
                />
                <div className={`newcheck ${props.noline ? "no-line" : ""}`}>
                  <strong className="num">{props.number}</strong>
                </div>
              </div>
            </div>
            <div className="row-reverse">
              <div className="column">
                <div className="columns ">
                  <div className="column is-4">
                    <label className="label">{props.label}</label>
                  </div>
                  <div className="column">
                    <div className="control">
                      {props.input1}
                      <p style={{ fontSize: 14, color: "#C1C1C1" }}>
                        El paciente no se encuentra en alguna barrera
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns ">
                  <div className="column is-4">
                    <label className="label">{props.label2}</label>
                  </div>
                  <div className="column">
                    <div className="control">{props.input2}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="column mt-2">
          <div className="columns is-vcentered-desktop is-mobile">
            <div
              className="column is-1 is-hidden-tablet"
              style={{ width: "50px" }}
            >
              <div className="control">
                <input
                  className="def_checkbox"
                  type="checkbox"
                  name={`check_${props.name2}`}
                  checked={props.checked2}
                  readOnly
                />
                <div
                  className={`newcheck4 no-line-desktop d-flex justify-content-center align-items-center ${
                    props.noline ? "no-line" : ""
                  }`}
                >
                  <strong className="num">{props.number2}</strong>
                </div>
              </div>
            </div>
            <div className="column">
              <div className="columns">
                <div className="column is-6">
                  <label className="label">{props.label3}</label>
                </div>
                <div className="column">
                  <div className="control">{props.input3}</div>
                </div>
              </div>
              <div className="columns">
                <div className="column is-6">
                  <label className="label">{props.label4}</label>
                </div>
                <div className="column">
                  <div className="control">{props.input4}</div>
                </div>
              </div>
              <div className="columns">
                <div className="column is-6 d-flex align-items-center">
                  <label className="label">{props.label5}</label>
                </div>
                <div className="column">
                  <div className="control">{props.input5}</div>
                </div>
              </div>
              <div className="columns">
                <div className="column is-6 d-flex align-items-center">
                  <label className="label">{props.label6}</label>
                </div>
                <div className="column">
                  <div className="control">{props.input6}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Reclamo;
