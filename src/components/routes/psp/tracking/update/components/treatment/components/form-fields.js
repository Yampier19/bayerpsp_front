import React from "react";
import { useEffect } from "react";
import { Fragment } from "react";
import { Field } from "formik";
import { useFormikContext } from "formik";

import { useTreatmentForm } from "../useTreatmentForm";
import { useTrackingContext } from "../../../steps/trackingSteps/tracking-step-provider";
import { arrayToStringsName } from "utils/arrayToStringsName";
import FormField from "components/form/formfield";
import FormField2 from "components/form/formfield2";
import FormField3 from "components/form/formfield3";
import Radio from "components/form/radio";
import Dropdown from "components/form/dropdown";
import FieldMultipleInputs from "./field-multiple-inputs";
import Reclamo from "./reclamo";
import ModalTipoEnvio from "./form-modal-type-shipping";

const FormFields = () => {
  const { values, errors } = useFormikContext();
  const { product, setPercetCurrentStep } = useTrackingContext();
  const {
    causeNoReclamation,
    isLoadingClaims,
    educationNoReason,
    isLoadingNoEducationReason,
    educationTheme,
    isLoadingEducationThemes,
    listDoctors,
    isLoadingListAllDoctors,
    previusTreatment,
    isLoadingPreviusTreatments,
    listDose,
    isLoadingDose,
    insurrance,
    isLoadingInsurance,
    cities,
    isLoadingCities,
    logisticOperator,
    isLoadingOperator,
    productsByMedicalProducts,
    isLoadingProductsByMedicalProducts,
  } = useTreatmentForm();

  useEffect(() => {
    if (Object.entries(errors).length > 0) {
      setPercetCurrentStep(((19 - Object.entries(errors).length) * 100) / 19);
    } else {
      if (Object.entries(values).length > 6) {
        setPercetCurrentStep(100);
      } else {
        setPercetCurrentStep(0);
      }
    }
    //eslint-disable-next-line
  }, [values, errors]);
  return (
    <Fragment>
      <FormField3
        number1="1"
        number2="1.1"
        number3="1.2"
        label1="Programacion Visita Inicial"
        label2="Visita Inicial Efectiva*"
        label3={
          values.effective_initial_visit === "Si"
            ? "Fecha Visita Inicial"
            : values.effective_initial_visit === "No"
            ? "Causa No Visitas*"
            : ""
        }
        checked={
          values.effective_initial_visit === "Si" ||
          (values.effective_initial_visit === "No" &&
            values.cause_no_visit !== "")
        }
        input1={
          <Field name="initial_visit_schedule">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input
                  className="input cool-input"
                  type="date"
                  {...field}
                  min={new Date().toISOString().split("T")[0]}
                />
                {touched.initial_visit_schedule &&
                errors.initial_visit_schedule ? (
                  <div className="help is-danger">
                    {errors.initial_visit_schedule}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="effective_initial_visit">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Radio {...field} />
                {touched.effective_initial_visit &&
                errors.effective_initial_visit ? (
                  <div className="help is-danger">
                    {errors.effective_initial_visit}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input3={
          <Fragment>
            {values.effective_initial_visit === "Si" ? (
              <Field name="date_visit_started">
                {({ field, form: { touched, errors } }) => (
                  <div>
                    <input
                      className="input cool-input"
                      type="date"
                      max={new Date().toISOString().split("T")[0]}
                      {...field}
                    />
                    {touched.date_visit_started && errors.date_visit_started ? (
                      <div className="help is-danger">
                        {errors.date_visit_started}
                      </div>
                    ) : null}
                  </div>
                )}
              </Field>
            ) : values.effective_initial_visit === "No" ? (
              <Field name="cause_no_visit">
                {({ field, form: { touched, errors } }) => (
                  <div>
                    <Dropdown
                      {...field}
                      arrowColor="has-text-hblue"
                      options={[
                        "Falta de Contacto",
                        "Paciente sin Acudiente",
                        "Dirección Errada",
                        "Paciente sin tiempo para atender Visita",
                        "Desconfianza",
                      ]}
                    />
                    {touched.cause_no_visit && errors.cause_no_visit ? (
                      <div className="help is-danger">
                        {errors.cause_no_visit}
                      </div>
                    ) : null}
                  </div>
                )}
              </Field>
            ) : null}
          </Fragment>
        }
      />
      <FormField
        label="Fecha de formulación"
        number="2"
        checked={values.formulation_date !== ""}
      >
        <Field name="formulation_date">
          {({ field, form: { touched, errors } }) => (
            <div>
              <input
                className="input cool-input"
                type="date"
                max={new Date().toISOString().split("T")[0]}
                {...field}
              />
              {touched.formulation_date && errors.formulation_date ? (
                <div className="help is-danger">{errors.formulation_date}</div>
              ) : null}
            </div>
          )}
        </Field>
      </FormField>
      <Reclamo
        label="Reclamo*"
        label2={
          values.claim === "Si"
            ? "Fecha de reclamación"
            : values.claim === "No"
            ? "Causal No Reclamacion"
            : null
        }
        label3="Active para cambio"
        label4="Se brindo educación"
        label5={
          values.education === "Si"
            ? "Tema"
            : values.education === "No"
            ? "Motivo"
            : null
        }
        label6={values.education === "Si" && "Fecha de educación"}
        number="3"
        number2="3.1"
        checked1={values.claim !== ""}
        input1={
          <Field name="claim">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Radio {...field} />
                {touched.claim && errors.claim ? (
                  <div className="help is-danger">{errors.claim}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          values.claim === "Si" ? (
            <Field name="claim_date">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <input
                    {...field}
                    className="input cool-input"
                    type="date"
                    max={new Date().toISOString().split("T")[0]}
                  />
                  {touched.claim_date && errors.claim_date ? (
                    <div className="help is-danger">{errors.claim_date}</div>
                  ) : null}
                </div>
              )}
            </Field>
          ) : values.claim === "No" ? (
            <Field name="cause_no_claim">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <Dropdown
                    {...field}
                    arrowColor="has-text-hblue"
                    options={causeNoReclamation}
                    disabled={isLoadingClaims}
                  />
                  {touched.cause_no_claim && errors.cause_no_claim ? (
                    <div className="help is-danger">
                      {errors.cause_no_claim}
                    </div>
                  ) : null}
                </div>
              )}
            </Field>
          ) : null
        }
        input3={
          <Field name="active_for_change">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Radio {...field} />
                {touched.active_for_change && errors.active_for_change ? (
                  <div className="help is-danger">
                    {errors.active_for_change}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input4={
          <Field name="education">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Radio {...field} />
                {touched.education && errors.education ? (
                  <div className="help is-danger">{errors.education}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input5={
          values.education === "Si" ? (
            <Field name="education_theme">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <Dropdown
                    {...field}
                    options={educationTheme}
                    arrowColor="has-text-hblue"
                    disabled={isLoadingEducationThemes}
                  />
                  {touched.education_theme && errors.education_theme ? (
                    <div className="help is-danger">
                      {errors.education_theme}
                    </div>
                  ) : null}
                </div>
              )}
            </Field>
          ) : values.education === "No" ? (
            <Field name="education_reason">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <Dropdown
                    {...field}
                    options={educationNoReason}
                    arrowColor="has-text-hblue"
                    disabled={isLoadingNoEducationReason}
                  />
                  {touched.education_reason && errors.education_reason ? (
                    <div className="help is-danger">
                      {errors.education_reason}
                    </div>
                  ) : null}
                </div>
              )}
            </Field>
          ) : null
        }
        input6={
          values.education === "Si" && (
            <Field name="education_date">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <input
                    {...field}
                    className="input cool-input"
                    type="date"
                    max={new Date().toISOString().split("T")[0]}
                  />
                  {touched.education_date && errors.education_date ? (
                    <div className="help is-danger">
                      {errors.education_date}
                    </div>
                  ) : null}
                </div>
              )}
            </Field>
          )
        }
      />
      <FormField2
        number1="4"
        number2="4.1"
        label1="Número de cajas / unidades"
        label2={
          product.name.includes("Eylia") && "Agregar información de aplicación"
        }
        checked1={values.number_box !== ""}
        input1={
          <Field name="number_box">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input {...field} type="text" className="input cool-input" />
                {touched.number_box && errors.number_box ? (
                  <div className="help is-danger">{errors.number_box}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          product.name.includes("Eylia") && (
            <Field name="add_application_information ">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <input {...field} type="text" className="input cool-input" />
                  {touched.add_application_information &&
                  errors.add_application_information ? (
                    <div className="help is-danger">
                      {errors.add_application_information}
                    </div>
                  ) : null}
                </div>
              )}
            </Field>
          )
        }
      />
      <FormField
        label="Medicamento hasta*"
        number="5"
        checked={values.medication_date !== ""}
      >
        <Field name="medication_date">
          {({ field, form: { touched, errors } }) => (
            <div>
              <input className="input cool-input" type="date" {...field} />
              {touched.medication_date && errors.medication_date ? (
                <div className="help is-danger">{errors.medication_date}</div>
              ) : null}
            </div>
          )}
        </Field>
      </FormField>
      <FormField3
        number1="6"
        number2="6.1"
        number3="6.2"
        checked={
          (values.comunication !== "" && values.reason_communication !== "") ||
          (values.comunication !== "" && values.reason_not_communication !== "")
        }
        label1="Se logro la comunicación*"
        label2={
          values.comunication === "Si"
            ? "Motivo de la comunicación*"
            : values.comunication === "No"
            ? "Motivo de la NO comunicación*"
            : null
        }
        label3="Número de intentos"
        input1={
          <Field name="comunication">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Radio {...field} />
                {touched.comunication && errors.comunication ? (
                  <div className="help is-danger">{errors.comunication}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          values.comunication === "Si" ? (
            <Field name="reason_communication">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <Dropdown
                    {...field}
                    arrowColor="has-text-hblue"
                    options={[
                      "Actualizacion de Datos",
                      "Campana",
                      "Cumpleanos",
                      "Egreso",
                      "Encuesta",
                      "Ingreso",
                      "Reclamacion",
                      "Remision de Caso",
                      "Respuesta de Caso",
                      "Seguimiento",
                      "Solicitud",
                    ]}
                  />
                  {touched.reason_communication &&
                  errors.reason_communication ? (
                    <div className="help is-danger">
                      {errors.reason_communication}
                    </div>
                  ) : null}
                </div>
              )}
            </Field>
          ) : values.comunication === "No" ? (
            <Field name="reason_not_communication">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <Dropdown
                    {...field}
                    arrowColor="has-text-hblue"
                    options={[
                      "Apagado",
                      "No Esta",
                      "No Contesta",
                      "No Vive Ahi",
                      "Numero Equivocado",
                      "Telefono Ocupado",
                      "Telefono Fuera de Servicio",
                      "Otro",
                    ]}
                  />
                  {touched.reason_not_communication &&
                  errors.reason_not_communication ? (
                    <div className="help is-danger">
                      {errors.reason_not_communication}
                    </div>
                  ) : null}
                </div>
              )}
            </Field>
          ) : null
        }
        input3={
          <Field name="number_attemps">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Dropdown
                  {...field}
                  arrowColor="has-text-hblue"
                  options={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]}
                />
                {touched.number_attemps && errors.number_attemps ? (
                  <div className="help is-danger">{errors.number_attemps}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormField2
        number1="7"
        number2="7.1"
        label1="Tipo de llamada*"
        label2="Medio de contacto*"
        checked1={values.call_type !== "" && values.contact_medium !== ""}
        input1={
          <Field name="call_type">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Dropdown
                  {...field}
                  arrowColor="has-text-hblue"
                  options={["Entrada", "Salida"]}
                />
                {touched.call_type && errors.call_type ? (
                  <div className="help is-danger">{errors.call_type}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="contact_medium">
            {({
              field: { name },
              form: { errors, touched, setFieldValue, values },
            }) => (
              <div>
                <div className="row ">
                  <div className="col">
                    <button
                      className={`btn ${
                        values[name] === "Electronico"
                          ? "has-background-hblue"
                          : "has-text-hblue"
                      }`}
                      onClick={() => setFieldValue(name, "Electronico")}
                    >
                      <i class="fas fa-envelope"></i>
                    </button>
                  </div>
                  <div className="col">
                    <button
                      className={`btn ${
                        values[name] === "Telefonico"
                          ? "has-background-hblue"
                          : "has-text-hblue"
                      }`}
                      onClick={() => setFieldValue(name, "Telefonico")}
                    >
                      <i class="fas fa-phone-volume"></i>
                    </button>
                  </div>
                  <div className="col">
                    <button
                      className={`btn ${
                        values[name] === "Visita"
                          ? "has-background-hblue"
                          : "has-text-hblue"
                      }`}
                      onClick={() => setFieldValue(name, "Visita")}
                    >
                      <i class="fas fa-user"></i>
                    </button>
                  </div>
                </div>
                {touched.contact_medium && errors.contact_medium ? (
                  <div className="help is-danger">{errors.contact_medium}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormField2
        number1="8"
        number2="8.1"
        label1="Asegurador*"
        label2="Operador logistico*"
        checked1={values.insurance !== "" && values.logistic_operator !== ""}
        input1={
          <Field name="insurance">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Dropdown
                  {...field}
                  arrowColor="has-text-hblue"
                  options={insurrance}
                  disabled={isLoadingInsurance}
                />
                {touched.insurance && errors.insurance ? (
                  <div className="help is-danger">{errors.insurance}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="logistic_operator">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Dropdown
                  {...field}
                  arrowColor="has-text-hblue"
                  options={logisticOperator}
                  disabled={values.insurance === "" && isLoadingOperator}
                />
                {touched.logistic_operator && errors.logistic_operator ? (
                  <div className="help is-danger">
                    {errors.logistic_operator}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormField2
        number1="9"
        number2="9.1"
        label1="Médico*"
        label2="IPS que atiende*"
        checked1={values.doctor_id !== "" && values.ips !== ""}
        input1={
          <Field name="doctor_id">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Dropdown
                  {...field}
                  arrowColor="has-text-hblue"
                  options={listDoctors}
                  disabled={isLoadingListAllDoctors}
                />
                {touched.doctor_id && errors.doctor_id ? (
                  <div className="help is-danger">{errors.doctor_id}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="ips">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input {...field} type="text" className="input cool-input" />
                {touched.ips && errors.ips ? (
                  <div className="help is-danger">{errors.ips}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormField2
        number1="10"
        number2="10.1"
        label1="Punto de Entrega"
        label2="Ciudad de Entrega*"
        checked1={values.city !== ""}
        input1={
          <Field name="delivery_point">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input {...field} type="text" className="input cool-input" />
                {touched.delivery_point && errors.delivery_point ? (
                  <div className="help is-danger">{errors.delivery_point}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="city">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Dropdown
                  {...field}
                  arrowColor="has-text-hblue"
                  options={cities}
                  disabled={isLoadingCities}
                />
                {touched.city && errors.city ? (
                  <div className="help is-danger">{errors.city}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormField
        label="Número de Autorización*"
        number="11"
        checked={values.authorization_number !== ""}
      >
        <Field name="authorization_number">
          {({ field, form: { touched, errors } }) => (
            <div>
              <Dropdown
                {...field}
                arrowColor="has-text-hblue"
                options={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]}
              />
              {touched.authorization_number && errors.authorization_number ? (
                <div className="help is-danger">
                  {errors.authorization_number}
                </div>
              ) : null}
            </div>
          )}
        </Field>
      </FormField>
      <FormField3
        number1="12"
        number2="12.1"
        number3="12.2"
        checked={
          (values.doctor_formulator !== "" &&
            values.difficulty_access === "No") ||
          (values.doctor_formulator !== "" &&
            values.difficulty_access === "Si" &&
            values.type_difficulty !== "")
        }
        label1="Médico Formulador"
        label2="Dificultad en el acceso"
        label3={values.difficulty_access === "Si" && "Tipo de dificultad"}
        input1={
          <Field name="doctor_formulator">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Dropdown
                  {...field}
                  arrowColor="has-text-hblue"
                  options={["Aprobado", "Pendiente Radicar", "Radicado"]}
                />
                {touched.doctor_formulator && errors.doctor_formulator ? (
                  <div className="help is-danger">
                    {errors.doctor_formulator}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="difficulty_access">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Radio {...field} />
                {touched.difficulty_access && errors.difficulty_access ? (
                  <div className="help is-danger">
                    {errors.difficulty_access}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input3={
          values.difficulty_access === "Si" && (
            <Field name="type_difficulty">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <input className="input cool-input" type="text" {...field} />
                  {touched.type_difficulty && errors.type_difficulty ? (
                    <div className="help is-danger">
                      {errors.type_difficulty}
                    </div>
                  ) : null}
                </div>
              )}
            </Field>
          )
        }
      />
      <FormField2
        number1="13"
        number2="13.1"
        label1="Autor"
        label2="Genera Solicitud*"
        checked1={values.autor !== "" && values.generate_request !== ""}
        input1={
          <Field name="autor">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  {...field}
                  readOnly
                />
                {touched.autor && errors.autor ? (
                  <div className="help is-danger">{errors.autor}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="generate_request">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Radio {...field} />
                {touched.generate_request && errors.generate_request ? (
                  <div className="help is-danger">
                    {errors.generate_request}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormField2
        number1="14"
        number2="14.1"
        label1="Evento Adverso*"
        label2={values.adverse_event === "Si" && "Tipo de evento adverso"}
        checked1={values.adverse_event !== ""}
        input1={
          <Field name="adverse_event">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Radio {...field} />
                {touched.adverse_event && errors.adverse_event ? (
                  <div className="help is-danger">{errors.adverse_event}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          values.adverse_event === "Si" && (
            <Field name="type_adverse_event">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <Dropdown
                    {...field}
                    arrowColor="has-text-hblue"
                    options={[
                      "Farmacovigilancia",
                      "Tecnovigilancia Betaconnet/ Omrron",
                      "Tecnovigilancia I-Neb",
                    ]}
                  />

                  {touched.type_adverse_event && errors.type_adverse_event ? (
                    <div className="help is-danger">
                      {errors.type_adverse_event}
                    </div>
                  ) : null}
                </div>
              )}
            </Field>
          )
        }
      />
      <FormField2
        number1="15"
        number2="15.1"
        label1="Fecha de la Próxima llamada*"
        label2="Motivo de la Próxima llamada*"
        checked1={
          values.date_next_call !== "" && values.reason_next_call !== ""
        }
        input1={
          <Field name="date_next_call">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input
                  className="input cool-input"
                  type="date"
                  {...field}
                  min={new Date().toISOString().split("T")[0]}
                />
                {touched.date_next_call && errors.date_next_call ? (
                  <div className="help is-danger">{errors.date_next_call}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="reason_next_call">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Dropdown
                  {...field}
                  arrowColor="has-text-hblue"
                  options={[
                    "Actualizacion de Datos",
                    "Campanas",
                    "Cumpleano",
                    "Egreso",
                    "Encuestas",
                    "Ingreso",
                    "Reclamacion",
                    "Remision de Caso",
                    "Respuesta de Caso",
                    "Seguimiento",
                  ]}
                />
                {touched.reason_next_call && errors.reason_next_call ? (
                  <div className="help is-danger">
                    {errors.reason_next_call}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormField
        number="16"
        label="Observaciones próxima llamada"
        checked={values.observation_next_call !== ""}
      >
        <Field name="observation_next_call">
          {({ field, form: { touched, errors } }) => (
            <div>
              <input className="input cool-input" type="text" {...field} />
              {touched.observation_next_call && errors.observation_next_call ? (
                <div className="help is-danger">
                  {errors.observation_next_call}
                </div>
              ) : null}
            </div>
          )}
        </Field>
      </FormField>
      <FormField
        number="17"
        label="Consecutivo"
        checked={values.consecutive !== ""}
      >
        <Field name="consecutive">
          {({ field, form: { touched, errors } }) => (
            <div>
              <input className="input cool-input" type="text" {...field} />
              {touched.consecutive && errors.consecutive ? (
                <div className="help is-danger">{errors.consecutive}</div>
              ) : null}
            </div>
          )}
        </Field>
      </FormField>
      <FieldMultipleInputs
        number1="18"
        label1="Genera requerimiento*"
        label2={values.generate_requirement === "Si" && "Requiere apoyo de*"}
        label3={
          values.generate_requirement === "Si" && "Tipo de transferencia*"
        }
        checked={
          values.generate_requirement === "No" ||
          (values.generate_requirement === "Si" &&
            values.require_support !== "" &&
            values.transfer_type !== "")
        }
        input1={
          <Field name="generate_requirement">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Radio {...field} />
                {touched.generate_requirement && errors.generate_requirement ? (
                  <div className="help is-danger">
                    {errors.generate_requirement}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          values.generate_requirement === "Si" && (
            <Field name="require_support">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <Dropdown
                    {...field}
                    arrowColor="has-text-hblue"
                    options={[
                      "Producto sin Valor Comercial",
                      "Apoyo en Psicologia",
                      "Falta de Contacto",
                      "Reentrenamiento en Dipositivos",
                      "Soporte en Dispositivos",
                      "Movilidad",
                      "Apoyo con Reclamacion",
                    ]}
                  />
                  {touched.require_support && errors.require_support ? (
                    <div className="help is-danger">
                      {errors.require_support}
                    </div>
                  ) : null}
                </div>
              )}
            </Field>
          )
        }
        input3={
          values.generate_requirement === "Si" && (
            <Field name="transfer_type">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <input className="input cool-input" type="text" {...field} />
                  {touched.transfer_type && errors.transfer_type ? (
                    <div className="help is-danger">{errors.transfer_type}</div>
                  ) : null}
                </div>
              )}
            </Field>
          )
        }
      />
      <FormField
        number="19"
        label="Tratamiento previo"
        checked={values.previous_treatment !== ""}
      >
        <Field name="previous_treatment">
          {({ field, form: { touched, errors } }) => (
            <div>
              <Dropdown
                {...field}
                arrowColor="has-text-hblue"
                options={previusTreatment}
                disabled={isLoadingPreviusTreatments}
              />
              {touched.previous_treatment && errors.previous_treatment ? (
                <div className="help is-danger">
                  {errors.previous_treatment}
                </div>
              ) : null}
            </div>
          )}
        </Field>
      </FormField>
      <FormField2
        number1="20"
        number2="21"
        label1="Medicamento"
        label2="Dosis Tratamiento*"
        checked1={values.product !== "" && values.product_dose_id !== ""}
        input1={
          <Field name="product">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  readOnly
                  {...field}
                />
                {touched.product && errors.product ? (
                  <div className="help is-danger">{errors.product}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="product_dose_id">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Dropdown
                  {...field}
                  arrowColor="has-text-hblue"
                  options={listDose}
                  disabled={isLoadingDose}
                />
                {touched.product_dose_id && errors.product_dose_id ? (
                  <div className="help is-danger">{errors.product_dose_id}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormField2
        number1="21"
        number2="21.1"
        label1="Solicitud de Envio"
        label2={values.Shipping_request === "Si" && "Tipo de envio"}
        checked1={
          values.Shipping_request === "No" ||
          (values.Shipping_request === "Si" && values.product_send.length > 0)
        }
        input1={
          <Field name="Shipping_request">
            {({ field, form: { touched, errors } }) => (
              <div>
                <Radio {...field} />
                {touched.Shipping_request && errors.Shipping_request ? (
                  <div className="help is-danger">
                    {errors.Shipping_request}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          values.Shipping_request === "Si" && (
            <Field name="product_send">
              {({ field, form: { touched, errors } }) => (
                <div>
                  <input
                    className="input cool-input"
                    readOnly
                    value={arrayToStringsName(values.product_send, "supplier")}
                  />
                  <ModalTipoEnvio
                    response={productsByMedicalProducts.response}
                    data={productsByMedicalProducts.data}
                    isLoading={isLoadingProductsByMedicalProducts}
                  />
                  {touched.product_send && errors.product_send ? (
                    <div className="help is-danger">{errors.product_send}</div>
                  ) : null}
                </div>
              )}
            </Field>
          )
        }
      />
      <FormField
        label="Descripción de Comunicación"
        number="22"
        checked={values.communication_description !== ""}
      >
        <Field name="communication_description">
          {({ field, form: { touched, errors } }) => (
            <div>
              <input className="input cool-input" type="text" {...field} />
              {touched.communication_description &&
              errors.communication_description ? (
                <div className="help is-danger">
                  {errors.communication_description}
                </div>
              ) : null}
            </div>
          )}
        </Field>
      </FormField>
      <FormField label="Notas" number="23" checked={values.note !== ""}>
        <Field name="note">
          {({ field, form: { touched, errors } }) => (
            <div>
              <input className="input cool-input" type="text" {...field} />
              {touched.note && errors.note ? (
                <div className="help is-danger">{errors.note}</div>
              ) : null}
            </div>
          )}
        </Field>
      </FormField>
      <FormField
        label="Archivo"
        number="24"
        checked={values.archivo !== ""}
        noline
      >
        <Field name="archivo">
          {({ field: { name }, form: { setFieldValue } }) => (
            <div>
              <label
                for="input_file_treatment"
                className="button is-rounded is-primary"
              >
                {values.archivo
                  ? "Archivo Seleccionado"
                  : "Seleccionar archivo"}{" "}
              </label>
              <span style={{ verticalAlign: "sub", marginLeft: "10px" }}>
                {values.archivo && values.archivo.name}
              </span>
              <input
                accept=".pdf, .jpg"
                id="input_file_treatment"
                style={{ display: "none" }}
                type="file"
                onChange={(e) => setFieldValue(name, e.currentTarget.files[0])}
              />
              {values.archivo && values.archivo.size / 1024 > 5000 ? (
                <div className="help is-danger">
                  El archivo es demasiado grande
                </div>
              ) : null}
            </div>
          )}
        </Field>
      </FormField>
    </Fragment>
  );
};

export default FormFields;
