import React from "react";

const FieldMultipleInputs = (props) => {
  return (
    <div className="field21 mb-6">
      <div className="columns " style={{ minHeight: "100px" }}>
        <div className="column">
          <div className="columns  is-mobile">
            <div className="column is-1" style={{ width: "50px" }}>
              <div className="control">
                <input
                  className="def_checkbox"
                  type="checkbox"
                  name={`check_${props.name}`}
                  checked={props.checked}
                  readOnly
                />
                <div className={`newcheck ${props.noline ? "no-line" : ""}`}>
                  <strong className="num">{props.number1}</strong>
                </div>
              </div>
            </div>

            <div className="column">
              <div className="columns">
                <div className="column">
                  <div className="columns">
                    <div className="column is-4 d-flex align-items-center">
                      <label className="label">{props.label1}</label>
                    </div>
                    <div className="column">
                      <div className="control">{props.input1}</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <div className="columns">
                    <div className="column is-4 d-flex align-items-center">
                      <label className="label">{props.label2}</label>
                    </div>
                    <div className="column">
                      <div className="control">{props.input2}</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <div className="columns">
                    <div className="column is-4 d-flex align-items-center">
                      <label className="label">{props.label3}</label>
                    </div>
                    <div className="column">
                      <div className="control">{props.input3}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FieldMultipleInputs;
