import React from "react";
import { Fragment } from "react";
import { When } from "react-if";
import { Unless } from "react-if";

import Table from "./components/table";
import LoadingComponent from "components/base/loading-component";
import { useHistoryClaimByTreatment } from "services/bayer/psp/treatment/useTreatment";
import { useTrackingContext } from "../../../steps/trackingSteps/tracking-step-provider";

const HistoryClaimsComponent = () => {
  const { treatment } = useTrackingContext();
  const { data: History, isLoading } = useHistoryClaimByTreatment(treatment.id);
  return (
    <Fragment>
      <When condition={!isLoading}>
        <section
          className="is-flex-grow-1"
          style={{ minHeight: "500px", overflowY: "hidden" }}
        >
          <div
            className="coolscroll hblue"
            style={{ overflowX: "scroll", height: "100%", width: "100%" }}
          >
            <div className="columns is-mobile">
              <div
                className="column is-1 is-hidden-tablet has-text-centered"
                style={{ minWidth: "10px" }}
              ></div>
              <div className="column">
                <div className="columns is-mobile">
                  <div
                    className="column is-1 is-hidden-tablet has-text-centered"
                    style={{ minWidth: "10px" }}
                  ></div>
                  <div className="column">
                    <Table data={History?.data} />
                  </div>
                  <div
                    className="column is-1 is-hidden-mobile"
                    style={{ maxWidth: "10px" }}
                  ></div>
                </div>
              </div>
              <div
                className="column is-1 is-hidden-mobile"
                style={{ maxWidth: "10px" }}
              ></div>
            </div>
          </div>
        </section>
      </When>
      <Unless condition={!isLoading}>
        <LoadingComponent loadingText="Cargando datos" />
      </Unless>
    </Fragment>
  );
};

export default HistoryClaimsComponent;
