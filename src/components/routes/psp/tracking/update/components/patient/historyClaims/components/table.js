import React from "react";
import { format } from "date-fns";
import { es } from "date-fns/locale";
import { capitalizeFirstLetter } from "utils/capitalizeFirstLetter";
import DataTable from "components/base/datatable";

const HistoryClaimsTable = ({ data }) => {
  const columns = [
    {
      name: "created_at",
      label: "MES",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) =>
          capitalizeFirstLetter(
            format(new Date(value.split(",")[0]), "MMMM", { locale: es })
          ),
      },
    },
    {
      name: "claim",
      label: "RECLAMACIÓN",
      options: { filter: true, sort: true },
    },
    {
      name: "claim_date",
      label: "FECHA DE RECLAMACIÓN",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => (value ? value : "No hay dato"),
      },
    },
    {
      name: "cause_no_claim",
      label: "MOTIVO DE NO RECLAMACIÓN",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => (value ? value.name : "No hay dato"),
      },
    },
  ];

  return (
    <DataTable
      data={data}
      columns={columns}
      title={"Historico Reclamaciones"}
      color="#6992D6"
    />
  );
};

export default HistoryClaimsTable;
