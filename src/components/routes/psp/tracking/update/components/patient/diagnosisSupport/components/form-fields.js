import React from "react";
import { useEffect } from "react";
import { Fragment } from "react";
import { Field } from "formik";
import { useFormikContext } from "formik";

import FormField from "components/form/formfield";
import FormField2 from "components/form/formfield2";
import FormCheck from "./check-form";

const FormFields = () => {
  const { values, setFieldValue } = useFormikContext();

  useEffect(() => {
    if (values.exams.length > 0) {
      setFieldValue("quantity_exams", values.exams.length);
    } else {
      setFieldValue("quantity_exams", "");
    }
    //eslint-disable-next-line
  }, [values.exams]);

  return (
    <Fragment>
      <FormField2
        number1="1"
        number2="1.1"
        label1="PAP"
        label2="Terapia"
        checked1={values.PAP !== "" && values.product_name !== ""}
        input1={
          <Field name="PAP">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input
                  {...field}
                  type="text"
                  className="input cool-input"
                  disabled
                />
                {touched.PAP && errors.PAP ? (
                  <div className="help is-danger">{errors.PAP}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="product_name">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input
                  {...field}
                  type="text"
                  className="input cool-input"
                  disabled
                />
                {touched.product_name && errors.product_name ? (
                  <div className="help is-danger">{errors.product_name}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormCheck checked1={values.exams.length > 0} />
      {values.quantity_exams === "" ? (
        <div className="help is-danger">Este campo es obligatorio</div>
      ) : null}
      <FormField2
        number1="3"
        number2="3.1"
        label1="Cantidad de examenes"
        label2="Número de voucher"
        checked1={values.quantity_exams !== "" && values.voucher_number !== ""}
        input1={
          <Field name="quantity_exams">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input
                  {...field}
                  type="text"
                  className="input cool-input"
                  disabled
                />
                {touched.quantity_exams && errors.quantity_exams ? (
                  <div className="help is-danger">{errors.quantity_exams}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="voucher_number">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input {...field} type="text" className="input cool-input" />
                {touched.voucher_number && errors.voucher_number ? (
                  <div className="help is-danger">{errors.voucher_number}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />

      <FormField2
        number1="4"
        number2="4.1"
        label1="Centro médico diagnostico"
        label2="Gestión"
        checked1={
          values.diagnostic_medical_center !== "" && values.management !== ""
        }
        input1={
          <Field name="diagnostic_medical_center">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input {...field} type="text" className="input cool-input" />
                {touched.diagnostic_medical_center &&
                errors.diagnostic_medical_center ? (
                  <div className="help is-danger">
                    {errors.diagnostic_medical_center}
                  </div>
                ) : null}
              </div>
            )}
          </Field>
        }
        input2={
          <Field name="management">
            {({ field, form: { touched, errors } }) => (
              <div>
                <input {...field} type="text" className="input cool-input" />
                {touched.management && errors.management ? (
                  <div className="help is-danger">{errors.management}</div>
                ) : null}
              </div>
            )}
          </Field>
        }
      />
      <FormField
        label="Adjuntar archivo"
        number="5"
        noline
        checked={values.file !== ""}
      >
        <Field name="file">
          {({ field: { name }, form: { touched, errors, setFieldValue } }) => (
            <div>
              <label
                htmlFor="input_file_diagnosis"
                className="button is-rounded is-primary"
              >
                {values.file ? "Archivo Seleccionado" : "Seleccionar archivo"}
              </label>
              <span style={{ verticalAlign: "sub", marginLeft: "10px" }}>
                {values.file && values.file.name}
              </span>
              <input
                accept=".pdf"
                id="input_file_diagnosis"
                style={{ display: "none" }}
                type="file"
                onChange={(e) => setFieldValue(name, e.target.files[0])}
              />
              {touched.file && errors.file ? (
                <div className="help is-danger">{errors.file}</div>
              ) : null}
              {values.file && values.file.size / 1024 > 5000 ? (
                <div className="help is-danger">
                  El archivo es demasiado grande
                </div>
              ) : null}
            </div>
          )}
        </Field>
      </FormField>
    </Fragment>
  );
};

export default FormFields;
