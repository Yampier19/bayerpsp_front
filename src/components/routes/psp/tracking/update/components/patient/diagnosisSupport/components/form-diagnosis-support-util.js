import * as Yup from "yup";
import { useState } from "react";

import { useTrackingContext } from "../../../../steps/trackingSteps/tracking-step-provider";
import { usePatientStepContext } from "../../../../steps/patientSteps/patient-step-provider";
import { useCreateDiagnosticSupport } from "services/bayer/psp/tracking/useTracking";
import { ToastUtil } from "components/base/toast/toast-util";
import { Toast } from "components/base/toast/ToastContainer";
import { ConvertToFormData } from "utils/convertFormData";

const useDiagnosisSupportUtil = () => {
  const { user, product, treatment } = useTrackingContext();
  const { setCurrentPatientStep } = usePatientStepContext();
  const { mutateAsync: createDiagnostic } = useCreateDiagnosticSupport();
  const [loading, setLoading] = useState(false);

  const initialValues = {
    PAP: user.document_number.toString() || "",
    product_name: product.name || "",
    treatment_id: treatment.id || "",
    product_id: product.id || "",
    quantity_exams: "",
    voucher_number: "",
    diagnostic_medical_center: "",
    management: "",
    exams: [],
    file: "",
  };

  const requiredFieldType = "Este campo es obligatorio";

  const validationSchema = Yup.object().shape({
    PAP: Yup.string().required(requiredFieldType),
    product_name: Yup.string().required(requiredFieldType),
    product_id: Yup.string().required(requiredFieldType),
    quantity_exams: Yup.string().required(requiredFieldType),
    voucher_number: Yup.string().required(requiredFieldType),
    diagnostic_medical_center: Yup.string().required(requiredFieldType),
    management: Yup.string().required(requiredFieldType),
    file: Yup.mixed(),
    exams: Yup.array().of(
      Yup.object().shape({
        name: Yup.string(),
        price: Yup.string().matches(
          /^[0-9]+(,[0-9]+)*/,
          "Solo se admiten numeros"
        ),
        order: Yup.number(),
      })
    ),
  });

  const submitForm = (values, { resetForm }) => {
    setLoading(true);
    const data = ConvertToFormData(values);
    createDiagnostic(data)
      .then((res) => {
        if (res.code === 200 && res.response === true) {
          const message = ToastUtil(res.message, "success");
          Toast(message);
          resetForm();
          setCurrentPatientStep(5);
        } else {
          if (res.message) {
            const message = ToastUtil(res.message, "error");
            Toast(message);
          } else {
            const message = ToastUtil(res.error, "error");
            Toast(message);
          }
        }
      })
      .catch((error) => {
        const message = ToastUtil(error.message, "error");
        Toast(message);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return { initialValues, validationSchema, submitForm, loading };
};

export default useDiagnosisSupportUtil;
