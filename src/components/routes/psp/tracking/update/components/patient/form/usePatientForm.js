import React, { useState, useEffect } from "react";
import { useFormikContext } from "formik";

import Dropdown from "components/form/dropdown";
import Radio from "components/form/radio";
import { useTrackingContext } from "../../../steps/trackingSteps/tracking-step-provider";
import {
  useListStatus,
  usePathologicalClassificationByProduct,
} from "services/bayer/psp/patient/usePatient";
import { useListAllDepartements } from "services/bayer/psp/departments/useDepartments";
import { useListCitiesByDepartment } from "services/bayer/psp/cities/useCities";
import { emailTypesList } from "utils/constants/mail.list";
import { DropdownOthers } from "components/form/dropdown-others";
import { getRequired } from "utils/forms";
import { usePatientFormUtil } from "../components/form-patient-util";

export const usePatientForm = () => {
  const { values, setFieldValue, errors } = useFormikContext();
  const { validationSchema } = usePatientFormUtil();
  const [loadingFields, setLoadingFields] = useState({loaded:false, 
    values:[
      { name: 'department_id', realized: false },
      { name: 'patient_statu_id', realized: false },
      { name: 'pathological_classification', realized: false },
    ],
    trigger: false
  });
  const _setLoadingFields = (_name) =>{
    const {values, trigger} = loadingFields;
    const index = values.findIndex(({name})=> name === _name);
    const _value =values [index];
    _value.realized = true;
    const _loaded = values.filter(({realized})=>realized).length === values.length;
    setLoadingFields(prevState => ({loaded: _loaded, values: values, trigger: !trigger}));
  }

  const initialForm = [
    {
      fields: 1,
      names: ["id"],
      labels: ["Código de usuario"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" readOnly {...props} />
        ),
      ],
    },
    {
      fields: 1,
      names: ["patient_statu_id"],
      labels: ["Estado del paciente *"],
      childs: [
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["date_active", "solicitudCambioEstadoPaciente"],
      labels: ["Fecha de activación*", "Solicitar cambio de estado paciente"],
      childs: [
        (props) => (
          <input className="input cool-input" type="date" {...props} />
        ),
        (props) => <Radio {...props} />,
      ],
    },
    {
      fields: 2,
      names: ["fechaRetiro", "motivoRetiro"],
      labels: ["Fecha de retiro", "Motivo de retiro"],
      childs: [
        (props) => (
          <input className="input cool-input" type="date" max={new Date().toISOString().split("T")[0]} {...props} />
        ),
        (props) => (
          <Dropdown
            options={[
              "Cambio de tratamiento",
              "Embarazo",
              "Evento adverso",
              "Falta de contacto",
              "Fuera del pais",
              "Muerte",
              "No interesado",
              "Off label",
              "Orden medica",
              "Otro",
              "Progresion de la enfermedad",
              "Terminacion del tratamiento",
              "Voluntario",
              "Secuenciacion",
              "NO APLICA",
            ]}
            arrowColor="has-text-hblue"
            {...props} 
          />
        ),
      ],
    },
    {
      fields: 1,
      names: ["retiroObs"],
      labels: ["Observaciones motivo de retiro"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["name", "last_name"],
      labels: ["Nombre*", "Apellido*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["document_type", "document_number"],
      labels: ["Tipo de documento*", "Número de identificación*"],
      childs: [
        (props) => (
          <Dropdown options={["CC", "TI", "DNI"]} arrowColor="has-text-primary" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 1,
      names: ["phone"],
      labels: ["Telefono*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["email", "email_type"],
      labels: ["Correo electrónico*", "@"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => <DropdownOthers options={emailTypesList()} {...props} />,
      ],
    },
    {
      fields: 2,
      names: ["department_id", "city_id"],
      labels: ["Departamento*", "Ciudad*"],
      childs: [
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["neighborhood", "address"],
      labels: ["Barrio*", "Dirección*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["date_birth", "age"],
      labels: ["Fecha de nacimiento*", "Edad"],
      childs: [
        (props) => (
          <input
            className="input cool-input"
            type="date"
            // onChange={(e) => {
            //   customHandleChange(e);
            //   formik.setFieldValue("age", getAge(formik.values.date_birth));
            // }}
            {...props}
          />
        ),
        (props) => (
          <input className="input cool-input" type="text" readOnly {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["guardian", "guardian_phone"],
      labels: ["Acudiente", "Telefono acudiente"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["pathological_classification", "therapy_start_date"],
      labels: ["Clasificación patologica*", "Fecha inicio terapia*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="date" {...props} />
        ),
      ],
    },

    // {
    //   fields: 2,
    //   names: [''],
    //   labels: [''],
    //   childs: [
    //   ]
    // },
  ];

  const { product, setPercetCurrentStep } = useTrackingContext();
  const [patientForm, setPatientForm] = useState({
    form: initialForm,
    changed: false,
  });

  /* ~~~****** React-query ********~* */
  const setDropdownOptions = (name = "", options = []) => {
    const { form, changed } = patientForm;
    const index = form.findIndex(({ names }) => names.includes(name));
    if(index === -1) return; 
    const formCopy = form;
    const childIndex = formCopy[index].names.indexOf(name);
    const Child = formCopy[index].childs[childIndex];
    const NewChlid = (otherProps = {}) =>
      React.createElement(Dropdown, {
        ...Child().props,
        disabled: false,
        options: options,
        ...otherProps,
      });
    formCopy[index].childs[childIndex] = NewChlid;
    setPatientForm({ form: formCopy, changed: !changed });
  };
  const setDisabledDropdown = (name = "") => {
    const { form, changed } = patientForm;
    const index = form.findIndex(({ names }) => names.includes(name));
    const formCopy = form;
    const childIndex = formCopy[index].names.indexOf(name);
    const Child = formCopy[index].childs[childIndex];
    const NewChlid = (otherProps = {}) =>
      React.createElement(Dropdown, {
        ...Child().props,
        disabled: true,
        ...otherProps,
      });
    formCopy[index].childs[childIndex] = NewChlid;
    setPatientForm({ form: formCopy, changed: !changed });
  };
  const reFetchQueryConfig = {
    enabled: false,
    refetchOnWindowFocus: false,
    timeout: 1000,
  };
  const queryConfig = {
    refetchOnWindowFocus: false,
    refetchOnmount: false,
    refetchOnReconnect: true,
    retry: false,
  };
  const { refetch: refreshCities } = useListCitiesByDepartment(
    values.department_id,
    reFetchQueryConfig
  );
  const { refetch: refreshPathologicalClassification } =
    usePathologicalClassificationByProduct(product.id, reFetchQueryConfig);
  const {  refetch: getPatientStatus } = useListStatus({
    queryConfig,
  });
  const { refetch: getDepartamets } = useListAllDepartements({
    queryConfig,
  });
  useEffect(() => {
    getDepartamets().then(({ data, isSuccess })=>{
      if (isSuccess && data.code === 200) {
        setDropdownOptions("department_id", data.data);
        _setLoadingFields("department_id")
      }
    })
  }, [])
  /* ~~~****** React-query ********~* */

  /* ~~~****** Form-array-methods ********~* */
  const appendFieldInIndex = (field = {}, index = 0) => {
    console.log('apended in index', index);
    if (index === 0) return;
    const { form, changed } = patientForm;
    const patientFormCopy = form;
    patientFormCopy.splice(index - 1, 0, field);
    setPatientForm({ form: patientFormCopy, changed: !changed });
  };
  const deleteFieldInIndex = (index = 0) => {
    if (index === 0) return;
    const { form, changed } = patientForm;
    const patientFormCopy = form;
    patientFormCopy.splice(index - 1, 1);
    setPatientForm({ form: patientFormCopy, changed: !changed });
  };
  useEffect(() => {
    const { name } = product;
    const productsDictionary = {
      XOFIGO: "Xofigo",
      NEXAVAR: "Nexavar",
      BETAFERON: "Betaferon",
      KOGENATE: "Kogenate",
      EYLIA: "Eylia",
      STIVARGA: "Stivarga",
    };
    if (name) {
      if (name.includes(productsDictionary.XOFIGO)) {
        deleteFieldInIndex(2);
        // deleteFieldInIndex(11);
        getPatientStatus().then(({ data, isSuccess })=>{
          if(isSuccess && data.code ===200){
            const xofigoFields = [
              {
                position: 2,
                field: {
                  fields: 2,
                  names: ["xofigo_code", "patient_statu_id"],
                  labels: ["Código Xofigo", "Estado paciente*"],
                  childs: [
                    (props) => (
                      <input className="input cool-input" type="text" {...props} />
                    ),
                    (props) => (
                      <Dropdown
                        options={data.data}
                        arrowColor="has-text-primary"
                        {...props}
                      />
                    ),
                  ],
                },
              },
              // {
              //   position: 11,
              //   field: {
              //     fields: 1,
              //     names: ["neighborhood"],
              //     labels: ["Barrio"],
              //     childs: [
              //       (props) => (
              //         <input className="input cool-input" type="text" {...props} />
              //       ),
              //     ],
              //   },
              // },
              // {
              //   position:12,
              //   field:{
              //     fields: 1,
              //     names: ['address'],
              //     labels:['Dirección*'],
              //     childs: [
              //       (props) => (
              //         <input className="input cool-input" type="text" {...props} />
              //       ),
              //     ],
              //   }
              // },
              // {
              //   position: 13,
              //   field:{
              //     fields: 2,
              //     names: ['via', 'via_detail'], 
              //     labels: ['Via*', 'Detalles via*'], 
              //     childs:[
              //       (props) =><Dropdown
              //         options={[
              //           "ANILLO VIAL",
              //           "AUTOPISTA",
              //           "AVENIDA",
              //           "BOULEVAR",
              //           "CALLE",
              //           "CALLEJON",
              //           "CARRERA",
              //           "CIRCUNVALAR",
              //           "CONDOMINIO",
              //           "DIAGONAL",
              //           "KILOMETRO",
              //           "LOTE",
              //           "SALIDA",
              //           "SECTOR",
              //           "TRANSVERSAL",
              //           "VEREDA",
              //           "VIA",
              //         ]}
              //         arrowColor="has-text-primary"
              //         {...props}
              //       />,
              //       (props) => <input className="input cool-input" type="text" {...props} />
              //     ]
              //   }
              // },
              // {
              //   position: 14,
              //   field: {
              //     fields: 2,
              //     names: ['number_indicative', 'number_indicative_two'],
              //     labels: ['Numero*', 'Numero secundario*'],
              //     childs: [
              //       (props) => <input className="input cool-input" type="text" {...props} />,
              //       (props) => <input className="input cool-input" type="text" {...props} />
              //     ]
              //   }
              // },
              // {
              //   position: 15,
              //   field: {
              //     fields: 2,
              //     names: ['inside', 'inside_detail'],
              //     labels: ['Interior', 'Detalles interior'],
              //     childs: [
              //       (props) => <Dropdown
              //         options={[
              //           "APARTAMENTO",
              //           "BARRIO",
              //           "BLOQUE",
              //           "CASA",
              //           "CIUDADELA",
              //           "CONJUNTO",
              //           "CONJUNTO RESIDENCIAL",
              //           "EDIFICIO",
              //           "ENTRADA",
              //           "ETAPA",
              //           "INTERIOR",
              //           "MANZANA",
              //           "NORTE",
              //           "OFICINA",
              //           "OCCIDENTE",
              //           "ORIENTE",
              //           "PENTHOUSE",
              //           "PISO",
              //           "PORTERIA",
              //           "SOTANO",
              //           "SUR",
              //           "TORRE",
              //         ]}
              //         arrowColor="has-text-primary"
              //         {...props}
              //       />,
              //       (props) => <input className="input cool-input" type="text" {...props} />
              //     ]
              //   }
              // },
            ];
            xofigoFields.forEach(({ field, position }) => {
              appendFieldInIndex(field, position);
            });
            _setLoadingFields("patient_statu_id")
          }
        })
      }else{
        getPatientStatus().then(({ data, isSuccess }) => {
          if (isSuccess && data.code === 200) {
            setDropdownOptions("patient_statu_id", data.data);
            _setLoadingFields("patient_statu_id")
          }
        })
      }
    }
    
    refreshPathologicalClassification().then(({ data, isSuccess }) => {
      if (isSuccess) {
        const { data: pc, code } = data;
        if (code == 200) {
          setDropdownOptions("pathological_classification", pc);
          _setLoadingFields("pathological_classification")
        };
      }
    }); 
  }, [product]);
  /* ~~~****** Form-array-methods ********~* */

  /* ~~~******* local handlers ********~* */
  const getAge = (dateString) => {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    if (dateString !== null && dateString !== "") {
      return age;
    } else {
      return "";
    }
  };
  useEffect(() => {
      setDisabledDropdown("city_id");
      refreshCities().then(({ data, isSuccess }) => {
        if (isSuccess) {
          const { data: cities, code } = data;
          code == 200 && setDropdownOptions("city_id", cities);
        }
      });
  }, [values.department_id]);
  useEffect(() => {
    setFieldValue("age", getAge(values.date_birth));
  }, [values.date_birth]);
  /* ~~~******* local handlers ********~* */

  useEffect(() => {
    const requiredFields = getRequired(validationSchema);
    const totalRequiredFields = requiredFields.length;
    const totalErrors = Object.entries(errors).length;
    setPercetCurrentStep(
      ((totalRequiredFields - totalErrors) * 100) / totalRequiredFields
    );
  }, [values, errors]);

  return { form: patientForm, isLoading: !loadingFields.loaded};
};
