import React from "react-router-dom";

import DataTable from "components/base/datatable";

const Table = ({ data }) => {
  const columns = [
    {
      name: "id",
      label: "ID DIAGNOSTICO",
      options: { filter: true, sort: true },
    },
    { name: "PAP", label: "PAP", options: { filter: true, sort: true } },
    {
      name: "product",
      label: "TERAPIA",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => value.name,
      },
    },
    {
      name: "quantity_exams",
      label: "EXAMENES CANTIDAD",
      options: { filter: true, sort: true },
    },
    {
      name: "voucher_number",
      label: "NÚMERO VAUCHER",
      options: { filter: true, sort: true },
    },
    {
      name: "diagnostic_medical_center",
      label: "CENTRO MÉDICO",
      options: { filter: true, sort: true },
    },
    {
      name: "file",
      label: "ARCHIVO",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => (
          <div
            className={`column has-no-border py-0 pr-0 p-2`}
            style={{ width: "100%" }}
          >
            <div
              className="columns is-mobile my-0"
              style={{ height: "100%", background: "" }}
            >
              <div className="row">
                <div className="col ml-3">
                  <a
                    href={value}
                    target={"_blank"}
                    className="btn"
                    rel="noreferrer"
                  >
                    <i class="fas fa-file-pdf"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        ),
      },
    },
    {
      name: "management",
      label: "GESTIONAR",
      options: {
        customBodyRender: () => (
          <div
            className={`column has-no-border py-0 pr-0 p-2`}
            style={{ width: "100%" }}
          >
            <div
              className="columns is-mobile my-0"
              style={{ height: "100%", background: "" }}
            >
              <div className="row">
                <div className="col ml-3">
                  <button
                    className="btn"
                    disabled
                    /* to={`tracking/update/${d.id}`} */
                  >
                    <i class="fas fa-edit"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        ),
      },
    },
  ];

  return (
    <DataTable data={data} columns={columns} title={"Listado de ingresos"} />
  );
};

export default Table;
