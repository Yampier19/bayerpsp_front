import React from "react";
import DataTable from "components/base/datatable";

const ProductsTable = (props) => {
  const columns = [
    {
      name: "pap",
      label: "PAP",
      options: { filter: true, sort: true },
    },
    {
      name: "status",
      label: "ESTATUS PACIENTE",
      options: { filter: true, sort: true },
    },
    {
      name: "dosis",
      label: "DOSIS",
      options: { filter: true, sort: true },
    },
    {
      name: "salida",
      label: "DE SALIDA",
      options: { filter: true, sort: true },
    },
    {
      name: "entrega",
      label: "ENTREGA",
      options: { filter: true, sort: true },
    },
    {
      name: "lote",
      label: "No. LOTE",
      options: { filter: true, sort: true },
    },
    {
      name: "vencimiento",
      label: "VENCIMIENTO",
      options: { filter: true, sort: true },
    },
    {
      name: "usuario",
      label: "USUARIO",
      options: { filter: true, sort: true },
    },
    {
      name: "observacion",
      label: "OBSERVACIONES",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => (
          <div
            className="column has-no-border py-0 pr-0 p-2"
            style={{ width: "100%" }}
          >
            <div
              className="columns is-mobile my-0"
              style={{ height: "100%", background: "" }}
            >
              <div className="row">
                <div className="col d-flex justify-content-center align-items-center">
                  <p className="text-center">
                    Lorem ipsum dolor sit amet, consectetur adipiscing
                  </p>
                </div>
                <div
                  className="col-2 py-3"
                  style={{ backgroundColor: "#6992D6" }}
                >
                  <div className="row-reverse ">
                    <div className="col d-flex justify-content-center mt-5 pb-1">
                      <label className="text-white">P</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ),
      },
    },
  ];

  const data = [
    {
      pap: "123456",
      status: "123456",
      dosis: "DOSIS",
      salida: "DD - MM - AAAA",
      entrega: "DD - MM - AAAA",
      lote: "123",
      vencimiento: "DD - MM - AAAA",
      usuario: "USUARIO",
    },
    {
      pap: "123456",
      status: "123456",
      dosis: "DOSIS",
      salida: "DD - MM - AAAA",
      entrega: "DD - MM - AAAA",
      lote: "123",
      vencimiento: "DD - MM - AAAA",
      usuario: "USUARIO",
    },
  ];

  return (
    <section
      className="is-flex-grow-1"
      style={{ minHeight: "500px", overflowY: "hidden" }}
    >
      <div
        className="coolscroll hblue"
        style={{ overflowX: "scroll", height: "100%", width: "100%" }}
      >
        <div className="columns is-mobile">
          <div
            className="column is-1 is-hidden-tablet has-text-centered"
            style={{ minWidth: "10px" }}
          ></div>
          <div className="column">
            <div className="columns is-mobile">
              <div
                className="column is-1 is-hidden-tablet has-text-centered"
                style={{ minWidth: "10px" }}
              ></div>
              <div className="column">
                <DataTable
                  data={data}
                  columns={columns}
                  title={"Medicamentos sin valor comercial"}
                  color="#6992D6"
                />
              </div>
              <div
                className="column is-1 is-hidden-mobile"
                style={{ maxWidth: "10px" }}
              ></div>
            </div>
          </div>
          <div
            className="column is-1 is-hidden-mobile"
            style={{ maxWidth: "10px" }}
          ></div>
        </div>
      </div>
    </section>
  );
};

export default ProductsTable;
