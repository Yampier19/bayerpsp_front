import React from "react";
import _ from "lodash";
import * as Yup from "yup";

import { useTrackingContext } from "../../../steps/trackingSteps/tracking-step-provider";
import { useEditPatient } from "services/bayer/psp/patient/usePatient";
import {  set_modal_on } from "redux/actions/modalActions";
import store from "redux/store";
import { InfoModal } from "components/base/modal-system/models/modal";
import { ToastUtil } from "components/base/toast/toast-util";
import { Toast } from "components/base/toast/ToastContainer";

export const usePatientFormUtil = () => {
  const requiredFieldType = "Campo obligatorio";
  const { user: _user, treatment: _treatment, setCurrentStep } = useTrackingContext();
  const { isLoading: loadingEditPatient, mutateAsync: editPatient } = useEditPatient();
  const user = _user ? _user : {};
  const treatment = _treatment ? _treatment : {};

  if(process.env.NODE_ENV !== "production"){
    console.log("userr->", user, treatment);
  }

  const dateParser = (date = "") => {
    if (!date) return "";
    return date.split(" ")[0];
  };
  const splitEmail = (email = "") => {
    if (!email) return "";
    return email.split("@");
  };
  const lowerCase = (value = "") => {
    if(!value) return "";
    return value.toLocaleLowerCase();
  }
  const parseIntToString = (value = 0) => {
    if(!value) return"";
    return String(value)
  }
  const initialValues = {
    id: user.id || 0,
    patient_statu_id: user.patient_statu_id || "",
    date_active: dateParser(user.date_active) || "",
    name: user.name || "",
    last_name: user.last_name || "",
    document_type: user.document_type || "",
    document_number: user.document_number || "",
    phone: parseIntToString(_.get(_.get(user, ["phones"], [])[0], ["number"], 0)) || "",
    email: splitEmail(user.email)[0] || "",
    department_id: user.department_id || "",
    city_id: user.city_id || "",
    neighborhood: user.neighborhood || "",
    address: user.address || "",
    date_birth: dateParser(user.date_birth) || "",
    age: user.age || 0,
    guardian: user.guardian || "",
    guardian_phone: user.guardian_phone || "",
    pathological_classification: treatment.pathological_classification || "",
    therapy_start_date: dateParser(treatment.therapy_start_date) || "",
    email_type: lowerCase(splitEmail(user.email)[1]) || "",
    xofigo_code: ""
  };
  const validationSchema = Yup.object().shape({
    id: Yup.number().typeError("Se debe especificar un valor numerico"),
    patient_statu_id: Yup.string().required(requiredFieldType),
    department_id: Yup.string().required(requiredFieldType),
    city_id: Yup.string().required(requiredFieldType),
    neighborhood: Yup.string().required(requiredFieldType),
    address: Yup.string().required(requiredFieldType),
    name: Yup.string().required(requiredFieldType),
    last_name: Yup.string().required(requiredFieldType),
    date_active: Yup.string().required(requiredFieldType),
    email: Yup.string().required(requiredFieldType),
    phone: Yup.string().required(requiredFieldType),
    document_number: Yup.string().required(requiredFieldType),
    date_birth: Yup.string().required(requiredFieldType),
    age: Yup.number()
      .integer()
      .min(1, "La edad debe ser mayor a 1")
      .typeError("se debe especificar un valor numerico"),
    guardian: Yup.string(),
    guardian_phone: Yup.number().typeError(
      "se debe especificar un valor numerico"
    ),
    pathological_classification: Yup.string().required(requiredFieldType),
    document_type: Yup.string().required(requiredFieldType),
    email_type: Yup.string().required(requiredFieldType),
    xofigo_code: Yup.string()
  });
  const submitForm = async (valuesOriginal) => {
    const values = Object.assign({}, valuesOriginal);
    values.application_system_id = 1;
    const parseToNumber = [
      "city_id",
      "department_id",
      "document_number",
      "patient_statu_id",
      "guardian_phone",
    ];
    parseToNumber.forEach((value) => {
      if (typeof values[value]  === "number") return null;
      if (
        values[value] &&
        values[value].length > 0 &&
        !isNaN(parseInt(values[value]))
      ) {
        values[value] = parseInt(values[value]);
      } else {
        values[value] = 0;
      }
    });
    const callbackValues = [
      // () => {
      //   values.age = getAge(values.date_birth);
      // },
      () => {
        const { number_indicative, phone, phone_number } = values;
        const phones = [];
        if (number_indicative && phone_number) {
          phones.push(String(number_indicative + " " + phone_number));
        }
        phones.push(String(phone));
        values.phones = phones;
      },
      () => {
        const _email = values.email + '@' + values.email_type;
        values.email = _email;
      }
    ];
    callbackValues.forEach((action) => {
      action();
    });
    const valuesToDelete = [
      "notas",
      "number_indicative",
      "id",
      "phone",
      "number_indicative",
      "phone_number",
      "email_type",
      "therapy_start_date"
    ];
    valuesToDelete.forEach((value) => {
      delete values[value];
    });
    const { id } = valuesOriginal;
    const patient = { id,data:{patient: values} }
    const response = await editPatient(patient).catch((err) => {
      const message = ToastUtil("Error desconocido", "error");
      Toast(message);
      console.log("err--->", err);
      // mostrar errores
    });
    console.log("Edicion del user->", response, values);
    if (response && response.code === 200 && response.response === true) {
      const modalReducer = store.getState().modalReducer;
      const { _icons } = modalReducer;
      const modal = new InfoModal(
        `El usario ha sido editado éxitosamente`,
        _icons.CHECK_PRIMARY
      );
      store.dispatch(set_modal_on(modal));
      setCurrentStep(1);
    } else {
      // mostrar errores
      const message = ToastUtil(response.message || "Error desconocido", "error");
      Toast(message);
    }
  };
  return { submitForm, validationSchema, initialValues, loadingEditPatient };
};
