import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useFormikContext } from "formik";

const FieldCheckInput = ({ nameField, name, arrayHelpers, orderField }) => {
  const [disabledInput, setDisabledInput] = useState(false);
  const { values, setFieldTouched, setErrors, setFieldValue } =
    useFormikContext();
  const [valueInput, setValueInput] = useState("");
  useEffect(() => {
    if (valueInput !== "") {
      if (values.exams.length > 0) {
        if (values.exams.findIndex((d) => d.order === orderField) !== -1) {
          arrayHelpers.replace(
            values.exams.findIndex((d) => d.order === orderField),
            {
              order: orderField,
              name: nameField,
              price: valueInput,
            }
          );
        } else {
          arrayHelpers.push({
            order: orderField,
            name: nameField,
            price: valueInput,
          });
        }
      } else {
        arrayHelpers.push({
          order: orderField,
          name: nameField,
          price: valueInput,
        });
      }
    }
    //eslint-disable-next-line
  }, [valueInput]);

  const handleChange = (e) => {
    if (e.target.checked === true) {
      setFieldTouched(name, true);
      setFieldValue("quantity_exams", "");
    } else {
      setErrors({});
      arrayHelpers.remove(
        values.exams.findIndex((d) => d.order === orderField)
      );
      setFieldTouched(name, false);
      setValueInput("");
    }
    setDisabledInput(e.target.checked);
  };
  return (
    <div className="row">
      <div className="col mb-3">
        <label class="radio">
          <input
            type="checkbox"
            onChange={handleChange}
            value={!disabledInput}
          />
          &nbsp;
        </label>
        &nbsp;&nbsp; {nameField}
      </div>
      <div className="col mb-3">
        <div className="row">
          <div className="col">
            <label>Valor del examen</label>
          </div>
          <div className="col">
            <input
              onChange={(e) => setValueInput(e.target.value)}
              type="text"
              className="input cool-input"
              disabled={disabledInput ? false : true}
              value={valueInput}
            />
            {disabledInput && valueInput === "" && (
              <div className="help is-danger">* Requerido</div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default FieldCheckInput;
