import React from "react";
import { Form, Formik } from "formik";

import { FormFields } from "../components/form-fields";
import { usePatientFormUtil } from "../components/form-patient-util";
import { FormButton } from "../components/form-button";

const FormPatient = () => {
  const {initialValues, validationSchema, submitForm, loadingEditPatient} = usePatientFormUtil()

  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      validationSchema={validationSchema}
      children={
        <Form  >
          <FormFields />
          <FormButton loading={loadingEditPatient} />
        </Form>
      }
      onSubmit={submitForm}
    />
  );
};

export default FormPatient;
