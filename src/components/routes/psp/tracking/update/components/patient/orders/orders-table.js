import React from "react";
import DataTable from "components/base/datatable";

const OrdersTable = (props) => {
  const columns = [
    {
      name: "guia",
      label: "No. DE GUÍA",
      options: { filter: true, sort: true },
    },
    {
      name: "serial",
      label: "SERIAL DE PRODUCTO",
      options: { filter: true, sort: true },
    },
    {
      name: "producto",
      label: "NOMBRE DEL PRODUCTO",
      options: { filter: true, sort: true },
    },
    {
      name: "medicamento",
      label: "NOMBRE MEDICAMENTO",
      options: { filter: true, sort: true },
    },
    {
      name: "cantidad",
      label: "CANTIDAD",
      options: { filter: true, sort: true },
    },
    {
      name: "pap",
      label: "PAP PACIENTE",
      options: { filter: true, sort: true },
    },
    {
      name: "direccion",
      label: "DIRECCIÓN",
      options: { filter: true, sort: true },
    },
    {
      name: "destinario",
      label: "DESTINATARIO",
      options: { filter: true, sort: true },
    },
    {
      name: "nacimiento",
      label: "DE NACIMIENTO",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => (
          <div
            className="column has-no-border py-0 pr-0 p-2"
            style={{ width: "100%" }}
          >
            <div
              className="columns is-mobile my-0"
              style={{ height: "100%", background: "" }}
            >
              <div className="row">
                <div className="col d-flex justify-content-center align-items-center">
                  <p className="text-center">07-12-2021</p>
                </div>
                <div
                  className="col-2 py-3"
                  style={{ backgroundColor: "#6992D6" }}
                >
                  <div className="row-reverse ">
                    <div className="col d-flex justify-content-center mt-5 pb-1">
                      <label className="text-white">P</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ),
      },
    },
  ];

  const data = [
    {
      guia: "123456",
      serial: "123456",
      producto: "NOMBRE DEL PRODUCTO",
      medicamento: "NOMBRE MEDICAMENTO",
      cantidad: "CANTIDAD",
      pap: "PAP PACIENTE",
      direccion: "DIRECCIÓN",
      destinario: "DESTINATARIO",
    },
    {
      guia: "123456",
      serial: "123456",
      producto: "NOMBRE DEL PRODUCTO",
      medicamento: "NOMBRE MEDICAMENTO",
      cantidad: "CANTIDAD",
      pap: "PAP PACIENTE",
      direccion: "DIRECCIÓN",
      destinario: "DESTINATARIO",
    },
  ];

  return (
    <section
      className="is-flex-grow-1"
      style={{ minHeight: "500px", overflowY: "hidden" }}
    >
      <div
        className="coolscroll hblue"
        style={{ overflowX: "scroll", height: "100%", width: "100%" }}
      >
        <div className="columns is-mobile">
          <div
            className="column is-1 is-hidden-tablet has-text-centered"
            style={{ minWidth: "10px" }}
          ></div>
          <div className="column">
            <div className="columns is-mobile">
              <div
                className="column is-1 is-hidden-tablet has-text-centered"
                style={{ minWidth: "10px" }}
              ></div>
              <div className="column">
                <DataTable
                  data={data}
                  columns={columns}
                  title={"Mis Pedidos"}
                  color="#6992D6"
                />
              </div>
              <div
                className="column is-1 is-hidden-mobile"
                style={{ maxWidth: "10px" }}
              ></div>
            </div>
          </div>
          <div
            className="column is-1 is-hidden-mobile"
            style={{ maxWidth: "10px" }}
          ></div>
        </div>
      </div>
    </section>
  );
};

export default OrdersTable;
