import React from "react";
import { Switch } from "react-if";
import { Case } from "react-if";
import _ from "lodash";
import { Field } from "formik";
import { FieldArray } from "formik";

import { useTrackingContext } from "../../../../steps/trackingSteps/tracking-step-provider";
import FieldsData from "../util/fields.json";
import FieldCheck from "./field-check-input";
import "./formfield.scss";

function CheckForm(props) {
  const { product } = useTrackingContext();
  return (
    <div>
      <div className="field23 mb-6">
        <div className="columns " style={{ minHeight: "100px" }}>
          <div className="column">
            <div className="columns  is-mobile">
              <div className="column is-1" style={{ width: "50px" }}>
                <div className="control">
                  <input
                    className="def_checkbox"
                    type="checkbox"
                    name={`check_${props.name1}`}
                    checked={props.checked1}
                    readOnly
                  />
                  <div
                    className={`newcheck d-flex justify-content-center align-items-center ${
                      props.noline ? "no-line-desktop" : ""
                    }`}
                  >
                    <strong className="num">2</strong>
                  </div>
                </div>
              </div>
              <div className="column">
                <div className="columns ">
                  <div className="column is-3">
                    <label className="label">
                      Seleccione los exámenes a solicitar
                    </label>
                  </div>
                  <div className="column">
                    <FieldArray
                      name="exams"
                      render={(arrayHelpers) => (
                        <Switch>
                          <Case condition={_.includes(product.name, "Xofigo")}>
                            {FieldsData.xofigo.map((d, index) => (
                              <Field name={`exams.${index}`}>
                                {({ field }) => (
                                  <FieldCheck
                                    orderField={d.order}
                                    nameField={d.name}
                                    availableInput={d.available}
                                    {...field}
                                    index={index}
                                    arrayHelpers={arrayHelpers}
                                  />
                                )}
                              </Field>
                            ))}
                          </Case>
                          <Case condition={_.includes(product.name, "Eylia")}>
                            {FieldsData.eylia.map((d, index) => (
                              <Field name={`exams.${index}`}>
                                {({ field }) => (
                                  <FieldCheck
                                    orderField={d.order}
                                    nameField={d.name}
                                    availableInput={d.available}
                                    {...field}
                                    index={index}
                                    arrayHelpers={arrayHelpers}
                                  />
                                )}
                              </Field>
                            ))}
                          </Case>
                        </Switch>
                      )}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CheckForm;
