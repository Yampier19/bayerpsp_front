import React from "react";
import { Fragment } from "react";
import { When } from "react-if";
import { Unless } from "react-if";

import Table from "./components/table";
import LoadingComponent from "components/base/loading-component";
import { useTrackingContext } from "../../../steps/trackingSteps/tracking-step-provider";
import { useListDiagnosticByTreatment } from "services/bayer/psp/tracking/useTracking";

const ProcessDiagnosisSupport = () => {
  const { treatment } = useTrackingContext();
  const { data: DiagnosticsSupport, isLoading } = useListDiagnosticByTreatment(
    treatment.id
  );
  return (
    <Fragment>
      <When condition={!isLoading}>
        <div className="columns is-mobile">
          <div
            className="column is-1 is-hidden-tablet has-text-centered"
            style={{ minWidth: "10px" }}
          ></div>
          <div className="column">
            <div className="columns is-mobile">
              <div
                className="column is-1 is-hidden-tablet has-text-centered"
                style={{ minWidth: "10px" }}
              ></div>
              <div className="column">
                <Table data={DiagnosticsSupport?.data} />
              </div>
              <div
                className="column is-1 is-hidden-mobile"
                style={{ maxWidth: "10px" }}
              ></div>
            </div>
          </div>
          <div
            className="column is-1 is-hidden-mobile"
            style={{ maxWidth: "10px" }}
          ></div>
        </div>
      </When>
      <Unless condition={!isLoading}>
        <LoadingComponent loadingText="Cargando datos" />
      </Unless>
    </Fragment>
  );
};

export default ProcessDiagnosisSupport;
