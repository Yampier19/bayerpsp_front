import React from "react";
import { Formik } from "formik";
import { Form } from "formik";

import FormFields from "./components/form-fields";
import FormButton from "./components/form-button";
import useDiagnosisSupportUtil from "./components/form-diagnosis-support-util";

const DiagnosisSupportComponent = () => {
  const { initialValues, validationSchema, submitForm, loading } =
    useDiagnosisSupportUtil();
  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={submitForm}
    >
      <Form style={{ overflowY: "auto", height: "95%", paddingTop: "10px" }}>
        <FormFields />
        <FormButton loading={loading} />
      </Form>
    </Formik>
  );
};

export default DiagnosisSupportComponent;
