import React, { useEffect, useState } from "react";
import { Fragment } from "react";
import { useParams } from "react-router-dom";
import { When } from "react-if";
import { Unless } from "react-if";

import LoadingComponent from "components/base/loading-component";
import FormPatient from "../../components/patient/form/FormPatient";
import { useListOneById } from "services/bayer/psp/treatment/useTreatment";
import { useTrackingContext } from "../trackingSteps/tracking-step-provider";
import { usePatientStepContext } from "./patient-step-provider";

const FormPatientComponent = () => {
  const { setUser, setTreatment, setProduct, setComunications } =
    useTrackingContext();
  const [isLoadingData, setLoading] = useState(true);
  const { setProductEspecial } = usePatientStepContext();
  const { id: patientId } = useParams();
  const queryConfig = {
    refetchOnWindowFocus: true,
    refetchOnmount: false,
    refetchOnReconnect: true,
    retry: false,
  };
  const { refetch: getTreatment } = useListOneById(patientId, {
    queryConfig,
  });
  useEffect(() => {
    getTreatment()
      .then(({ isSuccess, data }) => {
        if (isSuccess && data.code === 200) {
          setUser({ ...data.data.patient });
          setTreatment({ ...data.data });
          setComunications(data.data.treatmentFollowsComunication);
          if (
            data.data.product.includes("Xofigo") ||
            data.data.product.includes("Eylia")
          ) {
            setProductEspecial(true);
          }
          setProduct({ name: data.data.product, id: data.data.product_id });
        }
        setLoading(false);
      })
      .catch((error) => {
        console.log("Error has ocurred", error);
        setLoading(false);
      });
    return () => {
      setLoading(true);
    };
  }, []);
  return (
    <Fragment>
      <When condition={!isLoadingData}>
        <section
          className="section px-0 is-flex-grow-1 py-4 coolscroll"
          style={{ height: "1px", overflow: "hidden", minHeight: "500px" }}
        >
          <div className="tab is-active" style={{ height: "100%" }}>
            <div
              className="coolscroll hblue"
              style={{ height: "100%", overflow: "auto" }}
            >
              <FormPatient />
            </div>
          </div>
        </section>
      </When>
      <Unless condition={!isLoadingData}>
        <LoadingComponent loadingText="Cargando datos del seguimiento" />
      </Unless>
    </Fragment>
  );
};

export default FormPatientComponent;
