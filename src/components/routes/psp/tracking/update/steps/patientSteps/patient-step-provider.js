import React from "react";
import { createContext } from "react";
import { useContext } from "react";
import { useState } from "react";

export const PatientStepContext = createContext();
export const usePatientStepContext = () => useContext(PatientStepContext);

const PatientStepProvider = ({ children }) => {
  const [historyClaims, setHistoryClaims] = useState([]);
  const [currentPatientStep, setCurrentPatientStep] = useState(0);
  const [productEspecial, setProductEspecial] = useState(false);

  const context = {
    currentPatientStep,
    setCurrentPatientStep,
    historyClaims,
    setHistoryClaims,
    productEspecial,
    setProductEspecial,
  };
  return <PatientStepContext.Provider value={context} children={children} />;
};

export default PatientStepProvider;
