import React from "react";
import { Fragment } from "react";
import { When } from "react-if";

import { usePatientStepContext } from "../patient-step-provider";

const Tabs = () => {
  const { currentPatientStep, productEspecial, setCurrentPatientStep } =
    usePatientStepContext();
  return (
    <Fragment>
      <section className="my-5">
        <button
          className={`button is-rounded ${
            currentPatientStep !== 1
              ? "has-text-hblack"
              : "is-hblue has-text-white "
          }`}
          onClick={() => setCurrentPatientStep(1)}
        >
          Historico reclamaciones
        </button>
        &nbsp;
        <button
          className={`button is-rounded ${
            currentPatientStep !== 2
              ? "has-text-hblack"
              : "is-hblue has-text-white "
          }`}
          onClick={() => setCurrentPatientStep(2)}
        >
          Mis pedidos
        </button>
        &nbsp;
        <button
          className={`button is-rounded ${
            currentPatientStep !== 3
              ? "has-text-hblack"
              : "is-hblue has-text-white "
          }`}
          onClick={() => setCurrentPatientStep(3)}
        >
          Medicamentos sin valor comercial
        </button>
        &nbsp;
        <When condition={productEspecial}>
          <button
            className={`button is-rounded ${
              currentPatientStep !== 4
                ? "has-text-hblack"
                : "is-hblue has-text-white "
            }`}
            onClick={() => setCurrentPatientStep(4)}
          >
            Apoyo Diagnostico
          </button>
        </When>
        &nbsp;
        <When condition={productEspecial}>
          <button
            className={`button is-rounded ${
              currentPatientStep !== 5
                ? "has-text-hblack"
                : "is-hblue has-text-white "
            }`}
            onClick={() => setCurrentPatientStep(5)}
          >
            Gestión Apoyo Diagnostico
          </button>
        </When>
        &nbsp;
      </section>
    </Fragment>
  );
};

export default Tabs;
