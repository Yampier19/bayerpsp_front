import React from "react";

import OrdersTable from "../../components/patient/orders/orders-table";

const OrdersComponent = () => {
  return <OrdersTable />;
};

export default OrdersComponent;
