import React from "react";

import ProductsWithoutComercial from "../../components/patient/productsWithoutComercial/products-table";

const OrdersWithoutComercialComponent = () => {
  return <ProductsWithoutComercial />;
};

export default OrdersWithoutComercialComponent;
