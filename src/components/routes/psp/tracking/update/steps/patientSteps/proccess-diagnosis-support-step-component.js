import React from "react";

import ProccessDiagnosisSupport from "../../components/patient/proccessDiagnosisSupport/process-diagnosis-support";

const ProccessDiagnosisSupportComponent = () => {
  return (
    <section
      className="section px-0 is-flex-grow-1 py-4 coolscroll"
      style={{ height: "1px", overflow: "hidden", minHeight: "500px" }}
    >
      <ProccessDiagnosisSupport />
    </section>
  );
};

export default ProccessDiagnosisSupportComponent;
