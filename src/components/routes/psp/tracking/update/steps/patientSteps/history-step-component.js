import React from "react";
import HistoryClaims from "../../components/patient/historyClaims/history-claims-component";

const HistoryClaimsComponent = () => {
  return <HistoryClaims />;
};

export default HistoryClaimsComponent;
