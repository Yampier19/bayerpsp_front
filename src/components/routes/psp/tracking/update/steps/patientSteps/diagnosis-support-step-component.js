import React from "react";

import FormDiagnosisSupportComponet from "../../components/patient/diagnosisSupport/diagnosis-support-component";

const DiagnosisSupportComponent = () => {
  return (
    <section
      className="section px-0 is-flex-grow-1 py-4 coolscroll"
      style={{ height: "1px", overflow: "hidden", minHeight: "500px" }}
    >
      <FormDiagnosisSupportComponet />
    </section>
  );
};

export default DiagnosisSupportComponent;
