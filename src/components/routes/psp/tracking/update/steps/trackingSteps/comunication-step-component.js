import React from "react";

import FormComunicationComponent from "../../components/comunication/form-comunication-component";

const ComunicationComponent = () => {
  return (
    <section
      className="section px-0 is-flex-grow-1 py-4 coolscroll"
      style={{ height: "1px", overflow: "hidden", minHeight: "500px" }}
    >
      <div className="tab is-active" style={{ height: "100%" }}>
        <div
          className="coolscroll hblue"
          style={{ height: "100%", overflow: "auto" }}
        >
          <FormComunicationComponent />
        </div>
      </div>
    </section>
  );
};

export default ComunicationComponent;
