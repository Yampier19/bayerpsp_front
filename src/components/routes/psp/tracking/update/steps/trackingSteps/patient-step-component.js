import React from "react";
import _ from "lodash";
import { Fragment } from "react";
import { When } from "react-if";

import { usePatientStepContext } from "../patientSteps/patient-step-provider";
import { useTrackingContext } from "./tracking-step-provider";

import Tabs from "../patientSteps/components/tabs";
import FormPatientComponent from "../patientSteps/form-patient-step-component";
import HistoryClaimsComponent from "../patientSteps/history-step-component";
import OrdersComponent from "../patientSteps/orders-step-component";
import OrderWithoutComercialComponent from "../patientSteps/orders-whitout-comercial-step-component";
import DiagnosisSupportComponent from "../patientSteps/diagnosis-support-step-component";
import ProcessDiagnosisSupportComponet from "../patientSteps/proccess-diagnosis-support-step-component";

const PatientStepComponent = () => {
  const { currentPatientStep, productEspecial } = usePatientStepContext();
  const { user, treatment } = useTrackingContext();

  const firstStep = <FormPatientComponent />;
  const secondStep = <HistoryClaimsComponent />;
  const thirdStep = <OrdersComponent />;
  const fourthStep = <OrderWithoutComercialComponent />;
  const fiveStep = <DiagnosisSupportComponent />;
  const sixthStep = <ProcessDiagnosisSupportComponet />;

  return (
    <Fragment>
      <When condition={!_.isNull(user) && !_.isNull(treatment)}>
        <Tabs />
      </When>
      {currentPatientStep === 0 && firstStep}
      {currentPatientStep === 1 && secondStep}
      {currentPatientStep === 2 && thirdStep}
      {currentPatientStep === 3 && fourthStep}
      {currentPatientStep === 4 && productEspecial === true && fiveStep}
      {currentPatientStep === 5 && productEspecial === true && sixthStep}
    </Fragment>
  );
};

export default PatientStepComponent;
