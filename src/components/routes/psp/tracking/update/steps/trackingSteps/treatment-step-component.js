import React from "react";

import FormTreatment from "../../components/treatment/FormTreatment";
import FormTreatmentComponent from "../../components/treatment/form-treatment-component";

const TreatmentComponent = () => {
  return (
    <section
      className="section px-0 is-flex-grow-1 py-4 coolscroll"
      style={{ height: "1px", overflow: "hidden", minHeight: "500px" }}
    >
      <div className="tab is-active" style={{ height: "100%" }}>
        <div
          className="coolscroll hblue"
          style={{ height: "100%", overflow: "auto" }}
        >
          <FormTreatmentComponent />
        </div>
      </div>
    </section>
  );
};

export default TreatmentComponent;
