import React from "react";
import _ from "lodash";
import { Fragment } from "react";
import { When } from "react-if";

import { useTrackingContext } from "./tracking-step-provider";

import Tabs from "./components/tabs";
import PatientStepComponent from "./patient-step-component";
import TreatmentStepComponent from "./treatment-step-component";
import ComunicationStepComponent from "./comunication-step-component";
import PatientStepProvider from "../patientSteps/patient-step-provider";

const TrackingStepComponent = () => {
  const { user, treatment, currentStep } = useTrackingContext();

  const firstStep = <PatientStepProvider children={<PatientStepComponent />} />;
  const secondStep = <TreatmentStepComponent />;
  const thirdStep = <ComunicationStepComponent />;
  return (
    <Fragment>
      <When condition={!_.isNull(user) && !_.isNull(treatment)}>
        <Tabs />
      </When>
      {currentStep === 0 && firstStep}
      {currentStep === 1 && secondStep}
      {currentStep === 2 && thirdStep}
    </Fragment>
  );
};

export default TrackingStepComponent;
