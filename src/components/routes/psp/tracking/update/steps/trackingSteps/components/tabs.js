import React from "react";

import { useTrackingContext } from "../tracking-step-provider";

import ProgressIcon from "components/base/progress-icon";
import ProgressBar from "components/base/progress-bar";
import styles from "../../../trackingUpdate.scss";

const Tabs = () => {
  const {
    currentStep,
    percentCurrentStep,
    setCurrentStep,
    setPercetCurrentStep,
  } = useTrackingContext();

  const handleStep = (step) => {
    setCurrentStep(step);
    setPercetCurrentStep(0);
  };

  return (
    <section className="section p-0 m-0">
      <div className="columns is-vcentered">
        <div className="column">
          <div className="tabs is-boxed trackingTabs">
            <ul>
              <li
                className={`formTab ${currentStep === 0 ? "is-active" : ""}  ${
                  styles.tabs
                }`}
                onClick={() => handleStep(0)}
              >
                {/* eslint-disable-next-line */}
                <a>
                  <ProgressIcon
                    className="has-background-hblue has-text-white"
                    value={currentStep === 0 && percentCurrentStep}
                    icon={
                      currentStep === 0 && percentCurrentStep === 100 ? (
                        <i className="fas fa-check"></i>
                      ) : (
                        <i class="fas fa-user"></i>
                      )
                    }
                  ></ProgressIcon>{" "}
                  &nbsp; Paciente
                </a>
              </li>
              <li
                className={`formTab ${styles.tabs} ${
                  currentStep === 1 ? "is-active" : ""
                }`}
                onClick={() => handleStep(1)}
                data-tab="tab2"
              >
                {/* eslint-disable-next-line */}
                <a>
                  <ProgressIcon
                    className="has-background-hblue has-text-white"
                    value={currentStep === 1 && percentCurrentStep}
                    icon={
                      currentStep === 1 && percentCurrentStep === 100 ? (
                        <i className="fas fa-check"></i>
                      ) : (
                        <i className="fas fa-pills"></i>
                      )
                    }
                  ></ProgressIcon>{" "}
                  &nbsp; Tratamiento
                </a>
              </li>
              <li
                className={`formTab ${styles.tabs} ${
                  currentStep === 2 ? "is-active" : ""
                }`}
                onClick={() => handleStep(2)}
                data-tab="tab3"
              >
                {/* eslint-disable-next-line */}
                <a>
                  <ProgressIcon
                    className="has-background-hblue has-text-white"
                    value={currentStep === 2 && percentCurrentStep}
                    icon={
                      currentStep === 2 && percentCurrentStep === 100 ? (
                        <i className="fas fa-check"></i>
                      ) : (
                        <i className="far fa-comment-dots"></i>
                      )
                    }
                  ></ProgressIcon>
                  &nbsp; Comunicaciones
                </a>
              </li>
            </ul>
          </div>
        </div>

        <div className="column is-3">
          <ProgressBar
            className="has-background-hblue"
            value={percentCurrentStep}
          ></ProgressBar>
        </div>
      </div>
    </section>
  );
};

export default Tabs;
