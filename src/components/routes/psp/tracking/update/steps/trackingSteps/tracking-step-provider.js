import React from "react";
import { createContext } from "react";
import { useContext } from "react";
import { useState } from "react";

export const StepContext = createContext();
export const useTrackingContext = () => useContext(StepContext);

const TrackingStepProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [currentStep, setCurrentStep] = useState(0);
  const [percentCurrentStep, setPercetCurrentStep] = useState(0);
  const [treatment, setTreatment] = useState(null);
  const [product, setProduct] = useState({ name: "", id: 0 });
  const [comunications, setComunications] = useState([]);

  const context = {
    user,
    setUser,
    currentStep,
    setCurrentStep,
    treatment,
    setTreatment,
    percentCurrentStep,
    setPercetCurrentStep,
    product,
    setProduct,
    comunications,
    setComunications,
  };
  return <StepContext.Provider children={children} value={context} />;
};

export default TrackingStepProvider;
