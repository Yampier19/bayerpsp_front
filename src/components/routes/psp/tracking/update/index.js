import React from "react";

import TrackingStepProvider from "./steps/trackingSteps/tracking-step-provider";
import TrackingStepComponent from "./steps/trackingSteps/tracking-step-component";

const TrackingUpdatePatient = () => {
  return (
    <TrackingStepProvider>
      <div
        className="is-flex is-flex-direction-column "
        style={{ height: "100%" }}
      >
        <section className="section p-0">
          <h1 className="subtitle is-3 mb-3">
            <strong className="has-text-hblue">SEGUIMIENTO</strong>
          </h1>
          <h2 className="subtitle has-text-hblue">EDITAR</h2>
        </section>
        <TrackingStepComponent />
      </div>
    </TrackingStepProvider>
  );
};

export default TrackingUpdatePatient;
