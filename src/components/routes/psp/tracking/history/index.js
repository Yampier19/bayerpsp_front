import { useState, useEffect } from "react";

import axios from "axios";

const TrackingHistory = (props) => {
  const url = "https://jsonplaceholder.typicode.com/posts";

  const [news, setNews] = useState([]);

  useEffect(() => {
    axios
      .get(url)
      .then((res) => {
        setNews(res.data.splice(0, 50));
      })
      .then((err) => console.log(err));
  }, []);

  return (
    <div
      className="is-flex is-flex-direction-column "
      style={{ height: "100%" }}
    >
      {/* header */}
      <section className="section px-0 py-0">
        <h1 className="title has-text-hblue is-2">Seguimiento</h1>
        <h1 className="subtitle has-text-hblue ">
          Editar -{" "}
          <strong className="has-text-hblue">Historico de adherencia</strong>
        </h1>
      </section>

      <br />
      <br />

      {/* second level - table */}
      <section
        className="coolscroll is-flex-grow-1"
        style={{ minHeight: "500px", overflowY: "scroll" }}
      >
        <div style={{ minWidth: "1450px", overflow: "scroll" }}>
          <div className="box table-header has-background-hblue-light">
            <div className="columns has-text-hblue is-mobile fila">
              <div className="column is-2">Mes</div>
              <div className="column">Reclamación</div>
              <div className="column">De reclamación</div>
              <div className="column no-border">Producto</div>
            </div>
          </div>
          <br />

          {news.map((neww, i) => (
            <div className="mb-6">
              <div className="box" key={i}>
                <div className="columns fila2 is-mobile">
                  <div className="column is-2 ">{"Julio"}</div>
                  <div className="column">
                    {Math.floor(Math.random() * 3) > 1 ? "SI" : "NO"}
                  </div>
                  <div className="column">DD - MM - YYYY</div>
                  <div className="column" style={{ minWidth: "200px" }}>
                    <s>Abierto a texto explicativo</s>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </section>

      <section className="section px-0 pb-0">
        <h1 className="subtitle has-text-hblue">
          se encontraron {news.length} registros
        </h1>
      </section>
    </div>
  );
};

export default TrackingHistory;
