import {useState, useEffect} from 'react';

import CreateFilter from 'components/base/buttons/create-filter';

import DraggablePanel from './draggable-panel';

import {tabClicked} from 'components/commons/tabs/tabClicked';

import {
    patientFilters,
    claimFilters,
    conciliationFilters,
    therapyFilters,
    wallsFilters,
    medicFilters
} from './defaultFilters';

import './reports.scss';


const Reports = props => {

    const [filters1, setFilters1] = useState(patientFilters);
    const [filters2, setFilters2] = useState(claimFilters);
    const [filters3, setFilters3] = useState(conciliationFilters);
    const [filters4, setFilters4] = useState(therapyFilters);
    const [filters5, setFilters5] = useState(wallsFilters);
    const [filters6, setFilters6] = useState(medicFilters);



    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 pt-0 pb-3 ">
                <div className="columns is-mobile">
                    <div className="column is-3-desktop ">
                        <h1 className="subtitle has-text-hred is-3 mb-1"  style={{ whiteSpace: 'nowrap'}}>
                            <strong className="has-text-hred">Reportes</strong>
                        </h1>
                        <h1 className="subtitle has-text-hred">Filtros</h1>
                    </div>
                    <div className="column is-3-desktop is-offset-6-desktop is-6-mobile">
                        <CreateFilter/>
                    </div>
                </div>
            </section>



            <br/>
            <br/>

            {/* tabs */}
            <section className="section p-0 m-0">
                <div className="columns is-vcentered">

                    <div className="column">
                        <div className="tabs is-boxed reportTabs">
                            <ul>
                                <li className={`formTab is-active mx-2`} onClick={tabClicked} data-tab="tab1">
                                    <a>
                                        Paciente
                                    </a>
                                </li>
                                <li className={`formTab mx-2`} onClick={tabClicked} data-tab="tab2">
                                    <a>
                                        Reclamación
                                    </a>
                                </li>
                                <li className={`formTab mx-2`} onClick={tabClicked} data-tab="tab3">
                                    <a>
                                        Consiliación
                                    </a>
                                </li>
                                <li className={`formTab mx-2`} onClick={tabClicked} data-tab="tab4">
                                    <a>
                                        Terapias
                                    </a>
                                </li>
                                <li className={`formTab mx-2`} onClick={tabClicked} data-tab="tab5">
                                    <a>
                                        Barreras
                                    </a>
                                </li>
                                <li className={`formTab mx-2`} onClick={tabClicked} data-tab="tab6">
                                    <a>
                                        Muestas medicas
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>


                </div>
            </section>

            {/* forms */}
            <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                <div id="tab1" className="tab is-active py-4 coolscroll" style={{height: '100%', overflowY: 'scroll'}}>

                    <DraggablePanel items={filters1} setItems={setFilters1}/>
                </div>

                <div id="tab2" className="tab"  style={{height: '100%', overflowY: 'scroll'}}>
                    <DraggablePanel items={filters2} setItems={setFilters2}/>
                </div>

                <div id="tab3" className="tab"  style={{height: '100%', overflowY: 'scroll'}}>
                    <DraggablePanel items={filters3} setItems={setFilters3}/>
                </div>

                <div id="tab4" className="tab"  style={{height: '100%', overflowY: 'scroll'}}>
                    <DraggablePanel items={filters4} setItems={setFilters4}/>
                </div>

                <div id="tab5" className="tab"  style={{height: '100%', overflowY: 'scroll'}}>
                    <DraggablePanel items={filters5} setItems={setFilters5}/>
                </div>

                <div id="tab6" className="tab"  style={{height: '100%', overflowY: 'scroll'}}>
                    <DraggablePanel items={filters6} setItems={setFilters6}/>
                </div>

            </section>



        </div>
    );
}

export default Reports;
