
export const dataPatient = [];

for(let i = 0; i < 50; i++){
    dataPatient.push(
        {
            id: 'id',
            estado: 'estado',
            fechaActivacion: 'DD-MM-AAAA',
            fechaRetiro: 'DD-MM-AAAA',
            motivoRetiro: 'motivo de retiro',
            observaciónMotivoRetiro: 'observación motivo de retiro',
            identificacion: '123456',
            nombres: 'nombres',
            apellidos: 'apellidos',
            telefono1: '4455464',
            telefono2: '4455464',
            telefono3: '4455464',
            correoElectronico: 'paciente@correo.com',
            direccion: 'direccion',
            barrio: 'barrio',
            departamento: 'department',
            ciudad: 'ciudad',
            genero: 'genero',
            fechaNacimiento: 'DD-MM-AA',
            edad: 'edad',
            acudiente: 'acudiente',
            telefonoAcudiente: '46556',
            codigoXofigo: 'codigo xofigo',
            estado: 'estado',
            idUltimaGestion: 'id ultima gestion',
            usuarioCreacion: 'usuario creacion'
        }
    );
}

export const data2 = [];

for(let i = 0; i < 30; i++){
    data2.push({
        id: 'id',
        producto: 'producto',
        nombre: 'nombre',
        clasificacionPatologica: 'clasificación patologica',
        tratamientoPrevio: 'tratamiento previo',
        consentimiento: 'consentimiento',
        fehchaInicioTerapia: 'DD-MM-AAAA',
        regimen: 'regimen',
        asegurador: 'asegurador',
        operadorLogistico: 'operador logistico',
        puntoDeEntrega: 'punto de entrega',
        fechaUltimoReclamacion: 'DD-MM-AAAA',
        otrosOperadores: 'otros operadores',
        mediosAdquisicion: 'medios de adquisicion',
        medico: 'medico',
        especialidad: 'especialidad',
        paramedico: 'paramedico',
        zonaAtencionParamedico: 'Zona atencion paramedico',
        ciudadBaseParamedico: 'ciudad base paramedico',
        notas: 'notas',
        adjunto: 'adjunto'
    });
}
