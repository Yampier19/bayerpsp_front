import {useState, useEffect} from 'react';

import FilterBar from 'components/base/filter-bar';
import Table from 'components/base/table';
import DownloadButton from 'components/base/buttons/download';

import axios from 'axios';

import {dataPatient, data2} from './data';
import {format1} from './format';

const DataList = props => {

    const url = 'https://jsonplaceholder.typicode.com/posts';

    const [news, setNews] = useState([]);

    useEffect(
        () => {

            axios.get(url)
            .then(res => {

                setNews(res.data.splice(0, 50));

            })
            .then(err => console.log(err));

        }, []
    );

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 py-0">

                <div className="columns">
                    <div className="column">
                        <h1 className="title has-text-hred is-2">Reportes</h1>
                        <h1 className="subtitle has-text-hred ">Listado de paciente</h1>
                    </div>
                    <div className="column is-2" style={{minWidth: '280px'}}>
                        <DownloadButton color="hred"/>
                    </div>
                </div>

            </section>



            <br/>
            <br/>

            {/* filter bar */}
            <section className="section p-0 pb-1 ">
                <div className="columns">
                    <div className="column">
                        <FilterBar
                        textColorClass="has-text-hred"
                        filters={[
                            {
                                name: "Columnas",
                                options: [{name: "op1"}, {name: "op2"}]
                            },
                            {
                                name: "Busqueda avanzada",
                                options: []
                            }
                        ]}
                        moreFiltersOn
                        tags={null}
                    />
                    </div>
                    <div className="column is-2-desktop has-text-right has-text-left-mobile">
                        <div className="field">
                            <div className="control has-icons-right">
                                <input className="input searchbar" type="text" placeholder="Buscar" />
                                <span className="icon is-small is-right has-text-hred-light">
                                    <i className="fas fa-search"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </section>


            {/* second level - table */}
            <section className="is-flex-grow-1" style={{minHeight: '500px', overflow: 'hidden'}}>
                <div className="is-inline-flex is-flex-direction-row coolscroll hred is-flex-wrap-nowrap" style={{overflow: 'auto', height:"100%", width: "100%"}} >
                    <div className="mr-3">
                        <Table format={format1} data={dataPatient} ignore={[]} name="P A C I E N T E S"/>
                    </div>
                    <div className="">
                        <Table format={format1} data={dataPatient} ignore={[]} name="T R A T A M I E N T O"/>
                    </div>

                </div>
            </section>

            {/* data count */}
            <section className="section px-0 pb-0">

                <h1 className="subtitle has-text-hred">
                    se encontraron {news.length} registros
                </h1>

            </section>

        </div>

    );
}

export default DataList;
