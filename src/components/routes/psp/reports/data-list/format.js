export const format1 = {
    columns: [
        {
             key: 'id',
             name: 'ID',
             size: 1
        },
        {
             key: 'estado',
             name: 'Estado',
             size: 1
        },
        {
             key: 'fechaActivacion',
             name: 'Fecha de activación',
             size: 2
        },
        {
             key: 'fechaRetiro',
             name: 'Fecha de retiro',
             size: 2
        },
        {
             key: 'motivoRetiro',
             name: 'Motivo de retiro',
             size: 2
        },
        {
             key: 'observaciónMotivoRetiro',
             name: 'Observación motivo de retiro',
             size: 2
        },
        {
             key: 'identificacion',
             name: 'Identificación',
             size: 2
        },
        {
             key: 'nombres',
             name: 'Nombres',
             size: 2
        },
        {
             key: 'apellidos',
             name: 'Apellidos',
             size: 2
        },
        {
             key: 'telefono1',
             name: 'Teléfono 1',
             size: 1
        },
        {
             key: 'telefono2',
             name: 'Teléfono 2',
             size: 1
        },
        {
             key: 'telefono3',
             name: 'Teléfono 3',
             size: 1
        },
        {
             key: 'correoElectronico',
             name: 'Correo electronico',
             size: 3
        },
        {
             key: 'direccion',
             name: 'Dirección',
             size: 3
        },
        {
             key: 'barrio',
             name: 'Barrio',
             size: 2
        },
        {
             key: 'departamento',
             name: 'Departamento',
             size: 1
        },
        {
             key: 'ciudad',
             name: 'Ciudad',
             size: 1
        },
        {
             key: 'genero',
             name: 'Género',
             size: 1
        },
        {
             key: 'fechaNacimiento',
             name: 'Fecha de nacimiento',
             size: 1
        },
        {
             key: 'edad',
             name: 'Edad',
             size: 1
        },
        {
             key: 'acudiente',
             name: 'Acudiente',
             size: 2
        },
        {
             key: 'telefonoAcudiente',
             name: 'Teléfono acudiente',
             size: 1
        },
        {
             key: 'codigoXofigo',
             name: 'Código xofigo',
             size: 2
        },
        {
             key: 'estado',
             name: 'Estado',
             size: 1
        },
        {
             key: 'idUltimaGestion',
             name: 'Id última gestión',
             size: 1
        },
        {
             key: 'usuarioCreacion',
             name: 'Usuario creación',
             size: 2
        },

    ],
    colors: {
        header: "has-background-hyellow-light",
        header_text: "has-text-hred"
    }
}
