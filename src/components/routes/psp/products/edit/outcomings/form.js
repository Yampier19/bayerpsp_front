import React from "react";
import { useEffect } from "react";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";

import FormField from "components/form/formfield";
import FormField2 from "components/form/formfield2";
import Dropdown from "components/form/dropdown";
import Radio from "components/form/radio";

import * as Yup from "yup";
import { useFormik } from "formik";

import { connect } from "react-redux";
import { set_modal_on } from "redux/actions/modalActions";
import { InfoModal } from "components/base/modal-system/models/modal";
import { useListAllProducts } from "services/bayer/psp/product/useProduct";
import { useDose } from "services/bayer/psp/patient/usePatient";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  borderRadius: 10,
  boxShadow: 24,
  p: 4,
};

const Form = (props) => {
  const { data: AllProducts } = useListAllProducts({
    retry: false,
  });
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const formSchema = Yup.object().shape({
    numeroRemision: Yup.string().required("Este campo es obligatorio"),
    numeroReferencia: Yup.string().required("Este campo es obligatorio"),
    isComercial: Yup.string().required("Este campo es obligatorio"),
    cantidadenviada: Yup.string().required("Este campo es obligatorio"),
    ciudad: Yup.string().required("Este campo es obligatorio"),
    direccion: Yup.string().required("Este campo es obligatorio"),
    nombrePaciente: Yup.string().required("Este campo es obligatorio"),
    fechaEnvio: Yup.string().required("Este campo es obligatorio"),
    fecha_egreso: Yup.string().required("Este campo es obligatorio"),
    ciudadEntrega: Yup.string().required("Este campo es obligatorio"),
    direccionEntrega: Yup.string().required("Este campo es obligatorio"),
    responsable: Yup.string().required("Este campo es obligatorio"),
    img_producto: Yup.string().required("Este campo es obligatorio"),
    observaciones: Yup.string().required("Este campo es obligatorio"),
    bodega: Yup.string().required("Este campo es obligatorio"),
    dose: Yup.string().when("isComercial", {
      is: (comercial) => comercial === "Si",
      then: Yup.string().required("Este campo es obligatorio"),
    }),
    producto: Yup.string().when("isComercial", {
      is: (comercial) => comercial === "Si" || comercial === "No",
      then: Yup.string().required("Este campo es obligatorio"),
    }),
    patient_id: Yup.string().when("isComercial", {
      is: (comercial) => comercial === "Si" || comercial === "No",
      then: Yup.string().required("Este campo es obligatorio"),
    }),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      numeroRemision: "",
      numeroReferencia: "",
      producto: "",
      cantidadenviada: "",
      ciudad: "",
      direccion: "",
      patient_id: "",
      nombrePaciente: "",
      fechaEnvio: "",
      fechaRecivido: "",
      ciudadEntrega: "",
      direccionEntrega: "",
      responsable: "",
      img_producto: "",
      observaciones: "",
      dose: "",
      isComercial: "",
      bodega: "",
      fecha_egreso: "",
    },
    validateOnChange: true,
    validationSchema: formSchema,
    onSubmit: async (values) => {
      // alert(JSON.stringify(values, null, 2));
      /* const { _icons } = props.modalReducer;
      props.set_modal_on(
        new InfoModal(
          "Su egreso ha sido creado satisfactoriamente",
          _icons.CHECK_HORANGE
        )
      ); */
    },
  });

  const {
    data: DoseByProduct,
    isLoading: isLoadingDose,
    refetch: refetchDose,
  } = useDose(formik.values.producto, {
    refetchOnWindowFocus: false,
    retry: false,
  });

  useEffect(() => {
    if (formik.values.producto !== "") {
      refetchDose();
    }
    //eslint-disable-next-line
  }, [formik.values.producto]);

  return (
    <form id="form" onSubmit={formik.handleSubmit} className="pr-3 py-3">
      <FormField
        label="Número de remisión"
        number="1"
        name="numeroRemision"
        checked={formik.values.numeroRemision}
      >
        <input
          className="input cool-input"
          type="text"
          name="numeroRemision"
          onChange={formik.handleChange}
          value={formik.values.numeroRemision}
        />
        {formik.touched.numeroRemision && formik.errors.numeroRemision ? (
          <div className="help is-danger"> {formik.errors.numeroRemision}</div>
        ) : null}
      </FormField>

      <FormField
        label="Número de referencia"
        number="2"
        name="numeroReferencia"
        checked={formik.values.numeroReferencia}
      >
        <input
          className="input cool-input"
          type="text"
          name="numeroReferencia"
          onChange={formik.handleChange}
          value={formik.values.numeroReferencia}
        />
        {formik.touched.numeroReferencia && formik.errors.numeroReferencia ? (
          <div className="help is-danger">{formik.errors.numeroReferencia}</div>
        ) : null}
      </FormField>
      <FormField2
        number1="3"
        number2="3.1"
        checked1={formik.values.isComercial}
        label1="¿Es un medicamento sin valor comercial?"
        label2={"Producto"}
        input1={
          <div>
            <Radio
              name="isComercial"
              onChange={formik.handleChange}
              value={formik.values.isComercial}
            />
            {formik.touched.isComercial && formik.errors.isComercial ? (
              <div className="help is-danger">{formik.errors.isComercial}</div>
            ) : null}
          </div>
        }
        input2={
          <div>
            <Dropdown
              arrowColor="has-text-horange"
              name="producto"
              options={AllProducts ? AllProducts?.data : []}
              onChange={formik.handleChange}
              value={formik.values.producto}
              disabled={formik.values.isComercial === ""}
            />
            {formik.touched.producto && formik.errors.producto ? (
              <div className="help is-danger"> {formik.errors.producto}</div>
            ) : null}
          </div>
        }
      />
      <FormField2
        number1="4"
        number2="4.1"
        name1="dose"
        name2="patient_id"
        checked1={formik.values.dose}
        checked2={formik.values.patient_id}
        label1="Dosis"
        label2="Codigo del paciente"
        input1={
          <div>
            <Dropdown
              name="dose"
              onChange={formik.handleChange}
              options={DoseByProduct ? DoseByProduct?.data : []}
              arrowColor="has-text-horange"
              disabled={
                formik.values.isComercial === "Si"
                  ? !isLoadingDose && !DoseByProduct
                  : true
              }
            />
            {formik.touched.dose && formik.errors.dose ? (
              <div className="help is-danger"> {formik.errors.dose}</div>
            ) : null}
          </div>
        }
        input2={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="patient_id"
              onChange={formik.handleChange}
              value={formik.values.patient_id}
              disabled={formik.values.isComercial === ""}
            />
            {formik.touched.patient_id && formik.errors.patient_id ? (
              <div className="help is-danger"> {formik.errors.patient_id}</div>
            ) : null}
          </div>
        }
      />
      <FormField2
        number1="5"
        number2="5.1"
        name1="cantidadenviada"
        name2="responsable"
        checked1={formik.values.cantidad}
        checked2={formik.values.responsable}
        label1="Cantidad Egresada"
        label2="Responsable"
        input1={
          <div>
            <Dropdown
              name="cantidadenviada"
              onChange={formik.handleChange}
              options={[
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
                19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34,
                35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
              ]}
              arrowColor="has-text-horange"
            />
            {formik.touched.cantidadenviada && formik.errors.cantidadenviada ? (
              <div className="help is-danger">
                {" "}
                {formik.errors.cantidadenviada}
              </div>
            ) : null}
          </div>
        }
        input2={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="responsable"
              onChange={formik.handleChange}
              value={formik.values.responsable}
            />
            {formik.touched.responsable && formik.errors.responsable ? (
              <div className="help is-danger"> {formik.errors.responsable}</div>
            ) : null}
          </div>
        }
      />

      <FormField2
        number1="6"
        number2="6.1"
        name1="bodega"
        name2="fecha_egreso"
        checked1={formik.values.bodega}
        checked2={formik.values.fecha_egreso}
        label1="Bodega"
        label2="Fecha de Egreso"
        input1={
          <div>
            <Dropdown
              name="bodega"
              onChange={formik.handleChange}
              options={["asd"]}
              arrowColor="has-text-horange"
            />
            {formik.touched.bodega && formik.errors.bodega ? (
              <div className="help is-danger"> {formik.errors.bodega}</div>
            ) : null}
          </div>
        }
        input2={
          <div>
            <input
              className="input cool-input"
              type="date"
              name="fecha_egreso"
              onChange={formik.handleChange}
              value={formik.values.fecha_egreso}
            />
            {formik.touched.fecha_egreso && formik.errors.fecha_egreso ? (
              <div className="help is-danger">{formik.errors.fecha_egreso}</div>
            ) : null}
          </div>
        }
      />

      <FormField
        label="Foto Producto"
        number="7"
        name="img_producto"
        checked={formik.values.img_producto}
      >
        <input
          type="file"
          name="img_producto"
          onChange={formik.handleChange}
          value={formik.values.img_producto}
        />
        {formik.touched.img_producto && formik.errors.img_producto ? (
          <div className="help is-danger"> {formik.errors.img_producto}</div>
        ) : null}
      </FormField>

      <FormField
        label="Observaciones"
        number="8"
        name="observaciones"
        checked={formik.values.observaciones}
        noline
      >
        <input
          className="input cool-input"
          type="text"
          name="observaciones"
          onChange={formik.handleChange}
          value={formik.values.observaciones}
        />
        {formik.touched.observaciones && formik.errors.observaciones ? (
          <div className="help is-danger"> {formik.errors.observaciones}</div>
        ) : null}
      </FormField>

      <br />

      <div className="field">
        <div className="control has-text-right has-text-centered-mobile">
          <button
            type="submit"
            /* onClick={handleOpen}  */ className="button is-horange"
          >
            CONTINUAR
          </button>

          <div>
            <Modal
              open={open}
              onClose={handleClose}
              aria-labelledby="modal-modal-title"
              aria-describedby="modal-modal-description"
            >
              <Box sx={style}>
                <div className="row-reverse">
                  <div clasName="col">
                    <div className="row">
                      <div className="col-3 d-flex justify-content-center align-items-center">
                        <i
                          className="fal fa-exclamation-triangle fa-5x"
                          style={{ color: "#E7967A" }}
                        ></i>
                      </div>
                      <div className="col">
                        <label className="lead">
                          ¿Esta seguro que desea descontar 20 kits de Xarelto
                          del inventario de la Bodega del Norte?*
                        </label>
                      </div>
                    </div>
                  </div>
                  <div clasName="col">
                    <div className="row mt-5">
                      <div className="col d-flex justify-content-end">
                        <button
                          className="button is-horange"
                          type="submit"
                          style={{ width: "200px" }}
                        >
                          Generar orden de salida
                        </button>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col mt-4">
                        <p for="" className="text-muted fa-xs text-center">
                          <i>
                            *Será temporal hasta que se haga efectiva la entrega
                            de los productos, una vez se notifique la entrega se
                            descontará permanentemente
                          </i>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </Box>
            </Modal>
          </div>
        </div>
      </div>
    </form>
  );
};
const mapStateToPros = (state) => ({
  modalReducer: state.modalReducer,
});

export default connect(mapStateToPros, {
  set_modal_on,
})(Form);
