import FormField from 'components/form/formfield';
import FormField2 from 'components/form/formfield2';
import Dropdown from  'components/form/dropdown';

import * as Yup from 'yup';
import {useFormik} from 'formik';

import {connect} from 'react-redux';
import {set_modal_on} from 'redux/actions/modalActions';
import {InfoModal} from 'components/base/modal-system/models/modal';

const Form = props => {

    const {data} = props;

    const formSchema = Yup.object().shape({
        numeroProveedor: Yup.string().required('Este campo es obligatorio'),
        nombreProveedor: Yup.string().required('Este campo es obligatorio'),
        producto: Yup.string().required('Este campo es obligatorio'),
        bodegaQueSurte: Yup.string(),
        ciudad: Yup.string(),
        direccion: Yup.string(),
        telefono: '',
        responsable: Yup.string(),
        transportador: Yup.string(),
        observaciones: Yup.string()
    });

    const formik = useFormik({
        enableReinitialize: true,
        initialValues:  {
            numeroProveedor: data.numeroProveedor || '',
            nombreProveedor: data.nombreProveedor || '',
            producto: data.producto || '',
            bodegaQueSurte: '',
            ciudad: '',
            direccion: '',
            telefono: '',
            responsable: '',
            transportador: '',
            observaciones: ''
        },
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            // alert(JSON.stringify(values, null, 2));
            const {_icons} = props.modalReducer;
            props.set_modal_on( new InfoModal('La devolución ha sido creado satisfactoriamente', _icons.CHECK_HORANGE) );
        }
    });

    return(
        <form id="form" onSubmit={formik.handleSubmit} className="pr-3 py-3">
            <FormField label="Número de proveedor" number="1" name="numeroProveedor" checked={formik.values.numeroProveedor}>
                <input className="input cool-input" type="text" name="numeroProveedor" onChange={formik.handleChange} value={formik.values.numeroProveedor} />
                {formik.touched.numeroProveedor && formik.errors.numeroProveedor ? <div className="help is-danger"> {formik.errors.numeroProveedor}</div> : null }
            </FormField>

            <FormField label="Nombre del proveedor" number="2" name="nombreProveedor" checked={formik.values.nombreProveedor}>
                <input className="input cool-input" type="text" name="nombreProveedor" onChange={formik.handleChange} value={formik.values.nombreProveedor} />
                {formik.touched.nombreProveedor && formik.errors.nombreProveedor ? <div className="help is-danger"> {formik.errors.nombreProveedor}</div> : null }
            </FormField>

            <FormField label="Producto" number="3" name="producto" checked={formik.values.producto}>
                <input className="input cool-input" type="text" name="producto" onChange={formik.handleChange} value={formik.values.producto} />
                {formik.touched.producto && formik.errors.producto ? <div className="help is-danger"> {formik.errors.producto}</div> : null }
            </FormField>

            <FormField label="Bodega que surte" number="4" name="bodegaQueSurte" checked={formik.values.bodegaQueSurte}>
                <Dropdown name="bodegaQueSurte" onChange={formik.handleChange} options={['asd']} arrowColor="has-text-horange"/>
                {formik.touched.bodegaQueSurte && formik.errors.bodegaQueSurte ? <div className="help is-danger"> {formik.errors.bodegaQueSurte}</div> : null }
            </FormField>

            <FormField2
                number1="5" number2="5.1" name1="ciudad" name2="direccion"
                checked1={formik.values.ciudad} checked2={formik.values.tipoLlamada}
                label1="Ciudad" label2="Dirección"
                input1={
                    <div>
                        <Dropdown name="ciudad" onChange={formik.handleChange} options={['asd']} arrowColor="has-text-horange"/>
                        {formik.touched.ciudad && formik.errors.ciudad ? <div className="help is-danger"> {formik.errors.ciudad}</div> : null }
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="text" name="direccion" onChange={formik.handleChange} value={formik.values.direccion} />
                        {formik.touched.direccion && formik.errors.direccion ? <div className="help is-danger"> {formik.errors.direccion}</div> : null }
                    </div>
                }
            />

            <FormField label="Telefono" number="6" name="responsable" checked={formik.values.responsable}>
                <input className="input cool-input" type="text" name="responsable" onChange={formik.handleChange} value={formik.values.responsable} />
                {formik.touched.responsable && formik.errors.responsable ? <div className="help is-danger"> {formik.errors.responsable}</div> : null }
            </FormField>

            <FormField label="Responsable" number="7" name="responsable" checked={formik.values.responsable}>
                <input className="input cool-input" type="text" name="responsable" onChange={formik.handleChange} value={formik.values.responsable} />
                {formik.touched.responsable && formik.errors.responsable ? <div className="help is-danger"> {formik.errors.responsable}</div> : null }
            </FormField>

            <FormField label="Transportador" number="8" name="transportador" checked={formik.values.transportador}>
                <input className="input cool-input" type="text" name="transportador" onChange={formik.handleChange} value={formik.values.transportador} />
                {formik.touched.transportador && formik.errors.transportador ? <div className="help is-danger"> {formik.errors.transportador}</div> : null }
            </FormField>

            <FormField label="Observaciones" number="9" name="observaciones" checked={formik.values.observaciones} noline>
                <input className="input cool-input" type="text" name="observaciones" onChange={formik.handleChange} value={formik.values.observaciones} />
                {formik.touched.observaciones && formik.errors.observaciones ? <div className="help is-danger"> {formik.errors.observaciones}</div> : null }
            </FormField>

            <br/>

            <div className="field">
                <div className="control has-text-right has-text-centered-mobile">
                    <button
                        className="button is-horange"
                        type="submit"
                        style={{width: '150px'}}
                    >
                        ACTUALIZAR
                    </button>
                </div>
            </div>

        </form>
    );
}
const mapStateToPros = state => ({
    modalReducer: state.modalReducer
})

export default connect(
    mapStateToPros,
    {
        set_modal_on
    }
)(Form);
