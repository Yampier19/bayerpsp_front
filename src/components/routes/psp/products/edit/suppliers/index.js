import {useEffect, useState} from 'react';
import axios from 'axios';

import {serverURL} from 'secrets';

import Form from './form';
import {useLocation} from 'react-router-dom';

const ProductsEditSuppliers = props => {


    const {search} = useLocation();
    const id = new URLSearchParams(search).get('id');

    const [data, setData] = useState({});

    useEffect(
        () => {
            const url = `${serverURL}/posts/${id}`;
            axios
            .get(url)
            .then(res => {
                setData({
                    numeroProveedor: res.data.id,
                    nombreProveedor: 'nombres y apellidos',
                    producto: 'acetaminofem'
                });
            })
            .catch(e => {})
        }, []
    );

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 pt-0 pb-3 ">
                <div className="columns is-mobile">
                    <div className="column is-3-desktop ">
                        <h1 className="subtitle has-text-horange is-3 mb-1"  style={{ whiteSpace: 'nowrap'}}>
                            <strong className="has-text-horange">PRODUCTOS</strong>
                        </h1>
                        <h1 className="subtitle has-text-horange ">Proveedores <strong className="has-text-horange">- Editar</strong></h1>
                    </div>
                    <div className="column is-3-desktop is-offset-6-desktop is-6-mobile">

                    </div>
                </div>
            </section>

            {/* form */}
            <section className="section px-0 is-flex-grow-1" style={{height: '1px', overflow: 'hidden'}}>
                <div className="coolscroll horange" style={{height: '100%', overflow: 'auto'}}>
                    <Form data={data}/>
                </div>
            </section>

        </div>
    );
}

export default ProductsEditSuppliers;
