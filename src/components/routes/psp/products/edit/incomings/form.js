import { useEffect } from "react";
import * as Yup from "yup";
import { useFormik } from "formik";
import { connect } from "react-redux";

import FormField from "components/form/formfield";
import FormField2 from "components/form/formfield2";
import Dropdown from "components/form/dropdown";
import Radio from "components/form/radio";
import { set_modal_on } from "redux/actions/modalActions";
import { InfoModal } from "components/base/modal-system/models/modal";
import { useListAllProducts } from "services/bayer/psp/product/useProduct";
import { useDose } from "services/bayer/psp/patient/usePatient";

const Form = (props) => {
  const { data: AllProducts, isLoading: isLoadingAllProducts } =
    useListAllProducts({
      retry: false,
    });

  const formSchema = Yup.object().shape({
    numeroRemision: Yup.string().required("Este campo es obligatorio"),
    numeroReferencia: Yup.string().required("Este campo es obligatorio"),
    isComercial: Yup.string().required("Este campo es obligatorio"),
    cantidad: Yup.string().required("Este campo es obligatorio"),
    responsable: Yup.string().required("Este campo es obligatorio"),
    ciudad: Yup.string().required("Este campo es obligatorio"),
    direccion: Yup.string().required("Este campo es obligatorio"),
    fechaIngreso: Yup.string().required("Este campo es obligatorio"),
    archivo: Yup.string().required("Este campo es obligatorio"),
    observaciones: Yup.string().required("Este campo es obligatorio"),
    proveedor: Yup.string().required("Este campo es obligatorio"),
    name_producto: Yup.string().when("isComercial", {
      is: (comercial) => comercial === "No",
      then: Yup.string().required("Este campo es obligatorio"),
    }),
    dose: Yup.string().when("isComercial", {
      is: (comercial) => comercial === "Si",
      then: Yup.string().required("Este campo es obligatorio"),
    }),
    producto: Yup.string().when("isComercial", {
      is: (comercial) => comercial === "Si" || comercial === "No",
      then: Yup.string().required("Este campo es obligatorio"),
    }),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      numeroRemision: "",
      numeroReferencia: "",
      producto: "",
      cantidad: "",
      responsable: "",
      ciudad: "",
      direccion: "",
      fechaIngreso: "",
      archivo: "",
      observaciones: "",
      proveedor: "",
      isComercial: "",
      name_producto: "",
      dose: "",
    },
    validateOnChange: true,
    validationSchema: formSchema,
    onSubmit: async (values) => {
      const { _icons } = props.modalReducer;
      props.set_modal_on(
        new InfoModal(
          "El ingreso ha sido creado satisfactoriamente",
          _icons.CHECK_HORANGE
        )
      );
    },
  });

  const {
    data: DoseByProduct,
    isLoading: isLoadingDose,
    refetch: refetchDose,
  } = useDose(formik.values.producto, {
    refetchOnWindowFocus: false,
    retry: false,
  });

  useEffect(() => {
    if (formik.values.producto !== "") {
      refetchDose();
    }
    //eslint-disable-next-line
  }, [formik.values.producto]);

  return (
    <form id="form" onSubmit={formik.handleSubmit} className="pr-3 py-3">
      <FormField
        label="Número de remisión"
        number="1"
        name="numeroRemision"
        checked={formik.values.numeroRemision}
      >
        <input
          className="input cool-input"
          type="text"
          name="numeroRemision"
          onChange={formik.handleChange}
          value={formik.values.numeroRemision}
        />
        {formik.touched.numeroRemision && formik.errors.numeroRemision ? (
          <div className="help is-danger"> {formik.errors.numeroRemision}</div>
        ) : null}
      </FormField>

      <FormField
        label="Número de referencia"
        number="2"
        name="numeroReferencia"
        checked={formik.values.numeroReferencia}
      >
        <input
          className="input cool-input"
          type="text"
          name="numeroReferencia"
          onChange={formik.handleChange}
          value={formik.values.numeroReferencia}
        />
        {formik.touched.numeroReferencia && formik.errors.numeroReferencia ? (
          <div className="help is-danger">
            {" "}
            {formik.errors.numeroReferencia}
          </div>
        ) : null}
      </FormField>
      <FormField2
        number1="3"
        number2="3.1"
        checked1={formik.values.isComercial}
        label1="¿Es un medicamento sin valor comercial?"
        label2={formik.values.isComercial === "No" && "Nombre del medicamento"}
        input1={
          <div>
            <Radio
              name="isComercial"
              onChange={formik.handleChange}
              value={formik.values.isComercial}
            />
            {formik.touched.isComercial && formik.errors.isComercial ? (
              <div className="help is-danger">{formik.errors.isComercial}</div>
            ) : null}
          </div>
        }
        input2={
          <div>
            {formik.values.isComercial === "No" ? (
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="name_producto"
                  onChange={formik.handleChange}
                  value={formik.values.name_producto}
                />
                {formik.touched.name_producto && formik.errors.name_producto ? (
                  <div className="help is-danger">
                    {formik.errors.name_producto}
                  </div>
                ) : null}
              </div>
            ) : null}
          </div>
        }
      />

      <FormField2
        number1="4"
        number2="4.1"
        name1="producto"
        name2="dose"
        checked1={formik.values.producto}
        checked2={formik.values.dose}
        label1="Producto"
        label2="Dosis"
        checked={formik.values.producto}
        input1={
          <div>
            <Dropdown
              name="producto"
              onChange={formik.handleChange}
              options={
                formik.values.isComercial === "Si"
                  ? ["Adempas", "Xofigo"]
                  : formik.values.isComercial === "No"
                  ? AllProducts?.data
                  : []
              }
              arrowColor="has-text-horange"
              disabled={
                formik.values.isComercial !== ""
                  ? formik.values.isComercial === "Si"
                    ? isLoadingAllProducts
                    : formik.values.isComercial === "No"
                    ? isLoadingAllProducts
                    : null
                  : true
              }
            />
            {formik.touched.producto && formik.errors.producto ? (
              <div className="help is-danger"> {formik.errors.producto}</div>
            ) : null}
          </div>
        }
        input2={
          <div>
            <Dropdown
              name="dose"
              onChange={formik.handleChange}
              options={DoseByProduct ? DoseByProduct?.data : []}
              arrowColor="has-text-horange"
              disabled={
                formik.values.isComercial === "Si"
                  ? !isLoadingDose && !DoseByProduct
                  : true
              }
            />
            {formik.touched.dose && formik.errors.dose ? (
              <div className="help is-danger"> {formik.errors.dose}</div>
            ) : null}
          </div>
        }
      />
      <FormField
        label="Cantidad"
        number="5"
        name="cantidad"
        checked={formik.values.cantidad}
      >
        <Dropdown
          name="dose"
          onChange={formik.handleChange}
          options={[
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
            37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
          ]}
          arrowColor="has-text-horange"
          value={formik.values.cantidad}
        />
        {formik.touched.cantidad && formik.errors.cantidad ? (
          <div className="help is-danger"> {formik.errors.cantidad}</div>
        ) : null}
      </FormField>

      <FormField
        label="Responsable"
        number="6"
        name="responsable"
        checked={formik.values.responsable}
      >
        <input
          className="input cool-input"
          type="text"
          name="responsable"
          onChange={formik.handleChange}
          value={formik.values.responsable}
        />
        {formik.touched.responsable && formik.errors.responsable ? (
          <div className="help is-danger"> {formik.errors.responsable}</div>
        ) : null}
      </FormField>

      <FormField2
        number1="7"
        number2="7.1"
        name1="ciudad"
        name2="direccion"
        checked1={formik.values.ciudad}
        checked2={formik.values.tipoLlamada}
        label1="Ciudad Bodega"
        label2="Ubicación Bodega"
        input1={
          <div>
            <Dropdown
              name="ciudad"
              onChange={formik.handleChange}
              options={["asd"]}
              arrowColor="has-text-horange"
            />
            {formik.touched.ciudad && formik.errors.ciudad ? (
              <div className="help is-danger"> {formik.errors.ciudad}</div>
            ) : null}
          </div>
        }
        input2={
          <div>
            <input
              className="input cool-input"
              type="text"
              name="direccion"
              onChange={formik.handleChange}
              value={formik.values.direccion}
            />
            {formik.touched.direccion && formik.errors.direccion ? (
              <div className="help is-danger"> {formik.errors.direccion}</div>
            ) : null}
          </div>
        }
      />

      <FormField
        label="Fecha de ingreso"
        number="8"
        name="responsable"
        checked={formik.values.responsable}
      >
        <input
          className="input cool-input"
          type="date"
          name="responsable"
          onChange={formik.handleChange}
          value={formik.values.responsable}
        />
        {formik.touched.responsable && formik.errors.responsable ? (
          <div className="help is-danger"> {formik.errors.responsable}</div>
        ) : null}
      </FormField>
      <FormField
        label="Proveedor"
        number="9"
        name="proveedor"
        checked={formik.values.proveedor}
      >
        <input
          className="input cool-input"
          type="text"
          name="proveedor"
          onChange={formik.handleChange}
          value={formik.values.proveedor}
        />
        {formik.touched.proveedor && formik.errors.proveedor ? (
          <div className="help is-danger"> {formik.errors.proveedor}</div>
        ) : null}
      </FormField>

      <FormField
        label="Foto producto"
        number="10"
        name="archivo"
        checked={formik.values.archivo}
      >
        <input
          type="file"
          name="archivo"
          onChange={formik.handleChange}
          value={formik.values.archivo}
          accept=".JPG"
        />
        {formik.touched.archivo && formik.errors.archivo ? (
          <div className="help is-danger"> {formik.errors.archivo}</div>
        ) : null}
      </FormField>

      <FormField
        label="Observaciones"
        number="11"
        name="observaciones"
        checked={formik.values.observaciones}
        noline
      >
        <input
          className="input cool-input"
          type="text"
          name="observaciones"
          onChange={formik.handleChange}
          value={formik.values.observaciones}
        />
        {formik.touched.observaciones && formik.errors.observaciones ? (
          <div className="help is-danger"> {formik.errors.observaciones}</div>
        ) : null}
      </FormField>
      <br />
      <div className="field">
        <div className="control has-text-right has-text-centered-mobile">
          <button
            className="button is-horange"
            type="submit"
            style={{ width: "150px" }}
          >
            CREAR
          </button>
        </div>
      </div>
    </form>
  );
};
const mapStateToPros = (state) => ({
  modalReducer: state.modalReducer,
});

export default connect(mapStateToPros, {
  set_modal_on,
})(Form);
