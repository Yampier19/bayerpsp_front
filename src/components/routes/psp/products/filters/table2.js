import {Link} from 'react-router-dom';
import DataTable from 'components/base/datatable'
const TableDiv2 = props => {

    const columns = [
        { name: "no_egreso", label: "No. EGRESO", options: { filter: true, sort: true } },
        { name: "referencia", label: "No. REFERENCIA", options: { filter: true, sort: true } },
        { name: "producto", label: "PRODUCTO", options: { filter: true, sort: true } },
        { name: "cantidad_egresada", label: "CANTIDAD EGRESADA", options: { filter: true, sort: true } },
        { name: "responsable", label: "RESPONSABLE", options: { filter: true, sort: true } },
        { name: "bodega", label: "CIUDAD BODEGA", options: { filter: true, sort: true } },
        { name: "ubicacion", label: "UBICACIÓN", options: { filter: true, sort: true } },
        { name: "egreso", label: "EGRESO", options: { filter: true, sort: true } },
        { name: "img_producto", label: "IMAGEN PRODUCTO", options: { filter: true, sort: true } },
        { name: "observaciones", label: "OBSERVACIONES", options: { filter: true, sort: true } },
        { name: "opciones", label: "OPCIONES", options: { filter: true, sort: true } },
      ];
    
      const data = [
        {
            no_egreso: "0001",
            referencia: "0001",
            producto: "XARELTO",
            cantidad_egresada: "02",
            responsable: "doctor",
            bodega: "bayer",
            ubicacion: "bogota",
            egreso: "04/11/2021",
            proveedor: "dental",
            img_producto: "Sin imagen",
            observaciones: "Ninguno",
            opciones: "EN DISTRIBUICION\nRASTREAR"
        },
        {
            no_egreso: "0001",
            referencia: "0001",
            producto: "XARELTO",
            cantidad_egresada: "02",
            responsable: "doctor",
            bodega: "bayer",
            ubicacion: "bogota",
            egreso: "04/11/2021",
            proveedor: "dental",
            img_producto: "Sin imagen",
            observaciones: "Ninguno",
            opciones: "EN DISTRIBUICION\nRASTREAR"
        },
     
      ];

    return(
        <div className="columns is-mobile">
            <div className="column is-1 is-hidden-tablet has-text-centered" style={{minWidth: '60px'}}></div>
                <div className="column">
                    <div className="columns is-mobile">
                        <div className="column is-1 is-hidden-tablet has-text-centered" style={{minWidth: '60px'}}></div>
                            <div className="column mx-auto">
                            <DataTable data={data} columns={columns} title={"Listado de egresos"} color="#E7967A"/>
                            </div>
                        <div className="column is-1 is-hidden-mobile" style={{maxWidth: '120px'}}></div>
                    </div>
                </div>
            <div className="column is-1 is-hidden-mobile" style={{maxWidth: '120px'}}></div>
        </div>
    );
}

export default TableDiv2;
