import {Link} from 'react-router-dom';
import DataTable from 'components/base/datatable'
const TableDiv3 = props => {

    const columns = [
        { name: "remision", label: "No. REMISIÓN", options: { filter: true, sort: true } },
        { name: "referencia", label: "No. REFERENCIA", options: { filter: true, sort: true } },
        { name: "producto", label: "PRODUCTO", options: { filter: true, sort: true } },
        { name: "cantidad", label: "CANTIDAD", options: { filter: true, sort: true } },
        { name: "responsable", label: "RESPONSABLE", options: { filter: true, sort: true } },
        { name: "bodega", label: "CIUDAD BODEGA", options: { filter: true, sort: true } },
        { name: "ubicacion", label: "UBICACIÓN", options: { filter: true, sort: true } },
        { name: "devolucion", label: "DEVOLUCIÓN", options: { filter: true, sort: true } },
        { name: "devolver", label: "¿QUIÉN DEVUELVE?", options: { filter: true, sort: true } },
        { name: "razon_devolucion", label: "RAZÓNO DE LA DEVOLUCIÓN", options: { filter: true, sort: true } },
        { name: "img_producto", label: "IMAGEN PRODUCTO", options: { filter: true, sort: true } },
      ];
    
      const data = [
        {
            remision: "0001",
            referencia: "0001",
            producto: "XARELTO",
            cantidad: "02",
            responsable: "doctor",
            bodega: "bayer",
            ubicacion: "bogota",
            devolucion: "devolucion",
            devolver: "doctor",
            razon_devolucion: "dental",
            img_producto: "Sin imagen",
        },
        {
            remision: "0001",
            referencia: "0001",
            producto: "XARELTO",
            cantidad: "02",
            responsable: "doctor",
            bodega: "bayer",
            ubicacion: "bogota",
            devolucion: "devolucion",
            devolver: "doctor",
            razon_devolucion: "dental",
            img_producto: "Sin imagen",
        },
     
      ];

    return(
        <div className="columns is-mobile">
            <div className="column is-1 is-hidden-tablet has-text-centered" style={{minWidth: '60px'}}></div>
                <div className="column">
                    <div className="columns is-mobile">
                        <div className="column is-1 is-hidden-tablet has-text-centered" style={{minWidth: '60px'}}></div>
                            <div className="column mx-auto">
                            <DataTable data={data} columns={columns} title={"Listado de devoluciones"} color="#E7967A"/>
                            </div>
                        <div className="column is-1 is-hidden-mobile" style={{maxWidth: '120px'}}></div>
                    </div>
                </div>
            <div className="column is-1 is-hidden-mobile" style={{maxWidth: '120px'}}></div>
        </div>
    );
}

export default TableDiv3;