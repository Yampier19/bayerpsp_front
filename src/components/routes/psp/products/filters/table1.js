import { Fragment } from "react";
import { When } from "react-if";
import { Unless } from "react-if";

import { useListAllProducts } from "services/bayer/psp/product/useProduct";
import DataTable from "components/base/datatable";
import LoadingComponent from "components/base/loading-component";

const Table = (props) => {
  const { isLoading } = useListAllProducts();
  const columns = [
    {
      name: "remision",
      label: "No. REMISIÓN",
      options: { filter: true, sort: true },
    },
    {
      name: "referencia",
      label: "No. REFERENCIA",
      options: { filter: true, sort: true },
    },
    {
      name: "producto",
      label: "PRODUCTO",
      options: { filter: true, sort: true },
    },
    {
      name: "cantidad",
      label: "CANTIDAD",
      options: { filter: true, sort: true },
    },
    {
      name: "responsable",
      label: "RESPONSABLE",
      options: { filter: true, sort: true },
    },
    {
      name: "bodega",
      label: "CIUDAD BODEGA",
      options: { filter: true, sort: true },
    },
    {
      name: "ubicacion",
      label: "UBICACIÓN",
      options: { filter: true, sort: true },
    },
    {
      name: "ingreso",
      label: "INGRESO",
      options: { filter: true, sort: true },
    },
    {
      name: "proveedor",
      label: "PROVEEDOR",
      options: { filter: true, sort: true },
    },
    {
      name: "img_producto",
      label: "IMAGEN PRODUCTO",
      options: { filter: true, sort: true },
    },
    {
      name: "observaciones",
      label: "OBSERVACIONES",
      options: { filter: true, sort: true },
    },
  ];

  const data = [
    {
      remision: "0001",
      referencia: "0001",
      producto: "XARELTO",
      cantidad: "02",
      responsable: "doctor",
      bodega: "bayer",
      ubicacion: "bogota",
      ingreso: "04/11/2021",
      proveedor: "dental",
      img_producto: "Sin imagen",
      observaciones: "Ninguno",
    },
    {
      remision: "0001",
      referencia: "0001",
      producto: "XARELTO",
      cantidad: "02",
      responsable: "doctor",
      bodega: "bayer",
      ubicacion: "bogota",
      ingreso: "04/11/2021",
      proveedor: "dental",
      img_producto: "Sin imagen",
      observaciones: "Ninguno",
    },
    {
      remision: "0001",
      referencia: "0001",
      producto: "XARELTO",
      cantidad: "02",
      responsable: "doctor",
      bodega: "bayer",
      ubicacion: "bogota",
      ingreso: "04/11/2021",
      proveedor: "dental",
      img_producto: "Sin imagen",
      observaciones: "Ninguno",
    },
    {
      remision: "0001",
      referencia: "0001",
      producto: "XARELTO",
      cantidad: "02",
      responsable: "doctor",
      bodega: "bayer",
      ubicacion: "bogota",
      ingreso: "04/11/2021",
      proveedor: "dental",
      img_producto: "Sin imagen",
      observaciones: "Ninguno",
    },
    {
      remision: "0001",
      referencia: "0001",
      producto: "XARELTO",
      cantidad: "02",
      responsable: "doctor",
      bodega: "bayer",
      ubicacion: "bogota",
      ingreso: "04/11/2021",
      proveedor: "dental",
      img_producto: "Sin imagen",
      observaciones: "Ninguno",
    },
  ];

  return (
    <Fragment>
      <When condition={!isLoading}>
        <div className="columns is-mobile">
          <div
            className="column is-1 is-hidden-tablet has-text-centered"
            style={{ minWidth: "10px" }}
          ></div>
          <div className="column">
            <div className="columns is-mobile">
              <div
                className="column is-1 is-hidden-tablet has-text-centered"
                style={{ minWidth: "10px" }}
              ></div>
              <div className="column">
                <DataTable
                  data={data}
                  columns={columns}
                  title={"Listado de ingresos"}
                  color="#E7967A"
                />
              </div>
              <div
                className="column is-1 is-hidden-mobile"
                style={{ maxWidth: "10px" }}
              ></div>
            </div>
          </div>

          <div
            className="column is-1 is-hidden-mobile"
            style={{ maxWidth: "10px" }}
          ></div>
        </div>
      </When>
      <Unless condition={!isLoading}>
        <LoadingComponent loadingText="Cargando datos..." />
      </Unless>
    </Fragment>
  );
};

export default Table;
