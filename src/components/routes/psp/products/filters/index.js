import React from "react";

import CreateMaterial from "components/base/buttons/create-material";

/* import FilterBar from "components/base/filter-bar"; */

import { tabClicked } from "components/commons/tabs/tabClicked";

import Table1 from "./table1";
import Table2 from "./table2";
import Table3 from "./table3";

import "./main.scss";

const ReportsCreate = (props) => {
  return (
    <div className="is-flex is-flex-direction-column ">
      {/* header */}
      <section className="section px-0 py-0">
        <div className="columns ">
          <div className="column">
            <h1 className="title has-text-horange is-2">Productos</h1>
            <h1 className="subtitle has-text-horange ">Filtros</h1>
          </div>
          <div className="column is-2" style={{ minWidth: "280px" }}>
            <CreateMaterial />
          </div>
        </div>
      </section>

      <br />
      <br />

      {/* tabs */}
      <section className="section p-0 m-0">
        <div className="columns is-vcentered">
          <div className="column">
            <div className="tabs is-boxed productsTabs">
              <ul>
                <li
                  className="formTab is-active mx-2"
                  onClick={tabClicked}
                  data-tab="tab1"
                >
                  {/* eslint-disable-next-line */}
                  <a>Ingresos</a>
                </li>
                <li
                  className="formTab mx-2"
                  onClick={tabClicked}
                  data-tab="tab2"
                >
                  {/* eslint-disable-next-line */}
                  <a>Egresos</a>
                </li>
                <li
                  className="formTab mx-2"
                  onClick={tabClicked}
                  data-tab="tab3"
                >
                  {/* eslint-disable-next-line */}
                  <a>Devoluciones</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <br />
      <br />
      {/* filter bar */}
      {/* <section className="section p-0 pb-1 ">
        <div className="columns">
          <div className="column">
            <FilterBar
              textColorClass="has-text-horange"
              filters={[
                {
                  name: "Fecha",
                  options: [{ name: "op1" }, { name: "op2" }],
                },
                {
                  name: "Producto",
                  options: [],
                },
                {
                  name: "Ciudad",
                  options: [],
                },
                {
                  name: "Cantidad",
                  options: [],
                },
              ]}
              moreFiltersOn
              tags={null}
            />
          </div>
          <div className="column is-2-desktop has-text-right has-text-left-mobile"></div>
        </div>
      </section> */}

      {/* tableS */}
      <section
        className="is-flex-grow-1 "
        style={{ minHeight: "100%", overflow: "hidden" }}
      >
        <div
          id="tab1"
          className="tab is-active py-4"
          style={{ height: "100%", overflow: "hidden" }}
        >
          <div
            className="coolscroll horange"
            style={{ height: "100%", overflow: "auto" }}
          >
            <Table1 />
          </div>
        </div>
        <div
          id="tab2"
          className="tab py-4"
          style={{ height: "100%", overflowY: "hidden" }}
        >
          <div
            className="coolscroll horange"
            style={{ height: "100%", overflow: "auto" }}
          >
            <Table2 />
          </div>
        </div>
        <div
          id="tab3"
          className="tab py-4"
          style={{ height: "100%", overflowY: "hidden" }}
        >
          <div
            className="coolscroll horange"
            style={{ height: "100%", overflow: "auto" }}
          >
            <Table3 />
          </div>
        </div>
      </section>
    </div>
  );
};

export default ReportsCreate;
