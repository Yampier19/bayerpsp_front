import React from "react";
import { When } from "react-if";

import MenuCard from "../../../cards/home/menucard";
import CreateBtn from "../../../base/buttons/create-btn";
import { useAuthorizationAction } from "providers/authorization/action/authorizationActionProvider";

const Home = (props) => {
  const {
    canNoveltysConsultView,
    canTrackingSearchView,
    canProductsInventoryView,
    canReportsListView,
  } = useAuthorizationAction();
  return (
    <div
      className="is-flex is-flex-direction-column "
      style={{ height: "100%" }}
    >
      {/* header */}
      <section className="">
        <h1 className="title has-text-primary is-3">Aplicación de PSP</h1>
        <h2 className="subtitle has-text-primary">
          Programa de seguimiento de pacientes
        </h2>
      </section>

      {/* first level */}
      <div className="columns is-mobile">
        <div className="column is-3-desktop is-offset-9-desktop is-6-mobile is-offset-6-mobile">
          <CreateBtn />
        </div>
      </div>

      <br />

      {/* second level */}
      <div className="columns is-multiline">
        <When condition={canNoveltysConsultView}>
          <div className="column mb-6 is-6-desktop is-12-touch is-3-widescreen">
            <div className="notification py-2 is-hgreen">
              <h1 className="subtitle is-6">NOVEDADES</h1>
            </div>
            <MenuCard
              title="Consultar Novedad"
              color="hgreen"
              to="/news/consult"
              search="?page=1"
            />
          </div>
        </When>
        <When condition={canTrackingSearchView}>
          <div className="column mb-6 is-6-desktop is-12-touch is-3-widescreen">
            <div className="notification py-2 is-hblue">
              <h1 className="subtitle is-6">SEGUIMIENTO</h1>
            </div>
            <MenuCard
              title="Consultar Seguimiento"
              color="hblue"
              to="/tracking"
            />
          </div>
        </When>
        <When condition={canProductsInventoryView}>
          <div className="column mb-6 is-6-desktop is-12-touch is-3-widescreen">
            <div className="notification py-2 is-horange">
              <h1 className="subtitle is-6">PRODUCTOS</h1>
            </div>
            <MenuCard title="Inventario" color="horange" to="/products" />
          </div>
        </When>
        <When condition={canReportsListView}>
          <div className="column mb-6 is-6-desktop is-12-touch is-3-widescreen">
            <div className="notification py-2 is-hred">
              <h1 className="subtitle is-6">REPORTES</h1>
            </div>
            <MenuCard title="Todos los reportes" color="hred" to="/reports" />
            <MenuCard title="Gráficas" color="hred" to="/reports/graphics" />
          </div>
        </When>
      </div>
    </div>
  );
};

export default Home;
