const FormField3 = props => {

    return(
        <div className="field2 mb-6">
            <div className="columns " style={{minHeight: '100px'}}>
                <div className="row-reverse">
                <div className="column">
                    <div className="columns  is-mobile">
                        <div className="column">
                            <div className="columns ">
                                <div className="column is-4">
                                     <label className="label">{props.label1}</label>
                                </div>
                                <div className="column">
                                    <div className="control">
                                        {props.input1}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div className="column">
                    <div className="columns  is-mobile">

                        <div className="column">
                            <div className="columns ">
                                <div className="column is-4">
                                     <label className="label">{props.label2}</label>
                                </div>
                                <div className="column">
                                    <div className="control">
                                        {props.input2}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                </div>
                
            </div>
           
            
        </div>
    );
}

export default FormField3;
