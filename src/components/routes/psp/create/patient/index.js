import { useState, useRef } from "react";

import CreateBtn from "components/base/buttons/create-btn";

import ProgressIcon from "components/base/progress-icon";
import ProgressBar from "components/base/progress-bar";

import { tabClicked } from "components/commons/tabs/tabClicked";
import { Search } from "components/base/search";
import TreatmentInfo from "components/routes/psp/create/patient/treatments"

import Form1 from "./form1";
import Form2 from "./form2";
import {
  formShape,
  fetchedDataShape,
  stepShape,
  FormCreatePatientContext,
} from "./context/form-context";
//import {formik} from './formData';

import "./tabs.scss";
import { FormCreatePatient } from "./patient-form";
import { FormCreateTreatment } from "./treatment-form";
import { SearchedPatientProvider } from "./context/searched-patient-context";

const CreatePatient = ({}) => {
  /* *~~*~~*~~*~~*~~*~~*~~*~ Context ~*~~*~~*~~*~~*~~*~~*~~*~~* */
  const [form, setForm] = useState(formShape);
  // const setForm = (value = formShape) => setform(value)
  const [step, setstep] = useState(stepShape);
  const setStep = (value = stepShape) => {
    if (value.current && value.current !== step.current)
      return setstep({ ...value, percent: 0 });
    setstep({ ...step, ...value });
  };
  const [fetchedData, setfetchedData] = useState(fetchedDataShape);
  const setFetchedData = (value = fetchedDataShape) => {
    const _values = Object.assign({}, value);
    if (process.env.NODE_ENV === "development") {
      Object.keys(value).forEach((key) => {
        const val = value[key];
        if (Array.isArray(val)) {
          if (val.length === 0) {
            _values[key] = [1, 2, 3, 4, 5, 6];
          }
        }
      });
    }
    setfetchedData((prevState) => ({ ...prevState, ..._values }));
  };
  const [country, setCountry] = useState("");
  const handleSubmit = (e) => {
    switch (step.current) {
      case "1":
        break;

      default:
        break;
    }
  };
  /* *~~*~~*~~*~~*~~*~~*~~*~ Context ~*~~*~~*~~*~~*~~*~~*~~*~~* */
  const refTabForm2 = useRef(null);
  const submitPatient = () => {
    tabClicked(refTabForm2);
    setStep({ current: 2 });
    refTabForm2.current.click();
  };

  return (
    <SearchedPatientProvider>
      <FormCreatePatientContext.Provider
        value={{
          form,
          setForm,
          step,
          setStep,
          country,
          setCountry,
          fetchedData,
          setFetchedData,
          handleSubmit,
          submitPatient,
        }}
      >
        <div
          className="is-flex is-flex-direction-column "
          style={{ height: "100%" }}
        >
          {/* header */}
          <section className="">
            <div className="columns is-mobile">
              <div className="column is-3-desktop ">
                <h1
                  className="subtitle has-text-primary is-3 mb-1"
                  style={{ whiteSpace: "nowrap" }}
                >
                  <strong className="has-text-primary">CREAR</strong>
                  <span className="is-hidden-touch"> - PACIENTE NUEVO</span>
                </h1>
                <h1 className="subtitle has-text-primary is-hidden-desktop is-6">
                  Paciente nuevo
                </h1>
              </div>
              <div className="column is-3-desktop is-offset-6-desktop is-6-mobile">
                <CreateBtn active="2" />
              </div>
            </div>
          </section>

          <hr className="has-background-dark is-hidden-touch" />

          {/* tabs */}
          <section className="section p-0 m-0 px-1">
            <div className="columns is-vcentered">
              <div className="column">
                <div className="tabs is-boxed createTab">
                  <ul>
                    <li
                      className="formTab is-active"
                      onClick={(e) => {
                        tabClicked(e);
                        setStep({ current: 1 });
                      }}
                      data-tab="tab1"
                    >
                      {/* eslint-disable-next-line */}
                      <a href="#">
                        <ProgressIcon
                          className="has-background-primary has-text-white"
                          value={step.current == 1 ? step.percent : 0}
                          color="#519EA1"
                          icon={<i class="fas fa-user"></i>}
                        />
                        &nbsp;
                        <span className="is-hidden-touch">General</span>
                      </a>
                    </li>
                    <li
                      ref={refTabForm2}
                      className="formTab"
                      onClick={(e) => {
                        if (
                          step.current === 1 &&
                          process.env.NODE_ENV !== "development"
                        )
                          return null;
                        tabClicked(e);
                        setStep({ current: 2 });
                      }}
                      data-tab="tab2"
                    >
                      <a>
                        <ProgressIcon
                          className="has-background-primary has-text-white"
                          value={step.current == 2 ? step.percent : 0}
                          color="#519EA1"
                          icon={<i class="fas fa-pills"></i>}
                        ></ProgressIcon>{" "}
                        &nbsp;
                        <span className="is-hidden-mobile">
                          Información de tratamiento 
                        </span>
                      </a>
                    </li>
                    <li className="formTab">
                      <a data-tab="tab5"  onClick={(e) => {
                      
                        tabClicked(e);
                       
                      }}><i class="fas fa-exclamation-triangle" ></i>
                      </a>
                    </li>
                    <li className="formTab">
                      <a>
                        <ProgressIcon
                          className="has-background-primary has-text-white"
                          value={step.current == 2 ? step.percent : 0}
                          color="#519EA1"
                          icon={<i class="fas fa-search"></i>}
                        ></ProgressIcon>{" "}
                        &nbsp;
                        <Search />
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="column is-hidden-desktop">
                <span className="title has-text-primary is-5">
                  {step.current === 1
                    ? "General"
                    : "Información del tratamiento"}
                </span>
              </div>

              <div className="column is-3">
                <ProgressBar
                  className="has-background-primary"
                  value={step.percent}
                ></ProgressBar>
              </div>
            </div>
          </section>

          {/* forms */}
          <section
            className="section px-0 is-flex-grow-1 py-4 coolscroll"
            style={{ height: "1px", overflow: "hidden", minHeight: "500px" }}
          >
            <div id="tab1" className="tab is-active" style={{ height: "100%" }}>
              <div
                className="coolscroll primary"
                style={{ height: "100%", overflow: "auto" }}
              >
                <FormCreatePatient />
                {/* <Form1
                  onSubmitCallback={(e) => {
                    tabClicked(refTabForm2);
                    setStep({ current: 2 })
                    refTabForm2.current.click();
                  }}
                /> */}
              </div>
            </div>

            <div id="tab2" className="tab" style={{ overflowY: "scroll" }}>
              <div
                className="coolscroll primary"
                style={{ height: "100%", overflow: "auto" }}
              >
                {/* <Form2 /> */}
                <FormCreateTreatment />
              </div>
            </div>

            <div id="tab5" className="tab" style={{ overflowY: "scroll" }}>
              <div
                className="coolscroll primary"
                style={{ height: "100%", overflow: "auto" }}
              >
                {/* <Form2 /> */}
                <TreatmentInfo />
              </div>
            </div>
          </section>
        </div>
      </FormCreatePatientContext.Provider>
    </SearchedPatientProvider>
  );
};

export default CreatePatient;
