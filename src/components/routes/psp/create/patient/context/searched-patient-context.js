import _ from "lodash";
import React, { useContext, createContext, useState } from "react";
import { formShape } from "./form-context";

const { treatment: _treatment, patient: _patient } = formShape;

export const SearchedPatientContext = createContext({
  patient: _patient,
  treatments: [_treatment],
  setData: () => {},
  isSearched: false,
  setTreatment: (treatment=_treatment) => {},
  treatment: _treatment,
});

export const useSearchedPatientContext = () =>
  useContext(SearchedPatientContext);

export const SearchedPatientProvider = ({ children }) => {
  const [patient, setPatient] = useState(_patient);
  const [treatments, setTreatments] = useState([_treatment]);
  const [isSearched, setIsSearched] = useState(false);
  const [treatment, setTreatment] = useState(null);
  const setData = (data) => {
    console.log("obtained data", data);
    setPatient(data);
    setTreatments(data.treatments);
    setIsSearched(true);
  };
  return (
    <SearchedPatientContext.Provider
      value={{
        patient,
        treatments,
        setData,
        isSearched,
        treatment,
        setTreatment,
      }}
      children={children}
    />
  );
};
