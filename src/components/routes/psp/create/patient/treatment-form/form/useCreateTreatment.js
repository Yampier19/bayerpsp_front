import React, { useState, useEffect, useContext } from "react";
import { useFormikContext } from "formik";

import { useCreateTreatmentFormUtil } from "../components/form-create-treatment-util";
import { FormCreatePatientContext } from "../../context/form-context";

import { useListAllProducts } from "services/bayer/psp/product/useProduct";
import {
  useListClaims,
  useNoEducationReason,
  usePathologicalClassificationByProduct,
  useDose,
  useAllTreatments,
  useListLogisticOperatorsByEnsurance,
  useListAllDoctors,
  usePreviusTreatments,
  useEducationThemes,
  useTreatmentStatus,
  useListInsuranceByDepartmentId,
  useListZones,
  useListCitiesByZone,
} from "services/bayer/psp/patient/usePatient";
import { setFetchedDataMiddleware } from "../../setFetchedDataMiddleware";

import Radio from "components/form/radio";
import Dropdown from "components/form/dropdown";
import { getRequired } from "utils/forms";
import { useSearchedPatientContext } from "../../context/searched-patient-context";

const createArrayOfNumbers = (min = 0, max = 10) => {
  return Array(max)
    .fill()
    .map((_, idx) => idx + min);
};

export const useCreateTreatmentForm = () => {
  /* ~~~****** Hooks ********~* */
  const { values, setFieldValue, errors } = useFormikContext();
  const { validationSchema } = useCreateTreatmentFormUtil();
  const { setStep, step, form } = useContext(FormCreatePatientContext);
  const { isSearched, treatment } = useSearchedPatientContext();
  const [disabledForm, setdisabledForm] = useState(false);
  const [loadingFields, setLoadingFields] = useState({
    loaded: false,
    values: [
      { name: "treatmentId", realized: false },
      { name: "product_id", realized: false },
      { name: "doctor_id", realized: false },
      { name: "treatment_previou_id", realized: false },
      { name: "zone", realized: false },
    ],
    trigger: false,
  });
  const _setLoadingFields = (_name) => {
    const { values, trigger } = loadingFields;
    const index = values.findIndex(({ name }) => name === _name);
    // if(index === -1)return;
    const _value = values[index];
    _value.realized = true;
    const _loaded =
      values.filter(({ realized }) => realized).length === values.length;
    setLoadingFields((prevState) => ({
      loaded: _loaded,
      values: values,
      trigger: !trigger,
    }));
  };
  /* ~~~****** Hooks ********~* */

  /* ~~~****** Form ********~* */
  const initialForm = [
    {
      fields: 1,
      names: ["treatmentId"],
      labels: ["ID tratamiento"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" readOnly {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["claim", ""],
      labels: ["Reclamo*", ""],
      childs: [(props) => <Radio {...props} />, (props) => null],
    },
    {
      fields: 2,
      names: ["education", ""],
      labels: ["¿Se brindo educacion?*", ""],
      childs: [(props) => <Radio {...props} />, (props) => null],
    },
    {
      fields: 2,
      names: ["number_box", "units"],
      labels: ["Número cajas*", "Unidades*"],
      childs: [
        (props) => (
          <Dropdown
            options={createArrayOfNumbers(0, 50)}
            arrowColor="has-text-primary"
            {...props}
          />
        ),
        (props) => (
          <Dropdown
            options={["Ampolla(s)", "Aplicacion", "Caja(s)"]}
            arrowColor="has-text-primary"
            {...props}
          />
        ),
      ],
    },
    {
      fields: 2,
      names: ["product_id", "product_dose_id"],
      labels: ["Producto*", "Dosis*"],
      childs: [
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["treatment_statu_id", "pathological_classification_id"],
      labels: ["Estado del tratamiento*", "Clasificación patológica*"],
      childs: [
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["treatment_previou_id", "consent"],
      labels: ["Tratamiento previo*", "Consentimiento*"],
      childs: [
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
        (props) => (
          <Dropdown
            options={["Fisico", "Verbal", "Sms", "Chat interactivo"]}
            arrowColor="has-text-primary"
            {...props}
          />
        ),
      ],
    },
    {
      fields: 2,
      names: ["therapy_start_date", "regime"],
      labels: ["Fecha de inicio terapia*", "Regimen*"],
      childs: [
        (props) => (
          <input
            className="input cool-input"
            type="date"
            max={new Date().toISOString().split("T")[0]}
            {...props}
          />
        ),
        (props) => (
          <Dropdown
            options={["Contributivo", "Especial", "Particular", "Subsidiado"]}
            arrowColor="has-text-primary"
            {...props}
          />
        ),
      ],
    },
    {
      fields: 2,
      names: ["insurance_id", "logistic_operator_id"],
      labels: ["Asegurador*", "Operador lógistico*"],
      childs: [
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["last_claim_date", "other_operator"],
      labels: ["Fecha última reclamación", "Otros operadores"],
      childs: [
        (props) => (
          <input
            className="input cool-input"
            type="date"
            max={new Date().toISOString().split("T")[0]}
            {...props}
          />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["delivery_point", "ips"],
      labels: ["Punto de entrega", "IPS que atiende*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["means_acquisition", "date_next_call"],
      labels: ["Medios de adquisición", "Fecha de la próxima llamada*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <input
            className="input cool-input"
            type="date"
            min={new Date().toISOString().split("T")[0]}
            {...props}
          />
        ),
      ],
    },
    {
      fields: 2,
      names: ["doctor_id", "specialty_id"],
      labels: ["Médico*", "Especialidad*"],
      childs: [
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["paramedic", "zone"],
      labels: [
        "Paramédico o representante",
        "Zona de atención paramédico o representante",
      ],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["code", "city_id"],
      labels: ["Código", "Ciudad base paramédico o representante"],
      childs: [
        (props) => (
          <Dropdown
            options={createArrayOfNumbers(1, 12)}
            arrowColor="has-text-primary"
            {...props}
          />
        ),
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["patient_part_PAAP", "add_application_information"],
      labels: ["Generar requerimiento*", "Agregar Información aplicaciones"],
      childs: [
        (props) => <Radio {...props} />,
        (props) => <Radio {...props} />,
      ],
    },
    {
      fields: 1,
      names: ["shipping_type"],
      labels: ["Tipo de envio*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 1,
      names: ["notes"],
      labels: ["Notas"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
  ];
  const [Form, setForm] = useState({
    form: initialForm,
    changed: false,
  });
  /* ~~~****** Form ********~* */

  /* ~~~****** Utils ********~* */
  const setDropdownOptions = (name = "", options = []) => {
    const { form, changed } = Form;
    const index = form.findIndex(({ names }) => names.includes(name));
    if (index === -1) return;
    const formCopy = form;
    const childIndex = formCopy[index].names.indexOf(name);
    const Child = formCopy[index].childs[childIndex];
    const NewChlid = (otherProps = {}) =>
      React.createElement(Dropdown, {
        ...Child().props,
        disabled: false,
        options: options,
        ...otherProps,
      });
    formCopy[index].childs[childIndex] = NewChlid;
    setForm({ form: formCopy, changed: !changed });
  };
  const setDisabledDropdown = (name = "") => {
    const { form, changed } = Form;
    const index = form.findIndex(({ names }) => names.includes(name));
    if (index === -1) return;
    const formCopy = form;
    const childIndex = formCopy[index].names.indexOf(name);
    const Child = formCopy[index].childs[childIndex];
    const NewChlid = (otherProps = {}) =>
      React.createElement(Dropdown, {
        ...Child().props,
        disabled: true,
        ...otherProps,
      });
    formCopy[index].childs[childIndex] = NewChlid;
    setForm({ form: formCopy, changed: !changed });
  };
  const setYesNoChildForm = (
    name = "",
    Childs = [{ name: "", label: "", NewComponent: () => {}, position: 0 }],
    fields = 2
  ) => {
    const { form, changed } = Form;
    const index = form.findIndex(({ names }) => names.includes(name));
    if (index === -1) return;
    const formCopy = form;
    formCopy[index].fields = fields;
    Childs.forEach(({ NewComponent, label, name, position }) => {
      if (name) formCopy[index].names[position] = name;
      if (label) formCopy[index].labels[position] = label;
      formCopy[index].childs[position] = NewComponent;
    });
    setForm({ form: formCopy, changed: !changed });
  };
  // const appendFieldInIndex = (field = {}, index = 0) => {
  //   console.log("apended in index", index);
  //   if (index === 0) return;
  //   const { form, changed } = Form;
  //   const formCopy = form;
  //   formCopy.splice(index - 1, 0, field);
  //   setForm({ form: formCopy, changed: !changed });
  // };
  // const deleteFieldInIndex = (index = 0) => {
  //   if (index === 0) return;
  //   const { form, changed } = Form;
  //   const formCopy = form;
  //   formCopy.splice(index - 1, 1);
  //   setForm({ form: formCopy, changed: !changed });
  // };
  const reFetchQueryConfig = {
    enabled: false,
    refetchOnWindowFocus: false,
    timeout: 1000,
  };
  // const queryConfig = {
  //   refetchOnWindowFocus: false,
  //   refetchOnmount: false,
  //   refetchOnReconnect: true,
  //   retry: false,
  // };
  /* ~~~****** Utils ********~* */

  /* ~~~****** React-query ********~* */
  const { refetch: refetchProducts } = useListAllProducts(reFetchQueryConfig);
  const { refetch: refetchClaims } = useListClaims(reFetchQueryConfig);
  const { refetch: refetchNoEducationReasons } =
    useNoEducationReason(reFetchQueryConfig);
  const { refetch: refetchTreatments } = useAllTreatments(reFetchQueryConfig);
  const { refetch: refetchDoctors } = useListAllDoctors(reFetchQueryConfig);
  const { refetch: refetchPreviousTreatments } =
    usePreviusTreatments(reFetchQueryConfig);
  const { refetch: refetchEducationThemes } =
    useEducationThemes(reFetchQueryConfig);
  const { refetch: refreshDoses } = useDose(
    values.product_id,
    reFetchQueryConfig
  );
  const { refetch: refetchOperators } = useListLogisticOperatorsByEnsurance(
    values.insurance_id,
    reFetchQueryConfig
  );
  const { refetch: refreshTreatmentStatus } = useTreatmentStatus(
    values.product_id,
    reFetchQueryConfig
  );
  const { refetch: refreshPathologicalClassification } =
    usePathologicalClassificationByProduct(
      values.product_id,
      reFetchQueryConfig
    );
  const { refetch: refetchInsurance } = useListInsuranceByDepartmentId(
    form.patient.department_id,
    reFetchQueryConfig
  );
  const { refetch: refetchZones } = useListZones(1, reFetchQueryConfig);
  const { refetch: refetchCities } = useListCitiesByZone(
    values.zone,
    reFetchQueryConfig
  );
  /* ~~~****** React-query ********~* */

  /* ~~~****** useEffect ********~* */
  const Yes = "Si",
    No = "No";
  useEffect(() => {
    refetchTreatments().then(({ data, isSuccess }) => {
      if (isSuccess && data.code === 200) {
        setFieldValue(
          "treatmentId",
          setFetchedDataMiddleware(data.data).length + 1
        );
        _setLoadingFields("treatmentId");
      }
    });
  }, []);
  useEffect(() => {
    refetchProducts().then(({ data, isSuccess }) => {
      if (isSuccess && data.code === 200) {
        setDropdownOptions("product_id", setFetchedDataMiddleware(data.data));
        _setLoadingFields("product_id");
      }
    });
  }, []);
  useEffect(() => {
    refetchDoctors().then(({ data, isSuccess }) => {
      if (isSuccess && data.code === 200) {
        setDropdownOptions("doctor_id", setFetchedDataMiddleware(data.data));
        _setLoadingFields("doctor_id");
      }
    });
  }, []);
  useEffect(() => {
    refetchPreviousTreatments().then(({ data, isSuccess }) => {
      if (isSuccess && data.code === 200) {
        setDropdownOptions(
          "treatment_previou_id",
          setFetchedDataMiddleware(data.data)
        );
        _setLoadingFields("treatment_previou_id");
      }
    });
  }, []);
  useEffect(() => {
    refetchZones().then(({ data, isSuccess }) => {
      if (isSuccess && data.code === 200) {
        setDropdownOptions("zone", setFetchedDataMiddleware(data.data));
        _setLoadingFields("zone");
      }
    });
  }, []);
  useEffect(() => {
    if (values.product_id) {
      setDisabledDropdown("product_dose_id");
      refreshDoses().then(({ data, isSuccess }) => {
        if (isSuccess && data.code === 200) {
          setDropdownOptions(
            "product_dose_id",
            setFetchedDataMiddleware(
              data.data.map((x) => ({ id: x.id, name: x.dose }))
            )
          );
          if(!isSearched) {setFieldValue("product_dose_id", "");}
        }
      });
    }
  }, [values.product_id]);
  useEffect(() => {
    if (values.product_id) {
      setDisabledDropdown("treatment_statu_id");
      refreshTreatmentStatus().then(({ data, isSuccess }) => {
        if (isSuccess && data.code === 200) {
          setDropdownOptions(
            "treatment_statu_id",
            setFetchedDataMiddleware(data.data)
          );
        }
      });
    }
  }, [values.product_id]);
  useEffect(() => {
    if (values.product_id) {
      setDisabledDropdown("pathological_classification_id");
      refreshPathologicalClassification().then(({ data, isSuccess }) => {
        if (isSuccess && data.code === 200) {
          setDropdownOptions(
            "pathological_classification_id",
            setFetchedDataMiddleware(data.data)
          );
          if(!isSearched) {setFieldValue("pathological_classification_id", "");}
        }
      });
    }
  }, [values.product_id]);
  useEffect(() => {
    if (values.insurance_id) {
      setDisabledDropdown("logistic_operator_id");
      refetchOperators().then(({ data, isSuccess }) => {
        if (isSuccess && data.code === 200) {
          setDropdownOptions(
            "logistic_operator_id",
            setFetchedDataMiddleware(data.data)
          );
        }
      });
    }
  }, [values.insurance_id]);
  useEffect(() => {
    if (values.doctor_id) {
      setDisabledDropdown("specialty_id");
      refetchDoctors().then(({ data, isSuccess }) => {
        if (isSuccess && data.code === 200 && data.data.length > 0) {
          const doctors = data.data;
          const selectedDoctorIndex = doctors.findIndex(
            (doctor) => doctor.id === Number(values.doctor_id)
          );
          if (selectedDoctorIndex !== -1) {
            const selectedDoctor = doctors[selectedDoctorIndex];
            const especialities = selectedDoctor.specialty;
            setDropdownOptions(
              "specialty_id",
              setFetchedDataMiddleware(especialities)
            );
          }
        }
      });
    }
  }, [values.doctor_id]);
  useEffect(() => {
    if (values.claim) {
      if (values.claim === Yes || values.claim === Yes.toUpperCase()) {
        setYesNoChildForm(
          "claim",
          [
            {
              name: "claim_date",
              label: "Fecha de reclamacion *",
              position: 1,
              NewComponent: (props) => (
                <input
                  className="input cool-input"
                  max={new Date().toISOString().split("T")[0]}
                  type="date"
                  {...props}
                />
              ),
            },
          ],
          2
        );
      }
      if (values.claim === No || values.claim === No.toUpperCase()) {
        setYesNoChildForm(
          "claim",
          [
            {
              name: "cause_no_claim_id",
              label: "Causa no reclamacion*",
              position: 1,
              NewComponent: (props) => (
                <Dropdown
                  options={[]}
                  arrowColor="has-text-primary"
                  {...props}
                />
              ),
            },
          ],
          2
        );
        setDisabledDropdown("cause_no_claim_id");
        refetchClaims().then(({ data, isSuccess }) => {
          if (isSuccess && data.code === 200) {
            setDropdownOptions(
              "cause_no_claim_id",
              setFetchedDataMiddleware(data.data)
            );
          }
        });
      }
    }
  }, [values.claim]);
  useEffect(() => {
    if (values.education) {
      if (values.education === Yes || values.education === Yes.toUpperCase()) {
        setYesNoChildForm(
          "education",
          [
            {
              name: "education_theme_id",
              label: "Tema*",
              position: 1,
              NewComponent: (props) => (
                <Dropdown
                  options={["Hola", "we"]}
                  arrowColor="has-text-primary"
                  {...props}
                />
              ),
            },
            {
              name: "education_date",
              label: "Fecha educación*",
              position: 2,
              NewComponent: (props) => (
                <input
                  className="input cool-input"
                  max={new Date().toISOString().split("T")[0]}
                  type="date"
                  {...props}
                />
              ),
            },
          ],
          3
        );
        setDisabledDropdown("education_theme_id");
        refetchEducationThemes().then(({ data, isSuccess }) => {
          if (isSuccess && data.code === 200) {
            setDropdownOptions(
              "education_theme_id",
              setFetchedDataMiddleware(data.data)
            );
          }
        });
      }
      if (values.education === No || values.education === No.toUpperCase()) {
        setYesNoChildForm(
          "education",
          [
            {
              name: "education_reason_id",
              label: "Motivo*",
              position: 1,
              NewComponent: (props) => (
                <Dropdown
                  options={[]}
                  arrowColor="has-text-primary"
                  {...props}
                />
              ),
            },
          ],
          2
        );
        setDisabledDropdown("education_reason_id");
        refetchNoEducationReasons().then(({ data, isSuccess }) => {
          setDropdownOptions(
            "education_reason_id",
            setFetchedDataMiddleware(data.data)
          );
        });
      }
    }
  }, [values.education]);
  useEffect(() => {
    if (form.patient.department_id) {
      setDisabledDropdown("insurance_id");
      refetchInsurance().then(({ data, isSuccess }) => {
        if (isSuccess && data.code === 200) {
          setDropdownOptions(
            "insurance_id",
            setFetchedDataMiddleware(data.data)
          );
        }
      });
    }
  }, [form.patient.department_id]);
  useEffect(() => {
    if (values.zone) {
      setDisabledDropdown("city_id");
      refetchCities().then(({ data, isSuccess }) => {
        if (isSuccess && data.code === 200) {
          setDropdownOptions("city_id", setFetchedDataMiddleware(data.data));
        }
      });
    }
  }, [values.zone]);
  useEffect(() => {
    if (step.current == 2) {
      const requiredFields = getRequired(validationSchema);
      const totalRequiredFields = requiredFields.length;
      const totalErrors = Object.entries(errors).length;
      const total =
        ((totalRequiredFields - totalErrors) * 100) / totalRequiredFields;
      setStep({ ...step, percent: total > 0 ? total : 0 });
    }
  }, [values, errors]);
  /* ~~~****** useEffect ********~* */

  return { form: Form, isLoading: !loadingFields.loaded };
};
