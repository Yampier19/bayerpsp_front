import React, { useState } from "react";
import { Form, Formik } from "formik";

import { FormButton } from "../patient-form/components/form-button";
import { FormFields } from "./components/form-fields";
import { useCreateTreatmentFormUtil } from "./components/form-create-treatment-util";
import { useSearchedPatientContext } from "../context/searched-patient-context";
import { Treatments } from "../treatments";

export const FormCreateTreatment = () => {
  const { submitForm, validationSchema, initialValues, loadingButton } =
    useCreateTreatmentFormUtil();
  const [isNewTreatment, setIsNewTreatment] = useState(false);
  const { isSearched, treatment } = useSearchedPatientContext();
  if (isSearched && !treatment && !isNewTreatment) {
    return <Treatments setIsNewTreatment={setIsNewTreatment} />;
  }
  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      validationSchema={validationSchema}
      children={
        <Form>
          <FormFields />
          {!isSearched && (
            <FormButton
              loading={loadingButton}
              name={"Registrar".toUpperCase()}
            />
          )}
        </Form>
      }
      onSubmit={submitForm}
    />
  );
};
