import React, { Fragment } from "react";
import { Field, useFormikContext } from "formik";
import FormField from "components/form/formfield";
import FormField2 from "components/form/formfield2";
import LoadingComponent from "components/base/loading-component";
import { useCreateTreatmentForm } from "../form/useCreateTreatment";
import InputMultiple from "../../InputMultiple";
import {useSearchedPatientContext} from '../../context/searched-patient-context'
import { isNull } from "lodash";

export const FormFields = () => {
  const { values } = useFormikContext();
  const { form, isLoading } = useCreateTreatmentForm();
  const { isSearched, treatment } = useSearchedPatientContext();
  const isDisabled = isSearched && !isNull(treatment);
  const disabledProps = isDisabled ? { disabled: isDisabled, readOnly: isDisabled } : {}
  const formLength = form.form.length;
  const isNullyValue = (value) => {
    if (typeof value === "string") {
      return value !== "";
    }
    if (typeof value === "number") {
      return value !== 0;
    }
  };
  if (isLoading) return <LoadingComponent loadingText="Cargando formulario" />;
  return (
    <Fragment>
      {form.form.map((form, index) => {
        const isLastElement = index === formLength-1;
        const number = index + 1;
        if (form.fields === 1) {
          const [name] = form.names;
          const checked = isNullyValue(values[name]);
          const [label] = form.labels;
          const [Child] = form.childs;
          return (
            <FormField
              name={name}
              key={index + "-create-patient"}
              checked={checked}
              label={label}
              number={String(number)}
              noline={isLastElement}
            >
              <Field name={name} id={name}>
                {({ field, form: { touched, errors } }) => (
                  <div>
                    {Child({ ...field, ...disabledProps})}
                    {touched[name] && errors[name] ? (
                      <div className="help is-danger">{errors[name]}</div>
                    ) : null}
                  </div>
                )}
              </Field>
            </FormField>
          );
        }
        if (form.fields === 2) {
          const [name1, name2] = form.names;
          const checked1 = isNullyValue(values[name1]),
            checked2 = isNullyValue(values[name2]);
          const [label1, label2] = form.labels;
          const [Child1, Child2] = form.childs;
          const input1 = (
            <Field name={name1}>
              {({ field, form: { touched, errors } }) => (
                <div>
                  {Child1({ ...field, ...disabledProps })}
                  {touched[name1] && errors[name1] ? (
                    <div className="help is-danger">{errors[name1]}</div>
                  ) : null}
                </div>
              )}
            </Field>
          );
          const input2 = (
            <Field name={name2}>
              {({ field, form: { touched, errors } }) => (
                <div>
                  {Child2({ ...field, ...disabledProps })}
                  {touched[name2] && errors[name2] ? (
                    <div className="help is-danger">{errors[name2]}</div>
                  ) : null}
                </div>
              )}
            </Field>
          );
          return (
            <FormField2
              key={index + "-create-patient"}
              number1={String(number)}
              number2={String(number + 0.1)}
              name1={name1}
              name2={name2}
              checked1={checked1}
              checked2={checked2}
              label1={label1}
              label2={label2}
              input1={input1}
              input2={input2}
              noline={isLastElement}
            />
          );
        }
        if (form.fields === 3) {
          const [name1, name2, name3] = form.names;
          const checked1 = isNullyValue(values[name1]),
            checked2 = isNullyValue(values[name2]),
            checked3 = isNullyValue(values[name3]);
          const [label1, label2, label3] = form.labels;
          const [Child1, Child2, Child3] = form.childs;
          const input1 = (
            <Field name={name1}>
              {({ field, form: { touched, errors } }) => (
                <div>
                  {Child1({ ...field, ...disabledProps })}
                  {touched[name1] && errors[name1] ? (
                    <div className="help is-danger">{errors[name1]}</div>
                  ) : null}
                </div>
              )}
            </Field>
          );
          const input2 = (
            <InputMultiple
              label1={label2}
              label2={label3}
              input1={
                <Field name={name2}>
                  {({ field, form: { touched, errors } }) => (
                    <div>
                      {Child2({ ...field, ...disabledProps })}
                      {touched[name2] && errors[name2] ? (
                        <div className="help is-danger">{errors[name2]}</div>
                      ) : null}
                    </div>
                  )}
                </Field>
              }
              input2={
                <Field name={name3}>
                  {({ field, form: { touched, errors } }) => (
                    <div>
                      {Child3({ ...field, ...disabledProps })}
                      {touched[name3] && errors[name3] ? (
                        <div className="help is-danger">{errors[name3]}</div>
                      ) : null}
                    </div>
                  )}
                </Field>
              }
            />
          );
          return (
            <FormField2
              key={index + "-create-patient"}
              number1={String(number)}
              number2={String(number + 0.1)}
              name1={name1}
              name2={name2}
              checked={checked1}
              checked2={checked2}
              label1={label1}
              label2={""}
              input1={input1}
              input2={input2}
              noline={isLastElement}
            />
          );
        }
      })}
    </Fragment>
  );
};
