import React, { useContext } from "react";
import _, { isNull } from "lodash";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";

import store from "redux/store";
import { set_modal_on } from "redux/actions/modalActions";
import { InfoModal } from "components/base/modal-system/models/modal";
import { ToastUtil } from "components/base/toast/toast-util";
import { Toast } from "components/base/toast/ToastContainer";
import { FormCreatePatientContext } from "../../context/form-context";
import { useCreatePatient } from "services/bayer/psp/patient/usePatient";
import { useSearchedPatientContext } from "../../context/searched-patient-context";
import { useCreateTreatment } from "services/bayer/psp/treatment/useTreatment";

const requiredFieldType = "Campo obligatorio";
export const useCreateTreatmentFormUtil = () => {
  const { setForm, form } = useContext(FormCreatePatientContext);
  // const { treatment} = useSearchedPatientContext();
  const { treatment: _treatment, isSearched, patient } = useSearchedPatientContext();
  const treatment = _treatment ? _treatment : {};
  const { push } = useHistory();

  const { isLoading: loadingPatientCreation, mutateAsync: createPatient } =
    useCreatePatient();
    const { isLoading: loadingTreatment, mutateAsync: createTreatment} = useCreateTreatment()

  const dateParser = (date = "") => {
    if (!date) return "";
    return date.split(" ")[0];
  };
  const idGetter = (objectName = "") => {
    return _.get(treatment, [objectName, "id"], 0);
  };
  const parseStringToInt = (value = "") => {
    if (!value) return 0;
    return parseInt(value);
  };

  const initialValues = {
    treatmentId: treatment.id || "",
    claim: treatment.claim || "",
    education: treatment.education || "",
    number_box: treatment.number_box || "",
    product_id: treatment.product_id || "",
    product_dose_id: idGetter("product_dose"),
    // dose_start: "",
    // patient_statu_id: "",
    pathological_classification_id: treatment.pathological_classification || "",
    treatment_previou_id: idGetter("previous_treatment"),
    consent: treatment.consent || "",
    therapy_start_date: dateParser(treatment.therapy_start_date),
    regime: treatment.regime || "",
    insurance_id: idGetter("insurance"),
    logistic_operator_id: idGetter("logistic_operator"),
    last_claim_date: dateParser(treatment.last_claim_date),
    other_operator: treatment.other_operator || "",
    delivery_point: treatment.delivery_point || "",
    ips: treatment.ips || "",
    means_acquisition: treatment.means_acquisition || "",
    date_next_call: dateParser(treatment.date_next_call),
    doctor_id: treatment.doctor_id || "",
    specialty_id: treatment.specialty,
    paramedic: treatment.paramedic || "",
    zone: treatment.zone || "",
    code: parseStringToInt(treatment.code),
    city_id: idGetter("city"),
    patient_part_PAAP: treatment.patient_part_PAAP || "",
    add_application_information: treatment.add_application_information || "",
    shipping_type: treatment.shipping_type || "",
    cause_no_claim_id: idGetter("cause_no_claim"),
    claim_date: dateParser(treatment.claim_date),
    education_date: dateParser(treatment.education_date),
    education_reason_id: idGetter("education_reason"),
    units: treatment.units || "",
    education_theme_id: idGetter("treatment_statu"),
    treatment_statu_id: idGetter("treatment_statu"),
    notes: treatment.note || "",
  };

  const validationSchema = Yup.object().shape({
    treatmentId: Yup.number().typeError(
      "se debe especificar un valor numerico"
    ),
    claim: Yup.string().required(requiredFieldType),
    education: Yup.string().required(requiredFieldType),
    number_box: Yup.string().required(requiredFieldType),
    product_id: Yup.string().required(requiredFieldType),
    // dose_start: Yup.string().required(requiredFieldType),
    pathological_classification_id: Yup.string().required(requiredFieldType),
    treatment_previou_id: Yup.string().required(requiredFieldType),
    consent: Yup.string().required(requiredFieldType),
    therapy_start_date: Yup.string().required(requiredFieldType),
    regime: Yup.string().required(requiredFieldType),
    insurance_id: Yup.string().required(requiredFieldType),
    logistic_operator_id: Yup.string().required(requiredFieldType),
    last_claim_date: Yup.string(),
    other_operator: Yup.string(),
    delivery_point: Yup.string(),
    means_acquisition: Yup.string(),
    date_next_call: Yup.string().required(requiredFieldType),
    doctor_id: Yup.string().required(requiredFieldType),
    specialty_id: Yup.string().required(requiredFieldType),
    paramedic: Yup.string(), //.required(requiredFieldType),
    zone: Yup.string(), //.required(requiredFieldType),
    code: Yup.string(),
    city_id: Yup.string(), //.required(requiredFieldType),
    patient_part_PAAP: Yup.string().required(requiredFieldType),
    add_application_information: Yup.string(requiredFieldType),
    shipping_type: Yup.string().required(requiredFieldType),
    // patient_statu_id: Yup.string().required(requiredFieldType),
    ips: Yup.string().required(requiredFieldType),
    treatment_statu_id: Yup.string(),
    product_dose_id:
      Yup.string().required(
        requiredFieldType
      ) /* Yup.string().when("dose_start", {
      is: (value) => value === "Si",
      then: Yup.string().required(requiredFieldType),
    }), */,
    cause_no_claim_id: Yup.string().when("claim", {
      is: (claim) => claim === "No",
      then: Yup.string().required(requiredFieldType),
    }),
    claim_date: Yup.string().when("claim", {
      is: (claim) => claim === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    education_date: Yup.string().when("education", {
      is: (education) => education === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    education_reason_id: Yup.string().when("education", {
      is: (education) => education === "No",
      then: Yup.string().required(requiredFieldType),
    }),
    education_theme_id: Yup.string().when("education", {
      is: (education) => education === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    units: Yup.string().when("number_box", {
      is: (number_box) => number_box !== "",
      then: Yup.string().required(requiredFieldType),
    }),
    notes: Yup.string(),
  });

  const submitForm = async (valuesOriginal) => {
    const values = Object.assign({}, valuesOriginal);
    const parseToNumber = [
      "education_reason_id",
      "cause_no_claim_id",
      "specialty_id",
      "doctor_id",
      "logistic_operator_id",
      "number_box",
      "product_id",
      // "patient_statu_id",
      "pathological_classification_id",
      "treatment_previou_id",
      "insurance_id",
      "treatment_statu_id",
      "product_dose_id",
      "education_theme_id",
    ];
    parseToNumber.forEach((value) => {
      if (
        values[value] &&
        values[value].length > 0 &&
        !isNaN(parseInt(values[value]))
      ) {
        values[value] = parseInt(values[value]);
      } else {
        values[value] = null;
      }
    });
    const valuesToDelete = ["treatmentId", "zone"];
    valuesToDelete.forEach((value) => {
      delete values[value];
    });
    const valuesToUpperCase = [
      "claim",
      "dose_start",
      "education",
      "patient_part_PAAP",
    ];
    valuesToUpperCase.forEach((value) => {
      values[value] = String(values[value]).toUpperCase();
    });
    const callbackValues = [
      () => {
        if (values.claim === "NO") {
          values.claim_date = null;
        }
        if (values.claim === "SI") {
          values.cause_no_claim_id = null;
        }
      },
      () => {
        if (values.education === "NO") {
          values.education_date = null;
          values.education_theme_id = null;
        }
        if (values.education === "SI") {
          values.education_reason_id = null;
        }
      },
    ];
    callbackValues.forEach((action) => {
      action();
    });
    values.patient_statu_id = form.patient.patient_statu_id;
    values.dose_start = "NO";
    if (isSearched && isNull(_treatment) && !isNull(patient)){
      values.patient_id = patient.id;
      console.log('validation', { treatment: values });
      const response = await createTreatment({treatment:values}).catch((err) => {
        console.log("err", err);
        const message = ToastUtil(
          "¡Vaya!, ocurrió un error inesperado, intenta más tarde.",
          "error"
        );
        Toast(message);
      });
      console.log("Creacion del tratamiento->", response);
      if (response && response.code === 200 && response.response === true) {
        const modalReducer = store.getState().modalReducer;
        const { _icons } = modalReducer;
        const modal = new InfoModal(
          `El tratamiento ha sido creado éxitosamente`,
          _icons.CHECK_PRIMARY
        );
        store.dispatch(set_modal_on(modal));
        push("/home");
      } else {
        // mostrar errores
        const message = ToastUtil(response.message, "error");
        Toast(message);
      }
    }else{
      console.log("Getted and parsed values", values);
      const newForm = { ...form, treatment: { ...form.treatment, ...values } };
      console.log("new", newForm);
      setForm(newForm);
      const response = await createPatient(newForm).catch((err) => {
        console.log("err", err);
        const message = ToastUtil(
          "¡Vaya!, ocurrió un error inesperado, intenta más tarde.",
          "error"
        );
        Toast(message);
      });
      console.log("Creacion del user->", response);
      if (response && response.code === 200 && response.response === true) {
        const modalReducer = store.getState().modalReducer;
        const { _icons } = modalReducer;
        const modal = new InfoModal(
          `El usario ha sido creado éxitosamente`,
          _icons.CHECK_PRIMARY
        );
        store.dispatch(set_modal_on(modal));
        push("/home");
      } else {
        // mostrar errores
        const message = ToastUtil(response.message, "error");
        Toast(message);
      }
    }
  };

  return {
    submitForm,
    validationSchema,
    initialValues,
    loadingButton: loadingPatientCreation || loadingTreatment,
  };
};
