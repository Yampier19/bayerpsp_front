import { useEffect } from "react";

import { connect } from "react-redux";
import { tracking_get_request } from "redux/actions/trackingActions";

const Dependencies = (props) => {
  const { dispatchDep } = props;
  const { trackingReducer } = props;

  const load_dependencies = async () => {
    const dep = [
      {
        url: `/api/patient`,
        req_name: "GET_ALL_PATIENTS",
        req_body: null,
      },
      {
        url: `/api/statu/patient`,
        req_name: "GET_PATIENT_STATUS_LIST",
        req_body: null,
      },
      {
        url: `/api/location/city`,
        req_name: "GET_CITIES",
        req_body: null,
      },
      {
        url: `/api/location/department`,
        req_name: "GET_DEPARTAMENTOS",
        req_body: null,
      },
    ];

    dep.map(async (d) => {
      await props.tracking_get_request(d.url, d.req_name, d.req_body);
    });
  };

  useEffect(() => {
    load_dependencies();
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    const data = trackingReducer["GET_ALL_PATIENTS"].data;
    // console.log(data);
    let _data = {
      paciente: {},
    };
    if (data != null && trackingReducer["GET_ALL_PATIENTS"].success) {
      _data = {
        paciente: {
          id: data.length,
        },
      };
    }

    dispatchDep({
      type: "set_paciente",
      payload: _data.paciente,
    });
    //eslint-disable-next-line
  }, [trackingReducer["GET_ALL_PATIENTS"]]);

  useEffect(() => {
    const data = trackingReducer["GET_PATIENT_STATUS_LIST"].data;
    // console.log(data);
    let _data = {
      status_list: [],
    };

    if (data != null && trackingReducer["GET_PATIENT_STATUS_LIST"].success) {
      _data = {
        status_list: data.map((status) => ({
          id: status.id,
          name: status.name,
        })),
      };
    }

    dispatchDep({
      type: "set_status_list",
      payload: _data.status_list,
    });
    //eslint-disable-next-line
  }, [trackingReducer["GET_PATIENT_STATUS_LIST"]]);

  useEffect(() => {
    const data = trackingReducer["GET_CITIES"].data;
    // console.log(data);
    let _data = {
      cities: [],
    };
    console.log(data);
    // console.log(data);
    //
    if (data != null && trackingReducer["GET_CITIES"].success) {
      _data = {
        cities: data.map((city) => ({
          id: city.id,
          name: city.name,
        })),
      };
    }

    dispatchDep({
      type: "set_form_cities",
      payload: _data.cities,
    });
    //eslint-disable-next-line
  }, [trackingReducer["GET_CITIES"]]);

  useEffect(() => {
    const data = trackingReducer["GET_DEPARTAMENTOS"].data;
    // console.log(data);
    let _data = {
      departamentos: [],
    };
    // console.log(data);
    //
    if (data != null && trackingReducer["GET_DEPARTAMENTOS"].success) {
      _data = {
        departamentos: data.map((dprtm) => ({
          id: dprtm.id,
          name: dprtm.name,
        })),
      };
    }

    dispatchDep({
      type: "set_form_departamentos",
      payload: _data.departamentos,
    });
    //eslint-disable-next-line
  }, [trackingReducer["GET_DEPARTAMENTOS"]]);

  useEffect(() => {
    const dep1 = trackingReducer["GET_ALL_PATIENTS"];
    const dep2 = trackingReducer["GET_PATIENT_STATUS_LIST"];

    dispatchDep({
      type: "req_status",
      loading: dep1.loading && dep2.loading,
      success: dep1.success && dep2.success,
      error: dep1.error || dep2.error,
    });
    //eslint-disable-next-line
  }, [
    //eslint-disable-next-line
    trackingReducer["GET_ALL_PATIENTS"],
    //eslint-disable-next-line
    trackingReducer["GET_PATIENT_STATUS_LIST"],
  ]);

  return null;
};

const mapStateToPros = (state) => ({
  trackingReducer: state.trackingReducer,
});

export default connect(mapStateToPros, {
  tracking_get_request,
})(Dependencies);
