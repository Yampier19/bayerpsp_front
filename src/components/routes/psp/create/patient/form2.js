import { useEffect, useContext, useState } from "react";

import FormField from "components/form/formfield";
import Dropdown from "components/form/dropdown";
import Radio from "components/form/radio";
import FormField2 from "components/form/formfield2";
import InputMultiple from "./InputMultiple";
import { useHistory } from "react-router-dom";

import { useFormik } from "formik";
import * as Yup from "yup";

import { getRequired } from "utils/forms";

import LoadingComponent from "components/base/loading-component";

import { connect } from "react-redux";
import { FormCreatePatientContext } from "./context/form-context";
import { useListAllProducts } from "services/bayer/psp/product/useProduct";
import {
  useListClaims,
  useNoEducationReason,
  usePathologicalClassificationByProduct,
  useDose,
  useAllTreatments,
  useListLogisticOperatorsByEnsurance,
  useListAllDoctors,
  usePreviusTreatments,
  useCreatePatient,
  useEducationThemes,
  useTreatmentStatus,
} from "services/bayer/psp/patient/usePatient";
import { set_modal_off, set_modal_on } from "redux/actions/modalActions";
import store from "redux/store";
import { InfoModal } from "components/base/modal-system/models/modal";
import { ToastUtil } from "components/base/toast/toast-util";
import { Toast } from "components/base/toast/ToastContainer";
import { LoadingButton } from "@mui/lab";

const Form = (props) => {
  /* *~~*~~*~~*~~*~~*~~*~~*~ Context ~*~~*~~*~~*~~*~~*~~*~~*~~* */
  const { fetchedData, setFetchedData, setStep, setForm, form } = useContext(
    FormCreatePatientContext
  );
  const requiredFieldType = "Campo obligatorio";
  const { push } = useHistory();
  /* *~~*~~*~~*~~*~~*~~*~~*~ Context ~*~~*~~*~~*~~*~~*~~*~~*~~* */

  /* *~~*~~*~~*~~*~~*~~*~~*~ Hooks ~*~~*~~*~~*~~*~~*~~*~~*~~* */
  const [pressedSubmit, setPressedSubmit] = useState(false);
  /* *~~*~~*~~*~~*~~*~~*~~*~ Hooks ~*~~*~~*~~*~~*~~*~~*~~*~~* */

  /* *~~*~~*~~*~~*~~*~~*~~*~ Mutations ~*~~*~~*~~*~~*~~*~~*~~*~~* */
  const { isLoading: loadingPatientCreation, mutateAsync: createPatient } =
    useCreatePatient();
  /* *~~*~~*~~*~~*~~*~~*~~*~ Mutations ~*~~*~~*~~*~~*~~*~~*~~*~~* */

  /* *~~*~~*~~*~~*~~*~~*~~*~~* form values *~~*~~*~~*~~*~~*~~*~~*~~* */
  const formSchema = Yup.object().shape({
    treatmentId: Yup.number().typeError(
      "se debe especificar un valor numerico"
    ),
    claim: Yup.string().required(requiredFieldType),
    education: Yup.string().required(requiredFieldType),
    number_box: Yup.string(),
    product_id: Yup.string().required(requiredFieldType),
    dose_start: Yup.string().required(requiredFieldType),
    pathological_classification_id: Yup.string().required(requiredFieldType),
    treatment_previou_id: Yup.string().required(requiredFieldType),
    consent: Yup.string().required(requiredFieldType),
    therapy_start_date: Yup.string().required(requiredFieldType),
    regime: Yup.string().required(requiredFieldType),
    insurance_id: Yup.string().required(requiredFieldType),
    logistic_operator_id: Yup.string().required(requiredFieldType),
    last_claim_date: Yup.string(),
    other_operator: Yup.string(),
    delivery_point: Yup.string(),
    means_acquisition: Yup.string(),
    date_next_call: Yup.string().required(requiredFieldType),
    doctor_id: Yup.string().required(requiredFieldType),
    specialty_id: Yup.string().required(requiredFieldType),
    paramedic: Yup.string().required(requiredFieldType),
    zone: Yup.string().required(requiredFieldType),
    code: Yup.string(),
    city_id: Yup.string().required(requiredFieldType),
    patient_part_PAAP: Yup.string().required(requiredFieldType),
    add_application_information: Yup.string(requiredFieldType),
    shipping_type: Yup.string().required(requiredFieldType),
    patient_statu_id: Yup.string().required(requiredFieldType),
    ips: Yup.string().required(requiredFieldType),
    treatment_statu_id: Yup.string(),
    product_dose_id: Yup.string().when("dose_start", {
      is: (value) => value === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    cause_no_claim_id: Yup.string().when("claim", {
      is: (claim) => claim === "No",
      then: Yup.string().required(requiredFieldType),
    }),
    claim_date: Yup.string().when("claim", {
      is: (claim) => claim === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    education_date: Yup.string().when("education", {
      is: (education) => education === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    education_reason_id: Yup.string().when("education", {
      is: (education) => education === "No",
      then: Yup.string().required(requiredFieldType),
    }),
    education_theme_id: Yup.string().when("education", {
      is: (education) => education === "Si",
      then: Yup.string().required(requiredFieldType),
    }),
    units: Yup.string().when("number_box", {
      is: (number_box) => number_box !== "",
      then: Yup.string().required(requiredFieldType),
    }),
  });
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      treatmentId: fetchedData.treatmentId,
      claim: "",
      education: "",
      number_box: "",
      product_id: "",
      product_dose_id: "",
      dose_start: "",
      patient_statu_id: "",
      pathological_classification_id: "",
      treatment_previou_id: "",
      consent: "",
      therapy_start_date: "",
      regime: "",
      insurance_id: "",
      logistic_operator_id: "",
      last_claim_date: "",
      other_operator: "",
      delivery_point: "",
      ips: "",
      means_acquisition: "",
      date_next_call: "",
      doctor_id: "",
      specialty_id: "",
      paramedic: "",
      zone: "",
      code: "",
      city_id: "",
      patient_part_PAAP: "",
      add_application_information: "",
      shipping_type: "",
      cause_no_claim_id: "",
      claim_date: "",
      education_date: "",
      education_reason_id: "",
      units: "",
      education_theme_id: "",
      treatment_statu_id: "",
    },
    validateOnChange: true,
    validationSchema: formSchema,
    onSubmit: async (valuesOriginal) => {
      const values = Object.assign({}, valuesOriginal);
      const parseToNumber = [
        "education_reason_id",
        "cause_no_claim_id",
        "specialty_id",
        "doctor_id",
        "logistic_operator_id",
        "number_box",
        "product_id",
        "patient_statu_id",
        "pathological_classification_id",
        "treatment_previou_id",
        "insurance_id",
        "treatment_statu_id",
        "product_dose_id",
        "education_theme_id"
      ];
      parseToNumber.forEach((value) => {
        if (
          values[value] &&
          values[value].length > 0 &&
          !isNaN(parseInt(values[value]))
        ) {
          values[value] = parseInt(values[value]);
        } else {
          values[value] = null;
        }
      });
      const valuesToDelete = ["treatmentId", "zone"];
      valuesToDelete.forEach((value) => {
        delete values[value];
      });
      const valuesToUpperCase = [
        "claim",
        "dose_start",
        "education",
        "patient_part_PAAP",
      ];
      valuesToUpperCase.forEach((value) => {
        values[value] = String(values[value]).toUpperCase();
      });
      const callbackValues = [
        () => {
          if (values.claim === "NO") {
            values.claim_date = null;
          }
          if(values.claim === "SI"){
            values.cause_no_claim_id = null;
          }
          console.log('values.claim', values.claim, values.claim_date, values.cause_no_claim_id);
        },
        () => {
          if (values.education === "NO") {
            values.education_date = null;
            values.education_theme_id = null;
          }
          if (values.education === "SI") {
            values.education_reason_id = null;
          }
          console.log('values.education', values.education, values.education_date, values.education_theme_id, values.education_reason_id);
        },
      ];
      callbackValues.forEach((action) => {
        action();
      });
      values.treatment_statu_id = 1;
      console.log("Getted and parsed values", values);
      const newForm = { ...form, treatment: { ...form.treatment, ...values } };
      console.log("new", newForm);
      setForm(newForm);
      const response = await createPatient(newForm).catch((err) => {
        console.log("err", err);
        // mostrar errores
      });
      console.log("Creacion del user->", response);
      if (response && response.code === 200 && response.response === true) {
        const modalReducer = store.getState().modalReducer;
        const { _icons } = modalReducer;
        const modal = new InfoModal(
          `El usario ha sido creado éxitosamente`,
          _icons.CHECK_PRIMARY
        );
        store.dispatch(set_modal_on(modal));
        push("/home");
      } else {
        // mostrar errores
        const message = ToastUtil(response.message, "error");
        Toast(message);
      }
    },
  });
  const uncompleteForm = Object.keys(formik.errors).length > 0;
  const requiredFields = getRequired(formSchema);
  /* *~~*~~*~~*~~*~~*~~*~~*~~* form values *~~*~~*~~*~~*~~*~~*~~*~~* */

  /* *~~*~~*~~*~~*~~*~~*~~*~ React-query ~*~~*~~*~~*~~*~~*~~*~~*~~* */
  const queryConfig = {
    refetchOnWindowFocus: false,
    refetchOnmount: false,
    refetchOnReconnect: true,
    retry: false,
  };
  const reFetchQueryConfig = {
    enabled: false,
    refetchOnWindowFocus: false,
    timeout: 1000,
  };
  const { isLoading: loadingProducts } = useListAllProducts({
    onSuccess: (data) => {
      setFetchedData({ product_id: data.data });
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });
  const { isLoading: loadingClaims } = useListClaims({
    onSuccess: (data) => {
      setFetchedData({ cause_no_claim_id: data.data });
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });
  const { isLoading: loadingNoEducationReasons } = useNoEducationReason({
    onSuccess: (data) => {
      setFetchedData({ education_reason_id: data.data });
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });
  const { isLoading: loadingTreatments } = useAllTreatments({
    onSuccess: (data) => {
      setFetchedData({
        treatmentId: data.data.length + 1,
      });
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });
  const { isLoading: loadingDoctors } = useListAllDoctors({
    onSuccess: (data) => {
      setFetchedData({ doctor_id: data.data });
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });
  const { isLoading: loadingreviousTreatments } = usePreviusTreatments({
    onSuccess: (data) => {
      setFetchedData({ treatment_previou_id: data.data });
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });
  const { isLoading: loadingEducationThemes } = useEducationThemes({
    onSuccess: (data) => {
      setFetchedData({ education_theme_id: data.data });
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });

  const { isRefetching: loadingDose, refetch: refreshDoses } = useDose(
    formik.values.product_id,
    reFetchQueryConfig
  );
  const { isRefetching: loadingOperators, refetch: refreshOperators } =
    useListLogisticOperatorsByEnsurance(
      formik.values.insurance_id,
      reFetchQueryConfig
    );
  const {
    isRefetching: loadingTreatmentStatus,
    refetch: refreshTreatmentStatus,
  } = useTreatmentStatus(formik.values.product_id, reFetchQueryConfig);
  const {
    isRefetching: loadingPathologicalClassification,
    refetch: refreshPathologicalClassification,
  } = usePathologicalClassificationByProduct(
    formik.values.product_id,
    reFetchQueryConfig
  );
  const loading1 =
    loadingProducts &&
    loadingTreatments &&
    loadingDoctors &&
    loadingreviousTreatments &&
    loadingClaims;
  const loadingForm =
    loading1 && loadingNoEducationReasons && loadingEducationThemes;
  /* *~~*~~*~~*~~*~~*~~*~~*~ React-query ~*~~*~~*~~*~~*~~*~~*~~*~~* */

  /* *~~*~~*~~*~~*~~*~~*~~*~~* local handlers *~~*~~*~~*~~*~~*~~*~~*~~* */
  const customHandleChange = (e) => {
    formik.handleChange(e);
    const form = e.target.form;
    const count = requiredFields.filter(
      (fieldName) => form[fieldName].value !== ""
    ).length;
    setStep({ percent: (count / requiredFields.length) * 100 });
  };
  useEffect(() => {
    if (formik.values.product_id.length > 0) {
      refreshDoses().then(({ data, isSuccess }) => {
        if (isSuccess) {
          const { data: doses, code } = data;
          code === 200 &&
            setFetchedData({
              dose: doses
                // .filter((x) => x.status === "ACTIVO")
                .map((x) => ({ id: x.id, name: x.dose })),
            });
          formik.setFieldValue("product_dose_id", "");
        }
      });
      refreshTreatmentStatus().then(({ data, isSuccess }) => {
        if (isSuccess) {
          const { data: treatmentsStatus, code } = data;
          if (treatmentsStatus && treatmentsStatus.length > 0) {
            formik.setFieldError("treatment_statu_id", requiredFieldType);
          } else {
            formik.setFieldError("treatment_statu_id", null);
            formik.setFieldValue("treatment_statu_id", "");
          }
          code === 200 &&
            setFetchedData({ treatment_statu_id: treatmentsStatus });
        }
      });
      refreshPathologicalClassification().then(({ data, isSuccess }) => {
        if (isSuccess) {
          const { data: pc, code } = data;
          code === 200 &&
            setFetchedData({ pathological_classification_id: pc });
          formik.setFieldValue("pathological_classification_id", "");
        }
      });
    }
    //eslint-disable-next-line
  }, [formik.values.product_id]);
  useEffect(() => {
    if (formik.values.insurance_id.length > 0) {
      refreshOperators().then(({ data, isSuccess }) => {
        if (isSuccess) {
          const { data: operators, code } = data;
          const _operators =
            operators.length > 0
              ? operators.filter((x) => x.status === "ACTIVO")
              : [1, 2, 3, 4, 5];
          code === 200 &&
            setFetchedData({
              logistic_operator_id: _operators,
            });
        }
      });
    }
    //eslint-disable-next-line
  }, [formik.values.insurance_id]);
  useEffect(() => {
    const selectedDoctorIndex = fetchedData.doctor_id.findIndex(
      (doctor) => doctor.id === Number(formik.values.doctor_id)
    );
    if (selectedDoctorIndex !== -1) {
      const selectedDoctor = fetchedData.doctor_id[selectedDoctorIndex];
      const especialities = selectedDoctor.specialty;
      setFetchedData({ specialty_id: especialities });
    }
    //eslint-disable-next-line
  }, [formik.values.doctor_id]);
  /* *~~*~~*~~*~~*~~*~~*~~*~~* local handlers *~~*~~*~~*~~*~~*~~*~~*~~* */

  return (
    <>
      {loadingForm ? (
        <LoadingComponent loadingText="Cargando datos" />
      ) : (
        <form id="form2" onSubmit={formik.handleSubmit} className="pr-3 py-3">
          <FormField
            label="Id tratamiento"
            number="1"
            name="treatmentId"
            checked={formik.values.treatmentId !== ""}
          >
            <input
              className="input cool-input"
              type="text"
              name="treatmentId"
              onChange={customHandleChange}
              value={formik.values.treatmentId}
              readOnly
            />
            {pressedSubmit && formik.errors.treatmentId ? (
              <div className="help is-danger"> {formik.errors.treatmentId}</div>
            ) : null}
          </FormField>

          <FormField2
            number1="2"
            number2="2.1"
            checked1={formik.values.claim}
            label1="Reclamo*"
            label2={
              formik.values.claim === "Si"
                ? "Fecha de reclamacion *"
                : formik.values.claim === "No"
                ? "Causa no reclamacion *"
                : ""
            }
            input1={
              <div>
                <Radio
                  name="claim"
                  onChange={customHandleChange}
                  value={formik.values.claim}
                />
                {pressedSubmit && formik.errors.claim ? (
                  <div className="help is-danger"> {formik.errors.claim}</div>
                ) : null}
              </div>
            }
            input2={
              <div>
                {formik.values.claim === "Si" ? (
                  <div>
                    <input
                      className="input cool-input"
                      type="date"
                      name="claim_date"
                      onChange={customHandleChange}
                      value={formik.values.claim_date}
                    />
                    {pressedSubmit && formik.errors.claim_date ? (
                      <div className="help is-danger">
                        {formik.errors.claim_date}
                      </div>
                    ) : null}
                  </div>
                ) : formik.values.claim === "No" ? (
                  <div>
                    <Dropdown
                      options={fetchedData.cause_no_claim_id}
                      name="cause_no_claim_id"
                      arrowColor="has-text-primary"
                      onChange={customHandleChange}
                      value={formik.values.cause_no_claim_id}
                    />
                    {pressedSubmit && formik.errors.cause_no_claim_id ? (
                      <div className="help is-danger">
                        {formik.errors.cause_no_claim_id}
                      </div>
                    ) : null}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="3"
            number2="3.1"
            checked1={formik.values.education}
            label1="¿Se brindo educacion?*"
            label2={
              formik.values.education === "Si"
                ? "Fecha*"
                : formik.values.education === "No"
                ? "Motivo*"
                : ""
            }
            input1={
              <div>
                <Radio
                  name="education"
                  onChange={customHandleChange}
                  value={formik.values.education}
                />
                {pressedSubmit && formik.errors.education ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.education}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                {formik.values.education === "No" ? (
                  <div>
                    <Dropdown
                      options={fetchedData.education_reason_id}
                      name="education_reason_id"
                      arrowColor="has-text-primary"
                      onChange={customHandleChange}
                      value={formik.values.education_reason_id}
                    />
                  </div>
                ) : formik.values.education === "Si" ? (
                  <div>
                    <InputMultiple
                      label1="Tema*"
                      label2="Fecha Educación*"
                      input1={
                        <Dropdown
                          options={fetchedData.education_theme_id}
                          name="education_theme_id"
                          arrowColor="has-text-primary"
                          onChange={customHandleChange}
                          value={formik.values.education_theme_id}
                        />
                      }
                      input2={
                        <input
                          className="input cool-input"
                          type="date"
                          name="education_date"
                          onChange={customHandleChange}
                          value={formik.values.education_date}
                        />
                      }
                    />
                    {/*
                    <Dropdown
                      options={fetchedData.education_reason_id}
                      name="education_reason_id"
                      arrowColor="has-text-primary"
                      onChange={customHandleChange}
                        value={formik.values.education_reason_id}
                    /> */}
                    {pressedSubmit && formik.errors.education_reason_id ? (
                      <div className="help is-danger">
                        {formik.errors.education_reason_id}
                      </div>
                    ) : null}
                  </div>
                ) : null}
              </div>
            }
            // input3={
            //   <div>

            //   </div>
            // }
          />

          <FormField2
            number1="4"
            number2="4.1"
            checked1={formik.values.number_box}
            checked2={formik.values.units}
            label1="Número cajas*"
            label2={`${formik.values.number_box !== "" ? "Unidades*" : ""}`}
            input1={
              <div>
                <Dropdown
                  name="number_box"
                  onChange={customHandleChange}
                  options={Array(51)
                    .fill()
                    .map((_, idx) => idx)}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.number_box ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.number_box}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                {formik.values.number_box !== "" ? (
                  <div>
                    <Dropdown
                      name="units"
                      onChange={customHandleChange}
                      options={["Ampolla(s)", "Aplicacion", "Caja(s)"]}
                      arrowColor="has-text-primary"
                    />
                    {pressedSubmit && formik.errors.units ? (
                      <div className="help is-danger">
                        {" "}
                        {formik.errors.units}
                      </div>
                    ) : null}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField2
            label1="Producto*"
            label2={`Estado del tratamiento${
              fetchedData.treatment_statu_id.length > 0 ? "*" : ""
            }`}
            number1="5"
            number2="5.1"
            checked1={formik.values.product_id}
            checked2={formik.values.treatment_statu_id}
            input1={
              <div>
                <Dropdown
                  disabled={loadingTreatmentStatus}
                  name="product_id"
                  onChange={customHandleChange}
                  options={fetchedData.product_id}
                  arrowColor="has-text-primary"
                />

                {pressedSubmit && formik.errors.product_id ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.product_id}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <Dropdown
                  disabled={loadingTreatmentStatus}
                  name={"treatment_statu_id"}
                  onChange={customHandleChange}
                  options={fetchedData.treatment_statu_id}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.treatment_statu_id ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.treatment_statu_id}
                  </div>
                ) : null}
              </div>
            }
          ></FormField2>

          <FormField2
            number1="6"
            number2="6.1"
            checked1={formik.values.dose_start}
            checked2={formik.values.product_dose_id}
            label1="Dosis de inicio*"
            label2={formik.values.dose_start === "Si" ? "Dosis*" : ""}
            input1={
              <div>
                <Radio
                  name="dose_start"
                  onChange={customHandleChange}
                  value={formik.values.dose_start}
                />
                {pressedSubmit && formik.errors.dose_start ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.dose_start}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                {formik.values.dose_start === "Si" && (
                  <div>
                    <Dropdown
                      disabled={loadingDose}
                      name="product_dose_id"
                      onChange={customHandleChange}
                      options={fetchedData.dose}
                      arrowColor="has-text-primary"
                    />
                    {pressedSubmit && formik.errors.product_dose_id ? (
                      <div className="help is-danger">
                        {" "}
                        {formik.errors.product_dose_id}
                      </div>
                    ) : null}
                  </div>
                )}
              </div>
            }
          />

          <FormField2
            number1="7"
            number2="7.1"
            checked1={formik.values.patient_statu_id}
            checked2={formik.values.pathological_classification_id}
            label1="Estado del paciente*"
            label2="Clasificación patológica*"
            input1={
              <div>
                <Dropdown
                  name="patient_statu_id"
                  onChange={customHandleChange}
                  options={fetchedData.patient_statu_id}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.patient_statu_id ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.patient_statu_id}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <Dropdown
                  disabled={loadingPathologicalClassification}
                  name="pathological_classification_id"
                  onChange={customHandleChange}
                  options={fetchedData.pathological_classification_id}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit &&
                formik.errors.pathological_classification_id ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.pathological_classification_id}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="8"
            number2="8.1"
            checked1={formik.values.treatment_previou_id}
            checked2={formik.values.consent}
            label1="Tratamiento previo*"
            label2="Consentimiento*"
            input1={
              <div>
                <Dropdown
                  name="treatment_previou_id"
                  onChange={customHandleChange}
                  options={fetchedData.treatment_previou_id}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.treatment_previou_id ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.treatment_previou_id}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <Dropdown
                  name="consent"
                  onChange={customHandleChange}
                  options={["Fisico", "Verbal", "Sms", "Chat interactivo"]}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.consent ? (
                  <div className="help is-danger"> {formik.errors.consent}</div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="9"
            number2="9.1"
            checked1={formik.values.therapy_start_date}
            checked2={formik.values.regime}
            label1="Fecha de inicio terapia*"
            label2="Regimen*"
            input1={
              <div>
                <input
                  className="input cool-input"
                  type="date"
                  name="therapy_start_date"
                  onChange={customHandleChange}
                  value={formik.values.therapy_start_date}
                />
                {pressedSubmit && formik.errors.therapy_start_date ? (
                  <div className="help is-danger">
                    {formik.errors.therapy_start_date}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <Dropdown
                  name="regime"
                  onChange={customHandleChange}
                  options={[
                    "Contributivo",
                    "Especial",
                    "Particular",
                    "Subsidiado",
                  ]}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.regime ? (
                  <div className="help is-danger"> {formik.errors.regime}</div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="10"
            number2="10.1"
            checked1={formik.values.insurance_id}
            checked2={formik.values.logistic_operator_id}
            label1="Asegurador*"
            label2="Operador lógistico*"
            input1={
              <div>
                <Dropdown
                  name="insurance_id"
                  onChange={customHandleChange}
                  options={fetchedData.insurance_id}
                  arrowColor="has-text-primary"
                  disabled={loadingOperators}
                />
                {pressedSubmit && formik.errors.insurance_id ? (
                  <div className="help is-danger">
                    {formik.errors.insurance_id}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <Dropdown
                  name="logistic_operator_id"
                  onChange={customHandleChange}
                  options={fetchedData.logistic_operator_id}
                  arrowColor="has-text-primary"
                  disabled={loadingOperators}
                />
                {pressedSubmit && formik.errors.logistic_operator_id ? (
                  <div className="help is-danger">
                    {formik.errors.logistic_operator_id}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="11"
            number2="11.1"
            checked1={formik.values.last_claim_date}
            checked2={formik.values.other_operator}
            label1="Fecha última reclamación"
            label2="Otros operadores"
            input1={
              <div>
                <input
                  className="input cool-input"
                  type="date"
                  max={new Date().toISOString().split("T")[0]}
                  name="last_claim_date"
                  onChange={customHandleChange}
                  value={formik.values.last_claim_date}
                />
                {pressedSubmit && formik.errors.last_claim_date ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.last_claim_date}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <Dropdown
                  name="other_operator"
                  onChange={customHandleChange}
                  options={[
                    "Otro operador 1",
                    "Otro operador 2",
                    "Otro operador 3",
                    "Otro operador 4",
                  ]}
                  arrowColor="has-text-primary"
                />
                {formik.errors.other_operator ? (
                  <div className="help is-danger">
                    {formik.errors.other_operator}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="12"
            number2="12.1"
            checked1={formik.values.delivery_point}
            checked2={formik.values.ips}
            label1="Punto de entrega"
            label2="IPS que atiende*"
            input1={
              <div>
                <input
                  name="delivery_point"
                  className="input cool-input"
                  type="text"
                  onChange={customHandleChange}
                  value={formik.values.delivery_point}
                />
                {pressedSubmit && formik.errors.delivery_point ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.delivery_point}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <input
                  name="ips"
                  className="input cool-input"
                  type="text"
                  onChange={customHandleChange}
                  value={formik.values.ips}
                />
                {pressedSubmit && formik.errors.ips ? (
                  <div className="help is-danger"> {formik.errors.ips}</div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="13"
            number2="13.1"
            checked1={formik.values.means_acquisition}
            checked2={formik.values.date_next_call}
            label1="Medios de adquisición"
            label2="Fecha de la próxima llamada*"
            input1={
              <div>
                <input
                  name="means_acquisition"
                  className="input cool-input"
                  type="text"
                  onChange={customHandleChange}
                  value={formik.values.means_acquisition}
                />
                {pressedSubmit && formik.errors.means_acquisition ? (
                  <div className="help is-danger">
                    {formik.errors.means_acquisition}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <input
                  name="date_next_call"
                  className="input cool-input"
                  type="date"
                  min={new Date().toISOString().split("T")[0]}
                  onChange={customHandleChange}
                  value={formik.values.date_next_call}
                />
                {pressedSubmit && formik.errors.date_next_call ? (
                  <div className="help is-danger">
                    {formik.errors.date_next_call}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="14"
            number2="14.1"
            checked1={formik.values.doctor_id}
            checked2={formik.values.specialty_id}
            label1="Médico*"
            label2="Especialidad*"
            input1={
              <div>
                <Dropdown
                  name="doctor_id"
                  onChange={customHandleChange}
                  options={fetchedData.doctor_id}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.doctor_id ? (
                  <div className="help is-danger">
                    {formik.errors.doctor_id}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <Dropdown
                  name="specialty_id"
                  onChange={customHandleChange}
                  options={fetchedData.specialty_id}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.specialty_id ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.specialty_id}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="15"
            number2="15.1"
            checked1={formik.values.paramedic}
            checked2={formik.values.zone}
            label1="Paramédico o representante*"
            label2="Zona de atención paramédico o representante*"
            input1={
              <div>
                <input
                  name="paramedic"
                  className="input cool-input"
                  type="text"
                  onChange={customHandleChange}
                  value={formik.values.paramedic}
                />
                {pressedSubmit && formik.errors.paramedic ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.paramedic}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <Dropdown
                  name="zone"
                  onChange={customHandleChange}
                  options={["Zona 1", "Zona 2", "Zona 3", "Zona 4"]}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.zone ? (
                  <div className="help is-danger"> {formik.errors.zone}</div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="16"
            number2="16.1"
            checked1={formik.values.code}
            checked2={formik.values.city_id}
            label1="Código"
            label2="Ciudad base paramédico o representante*"
            input1={
              <div>
                <Dropdown
                  name="code"
                  onChange={customHandleChange}
                  options={[1, 2, 3, 4] /* Esot debe ser del 1 al 12 */}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.code ? (
                  <div className="help is-danger"> {formik.errors.code}</div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <Dropdown
                  name="city_id"
                  onChange={customHandleChange}
                  options={[
                    { id: 1, name: "Ciudad 1" },
                    { id: 2, name: "Ciudad 2" },
                    { id: 3, name: "Ciudad 3" },
                    { id: 4, name: "Ciudad 4" },
                  ]}
                  arrowColor="has-text-primary"
                />
                {pressedSubmit && formik.errors.city_id ? (
                  <div className="help is-danger"> {formik.errors.city_id}</div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="17"
            number2="17.1"
            checked1={formik.values.patient_part_PAAP}
            checked2={formik.values.add_application_information}
            label1="Paciente hace parte del PAAP*"
            label2="Agregar Información aplicaciones"
            input1={
              <div>
                <Radio
                  name="patient_part_PAAP"
                  onChange={customHandleChange}
                  value={formik.values.patient_part_PAAP}
                />
                {pressedSubmit && formik.errors.patient_part_PAAP ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.patient_part_PAAP}
                  </div>
                ) : null}
              </div>
            }
            input2={
              <div>
                <Radio
                  name="add_application_information"
                  onChange={customHandleChange}
                  value={formik.values.add_application_information}
                />
                {pressedSubmit && formik.errors.add_application_information ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.add_application_information}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField
            label="Tipo de envio*"
            number="18"
            name="shipping_type"
            checked={formik.values.shipping_type}
            noline
          >
            <input
              className="input cool-input"
              type="text"
              name="shipping_type"
              onChange={customHandleChange}
              value={formik.values.shipping_type}
            />
            <div class="help">
              <div className="level is-mobile">
                <div className="level-right has-text-danger">
                  {pressedSubmit && formik.errors.shipping_type
                    ? formik.errors.shipping_type
                    : ""}
                </div>
                <div className="level-left">
                  <p>
                    <u>Agregar tratamiento</u>
                  </p>
                </div>
              </div>
            </div>
          </FormField>

          <br />

          <div className="has-text-right has-text-centered-mobile">
            <span
              className={`has-text-danger has-text-right ${
                uncompleteForm ? "" : "is-invisible"
              }`}
            >
              Por favor revise el formulario
            </span>
          </div>

          <br />

          <div className="field has-text-right has-text-centered-mobile">
            <div className="control">
                <LoadingButton
                  className="button is-primary"
                  type="submit"
                  loading={loadingPatientCreation}
                  style={{ width: "150px" }}
                  >
                  Registrar
              </LoadingButton>
            </div>
          </div>
        </form>
      )}
    </>
  );
};

const mapStateToPros = (state) => ({
  trackingReducer: state.trackingReducer,
});

export default connect(mapStateToPros, null)(Form);
