import { fontWeight } from "@mui/system";
import React from "react";
import { useSearchedPatientContext } from "../context/searched-patient-context";

const numbersToStringDictionary = ["cero", "un", "dos", "tres", "cuatro"];
export const Treatments = ({ setIsNewTreatment = () => {} }) => {
  const { treatments, patient } = useSearchedPatientContext();
  const treatmentsQuantity = treatments.length;
  const { id } = patient;
  const idFormater = (id) => {
    const _id = String("00" + id);
    const length = _id.length;
    return _id.substring(length - 3, length);
  };
  return (
    <div>
      <p>
        El paciente número {idFormater(id)}, actualmente se encuentra en{" "}
        {numbersToStringDictionary[treatmentsQuantity]} tratamiento
        {treatmentsQuantity > 1 ? "s" : ""}
      </p>
      {treatments.map((treatment, index) => (
        <Treatment
          key={"treatment__" + index}
          number={index + 1}
          treatment={treatment}
        />
      ))}
      <button onClick={() => setIsNewTreatment(true)}>
        {"Agregar un nuevo tratamiento".toUpperCase()}
      </button>
    </div>
  );
};

const Treatment = ({ number = 0, treatment }) => {
  const { setTreatment } = useSearchedPatientContext();
  return (
    <button onClick={() => setTreatment(treatment)}>Tratamiento{number}</button>
  );
};

export default function info_treatments() {
  return (
    <>
      <div className="row" style={styles.margenes}>
        <div className="col">
          <label for="">
            El paciente Número 001, actualmente se encuentra en dos tratamientos
          </label>
        </div>
      </div>
      <div className="row my-5">
        <div className="col-1 d-flex justify-content-center align-items-center">
          <i class="fas fa-pills fa-2x" style={styles.ico}></i>
        </div>
        <div className="col">
          <div className="row-reverse">
            <div className="col">
              <label for="" className="h5" style={styles.title}>Tratamiento 1</label>
            </div>
            <div className="col">
                <a href="" style={styles.description}>Ver detalles del tratamiento</a>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-1 d-flex justify-content-center align-items-center">
          <i class="fas fa-pills fa-2x" style={styles.ico}></i>
        </div>
        <div className="col">
          <div className="row-reverse">
            <div className="col">
              <label for="" className="h5" style={styles.title}>Tratamiento 2</label>
            </div>
            <div className="col">
              <a href="" style={styles.description}>Ver detalles del tratamiento</a>
            </div>
          </div>
        </div>
      </div>
      <div className="row" style={styles.margenes}>
        <div className="col">
            <button style={styles.button}>AGREGAR UN NUEVO TRATAMIENTO</button>
        </div>
      </div>
    </>
  );
}

const styles = {
  title: {
    color: '#519EA1',
    fontWeight: 'bold'
  },

  ico: {
    color: '#519EA1',
  },

  description: {
    textDecoration:'underline',
    color: 'black',
  },

  button: {
    background: '#519EA1',
    padding: 10,
    width: 400,
    borderRadius: 10,
    borderStyle: 'none',
    color: 'white',
    fontWeight: 'bold',
  },

  margenes: {
    marginTop: 70, 
    marginBottom: 70
  }

}
