import { useEffect } from "react";

import FormField from "components/form/formfield";
import Dropdown from "components/form/dropdown";
import FormField2 from "components/form/formfield2";

import { useFormik } from "formik";
import * as Yup from "yup";

import { getRequired } from "utils/forms";

import LoadingComponent from "components/base/loading-component";

import { connect } from "react-redux";
import { useContext } from "react";
import { FormCreatePatientContext } from "./context/form-context";
import { useListCitiesByDepartment } from "services/bayer/psp/cities/useCities";
import { useListAllDepartements } from "services/bayer/psp/departments/useDepartments";
import {
  useListAllPatients,
  useListStatus,
  useListInsuranceByDepartmentId,
} from "services/bayer/psp/patient/usePatient";
import { useState } from "react";
import { emailTypesList } from "utils/constants/mail.list";
import { Autocomplete } from "@mui/material";
import { TextField } from "@material-ui/core";
import { DropdownOthers } from "components/form/dropdown-others";

const Form = ({ onSubmitCallback = () => {} }) => {
  /* *~~*~~*~~*~~*~~*~~*~~*~ Context ~*~~*~~*~~*~~*~~*~~*~~*~~* */
  const { fetchedData, setFetchedData, setStep, setForm, form } = useContext(
    FormCreatePatientContext
  );
  const requiredFieldType = "Campo obligatorio";
  /* *~~*~~*~~*~~*~~*~~*~~*~ Context ~*~~*~~*~~*~~*~~*~~*~~*~~* */

  /* *~~*~~*~~*~~*~~*~~*~~*~ Hooks ~*~~*~~*~~*~~*~~*~~*~~*~~* */
  const [pressedSubmit, setPressedSubmit] = useState(false);
  /* *~~*~~*~~*~~*~~*~~*~~*~ Hooks ~*~~*~~*~~*~~*~~*~~*~~*~~* */

  /* *~~*~~*~~*~~*~~*~~*~~*~~* form values *~~*~~*~~*~~*~~*~~*~~*~~* */
  const formSchema = Yup.object().shape({
    patientId: Yup.number().typeError("se debe especificar un valor numerico"),
    patient_statu_id: Yup.string().required(requiredFieldType),
    date_active: Yup.string().required(requiredFieldType),
    email: Yup.string().required(requiredFieldType), //.email("email invalido"),
    name: Yup.string().required(requiredFieldType),
    last_name: Yup.string().required(requiredFieldType),
    document_number: Yup.number()
      .typeError("se debe especificar un valor numerico")
      .required(requiredFieldType)
      .test(
        "len",
        "Lonngitud invalida, debe ser mayor a 7",
        (val) => val && String(val).length > 6
      )
      .test(
        "len",
        "Lonngitud invalida, debe ser menor a 10",
        (val) => val && String(val).length < 11
      ),
    phone: Yup.number()
      .typeError("se debe especificar un valor numerico")
      .required(requiredFieldType),
    department_id: Yup.string().required(requiredFieldType),
    city_id: Yup.string().required(requiredFieldType),
    neighborhood: Yup.string().required(requiredFieldType),
    address: Yup.string().required(requiredFieldType),
    via: Yup.string().required(requiredFieldType),
    via_detail: Yup.string().required(requiredFieldType),
    number_indicative: Yup.string().required(requiredFieldType),
    number_indicative_two: Yup.string().required(requiredFieldType),
    phone_number: Yup.number().typeError(
      "se debe especificar un valor numerico"
    ),
    inside: Yup.string(),
    inside_detail: Yup.string(),
    gender: Yup.string().required(requiredFieldType),
    date_birth: Yup.string().required(requiredFieldType),
    age: Yup.number()
      .integer()
      .min(1, "La edad debe ser mayor a 1")
      .typeError("se debe especificar un valor numerico"),
    guardian: Yup.string(),
    guardian_phone: Yup.number().typeError(
      "se debe especificar un valor numerico"
    ),
    notas: Yup.string(),
    email_type: Yup.string().required(requiredFieldType),
  });
  const dateToday = () => {
    const date = new Date().toLocaleDateString().split("T")[0];
    const [day, month, year] = date.split("/");
    const lessThanTenParser = (value = 0) =>
      value < 10 ? "0" + String(value) : value;
    const _date = `${year}-${lessThanTenParser(month)}-${lessThanTenParser(
      day
    )}`;
    return _date;
  };
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      patientId: fetchedData.patientId,
      patient_statu_id: "",
      date_active: dateToday(),
      email: "",
      name: "",
      last_name: "",
      document_number: "",
      phone: "",
      department_id: "",
      city_id: "",
      neighborhood: "",
      address: "",
      via: "",
      via_detail: "",
      number_indicative: "",
      phone_number: "",
      inside: "",
      inside_detail: "",
      gender: "",
      date_birth: "",
      age: 0,
      guardian: "",
      guardian_phone: "",
      notas: "",
      number_indicative_two: "",
      email_type: "",
    },
    validateOnChange: true,
    validationSchema: formSchema,
    onSubmit: async (valuesOriginal) => {
      const values = Object.assign({}, valuesOriginal);
      const parseToNumber = [
        "city_id",
        "department_id",
        "document_number",
        "patient_statu_id",
        "guardian_phone",
      ];
      parseToNumber.forEach((value) => {
        if (
          values[value] &&
          values[value].length > 0 &&
          !isNaN(parseInt(values[value]))
        ) {
          values[value] = parseInt(values[value]);
        } else {
          values[value] = 0;
        }
      });
      const callbackValues = [
        () => {
          values.age = getAge(values.date_birth);
        },
        () => {
          const { number_indicative, phone, phone_number } = values;
          const phones = [];
          if (number_indicative && phone_number) {
            phones.push(String(number_indicative + " " + phone_number));
          }
          phones.push(String(phone));
          values.phones = phones;
        },
        ()=>{
          const _email = values.email + '@' + values.email_type;
          values.email = _email;
        }
      ];
      callbackValues.forEach((action) => {
        action();
      });
      const valuesToDelete = [
        "notas",
        "number_indicative",
        "patientId",
        "phone",
        "number_indicative",
        "phone_number",
        "email_type"
      ];
      valuesToDelete.forEach((value) => {
        delete values[value];
      });
      values.application_system_id = 1;
      values.document_type = "CC";
      console.log("Values are", values);
      setForm((prevState) => ({
        ...prevState,
        patient: { ...prevState.patient, ...values },
      }));
    },
  });
  const uncompleteForm = Object.keys(formik.errors).length > 0;
  const requiredFields = getRequired(formSchema);
  /* *~~*~~*~~*~~*~~*~~*~~*~~* form values *~~*~~*~~*~~*~~*~~*~~*~~* */

  /* *~~*~~*~~*~~*~~*~~*~~*~ React-query ~*~~*~~*~~*~~*~~*~~*~~*~~* */
  const reFetchQueryConfig = {
    enabled: false,
    refetchOnWindowFocus: false,
    timeout: 1000,
  };
  const queryConfig = {
    refetchOnWindowFocus: false,
    refetchOnmount: false,
    refetchOnReconnect: true,
    retry: false,
  };
  const { refetch: refreshCities, isRefetching: loadingCities } =
    useListCitiesByDepartment(formik.values.department_id, reFetchQueryConfig);
  const { refetch: refreshInsurance, isRefetching: loadingInsurance } =
    useListInsuranceByDepartmentId(
      formik.values.department_id,
      reFetchQueryConfig
    );

  const { isLoading: loadingDepartments } = useListAllDepartements({
    onSuccess: (data) => {
      setFetchedData({ department_id: data.data });
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });
  const { isLoading: loadingStatusPatient } = useListStatus({
    onSuccess: (data) => {
      setFetchedData({ patient_statu_id: data.data });
    },
    onError: (err) => {
      console.log("Here on error");
    },
    queryConfig,
  });
  // const { isLoading: loadingAllPatients } = useListAllPatients({
  //   onSuccess: (data) => {
  //     setFetchedData({ patientId: data.data.length + 1 });
  //   },
  //   onError: (err) => {
  //     console.log("Here on error");
  //   },
  //   queryConfig,
  // });
  const loadingForm = loadingDepartments && loadingStatusPatient;
  /* *~~*~~*~~*~~*~~*~~*~~*~ React-query ~*~~*~~*~~*~~*~~*~~*~~*~~* */

  /* *~~*~~*~~*~~*~~*~~*~~*~~* local handlers *~~*~~*~~*~~*~~*~~*~~*~~* */
  const customHandleChange = (e) => {
    formik.handleChange(e);
    const form = e.target.form;
    const count = requiredFields.filter(
      (fieldName) => form[fieldName].value !== ""
    ).length;
    setStep({ percent: (count / requiredFields.length) * 100 });
  };
  const onContinueClicked = (e) => {
    setPressedSubmit(true);
    // const validatedFields = await formik.validateForm();
    formik.validateForm().then((validatedFields) => {
      const validated = Object.keys(validatedFields).length > 0;
      console.log(e);
      if (!validated) onSubmitCallback(e);
    });
  };
  const getAge = (dateString) => {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    if (dateString !== null && dateString !== "") {
      return age;
    } else {
      return "";
    }
  };

  useEffect(() => {
    if (formik.values.department_id.length > 0) {
      refreshCities().then(({ data, isSuccess }) => {
        if (isSuccess) {
          const { data: cities, code } = data;
          code === 200 && setFetchedData({ city_id: cities });
        }
      });
      refreshInsurance().then(({ data, isSuccess }) => {
        if (isSuccess) {
          const { data: insurance, code } = data;
          code === 200 && setFetchedData({ insurance_id: insurance });
        }
      });
    }
    //eslint-disable-next-line
  }, [formik.values.department_id]);
  /* *~~*~~*~~*~~*~~*~~*~~*~~* local handlers *~~*~~*~~*~~*~~*~~*~~*~~* */

  useEffect(() => {
    formik.setFieldValue("age", getAge(formik.values.date_birth));
    //eslint-disable-next-line
  }, [formik.values.date_birth]);

  useEffect(() => {
    formik.setFieldValue(
      "address",
      `${formik.values.via} ${formik.values.via_detail}  ${
        formik.values.number_indicative
          ? `# ${formik.values.number_indicative} `
          : ""
      } ${
        formik.values.number_indicative_two
          ? `- ${formik.values.number_indicative_two}`
          : ""
      } ${formik.values.inside ? `${formik.values.inside} ` : ""} ${
        formik.values.inside_detail ? `${formik.values.inside_detail} ` : ""
      }`
    );
    //eslint-disable-next-line
  }, [
    formik.values.via,
    formik.values.via_detail,
    formik.values.number_indicative,
    formik.values.number_indicative_two,
    formik.values.inside,
    formik.values.inside_detail,
  ]);

  return (
    <>
      {loadingForm ? (
        <LoadingComponent loadingText="Cargando datos" />
      ) : true ? (
        <form
          onSubmit={formik.handleSubmit}
          className="coolscroll primary pr-3 py-3 "
        >
          <FormField
            label="Código de usuario"
            number="1"
            name="patientId"
            checked={formik.values.patientId !== ""}
          >
            <input
              className="input cool-input"
              type="text"
              name="patientId"
              onChange={customHandleChange}
              value={formik.values.patientId}
              readOnly
            />
            {pressedSubmit && formik.errors.patientId ? (
              <div className="help is-danger"> {formik.errors.patientId}</div>
            ) : null}
          </FormField>

          <FormField
            label="Estado del paciente*"
            number="2"
            name="patient_statu_id"
            checked={formik.values.patient_statu_id !== ""}
          >
            <Dropdown
              options={fetchedData.patient_statu_id}
              name="patient_statu_id"
              arrowColor="has-text-primary"
              onChange={customHandleChange}
              value={formik.values.patient_statu_id}
            />
            {pressedSubmit && formik.errors.patient_statu_id ? (
              <div className="help is-danger">
                {" "}
                {formik.errors.patient_statu_id}
              </div>
            ) : null}
          </FormField>

          <FormField
            label="Fecha de activación*"
            number="3"
            name="date_active"
            checked={formik.values.date_active !== ""}
          >
            <input
              className="input cool-input"
              type="date"
              name="date_active"
              onChange={customHandleChange}
              value={formik.values.date_active}
              readOnly
            />
            {pressedSubmit && formik.errors.date_active ? (
              <div className="help is-danger">{formik.errors.date_active}</div>
            ) : null}
          </FormField>

          <FormField2
            number1="4"
            number2="4.1"
            name1="email"
            name2="email"
            checked1={formik.values.email !== ""}
            checked2={formik.values.email_type !== ""}
            label1="Correo electrónico*"
            input1={
              <div>
                <input
                  className="input cool-input "
                  type="text"
                  name="email"
                  onChange={customHandleChange}
                  value={formik.values.email}
                />
                {pressedSubmit && formik.errors.email ? (
                  <div className="help is-danger"> {formik.errors.email}</div>
                ) : null}
              </div>
            }
            label2="@"
            input2={
              <div>
                <DropdownOthers
                  options={emailTypesList()}
                  value={formik.values.email_type}
                  onChange={formik.handleChange}
                  name="email_type"
                />
                {pressedSubmit && formik.errors.email_type ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.email_type}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="5"
            number2="5.1"
            name1="name"
            name2="last_name"
            checked1={formik.values.name !== ""}
            checked2={formik.values.last_name !== ""}
            label1="Nombre*"
            input1={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="name"
                  onChange={customHandleChange}
                  value={formik.values.name}
                />
                {pressedSubmit && formik.errors.name ? (
                  <div className="help is-danger"> {formik.errors.name}</div>
                ) : null}
              </div>
            }
            label2="Apellido*"
            input2={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="last_name"
                  onChange={customHandleChange}
                  value={formik.values.last_name}
                />
                {pressedSubmit && formik.errors.last_name ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.last_name}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField
            label="Identificación*"
            number="6"
            name="document_number"
            checked={formik.values.document_number !== ""}
          >
            <input
              className="input cool-input"
              type="text"
              name="document_number"
              onChange={(e) => {
                formik.setFieldValue("patientId", e.target.value);
                customHandleChange(e);
              }}
              value={formik.values.document_number}
            />
            {pressedSubmit && formik.errors.document_number ? (
              <div className="help is-danger">
                {formik.errors.document_number}
              </div>
            ) : null}
          </FormField>

          <FormField
            label="Telefono*"
            number="7"
            name="phone"
            checked={formik.values.phone !== ""}
          >
            <input
              className="input cool-input"
              type="text"
              name="phone"
              onChange={customHandleChange}
              value={formik.values.phone}
            />
            <div class="help">
              <div className="level is-mobile">
                <div className="level-right has-text-danger">
                  {pressedSubmit && formik.errors.phone
                    ? formik.errors.phone
                    : ""}
                </div>
                <div className="level-left">
                  <p>
                    <u>Agregar telefono</u>
                  </p>
                </div>
              </div>
            </div>
          </FormField>

          <FormField2
            number1="8"
            number2="8.1"
            name1="department_id"
            name2="city_id"
            checked1={formik.values.department_id !== ""}
            checked2={formik.values.city_id !== ""}
            label1="Departamento*"
            input1={
              <div>
                <Dropdown
                  options={fetchedData.department_id}
                  name="department_id"
                  arrowColor="has-text-primary"
                  onChange={customHandleChange}
                  value={formik.values.department_id}
                  disabled={loadingCities}
                />
                {pressedSubmit && formik.errors.department_id ? (
                  <div className="help is-danger">
                    {formik.errors.department_id}
                  </div>
                ) : null}
              </div>
            }
            label2="Ciudad*"
            input2={
              <div>
                <Dropdown
                  options={fetchedData.city_id}
                  name="city_id"
                  arrowColor="has-text-primary"
                  onChange={customHandleChange}
                  value={formik.values.city_id}
                  disabled={loadingCities}
                />
                {pressedSubmit && formik.errors.city_id ? (
                  <div className="help is-danger"> {formik.errors.city_id}</div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="9"
            number2="9.1"
            name1="neighborhood"
            name2="address"
            checked1={formik.values.neighborhood !== ""}
            checked2={formik.values.address !== ""}
            label1="Barrio*"
            input1={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="neighborhood"
                  onChange={customHandleChange}
                  value={formik.values.neighborhood}
                />
                {pressedSubmit && formik.errors.neighborhood ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.neighborhood}
                  </div>
                ) : null}
              </div>
            }
            label2="Dirección* "
            input2={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="address"
                  onChange={customHandleChange}
                  value={formik.values.address}
                  readOnly
                />
                {pressedSubmit && formik.errors.address ? (
                  <div className="help is-danger"> {formik.errors.address}</div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="10"
            number2="10.1"
            name1="via"
            name2="via_detail"
            checked1={formik.values.via !== ""}
            checked2={formik.values.via_detail !== ""}
            label1="Via*"
            input1={
              <div>
                <Dropdown
                  options={[
                    "ANILLO VIAL",
                    "AUTOPISTA",
                    "AVENIDA",
                    "BOULEVAR",
                    "CALLE",
                    "CALLEJON",
                    "CARRERA",
                    "CIRCUNVALAR",
                    "CONDOMINIO",
                    "DIAGONAL",
                    "KILOMETRO",
                    "LOTE",
                    "SALIDA",
                    "SECTOR",
                    "TRANSVERSAL",
                    "VEREDA",
                    "VIA",
                  ]}
                  name="via"
                  arrowColor="has-text-primary"
                  onChange={customHandleChange}
                  value={formik.values.via}
                />
                {pressedSubmit && formik.errors.via ? (
                  <div className="help is-danger"> {formik.errors.via}</div>
                ) : null}
              </div>
            }
            label2="Detalles via*"
            input2={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="via_detail"
                  onChange={customHandleChange}
                  value={formik.values.via_detail}
                />
                {pressedSubmit && formik.errors.via_detail ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.via_detail}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="11"
            number2="11.1"
            name1="number_indicative"
            name2="number_indicative_two"
            checked1={formik.values.number_indicative !== ""}
            checked2={formik.values.number_indicative_two !== ""}
            label1="Numero*"
            input1={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="number_indicative"
                  onChange={customHandleChange}
                  value={formik.values.number_indicative}
                />
                {pressedSubmit && formik.errors.number_indicative ? (
                  <div className="help is-danger">
                    {formik.errors.number_indicative}
                  </div>
                ) : null}
              </div>
            }
            label2="Numero secundario*"
            input2={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="number_indicative_two"
                  onChange={customHandleChange}
                  value={formik.values.number_indicative_two}
                />
                {pressedSubmit && formik.errors.number_indicative_two ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.number_indicative_two}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="12"
            number2="12.1"
            name1="inside"
            name2="inside_detail"
            checked1={formik.values.inside !== ""}
            checked2={formik.values.inside_detail !== ""}
            label1="Interior"
            input1={
              <div>
                <Dropdown
                  options={[
                    "APARTAMENTO",
                    "BARRIO",
                    "BLOQUE",
                    "CASA",
                    "CIUDADELA",
                    "CONJUNTO",
                    "CONJUNTO RESIDENCIAL",
                    "EDIFICIO",
                    "ENTRADA",
                    "ETAPA",
                    "INTERIOR",
                    "MANZANA",
                    "NORTE",
                    "OFICINA",
                    "OCCIDENTE",
                    "ORIENTE",
                    "PENTHOUSE",
                    "PISO",
                    "PORTERIA",
                    "SOTANO",
                    "SUR",
                    "TORRE",
                  ]}
                  name="inside"
                  arrowColor="has-text-primary"
                  onChange={customHandleChange}
                  value={formik.values.inside}
                />
                {pressedSubmit && formik.errors.inside ? (
                  <div className="help is-danger"> {formik.errors.inside}</div>
                ) : null}
              </div>
            }
            label2="Detalles interior"
            input2={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="inside_detail"
                  onChange={customHandleChange}
                  value={formik.values.inside_detail}
                />
                <div class="help">
                  <div className="level is-mobile">
                    <div className="level-right has-text-danger">
                      {pressedSubmit && formik.errors.inside_detail
                        ? formik.errors.inside_detail
                        : ""}
                    </div>
                    <div className="level-left">
                      <p>
                        <u>Agregar inside</u>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            }
          />

          <FormField
            label="Género*"
            number="13"
            name="gender"
            checked={formik.values.gender !== ""}
          >
            <Dropdown
              options={["Masculino".toUpperCase(), "Femenino".toUpperCase()]}
              name="gender"
              arrowColor="has-text-primary"
              onChange={customHandleChange}
              value={formik.values.gender}
            />
            {pressedSubmit && formik.errors.gender ? (
              <div className="help is-danger"> {formik.errors.gender}</div>
            ) : null}
          </FormField>

          <FormField2
            number1="14"
            number2="14.1"
            name1="date_birth"
            name2="age"
            checked1={formik.values.date_birth !== ""}
            checked2={formik.values.age !== ""}
            label1="Fecha de nacimiento*"
            input1={
              <div>
                <input
                  className="input cool-input"
                  type="date"
                  name="date_birth"
                  max={new Date().toISOString().split("T")[0]}
                  onChange={(e) => {
                    customHandleChange(e);
                  }}
                  value={formik.values.date_birth}
                />
                {pressedSubmit && formik.errors.date_birth ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.date_birth}
                  </div>
                ) : null}
              </div>
            }
            label2="Edad"
            input2={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="age"
                  onChange={customHandleChange}
                  value={formik.values.age}
                  readOnly
                />
                {formik.errors.age ? (
                  <div className="help is-danger"> {formik.errors.age}</div>
                ) : null}
              </div>
            }
          />

          <FormField2
            number1="15"
            number2="15.1"
            name1="guardian"
            name2="guardian_phone"
            checked1={formik.values.guardian !== ""}
            checked2={formik.values.guardian_phone !== ""}
            label1="Acudiente"
            input1={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="guardian"
                  onChange={customHandleChange}
                  value={formik.values.guardian}
                />
                {pressedSubmit && formik.errors.guardian ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.guardian}
                  </div>
                ) : null}
              </div>
            }
            label2="Telefono acudiente"
            input2={
              <div>
                <input
                  className="input cool-input"
                  type="text"
                  name="guardian_phone"
                  onChange={customHandleChange}
                  value={formik.values.guardian_phone}
                />
                {pressedSubmit && formik.errors.guardian_phone ? (
                  <div className="help is-danger">
                    {" "}
                    {formik.errors.guardian_phone}
                  </div>
                ) : null}
              </div>
            }
          />

          <FormField
            label="Notas"
            number="16"
            name="notas"
            checked={formik.values.notas !== ""}
          >
            <input
              className="input cool-input"
              type="text"
              name="notas"
              onChange={customHandleChange}
              value={formik.values.notas}
            />
            {pressedSubmit && formik.errors.notas ? (
              <div className="help is-danger"> {formik.errors.notas}</div>
            ) : null}
          </FormField>

          <br />
          <div className="has-text-right has-text-centered-mobile">
            <span className="has-text-danger has-text-right">
              {uncompleteForm ? "Por favor revise el formulario" : " "}
            </span>
          </div>
          <br />

          <div className="field has-text-right has-text-centered-mobile">
            <div className="control">
              <button
                className="button is-primary"
                type="submit"
                data-tab="tab2"
                style={{ width: "150px" }}
                disabled={false}
                onClick={onContinueClicked}
              >
                Continuar
              </button>
            </div>
          </div>
        </form>
      ) : (
        <div>Error cargando las dependencias del formulario</div>
      )}
    </>
  );
};

const mapStateToPros = (state) => ({
  newsReducer: state.newsReducer,
  trackingReducer: state.trackingReducer,
});

export default connect(mapStateToPros, null)(Form);
