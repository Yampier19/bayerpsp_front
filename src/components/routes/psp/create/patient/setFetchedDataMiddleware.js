export const setFetchedDataMiddleware = (array=[]) => {
  const _array = [1, 2, 3, 4, 5, 6]
  if(!array) return [1];
  if(!Array.isArray(array)) return _array
  if(process.env.NODE_ENV !== 'production'){
    if(array.length === 0) return _array
  }
  return array
}