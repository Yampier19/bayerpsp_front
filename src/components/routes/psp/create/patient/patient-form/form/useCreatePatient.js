import React, { useState, useEffect, useContext } from "react";
import { useFormikContext } from "formik";

import { useCreatePatientFormUtil } from "../components/form-create-patient-util";
import { useListStatus } from "services/bayer/psp/patient/usePatient";
import { useListAllDepartements } from "services/bayer/psp/departments/useDepartments";
import { useListCitiesByDepartment } from "services/bayer/psp/cities/useCities";
import Dropdown from "components/form/dropdown";
import { DropdownOthers } from "components/form/dropdown-others";
import { emailTypesList } from "utils/constants/mail.list";
import { getRequired } from "utils/forms";
import { FormCreatePatientContext } from "../../context/form-context";
import { setFetchedDataMiddleware } from "../../setFetchedDataMiddleware";
import { useSearchedPatientContext } from "../../context/searched-patient-context";

export const useCreatePatientForm = () => {
  /* ~~~****** Hooks ********~* */
  const { values, setFieldValue, errors, setFieldTouched } = useFormikContext();
  const { isSearched } = useSearchedPatientContext();
  const { validationSchema } = useCreatePatientFormUtil();
  const { setStep, step } = useContext(FormCreatePatientContext);
  const [loadingFields, setLoadingFields] = useState({
    loaded: false,
    values: [
      { name: "department_id", realized: false },
      { name: "patient_statu_id", realized: false },
    ],
    trigger: false,
  });
  const _setLoadingFields = (_name) => {
    const { values, trigger } = loadingFields;
    const index = values.findIndex(({ name }) => name === _name);
    const _value = values[index];
    _value.realized = true;
    const _loaded =
      values.filter(({ realized }) => realized).length === values.length;
    setLoadingFields((prevState) => ({
      loaded: _loaded,
      values: values,
      trigger: !trigger,
    }));
  };
  /* ~~~****** Hooks ********~* */

  /* ~~~****** Form ********~* */
  const initialForm = [
    {
      fields: 1,
      names: ["patientId"],
      labels: ["Código de usuario"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" readOnly {...props} />
        ),
      ],
    },
    {
      fields: 1,
      names: ["patient_statu_id"],
      labels: ["Estado del paciente *"],
      childs: [
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
      ],
    },
    {
      fields: 1,
      names: ["date_active"],
      labels: ["Fecha de activación*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="date" readOnly {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["email", "email_type"],
      labels: ["Correo electrónico*", "@"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => <DropdownOthers options={emailTypesList()} {...props} />,
      ],
    },
    {
      fields: 2,
      names: ["name", "last_name"],
      labels: ["Nombre*", "Apellido*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["document_type", "document_number"],
      labels: ["Tipo de documento*", "Número de identificación*"],
      childs: [
        (props) => (
          <Dropdown
            options={["CC", "TI", "DNI"]}
            arrowColor="has-text-primary"
            {...props}
          />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 1,
      names: ["phone"],
      labels: ["Telefono*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["department_id", "city_id"],
      labels: ["Departamento*", "Ciudad*"],
      childs: [
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
        (props) => (
          <Dropdown options={[]} arrowColor="has-text-primary" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["neighborhood", "address"],
      labels: ["Barrio*", "Dirección*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="text" readOnly {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["via", "via_detail"],
      labels: ["Via*", "Detalles via*"],
      childs: [
        (props) => (
          <Dropdown
            options={[
              "ANILLO VIAL",
              "AUTOPISTA",
              "AVENIDA",
              "BOULEVAR",
              "CALLE",
              "CALLEJON",
              "CARRERA",
              "CIRCUNVALAR",
              "CONDOMINIO",
              "DIAGONAL",
              "KILOMETRO",
              "LOTE",
              "SALIDA",
              "SECTOR",
              "TRANSVERSAL",
              "VEREDA",
              "VIA",
            ]}
            arrowColor="has-text-primary"
            {...props}
          />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["number_indicative", "number_indicative_two"],
      labels: ["Numero*", "Numero secundario*"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["inside", "inside_detail"],
      labels: ["Interior", "Detalles interior"],
      childs: [
        (props) => (
          <Dropdown
            options={[
              "APARTAMENTO",
              "BARRIO",
              "BLOQUE",
              "CASA",
              "CIUDADELA",
              "CONJUNTO",
              "CONJUNTO RESIDENCIAL",
              "EDIFICIO",
              "ENTRADA",
              "ETAPA",
              "INTERIOR",
              "MANZANA",
              "NORTE",
              "OFICINA",
              "OCCIDENTE",
              "ORIENTE",
              "PENTHOUSE",
              "PISO",
              "PORTERIA",
              "SOTANO",
              "SUR",
              "TORRE",
            ]}
            arrowColor="has-text-primary"
            {...props}
          />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 1,
      names: ["gender"],
      labels: ["Género*"],
      childs: [
        (props) => (
          <Dropdown
            options={["Masculino".toUpperCase(), "Femenino".toUpperCase()]}
            arrowColor="has-text-primary"
            {...props}
          />
        ),
      ],
    },
    {
      fields: 2,
      names: ["date_birth", "age"],
      labels: ["Fecha de nacimiento*", "Edad"],
      childs: [
        (props) => (
          <input className="input cool-input" type="date" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="text" readOnly {...props} />
        ),
      ],
    },
    {
      fields: 2,
      names: ["guardian", "guardian_phone"],
      labels: ["Acudiente", "Telefono acudiente"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
    {
      fields: 1,
      names: ["notas"],
      labels: ["Notas"],
      childs: [
        (props) => (
          <input className="input cool-input" type="text" {...props} />
        ),
      ],
    },
  ];
  const [patientForm, setPatientForm] = useState({
    form: initialForm,
    changed: false,
  });
  /* ~~~****** Form ********~* */

  /* ~~~****** Utils ********~* */
  const getAge = (dateString) => {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    if (dateString !== null && dateString !== "") {
      return age;
    } else {
      return "";
    }
  };
  const setDropdownOptions = (name = "", options = []) => {
    const { form, changed } = patientForm;
    const index = form.findIndex(({ names }) => names.includes(name));
    if (index === -1) return;
    const formCopy = form;
    const childIndex = formCopy[index].names.indexOf(name);
    const Child = formCopy[index].childs[childIndex];
    const NewChlid = (otherProps = {}) =>
      React.createElement(Dropdown, {
        ...Child().props,
        disabled: false,
        options: options,
        ...otherProps,
      });
    formCopy[index].childs[childIndex] = NewChlid;
    setPatientForm({ form: formCopy, changed: !changed });
  };
  const setDisabledDropdown = (name = "") => {
    const { form, changed } = patientForm;
    const index = form.findIndex(({ names }) => names.includes(name));
    const formCopy = form;
    const childIndex = formCopy[index].names.indexOf(name);
    const Child = formCopy[index].childs[childIndex];
    const NewChlid = (otherProps = {}) =>
      React.createElement(Dropdown, {
        ...Child().props,
        disabled: true,
        ...otherProps,
      });
    formCopy[index].childs[childIndex] = NewChlid;
    setPatientForm({ form: formCopy, changed: !changed });
  };
  // const appendFieldInIndex = (field = {}, index = 0) => {
  //   console.log("apended in index", index);
  //   if (index === 0) return;
  //   const { form, changed } = patientForm;
  //   const patientFormCopy = form;
  //   patientFormCopy.splice(index - 1, 0, field);
  //   setPatientForm({ form: patientFormCopy, changed: !changed });
  // };
  // const deleteFieldInIndex = (index = 0) => {
  //   if (index === 0) return;
  //   const { form, changed } = patientForm;
  //   const patientFormCopy = form;
  //   patientFormCopy.splice(index - 1, 1);
  //   setPatientForm({ form: patientFormCopy, changed: !changed });
  // };
  const reFetchQueryConfig = {
    enabled: false,
    refetchOnWindowFocus: false,
    timeout: 1000,
  };
  const queryConfig = {
    refetchOnWindowFocus: false,
    refetchOnmount: false,
    refetchOnReconnect: true,
    retry: false,
  };
  /* ~~~****** Utils ********~* */

  /* ~~~****** React-query ********~* */
  const { refetch: refreshCities } = useListCitiesByDepartment(
    values.department_id,
    reFetchQueryConfig
  );
  const { refetch: getPatientStatus } = useListStatus({
    queryConfig,
  });
  const { refetch: getDepartamets } = useListAllDepartements({
    queryConfig,
  });
  /* ~~~****** React-query ********~* */

  /* ~~~****** useEffect ********~* */
  useEffect(() => {
    getDepartamets().then(({ data, isSuccess }) => {
      if (isSuccess && data.code === 200) {
        setDropdownOptions("department_id", data.data);
        _setLoadingFields("department_id");
      }
    });
  }, []);
  useEffect(() => {
    getPatientStatus().then(({ data, isSuccess }) => {
      if (isSuccess && data.code === 200) {
        setDropdownOptions(
          "patient_statu_id",
          setFetchedDataMiddleware(data.data)
        );
        _setLoadingFields("patient_statu_id");
      }
    });
  }, []);
  useEffect(() => {
    setDisabledDropdown("city_id");
    if (values.department_id) {
      refreshCities().then(({ data, isSuccess }) => {
        if (isSuccess) {
          const { data: cities, code } = data;
          code == 200 &&
            setDropdownOptions("city_id", setFetchedDataMiddleware(cities));
          if (isSearched) {
            setDisabledDropdown("city_id");
          }
        }
      });
    }
  }, [values.department_id]);
  useEffect(() => {
    setFieldTouched("age", true);
    setFieldValue("age", getAge(values.date_birth));
  }, [values.date_birth]);
  useEffect(() => {
    const requiredFields = getRequired(validationSchema);
    const totalRequiredFields = requiredFields.length;
    const totalErrors = Object.entries(errors).length;
    const total =
      ((totalRequiredFields - totalErrors) * 100) / totalRequiredFields;
    setStep({ ...step, percent: total });
  }, [values, errors]);
  useEffect(() => {
    setFieldValue(
      "address",
      `${values.via} ${values.via_detail}  ${
        values.number_indicative ? `# ${values.number_indicative} ` : ""
      } ${
        values.number_indicative_two ? `- ${values.number_indicative_two}` : ""
      } ${values.inside ? `${values.inside} ` : ""} ${
        values.inside_detail ? `${values.inside_detail} ` : ""
      }`
    );
  }, [
    values.via,
    values.via_detail,
    values.number_indicative,
    values.number_indicative_two,
    values.inside,
    values.inside_detail,
  ]);
  useEffect(() => {
    if (values.document_number) {
      setFieldValue("patientId", values.document_number);
    }
  }, [values.document_number]);
  useEffect(() => {
    if (isSearched) {
      const { form, changed } = patientForm;
      const _patientForm = [];
      form.forEach((field) => {
        const { childs } = field;
        const _disabledChilds = [];
        childs.forEach((Child) => {
          const DisabledChild = (otherProps = {}) =>
            React.createElement(Child, {
              ...Child().props,
              disabled: true,
              readOnly: true,
              ...otherProps,
            });
          _disabledChilds.push(DisabledChild);
          return DisabledChild;
        });
        _patientForm.push({ ...field, childs: _disabledChilds });
        return { ...field, childs: _disabledChilds };
      });
      setPatientForm({ form: _patientForm, changed: !changed });
    }
  }, [isSearched]);
  /* ~~~****** useEffect ********~* */

  return { form: patientForm, isLoading: !loadingFields.loaded };
};
