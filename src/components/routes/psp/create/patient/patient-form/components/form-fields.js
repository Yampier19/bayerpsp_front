import React, { Fragment } from "react";
import { Field, useFormikContext } from "formik";
import { useCreatePatientForm } from "../form/useCreatePatient";
import FormField from "components/form/formfield";
import FormField2 from "components/form/formfield2";
import LoadingComponent from "components/base/loading-component";

export const FormFields = () => {
  const { values, errors } = useFormikContext();
  const { form, isLoading } = useCreatePatientForm();
  const formLength = form.form.length;
  const isNullyValue = (value) => {
    if (typeof value === "string") {
      return value !== ""
    }
    if (typeof value === "number") {
      return value !== 0
    }
  }

  if (isLoading) return <LoadingComponent loadingText="Cargando formulario" />
  return (
    <Fragment  >
      {form.form.map((form, index) => {
        const isLastElement = index === formLength - 1;
        const number = index + 1;
        if (form.fields === 1) {
          const [name] = form.names;
          const checked = isNullyValue(values[name]);
          const [label] = form.labels;
          const [Child] = form.childs;
          return (
            <FormField
              name={name}
              key={index + "-create-patient"}
              checked={checked}
              label={label}
              number={String(number)}
              noline={isLastElement}
            >
              <Field name={name} id={name} >
                {({ field, form: { touched, errors } }) => (
                  <div>
                    {Child({ ...field })}
                    {touched[name] && errors[name] ? (
                      <div className="help is-danger">{errors[name]}</div>
                    ) : null}
                  </div>
                )}
              </Field>
            </FormField>
          );
        }
        if (form.fields === 2) {
          const [name1, name2] = form.names;
          const checked1 = isNullyValue(values[name1]),
            checked2 = isNullyValue(values[name2]);
          const [label1, label2] = form.labels;
          const [Child1, Child2] = form.childs;
          const input1 = (
            <Field name={name1}>
              {({ field, form: { touched, errors } }) => (
                <div>
                  {Child1({ ...field })}
                  {touched[name1] && errors[name1] ? (
                    <div className="help is-danger">{errors[name1]}</div>
                  ) : null}
                </div>
              )}
            </Field>
          );
          const input2 = (
            <Field name={name2}>
              {({ field, form: { touched, errors } }) => (
                <div>
                  {Child2({ ...field })}
                  {touched[name2] && errors[name2] ? (
                    <div className="help is-danger">{errors[name2]}</div>
                  ) : null}
                </div>
              )}
            </Field>
          );
          return (
            <FormField2
              key={index + "-create-patient"}
              number1={String(number)}
              number2={String(number + 0.1)}
              name1={name1}
              name2={name2}
              checked1={checked1}
              checked2={checked2}
              label1={label1}
              label2={label2}
              input1={input1}
              input2={input2}
              noline={isLastElement}
            />
          );
        }
      })}
    </Fragment>
  );
}