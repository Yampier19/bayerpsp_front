import React from "react";
import { Fragment } from "react";
import { LoadingButton } from "@mui/lab";

export const FormButton = ({ loading = false, name = "CONTINUAR" }) => {
  return (
    <Fragment>
      <br />
      <div className="field">
        <div className="control has-text-right has-text-centered-mobile">
          <LoadingButton
            className="button is-hblue"
            type="submit"
            loading={loading}
            style={{ width: "150px" }}
          >
            {name}
          </LoadingButton>
        </div>
      </div>
    </Fragment>
  );
};
