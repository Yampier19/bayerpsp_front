import { useContext } from "react";
import _ from "lodash";
import * as Yup from "yup";

import { FormCreatePatientContext } from "../../context/form-context";
import { useSearchedPatientContext } from "../../context/searched-patient-context";

const requiredFieldType = "Campo obligatorio";
export const useCreatePatientFormUtil = () => {
  const { setForm, submitPatient } = useContext(
    FormCreatePatientContext
  );
  const {patient:_user} = useSearchedPatientContext();
  const user = _user ? _user : {};

  const dateToday = () => {
    const date = new Date().toLocaleDateString().split("T")[0];
    const [day, month, year] = date.split("/");
    const lessThanTenParser = (value = 0) =>
      value < 10 ? "0" + String(value) : value;
    const _date = `${year}-${lessThanTenParser(month)}-${lessThanTenParser(
      day
    )}`;
    return _date;
  };

  const getAge = (dateString) => {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    if (dateString !== null && dateString !== "") {
      return age;
    } else {
      return "";
    }
  };

  const dateParser = (date = "") => {
    if (!date) return "";
    return date.split(" ")[0];
  };
  const splitEmail = (email = "") => {
    if (!email) return "";
    return email.split("@");
  };
  const lowerCase = (value = "") => {
    if (!value) return "";
    return value.toLocaleLowerCase();
  }
  const parseIntToString = (value = 0) => {
    if (!value) return "";
    return String(value)
  }
  const addressData = (address="") => {
    if(!address) return "";
    const splitted = address.split(" ").filter((x) => x.length > 0);
    const [
      via,
      via_detail,
      _ignore,
      number_indicative,
      __ignore,
      number_indicative_two,
      inside,
      inside_detail,
    ] = splitted;
    return { via, via_detail, number_indicative, number_indicative_two, inside, inside_detail}
  }

  const initialValues = {
    patientId: user.document_number || "",
    patient_statu_id: user.patient_statu_id || "",
    date_active: dateParser(user.date_active) || dateToday(),
    email: splitEmail(user.email)[0] || "",
    email_type: lowerCase(splitEmail(user.email)[1]) || "",
    name: user.name || "",
    last_name: user.last_name || "",
    document_type: user.document_type || "CC",
    document_number: user.document_number || "",
    phone: parseIntToString(_.get(_.get(user, ["phones"], [])[0], ["number"], 0)) || "",
    department_id: user.department_id || "",
    city_id: user.city_id || "",
    neighborhood: user.neighborhood || "",
    address: user.address || "",
    via: addressData(user.address).via || "",
    via_detail: addressData(user.address).via_detail || "",
    number_indicative: addressData(user.address).number_indicative || "",
    number_indicative_two: addressData(user.address).number_indicative_two || "",
    phone_number: parseIntToString(_.get(_.get(user, ["phones"], [])[0], ["number"], 0)) || "",
    inside: addressData(user.address).inside || "",
    inside_detail: addressData(user.address).inside_detail || "",
    gender: user.gender || "",
    date_birth: dateParser(user.date_birth) || "",
    age: user.age || 0,
    guardian: user.guardian || "",
    guardian_phone: user.guardian_phone || "",
    notas: "",
  };
    // console.log('initial values', initialValues);

  const validationSchema = Yup.object().shape({
    patientId: Yup.number().typeError("se debe especificar un valor numerico"),
    patient_statu_id: Yup.string().required(requiredFieldType),
    date_active: Yup.string().required(requiredFieldType),
    email: Yup.string().required(requiredFieldType), //.email("email invalido"),
    name: Yup.string().required(requiredFieldType),
    last_name: Yup.string().required(requiredFieldType),
    document_number: Yup.number()
      .typeError("se debe especificar un valor numerico")
      .required(requiredFieldType)
      .test(
        "len",
        "Lonngitud invalida, debe ser mayor a 7",
        (val) => val && String(val).length > 6
      )
      .test(
        "len",
        "Lonngitud invalida, debe ser menor a 10",
        (val) => val && String(val).length < 11
      ),
    phone: Yup.number()
      .typeError("se debe especificar un valor numerico")
      .required(requiredFieldType),
    department_id: Yup.string().required(requiredFieldType),
    city_id: Yup.string().required(requiredFieldType),
    neighborhood: Yup.string().required(requiredFieldType),
    address: Yup.string().required(requiredFieldType),
    via: Yup.string().required(requiredFieldType),
    via_detail: Yup.string().required(requiredFieldType),
    number_indicative: Yup.string().required(requiredFieldType),
    number_indicative_two: Yup.string().required(requiredFieldType),
    phone_number: Yup.number().typeError(
      "se debe especificar un valor numerico"
    ),
    inside: Yup.string(),
    inside_detail: Yup.string(),
    gender: Yup.string().required(requiredFieldType),
    date_birth: Yup.string().required(requiredFieldType),
    age: Yup.number()
      .integer()
      .min(1, "La edad debe ser mayor a 1")
      .typeError("se debe especificar un valor numerico"),
    guardian: Yup.string(),
    guardian_phone: Yup.number().typeError(
      "se debe especificar un valor numerico"
    ),
    notas: Yup.string(),
    email_type: Yup.string().required(requiredFieldType),
    document_type: Yup.string().required(requiredFieldType),
  });

  const submitForm = async (valuesOriginal) => {
    const values = Object.assign({}, valuesOriginal);
    const parseToNumber = [
      "city_id",
      "department_id",
      "document_number",
      "patient_statu_id",
      "guardian_phone",
    ];
    parseToNumber.forEach((value) => {
      if (typeof values[value] === "number"){
        return;
      }
      if (
        values[value] &&
        values[value].length > 0 &&
        !isNaN(parseInt(values[value]))
      ) {
        values[value] = parseInt(values[value]);
      } else {
        values[value] = 0;
      }
    });
    const callbackValues = [
      () => {
        values.age = getAge(values.date_birth);
      },
      () => {
        const { number_indicative, phone, phone_number } = values;
        const phones = [];
        if (number_indicative && phone_number) {
          phones.push(String(number_indicative + " " + phone_number));
        }
        phones.push(String(phone));
        values.phones = phones;
      },
      () => {
        const _email = values.email + "@" + values.email_type;
        values.email = _email;
      },
    ];
    callbackValues.forEach((action) => {
      action();
    });
    const valuesToDelete = [
      "notas",
      "number_indicative",
      "patientId",
      "phone",
      "number_indicative",
      "phone_number",
      "email_type",
    ];
    valuesToDelete.forEach((value) => {
      delete values[value];
    });
    values.application_system_id = 1;
    console.log("Values are", values);
    setForm((prevState) => ({
      ...prevState,
      patient: { ...prevState.patient, ...values },
    }));
    submitPatient();
  };

  return { submitForm, validationSchema, initialValues };
};
