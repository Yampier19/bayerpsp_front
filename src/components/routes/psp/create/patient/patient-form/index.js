import React from "react";
import { Form, Formik } from "formik";

import { FormButton } from "./components/form-button";
import { useCreatePatientFormUtil } from "./components/form-create-patient-util";
import { FormFields } from "./components/form-fields";

export const FormCreatePatient = () => {
  const { submitForm, validationSchema, initialValues } =
    useCreatePatientFormUtil();
  return (
    <Formik
      enableReinitialize={true}
      initialValues={initialValues}
      validationSchema={validationSchema}
      children={
        <Form>
          <FormFields />
          <FormButton />
        </Form>
      }
      onSubmit={submitForm}
    />
  );
};
