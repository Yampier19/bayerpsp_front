
import CreateBtn from 'components/base/buttons/create-btn';
import Form from './form';

const CreateEPS = props => {

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 pt-0 pb-3 ">
                <div className="columns is-mobile">
                    <div className="column is-3-desktop ">
                        <h1 className="subtitle has-text-primary is-3 mb-1"  style={{ whiteSpace: 'nowrap'}}>
                            <strong className="has-text-primary">CREAR</strong>
                            <span className="is-hidden-touch"> - EPS / OPL</span >
                        </h1>
                        <h1 className="subtitle has-text-primary is-hidden-desktop">EPS / OPL</h1>
                    </div>
                    <div className="column is-3-desktop is-offset-6-desktop is-6-mobile">
                        <CreateBtn active="3"/>
                    </div>
                </div>
            </section>

            {/* form */}
            <section className="section px-0 is-flex-grow-1" style={{height: '1px', overflow: 'hidden'}}>
                <div className="coolscroll primary" style={{height: '100%', overflow: 'auto'}}>
                    <Form/>
                </div>
            </section>

        </div>
    );
}

export default CreateEPS;
