import FormField from 'components/form/formfield';

import * as Yup from 'yup';
import {useFormik} from 'formik';

import {connect} from 'react-redux';
import {set_modal_on} from 'redux/actions/modalActions';
import {InfoModal} from 'components/base/modal-system/models/modal';

const Form = props => {

    const formSchema = Yup.object().shape({
        asegurador: Yup.string().required('Este campo es obligatorio'),
        departamento: Yup.string().required('Este campo es obligatorio'),
        operadorLogistico: Yup.string().required('Este campo es obligatorio'),
    });

    const formik = useFormik({
        initialValues:  {
            asegurador: '',
            departamento: '',
            operadorLogistico: '',
        },
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            // alert(JSON.stringify(values, null, 2));
            const {_icons} = props.modalReducer;
            props.set_modal_on( new InfoModal('La EPS ha sido creada éxitosamente', _icons.CHECK_PRIMARY) );
        }
    });

    return(
        <form id="form" onSubmit={formik.handleSubmit} className="pr-3 py-3">
            <FormField label="Asegurador" number="1" name="asegurador" checked={formik.values.asegurador}>
                <input className="input cool-input" type="text" name="asegurador" onChange={formik.handleChange} value={formik.values.asegurador} />
                {formik.touched.asegurador && formik.errors.asegurador ? <div className="help is-danger"> {formik.errors.asegurador}</div> : null }
            </FormField>

            <FormField label="Departamento" number="2" name="departamento" checked={formik.values.departamento}>
                <input className="input cool-input" type="text" name="departamento" onChange={formik.handleChange} value={formik.values.departamento} />
                {formik.touched.departamento && formik.errors.departamento ? <div className="help is-danger"> {formik.errors.departamento}</div> : null }
            </FormField>

            <FormField label="Operador logistico" number="3" name="operadorLogistico" checked={formik.values.operadorLogistico} noline>
                <input className="input cool-input" type="text" name="operadorLogistico" onChange={formik.handleChange} value={formik.values.operadorLogistico} />
                {formik.touched.operadorLogistico && formik.errors.operadorLogistico ? <div className="help is-danger"> {formik.errors.operadorLogistico}</div> : null }
            </FormField>

            <br/>

            <div className="field">
                <div className="control has-text-right has-text-centered-mobile">
                    <button
                        className="button is-primary"
                        type="submit"
                        style={{width: '150px'}}
                    >
                        CREAR
                    </button>
                </div>
            </div>

        </form>
    );
}
const mapStateToPros = state => ({
    modalReducer: state.modalReducer
})

export default connect(
    mapStateToPros,
    {
        set_modal_on
    }
)(Form);
