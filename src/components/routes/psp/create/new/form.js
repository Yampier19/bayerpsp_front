import React from "react";
import { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";

import FormField from "components/form/formfield";
import Dropdown from "components/form/dropdown";
import FormDependencies from "./formDependencies";
import LoadingComponent from "components/base/loading-component";
import postHandler from "./postHandler";

import { connect } from "react-redux";

const Form = (props) => {
  const [dependencies, setDepencies] = useState({
    loading: true,
    id: "",
    productos: [],
    users: [],
  });

  const { push } = useHistory();

  const formSchema = Yup.object().shape({
    id: Yup.string(),
    asunto: Yup.string()
      .min(5, "El campo tiene que tener minimo 5 caracteres")
      .required("Campo obligatorio"),
    producto: Yup.string().required("Campo obligatorio"),
    novedad: Yup.string().required("Campo obligatorio"),
    fechaReporte: Yup.string().required("Campo obligatorio"),
    fechaRespuesta: Yup.string().required("Campo obligatorio"),
    observaciones: Yup.string()
      .min(5, "El campo tiene que tener minimo 5 caracteres")
      .required("Campo obligatorio"),
    responsable: Yup.string().required("Campo obligatorio"),
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      id: Number(dependencies.id) + 1,
      asunto: "",
      producto: "",
      novedad: "",
      fechaReporte: "",
      fechaRespuesta: "",
      observaciones: "",
      responsable: "",
    },
    validateOnChange: true,
    validationSchema: formSchema,
    onSubmit: async (values, handlers) => {
      // alert(JSON.stringify(values, null, 2));
      const res = await postHandler({ ...values, id: values.id - 1 });
      if (res) {
        handlers.resetForm();
        push("/news/consult?page=1");
      }
    },
  });

  return (
    <>
      <FormDependencies
        dependencies={dependencies}
        setDepencies={setDepencies}
      />
      {dependencies.loading ? (
        <LoadingComponent loadingText="Cargando datos" />
      ) : (
        <form
          id="form"
          onSubmit={formik.handleSubmit}
          className="coolscroll primary pr-3 py-3"
        >
          <FormField label="Id" number="1" name="id" checked={formik.values.id}>
            <input
              className="input cool-input"
              readOnly
              type="text"
              name="id"
              onChange={formik.handleChange}
              value={formik.values.id}
            />
            {formik.touched.userCode && formik.errors.userCode ? (
              <div className="help is-danger"> {formik.errors.userCode}</div>
            ) : null}
          </FormField>

          <FormField
            label="Asunto"
            number="2"
            name="asunto"
            checked={formik.values.asunto}
          >
            <input
              className="input cool-input"
              type="text"
              name="asunto"
              onChange={formik.handleChange}
              value={formik.values.asunto}
            />
            {formik.touched.asunto && formik.errors.asunto ? (
              <div className="help is-danger"> {formik.errors.asunto}</div>
            ) : null}
          </FormField>

          <FormField
            label="Producto"
            number="3"
            name="producto"
            checked={formik.values.producto}
          >
            <Dropdown
              name="producto"
              onChange={formik.handleChange}
              options={dependencies.productos}
              arrowColor="has-text-hblue"
              value={formik.values}
            />
            {formik.touched.producto && formik.errors.producto ? (
              <div className="help is-danger"> {formik.errors.producto}</div>
            ) : null}
          </FormField>

          <FormField
            label="Novedad"
            number="4"
            name="novedad"
            checked={formik.values.novedad}
          >
            <input
              className="input cool-input"
              type="text"
              name="novedad"
              onChange={formik.handleChange}
              value={formik.values.novedad}
            />
            {formik.touched.novedad && formik.errors.novedad ? (
              <div className="help is-danger"> {formik.errors.novedad}</div>
            ) : null}
          </FormField>

          <FormField
            label="Fecha de reporte"
            number="5"
            name="fechaReporte"
            checked={formik.values.fechaReporte}
          >
            <input
              className="input cool-input"
              type="date"
              name="fechaReporte"
              onChange={formik.handleChange}
              value={formik.values.fechaReporte}
            />
            {formik.touched.fechaReporte && formik.errors.fechaReporte ? (
              <div className="help is-danger">
                {" "}
                {formik.errors.fechaReporte}
              </div>
            ) : null}
          </FormField>

          <FormField
            label="Fecha de respuesta"
            number="6"
            name="fechaRespuesta"
            checked={formik.values.fechaRespuesta}
          >
            <input
              className="input cool-input"
              type="date"
              name="fechaRespuesta"
              onChange={formik.handleChange}
              value={formik.values.fechaRespuesta}
            />
            {formik.touched.fechaRespuesta && formik.errors.fechaRespuesta ? (
              <div className="help is-danger">
                {" "}
                {formik.errors.fechaRespuesta}
              </div>
            ) : null}
          </FormField>

          <FormField
            label="Observaciones"
            number="7"
            name="fechaRespuesta"
            checked={formik.values.observaciones}
          >
            <input
              className="input cool-input"
              type="text"
              name="observaciones"
              onChange={formik.handleChange}
              value={formik.values.observaciones}
            />
            {formik.touched.observaciones && formik.errors.observaciones ? (
              <div className="help is-danger">
                {" "}
                {formik.errors.observaciones}
              </div>
            ) : null}
          </FormField>

          <FormField
            label="Responsable"
            number="8"
            name="responsable"
            checked={formik.values.responsable}
            noline
          >
            <Dropdown
              name="responsable"
              onChange={formik.handleChange}
              options={dependencies.users}
              arrowColor="has-text-hblue"
              value={formik.values}
            />
            {formik.touched.responsable && formik.errors.responsable ? (
              <div className="help is-danger"> {formik.errors.responsable}</div>
            ) : null}
          </FormField>

          <br />

          <div className="field">
            <div className="control has-text-right has-text-centered-mobile">
              <button
                className={`button is-primary ${
                  props.newsReducer["CREATE_NEW"].loading ? "is-loading" : ""
                }`}
                type="submit"
                style={{ width: "150px" }}
              >
                CREAR
              </button>
            </div>
          </div>
        </form>
      )}
    </>
  );
};

const mapStateToPros = (state) => ({
  newsReducer: state.newsReducer,
});

export default connect(mapStateToPros, null)(Form);
