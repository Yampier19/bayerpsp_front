import store from "redux/store";

import { news_post_request } from "redux/actions/newsActions";

import { set_modal_on } from "redux/actions/modalActions";
import { InfoModal } from "components/base/modal-system/models/modal";

const postHandler = async (values) => {
  const url = `/api/novelty`;
  const req_name = "CREATE_NEW";
  const req_body = {
    novelty: {
      affair: values.asunto,
      novelty: values.novedad,
      report_date: values.fechaReporte,
      answer_date: values.fechaRespuesta,
      observation: values.observaciones,
      product_id: Number(values.producto),
      manager_id: Number(values.responsable),
      novelty_statu_id: 1,
    },
  };

  const done = await store.dispatch(news_post_request(url, req_name, req_body));

  if (done) {
    const modalReducer = store.getState().modalReducer;
    const { _icons } = modalReducer;
    const modal = new InfoModal(
      "La novedad ha sido creada éxitosamente",
      _icons.CHECK_PRIMARY
    );
    store.dispatch(set_modal_on(modal));
    return done;
  }
};

export default postHandler;
