import { useEffect } from "react";

import { connect } from "react-redux";
import { get_data_request } from "redux/actions/petitionActions";

const FormDependencies = (props) => {
  const { dependencies, setDepencies } = props;

  useEffect(() => {
    setDepencies({
      ...dependencies,
      loading: true,
    });
    const consult_dependencies = async () => {
      await props.get_data_request(`/api/product`, "PRODUCT");

      await props.get_data_request(`/api/novelty`, "NOVELTIES");

      await props.get_data_request(`/api/users`, "USERS");
    };
    consult_dependencies();
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    const data = props.petitionReducer["PRODUCT"].data;
    if (data == null) return;
    setDepencies({
      ...dependencies,
      loading: false,
      productos: data,
    });
    //eslint-disable-next-line
  }, [props.petitionReducer["PRODUCT"].data]);

  useEffect(() => {
    const data = props.petitionReducer["NOVELTIES"].data;
    if (data == null) return;
    setDepencies({
      ...dependencies,
      loading: false,
      id: data.length.toString(),
    });
    //eslint-disable-next-line
  }, [props.petitionReducer["NOVELTIES"].data]);

  useEffect(() => {
    const data = props.petitionReducer["USERS"].data;
    if (data == null) return;
    setDepencies({
      ...dependencies,
      loading: false,
      users: data,
    });
    //eslint-disable-next-line
  }, [props.petitionReducer["USERS"].data]);

  return null;
};

const mapStateToPros = (state) => ({
  petitionReducer: state.petitionReducer,
});

export default connect(mapStateToPros, {
  get_data_request,
})(FormDependencies);
