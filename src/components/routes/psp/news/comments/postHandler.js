import store from "redux/store";

import { news_post_request } from "redux/actions/newsActions";

const postHandler = async (values, urlData) => {
  const url = `/api/comment/novelty/${urlData.id}`;
  const req_name = "NEW_POST_COMMENT";
  const req_body = {
    comment: values.comment,
    files: [],
  };

  await store.dispatch(news_post_request(url, req_name, req_body));
};

export default postHandler;
