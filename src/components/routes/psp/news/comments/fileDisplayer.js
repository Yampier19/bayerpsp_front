// import fs from 'fs';

const FileDisplayer = (props) => {
  const files = props.files || [];
  // const _files = [];
  // useEffect(
  //     () => {
  //
  //         files.map((file, i) => {
  //
  //         });
  //
  //     }, []
  // );

  return (
    <div>
      {files.map((file, i) => (
        <div>{file.url}</div>
      ))}
    </div>
  );
};

export default FileDisplayer;
