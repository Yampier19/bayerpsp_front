import { useEffect } from "react";
import { connect } from "react-redux";

import { news_get_request } from "redux/actions/newsActions";

const Dependencies = (props) => {
  const { dispatchDep } = props;
  const { triggerReload } = props;

  const { id } = props.inputData;
  // console.log(props);

  const newsReducer = props.newsReducer;

  const load_dependencies = async () => {
    const dep = [
      {
        url: `/api/comment/novelty/${id}`,
        req_name: "GET_NEW_COMMENT",
        req_body: null,
      },
    ];

    dep.map(async (d) => {
      await props.news_get_request(d.url, d.req_name, d.req_body);
    });
  };

  useEffect(() => {
    load_dependencies();
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    load_dependencies();
    //eslint-disable-next-line
  }, [triggerReload]);

  useEffect(() => {
    const data = newsReducer["GET_NEW_COMMENT"].data;

    if (data != null && newsReducer["GET_NEW_COMMENT"].success) {
      let _data = {};

      _data = {
        comments: data.map((comment) => ({
          id: comment.id,
          body: comment.description,
          user: comment.user,
          date: comment.created_at,
          replies: comment.responses.map((reply) => ({
            id: reply.id,
            body: reply.description,
            user: reply.user,
            date: reply.created_at,
          })),
          files: comment.files.map((file) => ({
            id: file.id,
            url: file.file,
          })),
        })),
      };

      dispatchDep({
        type: "set_comments",
        payload: _data.comments,
      });
    }
    //eslint-disable-next-line
  }, [props.newsReducer["GET_NEW_COMMENT"]]);

  useEffect(() => {
    const dep1 = newsReducer["GET_NEW_COMMENT"];

    dispatchDep({
      type: "req_status",
      loading: dep1.loading,
      success: dep1.success,
      error: dep1.error,
    });
    //eslint-disable-next-line
  }, [newsReducer["GET_NEW_COMMENT"]]);

  return null;
};

const mapStateToPros = (state) => ({
  newsReducer: state.newsReducer,
});

export default connect(mapStateToPros, {
  news_get_request,
})(Dependencies);
