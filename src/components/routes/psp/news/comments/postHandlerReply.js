import store from "redux/store";
import { news_post_request } from "redux/actions/newsActions";

const postHandler = async (values, urlData) => {
  const url = `/api/comment/novelty/response/${urlData.id}`;
  const req_name = "NEW_POST_COMMENT_REPLY";
  const req_body = {
    description: values.comment,
    novelty_comment_id: values.comment_id,
  };
  await store.dispatch(news_post_request(url, req_name, req_body));
};

export default postHandler;
