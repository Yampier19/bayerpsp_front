import { useState, useReducer, useEffect } from "react";

import LoadingComponent from "components/base/loading-component";
import TextEditor from "components/base/text-editor";
import { Link, useParams } from "react-router-dom";

import Dependencies from "./dependencies";
import { dependencies_initState, depReducer } from "./depReducer";

import submitHandler from "./postHandler";
import submitHandler2 from "./postHandlerReply";

import ReactHtml from "raw-html-react";
import FileDisplayer from "./fileDisplayer";

import { connect } from "react-redux";

import "./reply.scss";

const Comments = (props) => {
  const { id } = useParams();
  const [dependencies, dispatchDep] = useReducer(
    depReducer,
    dependencies_initState
  );
  const [triggerReload, setTriggerReload] = useState(0);
  useEffect(() => {}, [dependencies.loading]);

  // console.log(dependencies);
  const user = props.authenticationReducer.sessionData.user;
  // console.log(user);
  const onSend = async (commentHTML, files, additionalData) => {
    const values = {
      comment: commentHTML,
      files,
    };

    submitHandler(values, { id }); //send comment to data base
    setTriggerReload(triggerReload + 1);
  };

  const onSendReply = async (commentHTML, files, additionalData) => {
    const values = {
      comment: commentHTML,
      comment_id: additionalData.id,
    };

    await submitHandler2(values, { id }); //send comment to data base
    setTriggerReload(triggerReload + 1);
  };

  return (
    <div
      className="is-flex is-flex-direction-column "
      style={{ height: "100%" }}
    >
      <Dependencies
        dependencies={dependencies}
        dispatchDep={dispatchDep}
        inputData={{ id }}
        triggerReload={triggerReload}
      />

      {/* header */}
      <section className="pb-3">
        <h1 className="title has-text-hgreen">Novedades</h1>
        <h2 className="subtitle has-text-hgreen">Historial</h2>
      </section>

      {/* tabs */}
      <section className="section px-0 py-0 pb-6">
        <div className="columns is-mobile is-gapless is-hidden-desktop">
          <div className="column">
            <Link
              to={`/news/history/${id}`}
              className="button is-small is-fullwidth is-rounded has-text-dark"
            >
              Historial
            </Link>
          </div>
          <div className="column">
            <Link
              to={`/news/update/${id}`}
              className="button is-small is-fullwidth is-rounded has-text-dark"
            >
              Editar
            </Link>
          </div>
          <div className="column">
            <button className="button is-small is-fullwidth is-hgreen is-rounded has-text-white">
              <strong>Comentarios</strong>
            </button>
          </div>
        </div>
        <div className="columns is-mobile is-hidden-touch">
          <div className="column is-2 is-offset-6">
            <Link
              to={`/news/history/${id}`}
              className="button is-fullwidth is-rounded has-text-dark"
            >
              Historial
            </Link>
          </div>
          <div className="column is-2">
            <Link
              to={`/news/update/${id}`}
              className="button is-fullwidth is-rounded has-text-dark"
            >
              Editar
            </Link>
          </div>
          <div className="column is-2">
            <button className="button is-fullwidth is-hgreen is-rounded has-text-white">
              <strong>Comentarios</strong>
            </button>
          </div>
        </div>
      </section>

      {/* comment */}
      <section className="">
        <div className="container is-max-desktop">
          <div className="media">
            <figure className="media-left">
              <div className="image is-32x32">
                <img
                  className="is-rounded"
                  src="https://bulma.io/images/placeholders/128x128.png"
                  alt="news_create_icon"
                />
                <div className="subtitle is-6 has-text-centered"></div>
              </div>
            </figure>
            <div className="media-content">
              <TextEditor
                id={0}
                placeholder="Añade un comentario..."
                onSendCallback={onSend}
                isLoading={
                  props.newsReducer["NEW_POST_COMMENT"].loading ? true : false
                }
              />
            </div>
          </div>
        </div>
      </section>
      <hr className="has-background-dark" />
      {/* comments */}
      <section
        className="is-flex-grow-1"
        style={{ minHeight: "500px", overflow: "hidden" }}
      >
        <div
          className="coolscroll hgreen "
          style={{ overflow: "auto", height: "100%", width: "100%" }}
        >
          <div className="container is-max-desktop">
            {dependencies.loading ? (
              <LoadingComponent loadingText="Cargando comentarios" />
            ) : (
              dependencies.comments.map((comment, i) => (
                <div key={i}>
                  <div className="media">
                    <figure className="media-left">
                      <div className="image is-32x32">
                        <img
                          className="is-rounded"
                          src="https://bulma.io/images/placeholders/128x128.png"
                          alt="news_create_icon"
                        />
                        <div className="subtitle is-6 has-text-centered"></div>
                      </div>
                    </figure>
                    <div className="media-content">
                      <h1 className="title has-text-hgreen is-5">
                        {comment.user.name}
                      </h1>
                      <h2 className="subtitle is-6">{comment.date}</h2>
                      <FileDisplayer files={comment.files} />
                      <ReactHtml html={comment.body} />
                      <br />
                      {comment.user !== user.name ? (
                        <TextEditor
                          id={i + 1}
                          placeholder="Responder comentario..."
                          onSendCallback={onSendReply}
                          isLoading={
                            props.newsReducer["NEW_POST_COMMENT"].loading
                              ? true
                              : false
                          }
                          additionalData={{ id: comment.id }}
                        />
                      ) : null}

                      <div className="is-hidden-touch reply">
                        {comment.replies.map((reply, j) => (
                          <div className="box is-hgreen-light px-2" key={j}>
                            <div className="media has-no-border">
                              <figure
                                class="media-left"
                                style={{ zIndex: "2" }}
                              >
                                <div className="image is-32x32">
                                  <img
                                    className="is-rounded"
                                    src="https://bulma.io/images/placeholders/128x128.png"
                                    alt="news_create_icon"
                                  />
                                  <div className="subtitle is-6 has-text-centered"></div>
                                </div>
                              </figure>
                              <div className="media-content">
                                <h1 className="title has-text-hgreen is-5">
                                  {reply.user}
                                </h1>
                                <h2 className="subtitle is-6">
                                  {comment.date}
                                </h2>
                                <p className="has-text-black">
                                  <ReactHtml html={reply.body} />
                                </p>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                  <div className="is-hidden-desktop reply">
                    {comment.replies.map((reply, j) => (
                      <div className="box is-hgreen-light px-2" key={j}>
                        <div className="media has-no-border">
                          <figure
                            className="media-left"
                            style={{ zIndex: "2" }}
                          >
                            <div className="image is-32x32">
                              <img
                                className="is-rounded"
                                src="https://bulma.io/images/placeholders/128x128.png"
                                alt="news_create_icon"
                              />
                              <div className="subtitle is-6 has-text-centered"></div>
                            </div>
                          </figure>
                          <div className="media-content">
                            <h1 className="title has-text-hgreen is-5">
                              {reply.user}
                            </h1>
                            <h2 className="subtitle is-6">{comment.date}</h2>
                            <p className="has-text-black">
                              <ReactHtml html={reply.body} />
                            </p>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                  <br />
                </div>
              ))
            )}
          </div>
        </div>
      </section>
    </div>
  );
};

const mapStateToProps = (state) => ({
  newsReducer: state.newsReducer,
  authenticationReducer: state.authenticationReducer,
});

export default connect(mapStateToProps)(Comments);
