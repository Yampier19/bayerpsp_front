import React from "react";

import { Link, useParams } from "react-router-dom";
import Form from "./form";

import LoadingComponent from "components/base/loading-component";
import { connect } from "react-redux";

const Update = (props) => {
  const { id } = useParams();

  // const {search} = useLocation();
  // const row = new URLSearchParams(search).get('row');

  return (
    <div
      className="is-flex is-flex-direction-column "
      style={{ height: "100%" }}
    >
      {/* header */}
      <section className="pb-3">
        <h1 className="title has-text-hgreen">Novedades</h1>
        <h2 className="subtitle has-text-hgreen">
          Consultar <strong className="has-text-hgreen">- Actualización</strong>
        </h2>
      </section>
      {/* tabs */}
      <section className="pb-3">
        <div className="columns is-mobile is-gapless is-hidden-desktop">
          <div className="column">
            <Link
              to={`/news/history/${id}`}
              className="button is-small is-fullwidth  is-rounded has-text-dark"
            >
              Historial
            </Link>
          </div>
          <div className="column">
            <button className="button is-small is-fullwidth is-rounded is-hgreen has-text-white">
              <strong>Editar</strong>
            </button>
          </div>
          <div className="column">
            <Link
              to={`/news/comments/${id}`}
              className="button is-small is-fullwidth is-rounded has-text-dark"
            >
              Comentarios
            </Link>
          </div>
        </div>
        <div className="columns is-mobile is-hidden-touch">
          <div className="column is-2 is-offset-6">
            <Link
              to={`/news/history/${id}`}
              className="button is-fullwidth is-rounded has-text-dark"
            >
              Historial
            </Link>
          </div>
          <div className="column is-2">
            <button className="button is-fullwidth is-rounded is-hgreen has-text-white">
              <strong>Editar</strong>
            </button>
          </div>
          <div className="column is-2">
            <Link
              to={`/news/comments/${id}`}
              className="button is-fullwidth is-rounded has-text-dark"
            >
              Comentarios
            </Link>
          </div>
        </div>
      </section>
      <br />
      <section
        className="section px-0 py-0 pb-3 is-flex-grow-1 coolscroll"
        style={{ overflowY: "hidden", overflowX: "hidden" }}
      >
        <div
          className="coolscroll hgreen"
          style={{ height: "100%", overflow: "auto" }}
        >
          {props.newsReducer["UPDATE_NEW"].loading ? (
            <LoadingComponent loadingText="Cargando datos" />
          ) : (
            <Form id={id} />
          )}
        </div>
      </section>
    </div>
  );
};

const mapStateToProps = (state) => ({
  newsReducer: state.newsReducer,
});

export default connect(mapStateToProps, null)(Update);
