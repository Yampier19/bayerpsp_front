import { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { connect } from "react-redux";
import { useHistory } from "react-router-dom";

import FormField from "components/form/formfield";
import LoadingComponent from "components/base/loading-component";
import submitHandler from "./submitHandler";
import Dependencies from "./dependencies";
import Dropdown from "components/form/dropdown";

export const formSchema = Yup.object().shape({
  id: Yup.string().required("Campo obligatorio"),
  asunto: Yup.string().required("Campo obligatorio"),
  producto: Yup.string().required("Campo obligatorio"),
  novedad: Yup.string().required("Campo obligatorio"),
  fechaReporte: Yup.string().required("Campo obligatorio"),
  fechaRespuesta: Yup.string().required("Campo obligatorio"),
  observaciones: Yup.string()
    .min(5, "El campo tiene que tener minimo 5 caracteres")
    .required("Campo obligatorio"),
  observacionesRespuesta: Yup.string(),
  estado: Yup.string().required("Campo obligatorio"),
  solicitudAdicional: Yup.string().required("Campo obligatorio"),
});

const Form = (props) => {
  const { id } = props;
  const { push } = useHistory();
  const [dependencies, setDepencies] = useState({
    loading: true,
    initialValues: {
      id: "",
      asunto: "",
      producto: "",
      novedad: "",
      fechaReporte: "",
      fechaRespuesta: "",
      observaciones: "",
      observacionesRespuesta: "",
      estado: "",
      solicitudAdicional: "",
    },
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: dependencies.initialValues,
    validateOnChange: true,
    validationSchema: formSchema,
    onSubmit: async (values) => {
      const res = await submitHandler(values, {
        id: id,
      });
      if (res) {
        push("/news/consult?page=1");
      }
    },
  });

  return (
    <div>
      <Dependencies
        dependencies={dependencies}
        setDepencies={setDepencies}
        input={{ id }}
      />
      {dependencies.loading ? (
        <LoadingComponent loadingText="Cargando datos" />
      ) : (
        <form
          id="form"
          onSubmit={formik.handleSubmit}
          className="coolscroll primary pr-3 py-3"
        >
          <FormField label="Id" number="1" name="id" checked={formik.values.id}>
            <input
              className="input cool-input"
              readOnly
              type="text"
              name="id"
              onChange={formik.handleChange}
              value={formik.values.id}
            />
            {formik.touched.id && formik.errors.id ? (
              <div className="help is-danger"> {formik.errors.id}</div>
            ) : null}
          </FormField>

          <FormField
            label="Asunto"
            number="2"
            name="asunto"
            checked={formik.values.asunto}
          >
            <input
              className="input cool-input"
              type="text"
              name="asunto"
              readOnly
              onChange={formik.handleChange}
              value={formik.values.asunto}
            />
            {formik.touched.asunto && formik.errors.asunto ? (
              <div className="help is-danger"> {formik.errors.asunto}</div>
            ) : null}
          </FormField>

          <FormField
            label="Producto"
            number="3"
            name="producto"
            checked={formik.values.producto}
          >
            <input
              className="input cool-input"
              type="text"
              readOnly
              name="producto"
              onChange={formik.handleChange}
              value={formik.values.producto}
            />
            {formik.touched.producto && formik.errors.producto ? (
              <div className="help is-danger"> {formik.errors.producto}</div>
            ) : null}
          </FormField>

          <FormField
            label="Novedad"
            number="4"
            name="novedad"
            checked={formik.values.novedad}
          >
            <input
              className="input cool-input"
              type="text"
              name="novedad"
              readOnly
              onChange={formik.handleChange}
              value={formik.values.novedad}
            />
            {formik.touched.novedad && formik.errors.novedad ? (
              <div className="help is-danger"> {formik.errors.novedad}</div>
            ) : null}
          </FormField>

          <FormField
            label="Fecha de reporte"
            number="5"
            name="fechaReporte"
            checked={formik.values.fechaReporte}
          >
            <input
              className="input cool-input"
              type="text"
              readOnly
              name="fechaReporte"
              onChange={formik.handleChange}
              value={formik.values.fechaReporte}
            />
            {formik.touched.fechaReporte && formik.errors.fechaReporte ? (
              <div className="help is-danger">
                {" "}
                {formik.errors.fechaReporte}
              </div>
            ) : null}
          </FormField>

          <FormField
            label="Fecha de respuesta"
            number="6"
            name="fechaRespuesta"
            checked={formik.values.fechaRespuesta}
          >
            <input
              className="input cool-input"
              type="date"
              name="fechaRespuesta"
              onChange={formik.handleChange}
              value={formik.values.fechaRespuesta}
            />
            {formik.touched.fechaRespuesta && formik.errors.fechaRespuesta ? (
              <div className="help is-danger">
                {" "}
                {formik.errors.fechaRespuesta}
              </div>
            ) : null}
          </FormField>

          <FormField
            label="Observaciones"
            number="7"
            name="observaciones"
            checked={formik.values.observaciones}
          >
            <input
              className="input cool-input"
              type="text"
              readOnly
              name="observaciones"
              onChange={formik.handleChange}
              value={formik.values.observaciones}
            />
            {formik.touched.observaciones && formik.errors.observaciones ? (
              <div className="help is-danger">
                {" "}
                {formik.errors.observaciones}
              </div>
            ) : null}
          </FormField>

          <FormField
            label="Observaciones Respuesta"
            number="8"
            name="observacionesRespuesta"
            checked={formik.values.observacionesRespuesta}
          >
            <input
              className="input cool-input"
              type="text"
              name="observacionesRespuesta"
              onChange={formik.handleChange}
              value={formik.values.observacionesRespuesta}
            />
            {formik.touched.observacionesRespuesta &&
            formik.errors.observacionesRespuesta ? (
              <div className="help is-danger">
                {" "}
                {formik.errors.observacionesRespuesta}
              </div>
            ) : null}
          </FormField>

          <FormField
            label="Estado"
            number="9"
            name="estado"
            checked={formik.values.estado}
          >
            <Dropdown
              name="estado"
              onChange={formik.handleChange}
              options={[
                { id: 1, name: "INICIADA" },
                { id: 2, name: "CANCELADA" },
                { id: 3, name: "FINALIZADA" },
              ]}
              arrowColor="has-text-hblue"
              value={formik.values.estado}
            />
            {formik.touched.estado && formik.errors.estado ? (
              <div className="help is-danger"> {formik.errors.estado}</div>
            ) : null}
          </FormField>

          <FormField
            label="Solicitud adicional"
            number="10"
            name="solicitudAdicional"
            checked={formik.values.solicitudAdicional}
            noline
          >
            <input
              className="input cool-input"
              type="text"
              name="solicitudAdicional"
              onChange={formik.handleChange}
              value={formik.values.solicitudAdicional}
            />
            {formik.touched.solicitudAdicional &&
            formik.errors.solicitudAdicional ? (
              <div className="help is-danger">
                {" "}
                {formik.errors.solicitudAdicional}
              </div>
            ) : null}
          </FormField>

          <br />

          <div className="field">
            <div className="control has-text-right has-text-centered-mobile">
              <button
                className="button is-hgreen"
                type="submit"
                style={{ width: "150px" }}
              >
                ACTUALIZAR
              </button>
            </div>
          </div>
        </form>
      )}
    </div>
  );
};

const mapStateToPros = (state) => ({
  authenticationReducer: state.authenticationReducer,
});

export default connect(mapStateToPros, null)(Form);
