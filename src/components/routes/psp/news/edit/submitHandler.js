import store from "redux/store";
import { news_put_request } from "redux/actions/newsActions";

import { set_modal_on } from "redux/actions/modalActions";
import { InfoModal } from "components/base/modal-system/models/modal";

const submitHandler = async (values, urlData) => {
  const url = `/api/novelty/${urlData.id}`;
  const req_name = "UPDATE_NEW";
  const req_body = {
    status_id: Number(values.estado),
    manager_id: Number(values.manager),
    answer_data: values.fechaRespuesta,
    observation: values.observacionesRespuesta,
  };

  const done = await store.dispatch(news_put_request(url, req_name, req_body));
  if (done) {
    const { _icons } = store.getState().modalReducer;
    const modal = new InfoModal(
      "La novedad se ha actualizado éxitosamente",
      _icons.CHECK_PRIMARY
    );
    store.dispatch(set_modal_on(modal));
    return done;
  }
};

export default submitHandler;
