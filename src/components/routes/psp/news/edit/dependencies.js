import { useEffect } from "react";
import { connect } from "react-redux";

import { news_get_request } from "redux/actions/newsActions";

const Dependencies = (props) => {
  const { dependencies, setDepencies } = props;
  const { triggerReload } = props;

  const { id } = props.input;
  // console.log(props);

  const load_dependencies = async () => {
    setDepencies({
      ...dependencies,
      loading: true,
    });

    const url = `/api/novelty/${id}`;
    const req_name = "GET_NEW";
    await props.news_get_request(url, req_name, null);
  };

  useEffect(() => {
    load_dependencies();

    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    load_dependencies();
    //eslint-disable-next-line
  }, [triggerReload]);

  useEffect(() => {
    const data = props.newsReducer["GET_NEW"].data;
    if (data == null) return;
    setDepencies({
      ...dependencies,
      initialValues: {
        id: data.id,
        asunto: data.affair,
        producto: data.product.name,
        novedad: data.novelty,
        fechaReporte: data.report_date,
        fechaRespuesta: data.answer_date,
        observaciones: data.observation,
        observacionesRespuesta: "respuesta a las observaciones",
        estado: data.novelty_statu.id,
        solicitudAdicional: "...",
        manager: data.manager.id,
      },
      loading: false,
    });
    //eslint-disable-next-line
  }, [props.newsReducer["GET_NEW"].data]);

  return null;
};

const mapStateToPros = (state) => ({
  newsReducer: state.newsReducer,
});

export default connect(mapStateToPros, {
  news_get_request,
})(Dependencies);
