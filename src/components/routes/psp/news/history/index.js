import { useState, useEffect } from "react";

import { endpoint } from "secrets";
import { Link, useParams } from "react-router-dom";

import LoadingComponent from "components/base/loading-component";
import ReactHtml from "raw-html-react";

import { connect } from "react-redux";
import { news_get_request } from "redux/actions/newsActions";

const NewsEdit = (props) => {
  const { id } = useParams();

  const [news, setNews] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const url = `${endpoint}/api/history/novelty/${id}`;
      const req_name = "NEW_HISTORY";
      await props.news_get_request(url, req_name, null);
    };
    getData();
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    const data = props.newsReducer["NEW_HISTORY"].data;
    if (data == null) return;
    let _news = [];
    data.forEach((d) => {
      _news.push({
        body: `<div>${d.user.name} ${d.description} <b>${d.novelty.novelty}</b></div>`,
        date: `${d.created_at}`,
        user: d.user,
      });
    });
    setNews(_news);
    //eslint-disable-next-line
  }, [props.newsReducer["NEW_HISTORY"].data]);

  return (
    <div
      className="is-flex is-flex-direction-column "
      style={{ height: "100%" }}
    >
      {/* header */}
      <section className="pb-3">
        <h1 className="title has-text-hgreen">Novedades</h1>
        <h2 className="subtitle has-text-hgreen">Historial</h2>
      </section>
      {/* tabs */}
      <section className="pb-3">
        <div className="columns is-mobile is-gapless is-hidden-desktop">
          <div className="column">
            <button className="button is-small is-fullwidth is-hgreen is-rounded has-text-white">
              <strong>Historial</strong>
            </button>
          </div>
          <div className="column">
            <Link
              to={`/news/update/${id}`}
              className="button is-small is-fullwidth is-rounded has-text-dark"
            >
              Editar
            </Link>
          </div>
          <div className="column">
            <Link
              to={`/news/comments/${id}`}
              className="button is-small is-fullwidth is-rounded has-text-dark"
            >
              Comentarios
            </Link>
          </div>
        </div>
        <div className="columns is-mobile is-hidden-touch">
          <div className="column is-2 is-offset-6">
            <button className="button is-fullwidth is-hgreen is-rounded has-text-white">
              <strong>Historial</strong>
            </button>
          </div>
          <div className="column is-2">
            <Link
              to={`/news/update/${id}`}
              className="button is-fullwidth is-rounded has-text-dark"
            >
              Editar
            </Link>
          </div>
          <div className="column is-2">
            <Link
              to={`/news/comments/${id}`}
              className="button is-fullwidth is-rounded has-text-dark"
            >
              Comentarios
            </Link>
          </div>
        </div>
      </section>
      {/* history */}
      <section
        className="px-0 py-0 pb-3 is-flex-grow-1 coolscroll hgreen"
        style={{ overflowY: "scroll", overflowX: "hidden" }}
      >
        <h1 className="subtitle has-text-hgreen is-6">Actividad</h1>
        <hr />
        {props.newsReducer["NEW_HISTORY"].loading ? (
          <LoadingComponent loadingText="Cargando datos" />
        ) : props.newsReducer["NEW_HISTORY"].success ? (
          news.map((neww, i) => (
            <div key={`history_${i}`}>
              <div className="columns is-mobile">
                <div className="column is-1" style={{ minWidth: "80px" }}>
                  <figure class="image is-32x32">
                    <img
                      className="is-rounded"
                      src="https://bulma.io/images/placeholders/128x128.png"
                      alt="icon_hystory_news"
                    />
                  </figure>
                </div>
                <div className="column">
                  <div className="columns">
                    <div className="column">
                      <ReactHtml html={neww.body} />
                      <br />
                      {neww.date}
                    </div>
                    <div className="column is-4 has-text-right has-text-left-mobile pr-6">
                      {neww.user?.name}
                    </div>
                  </div>
                </div>
              </div>
              <hr />
            </div>
          ))
        ) : null}
      </section>
    </div>
  );
};

const mapStateToProps = (state) => ({
  newsReducer: state.newsReducer,
});

export default connect(mapStateToProps, {
  news_get_request,
})(NewsEdit);
