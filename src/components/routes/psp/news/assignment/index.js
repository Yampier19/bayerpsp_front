const Assigment = (props) => {
  return (
    <div
      className="is-flex is-flex-direction-column "
      style={{ height: "100%" }}
    >
      {/* header */}
      <section className="section px-0 py-0 pb-6">
        <h1 className="title has-text-hgreen is-2">Novedades</h1>
        <h2 className="subtitle has-text-hgreen">Asignación</h2>
      </section>

      {/* comments */}
      <section
        className="section px-0 py-0 pb-3 is-flex-grow-1 coolscroll pr-1"
        style={{ overflowY: "scroll", overflowX: "scroll" }}
      >
        <table class="table" style={{ width: "100%" }}>
          <thead>
            <tr>
              <th className="has-text-hgreen">Novedad</th>
              <th className="has-text-hgreen">Estado</th>
              <th className="has-text-hgreen">Resumen</th>
              <th className="has-text-hgreen">Responsable</th>
              <th className="has-text-hgreen">Creacion</th>
            </tr>
          </thead>

          <tbody>
            <tr className="my-6">
              <td>Cod. novedad</td>
              <td>
                <span className="tag is-hgreen">N</span>
              </td>
              <td>Mostrar detalles</td>
              <td>
                <div className="columns is-mobile" style={{ minWidth: "50px" }}>
                  <div className="column is-2">
                    <figure class="image is-32x32">
                      <img
                        className="is-rounded"
                        src="https://bulma.io/images/placeholders/128x128.png"
                        alt="news_assignment_icon"
                      />
                    </figure>
                  </div>
                  <div className="column">Asesora 1</div>
                </div>
              </td>
              <td>DD-MM-AAAA</td>
            </tr>
            <tr className="my-6">
              <td>Cod. novedad</td>
              <td>
                <span className="tag is-hblue">N</span>
              </td>
              <td>Mostrar detalles</td>
              <td>
                <div className="columns is-mobile" style={{ minWidth: "50px" }}>
                  <div className="column is-2">
                    <figure class="image is-32x32">
                      <img
                        className="is-rounded"
                        src="https://bulma.io/images/placeholders/128x128.png"
                        alt="news_assignment_icon"
                      />
                    </figure>
                  </div>
                  <div className="column">Asesora 1</div>
                </div>
              </td>
              <td>DD-MM-AAAA</td>
            </tr>
            <tr className="my-6">
              <td>Cod. novedad</td>
              <td>
                <span className="tag ">N</span>
              </td>
              <td>Mostrar detalles</td>
              <td>
                <div className="columns is-mobile" style={{ minWidth: "50px" }}>
                  <div className="column is-2">
                    <figure class="image is-32x32">
                      <img
                        className="is-rounded"
                        src="https://bulma.io/images/placeholders/128x128.png"
                        alt="news_assignment_icon"
                      />
                    </figure>
                  </div>
                  <div className="column">Asesora 1</div>
                </div>
              </td>
              <td>DD-MM-AAAA</td>
            </tr>
          </tbody>
        </table>
      </section>
    </div>
  );
};

export default Assigment;
