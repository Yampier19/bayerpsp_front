import { useState } from "react";

import FilterBar from "components/base/filter-bar";
import { connect } from "react-redux";
import { Switch } from "react-if";
import { Case } from "react-if";

import Dependencies from "./dependencies";
import LoadingComponent from "components/base/loading-component";
import Table from "./table";

// import './general.scss';
import "./../table-list.scss";
import "./consult.scss";

const Consult = (props) => {
  const [dependencies, setDependencies] = useState({
    news: [],
    loading: false,
    success: false,
    error: false,
  });

  return (
    <div
      className="is-flex is-flex-direction-column "
      style={{ height: "100%" }}
    >
      <Dependencies
        dependencies={dependencies}
        setDependencies={setDependencies}
        input={null}
      />

      {/* header */}
      <section className="px-0 py-0">
        <h1 className="title has-text-hgreen">Novedades</h1>
        <h2 className="subtitle has-text-hgreen">Consultar</h2>
      </section>

      {/* first level - filters */}
      <section className="px-0 py-0">
        <div className="columns">
          <div className="column">
            <FilterBar
              textColorClass="has-text-hgreen"
              filters={[
                {
                  name: "ID",
                  options: [{ name: "op1" }, { name: "op2" }],
                },
                {
                  name: "Novedad",
                  options: [],
                },
                {
                  name: "Prioridad",
                  options: [],
                },
                {
                  name: "Estado",
                  options: [],
                },
                {
                  name: "País",
                  options: [],
                },
                {
                  name: "Responsable",
                  options: [],
                },
              ]}
              moreFiltersOn={false}
              tags={null}
            />
          </div>
          <div className="column is-2-desktop has-text-right has-text-left-mobile">
            <div className="field">
              <div className="control has-icons-right">
                <input
                  className="input searchbar"
                  type="text"
                  placeholder="Buscar"
                />
                <span className="icon is-small is-right">
                  <i className="fas fa-search"></i>
                </span>
              </div>
            </div>
          </div>
        </div>
      </section>

      {/* second level - table */}
      <section
        className="is-flex-grow-1"
        style={{ minHeight: "500px", overflow: "hidden" }}
      >
        <div
          className="coolscroll hgreen "
          style={{ overflow: "auto", height: "100%", width: "100%" }}
        >
          <div className="columns is-mobile">
            <div
              className="column is-1 is-hidden-tablet has-text-centered"
              style={{ minWidth: "80px" }}
            ></div>
            <div className="column">
              <Switch>
                <Case condition={dependencies.loading}>
                  <LoadingComponent loadingText="Cargando datos" />
                </Case>
                <Case condition={dependencies.error}>
                  <div>Error cargando los datos</div>
                </Case>
                <Case condition={dependencies.success}>
                  <Table data={dependencies.news} />
                </Case>
              </Switch>
              {/* <div
                className="box table-header has-background-hgreen-light"
                style={{ minWidth: "1450px" }}
              >
                <div className="columns has-text-hgreen is-mobile fila">
                  <div className="column is-1 hgreen p-2">ID</div>
                  <div className="column hgreen p-2">Asunto</div>
                  <div className="column hgreen p-2">Producto</div>
                  <div className="column hgreen p-2">Novedad</div>
                  <div className="column hgreen p-2">Observaciones</div>
                  <div className="column hgreen p-2">
                    <i class="fas fa-calendar-alt"></i> &nbsp; reporte
                  </div>
                  <div className="column hgreen p-2">
                    <i class="fas fa-calendar-alt"></i> &nbsp; respuesta
                  </div>
                  <div className="column has-no-border p-2">Responsable</div>
                </div>
              </div> */}
              {/* {dependencies.loading ? (
                <LoadingComponent loadingText="Cargando datos" />
              ) : dependencies.error ? (
                <div>Error cargando los datos</div>
              ) : dependencies.success ? (
                dependencies.news.map((neww, i) => (
                  <div
                    className="mb-3"
                    style={{ position: "relative" }}
                    key={i}
                  >
                    <div
                      className="box"
                      style={{
                        overflow: "hidden",
                        minWidth: "1450px",
                        borderRadius: "7px !important",
                      }}
                    >
                      <div className="columns fila2 is-mobile">
                        <div className="column p-2">{neww.id}</div>
                        <div className="column p-2">{neww.affair}</div>
                        <div className="column p-2">{neww.product?.name}</div>
                        <div className="column p-2">{neww.novelty}</div>
                        <div className="column p-2">{neww.observation}</div>
                        <div className="column p-2">{neww.report_date}</div>
                        <div className="column p-2">
                          <span className="tag is-hred">
                            {neww.answer_date}
                          </span>
                        </div>
                        <div className="column has-no-border py-0 pr-0 p-2">
                          <div
                            className="columns is-mobile my-0"
                            style={{ height: "100%", background: "" }}
                          >
                            <div className="column">{neww.manager?.name}</div>
                            <div
                              className={`column is-1 p-0 py-2 has-text-centered ${"has-background-hblue"}`}
                              style={{ minWidth: "30px" }}
                            >
                              <figure
                                class="image is-16x16"
                                style={{ left: "5px" }}
                              >
                                <img src={flag} alt="" />
                              </figure>
                              <br />
                              <span className="has-text-white">C</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div
                      className="edit-btns-news has-text-right"
                      style={{ width: "100%" }}
                    >
                      <div className="">
                        <Link
                          to={`/news/update/${neww.id}`}
                          className="button has-background-transparent has-no-border "
                        >
                          <span className="icon">
                            <i class="fas fa-edit"></i>
                          </span>
                          <span className="has-text-light2 is-hidden-mobile">
                            <u>Editar</u>
                          </span>
                        </Link>
                        <br className="is-hidden-tablet" />
                        <Link
                          to={`/news/comments/${neww.id}`}
                          className="button has-background-transparent has-no-border "
                        >
                          <span className="icon">
                            <i class="fas fa-comments"></i>
                          </span>
                          <span className="has-text-light2 is-hidden-mobile">
                            <u>Comentarios</u>
                          </span>
                        </Link>
                      </div>
                    </div>
                  </div>
                ))
              ) : null} */}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

const mapStateToProps = (state) => ({
  authenticationReducer: state.authenticationReducer,
  consultReducer: state.consultReducer,
});

export default connect(mapStateToProps, null)(Consult);
