import { Link } from "react-router-dom";

import DataTable from "components/base/datatable";
import flag from "./flag.png";
const Table = ({ data }) => {
  const columns = [
    {
      name: "id",
      label: "ID",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "affair",
      label: "Asunto",
      options: { filter: true, sort: true },
    },
    {
      name: "product",
      label: "Producto",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => <div>{value.name}</div>,
      },
    },
    {
      name: "novelty",
      label: "Novedad",
      options: { filter: true, sort: true },
    },
    {
      name: "observation",
      label: "Observaciones",
      options: { filter: true, sort: true },
    },
    {
      name: "report_date",
      label: "Reporte",
      options: { filter: true, sort: true },
    },
    {
      name: "answer_date",
      label: "Respuesta",
      options: { filter: true, sort: true },
    },
    {
      name: "manager",
      label: "Responsable",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => (
          <div className="column has-no-border py-0 pr-0 p-2">
            <div
              className="columns is-mobile my-0"
              style={{ height: "100%", background: "" }}
            >
              <div className="column">{value.name}</div>
              <div
                className={`column is-1 p-0 py-2 has-text-centered ${"has-background-hblue"}`}
                style={{ minWidth: "30px" }}
              >
                <figure class="image is-16x16" style={{ left: "5px" }}>
                  <img src={flag} alt="" />
                </figure>
                <br />
                <span className="has-text-white">C</span>
              </div>
            </div>
          </div>
        ),
      },
    },
    {
      name: "id",
      label: "Acciones",
      options: {
        sort: false,
        filter: false,
        customBodyRender: (value) => (
          <div
            className="edit-btns-news has-text-right"
            style={{ width: "100%" }}
          >
            <div className="">
              <Link
                to={`/news/update/${value}`}
                className="button has-background-transparent has-no-border "
              >
                <span className="icon">
                  <i class="fas fa-edit"></i>
                </span>
                <span className="has-text-light2 is-hidden-mobile">
                  <u>Editar</u>
                </span>
              </Link>
              <br className="is-hidden-tablet" />
              <Link
                to={`/news/comments/${value}`}
                className="button has-background-transparent has-no-border "
              >
                <span className="icon">
                  <i class="fas fa-comments"></i>
                </span>
                <span className="has-text-light2 is-hidden-mobile">
                  <u>Comentarios</u>
                </span>
              </Link>
            </div>
          </div>
        ),
      },
    },
  ];

  return (
          <div className="row">
            <div className="col">
              <DataTable data={data} columns={columns} title={"Lista de novedades"} color="#9CC9A3" />
            </div>
          </div>
  );
};

export default Table;
