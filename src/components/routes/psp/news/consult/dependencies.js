import { useEffect } from "react";

import { connect } from "react-redux";
import { news_get_request } from "redux/actions/newsActions";

const Dependencies = (props) => {
  const { dependencies, setDependencies } = props;
  const { triggerReload } = props || null;

  const load_dependencies = async () => {
    const url = `/api/novelty`;
    const req_name = "GET_ALL_NEWS";
    await props.news_get_request(url, req_name, null);
  };

  useEffect(() => {
    load_dependencies();
    //eslint-disable-next-line
  }, []);

  useEffect(
    () => {
      load_dependencies();
    },
    //eslint-disable-next-line
    [triggerReload]
  );

  useEffect(
    () => {
      const data = props.newsReducer["GET_ALL_NEWS"].data;
      let _data = [];
      if (data != null && props.newsReducer["GET_ALL_NEWS"].success) {
        _data = data;
      }
      const red = props.newsReducer["GET_ALL_NEWS"];
      setDependencies({
        ...dependencies,
        loading: red.loading,
        success: red.success,
        error: red.error,
        news: _data,
      });
    },
    //eslint-disable-next-line
    [props.newsReducer["GET_ALL_NEWS"]]
  );

  return null;
};

const mapStateToPros = (state) => ({
  newsReducer: state.newsReducer,
});

export default connect(mapStateToPros, {
  news_get_request,
})(Dependencies);
