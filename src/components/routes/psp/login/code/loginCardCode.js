import { useEffect } from "react";

import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { code_request } from "redux/actions/authenticationActions";

import * as Yup from "yup";
import { useFormik } from "formik";

const LoginCardCode = (props) => {
  const { code, isSendCode } = props.authenticationReducer;
  const history = useHistory();
  useEffect(() => {
    if (isSendCode) {
      history.push("/recover");
    }
    //eslint-disable-next-line
  }, [code.isLoading, isSendCode]);

  const formSchema = Yup.object().shape({
    code: Yup.number().required("Este campo es obligatorio."),
  });

  const formik = useFormik({
    initialValues: {
      code: "",
    },
    validateOnChange: true,
    validationSchema: formSchema,
    onSubmit: async (values) => {
      await props.code_request(values);
    },
  });
  return (
    <div id="loginCard" className="box has-background-primary">
      {/* title */}
      <header className="hero">
        <div className="hero-body my-5">
          <div className="container">
            <h1 className="title has-text-white has-text-centered">
              Olvidaste la contraseña
            </h1>
          </div>
        </div>
      </header>
      <div className="px-4">
        <p className="has-text-centered has-text-light px-6 mb-6 is-size-5">
          Por favor digite el código de confirmación enviado a su correo
        </p>
        <form onSubmit={formik.handleSubmit}>
          {/* password */}
          <div className="field">
            <div className="control">
              <input
                className="input is-rounded linput pl-5 py-5"
                type="number"
                name="code"
                value={formik.values.code}
                onChange={formik.handleChange}
                placeholder="Código de confirmación"
              />
              <p className="help has-text-white pl-5">
                {formik.touched.code && formik.errors.code
                  ? formik.errors.code
                  : ""}
              </p>
            </div>
            <div className="has-text-white">
              {code.error ? code.errorData : ""}
            </div>
          </div>
          <br />
          {/* submit */}
          <div className="field">
            <div className="control has-text-centered">
              <button
                className={`button is-light has-text-primary py-5 loginb ${
                  code.isLoading ? "is-loading" : ""
                }`}
                style={{ width: "50%" }}
                type="submit"
              >
                <span className="icon">
                  <i className="fas fa-chevron-right"></i>
                </span>
                <strong className="is-pulled-right">Continuar</strong>
              </button>
            </div>
          </div>
        </form>
        <br />
        <br />
        <br />
        <div className="login-footer has-text-centered has-text-light is-size-6">
          By People Marketing
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  authenticationReducer: state.authenticationReducer,
});

export default connect(mapStateToProps, {
  code_request,
})(LoginCardCode);
