import React from "react";

import LoginCardEnter from "./loginCardEnter";
import "../login.scss";

const LoginEnter = (props) => {
  return (
    <div className="App">
      <div className="hero is-fullheight login-bg">
        <div className="hero-header py-6 px-3 mt-6">
          <div className="container">
            <div className="columns">
              <div className="column is-5 card-login">
                <LoginCardEnter />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginEnter;
