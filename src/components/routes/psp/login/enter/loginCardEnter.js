import { useEffect } from "react";

import "../loginCard.scss";
import { Link, useHistory } from "react-router-dom";

import { connect } from "react-redux";
import { login_request } from "redux/actions/authenticationActions";

import * as Yup from "yup";
import { useFormik } from "formik";

const LoginCardEnter = (props) => {
  const { login, isLoggedIn } = props.authenticationReducer;
  // console.log('* *~~*~~*~~*~~* *');
  // console.log(login);
  const history = useHistory();

  useEffect(() => {
    if (isLoggedIn) {
      history.push("/home");
    }
    //eslint-disable-next-line
  }, [isLoggedIn]);

  const formSchema = Yup.object().shape({
    user_name: Yup.string().required("Este campo es obligatorio"),
    password: Yup.string().required("Este campo es obligatorio"),
  });

  const formik = useFormik({
    initialValues: {
      user_name: "",
      password: "",
    },
    validateOnChange: true,
    validationSchema: formSchema,
    onSubmit: async (values) => {
      await props.login_request({
        user_name: values.user_name,
        password: values.password,
      });

      // if(success)
      //     history.push('/home');
    },
  });

  const togglePasswordVisibility = (e) => {
    const elmnt = document.getElementById("login-enter-password");
    if (elmnt.type === "password") elmnt.type = "text";
    else elmnt.type = "password";
  };

  return (
    <div id="loginCard" className="box has-background-primary">
      {/* title */}
      <header className="hero">
        <div className="hero-body">
          <div className="container">
            <h1 className="title has-text-white has-text-centered">INGRESAR</h1>
          </div>
        </div>
      </header>
      <div className="px-4">
        <form onSubmit={formik.handleSubmit}>
          {/* user */}
          <div className="field">
            <div className="control ">
              <input
                className="input is-rounded linput pl-5 py-5"
                type="text"
                placeholder="Usuario"
                name="user_name"
                value={formik.values.user_name}
                onChange={formik.handleChange}
              />
              <p className="help has-text-white pl-5">
                {formik.touched.user_name && formik.errors.user_name
                  ? formik.errors.user_name
                  : ""}
              </p>
            </div>
          </div>
          {/* password */}
          <div className="field">
            <div className="control has-icons-right">
              <input
                id="login-enter-password"
                className="input is-rounded linput pl-5 py-5"
                type="password"
                placeholder="Contraseña"
                name="password"
                value={formik.values.password}
                onChange={formik.handleChange}
              />
              <span
                className="icon is-medium is-right has-text-hblack togglePasswordVisibility"
                onClick={togglePasswordVisibility}
              >
                <i className="far fa-lg fa-eye"></i>
              </span>
            </div>
            <p className="help has-text-white pl-5">
              {formik.touched.password && formik.errors.password
                ? formik.errors.password
                : ""}
            </p>
            <div className="has-text-white">
              {login.error ? login.errorData : ""}
            </div>
          </div>
          <br />
          {/* submit */}
          <div className="field">
            <div className="control has-text-centered">
              <button
                className={`button is-light has-text-primary py-5 loginb ${
                  login.isLoading ? "is-loading" : ""
                }`}
                style={{ width: "50%" }}
                type="submit"
              >
                <span className="icon">
                  <i className="fas fa-sign-in-alt"></i>
                </span>
                <strong className="is-pulled-right">LOGIN</strong>
              </button>
            </div>
          </div>
        </form>
        <br />
        {/* recover */}
        <div className="field has-text-centered">
          <Link
            to="/forgot-password"
            className="has-text-white is-underlined is-size-6 "
          >
            Olvidaste tu contraseña
          </Link>
        </div>
        <div className="login-footer has-text-centered has-text-light is-size-6">
          By People Marketing
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  authenticationReducer: state.authenticationReducer,
});

export default connect(mapStateToProps, {
  login_request,
})(LoginCardEnter);
