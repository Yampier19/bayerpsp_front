import React from "react";
import LoginCardPassword from "./loginCardPassword";

const LoginPassword = (props) => {
  return (
    <div className="App">
      <div
        className="hero is-fullheight login-bg"
        style={{ backgroundImage: "" }}
      >
        <div className="hero-header py-6 px-3 mt-6">
          <div className="container">
            <div className="columns">
              <div className="column is-5">
                <LoginCardPassword />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginPassword;
