import React from "react";
import { useEffect } from "react";

import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { email_request } from "redux/actions/authenticationActions";

import * as Yup from "yup";
import { useFormik } from "formik";

const LoginCardPassword = (props) => {
  const { email, isSendEmail } = props.authenticationReducer;

  const history = useHistory();
  useEffect(() => {
    if (isSendEmail) {
      history.push("/code");
    }
    //eslint-disable-next-line
  }, [email.isLoading]);

  const formSchema = Yup.object().shape({
    email: Yup.string()
      .email("Ingresa un correo valido.")
      .required("Este campo es obligatorio."),
  });

  const formik = useFormik({
    initialValues: {
      email: "",
    },
    validateOnChange: true,
    validationSchema: formSchema,
    onSubmit: async (values) => {
      await props.email_request(values);
    },
  });
  return (
    <div id="loginCard" className="box has-background-primary">
      {/* title */}
      <header className="hero">
        <div className="hero-body my-5">
          <div className="container">
            <h1 className="title has-text-white has-text-centered">
              Olvidaste la contraseña
            </h1>
          </div>
        </div>
      </header>
      <div className="px-4">
        <p className="has-text-centered has-text-light px-6 mb-6 is-size-5">
          Recibirá el código de confirmación al siguiente correo electrónico
        </p>
        <form onSubmit={formik.handleSubmit}>
          {/* password */}
          <div className="field">
            <div className="control">
              <input
                className="input is-rounded linput pl-5 py-5"
                type="text"
                name="email"
                value={formik.values.email}
                placeholder="cor*******@gmail.com"
                onChange={formik.handleChange}
              />
              <p className="help has-text-white pl-5">
                {formik.touched.email && formik.errors.email
                  ? formik.errors.email
                  : ""}
              </p>
            </div>
            <div className="has-text-white">
              {email.error ? email.errorData : ""}
            </div>
          </div>
          <br />
          {/* submit */}
          <div className="field">
            <div className="control has-text-centered">
              <button
                className={`button is-light has-text-primary py-5 loginb ${
                  email.isLoading ? "is-loading" : ""
                }`}
                style={{ width: "50%" }}
                type="submit"
              >
                <span className="icon">
                  <i className="far fa-envelope"></i>
                </span>
                <strong className="is-pulled-right">ENVIAR</strong>
              </button>
            </div>
          </div>
        </form>
        <br />
        <br />
        <br />
        <div className="login-footer has-text-centered has-text-light is-size-6">
          By People Marketing
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  authenticationReducer: state.authenticationReducer,
});

export default connect(mapStateToProps, {
  email_request,
})(LoginCardPassword);
