import React from "react";
import { useEffect } from "react";

import { useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { forgot_password_request } from "redux/actions/authenticationActions";

import * as Yup from "yup";
import { useFormik } from "formik";

const LoginCardRecover = (props) => {
  const { forgot_password, isSendForgot } = props.authenticationReducer;
  const history = useHistory();
  useEffect(() => {
    if (isSendForgot) {
      history.push("/login");
    }
    //eslint-disable-next-line
  }, [forgot_password.isLoading, isSendForgot]);

  const formSchema = Yup.object().shape({
    password: Yup.string().required("Este campo es obligatorio."),
    password_confirmation: Yup.string()
      .oneOf([Yup.ref("password"), null], "Las contraseñas deben ser iguales")
      .required("Este campo es obligatorio."),
  });

  const formik = useFormik({
    initialValues: {
      password: "",
      password_confirmation: "",
    },
    validateOnChange: true,
    validationSchema: formSchema,
    onSubmit: async (values) => {
      await props.forgot_password_request(values);
    },
  });

  const togglePasswordVisibility = (e) => {
    const elmnt = document.getElementById("forgot-password");
    if (elmnt.type === "password") elmnt.type = "text";
    else elmnt.type = "password";
  };

  const togglePasswordConfirmationVisibility = (e) => {
    const elmnt = document.getElementById("forgot-password-confirmation");
    if (elmnt.type === "password") elmnt.type = "text";
    else elmnt.type = "password";
  };

  return (
    <div id="loginCard" className="box has-background-primary">
      {/* title */}
      <header className="hero">
        <div className="hero-body my-5">
          <div className="container">
            <h1 className="title has-text-white has-text-centered">
              Reestablecer contraseña
            </h1>
          </div>
        </div>
      </header>
      <div className="px-4">
        <form onSubmit={formik.handleSubmit}>
          {/* user */}
          <div className="field">
            <div className="control has-icons-right">
              <input
                id="forgot-password"
                className="input is-rounded linput pl-5 py-5"
                type="password"
                name="password"
                placeholder="Nueva contraseña"
                value={formik.values.password}
                onChange={formik.handleChange}
              />
              <span
                className="icon is-medium is-right has-text-hblack togglePasswordVisibility"
                onClick={togglePasswordVisibility}
              >
                <i className="far fa-lg fa-eye"></i>
              </span>
            </div>
            <p className="help has-text-white pl-5">
              {formik.touched.password && formik.errors.password
                ? formik.errors.password
                : ""}
            </p>
          </div>
          <br />
          {/* password */}
          <div className="field">
            <div className="control has-icons-right">
              <input
                id="forgot-password-confirmation"
                className="input is-rounded linput pl-5 py-5"
                name="password_confirmation"
                type="password"
                placeholder="Confirmación nueva contraseña"
                value={formik.values.password_confirmation}
                onChange={formik.handleChange}
              />
              <span
                className="icon is-medium is-right has-text-hblack togglePasswordVisibility"
                onClick={togglePasswordConfirmationVisibility}
              >
                <i className="far fa-lg fa-eye"></i>
              </span>
            </div>
            <p className="help has-text-white pl-5">
              {formik.touched.password_confirmation &&
              formik.errors.password_confirmation
                ? formik.errors.password_confirmation
                : ""}
            </p>
          </div>
          <div className="has-text-white">
            {forgot_password.error ? forgot_password.errorData : ""}
          </div>
          <br />
          {/* submit */}
          <div className="field">
            <div className="control has-text-centered">
              <button
                className={`button is-light has-text-primary py-5 loginb ${
                  forgot_password.isLoading ? "is-loading" : ""
                }`}
                style={{ width: "50%" }}
                type="submit"
              >
                <span className="icon">
                  <i className="fas fa-chevron-right"></i>
                </span>
                <strong className="is-pulled-right">Continuar</strong>
              </button>
            </div>
          </div>
        </form>
        <br />
        {/* recover */}
        {/* <div className="field has-text-centered">
          <a className="has-text-white is-underlined is-size-6 ">
            Olvidaste tu contraseña
          </a>
        </div> */}
        <br />
        <br />
        <div className="login-footer has-text-centered has-text-light is-size-6">
          By People Marketing
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  authenticationReducer: state.authenticationReducer,
});

export default connect(mapStateToProps, {
  forgot_password_request,
})(LoginCardRecover);
