import MenuCard from '../../../cards/home/menucard';
import CreateBtn from '../../../base/buttons/create-btn';

import {Link} from 'react-router-dom';



const Home = props => {
    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>


            {/* header */}
            <section className="section px-0 pt-0">
                <div className="columns">
                    <div className="column is-4-desktop ">
                        <h1 className="title has-text-hyellow is-2">CRS</h1>
                        <h2 className="subtitle has-text-hyellow">Programa de compensación y reembolso</h2>
                    </div>
                </div>
            </section>

            {/* first level */}
            <div className="columns is-mobile">
                <div className="column is-3-desktop is-offset-9-desktop is-6-mobile is-offset-6-mobile">
                    <Link to="/support/create/patient" className="button is-hred is-fullwidth is-size-5 has-text-left btn2 is-hidden">
                        <span className="icon mr-1"><i className="fas fa-plus-circle"></i></span> &nbsp;
                        Crear
                    </Link>
                </div>
            </div>

            <br/>

            {/* second level */}
            <div className="columns">

                <div className="column mb-6">
                    <div className="notification py-2 is-horange"><h1 className="subtitle is-6">USUARIO NUEVO</h1></div>
                    <MenuCard
                        title="Nuevo médico"
                        color="horange"
                        to="/crs/create/medic"
                    />
                </div>

                <div className="column mb-6">
                    <div className="notification py-2 is-hred"><h1 className="subtitle is-6">SEGUIMIENTO</h1></div>
                    <MenuCard
                        title="Consultar Seguimiento"
                        color="hred"
                        to="/crs/tracking/main"
                    />

                </div>

            </div>

        </div>
    );
}

export default Home;
