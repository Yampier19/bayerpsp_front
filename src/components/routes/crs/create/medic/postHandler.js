import axios from 'axios';

import {endpoint} from 'secrets';
import store from 'redux/store';

import {tracking_post_request} from 'redux/actions/trackingActions';

import {set_modal_on} from 'redux/actions/modalActions';
import {InfoModal} from 'components/base/modal-system/models/modal';

const postHandler = async values => {

    const {access_token, token_type} = store.getState().authenticationReducer.sessionData;

    const headers = {
        'Authorization': token_type + ' ' + access_token
    }

    const url = `${endpoint}/api/patient`;
    const req_name = 'CREATE_PATIENT';

    const {paciente, tratamiento} = values;

    const req_body = {
        patient: {
            name: paciente.nombre,
            last_name: paciente.apellido,
            document: paciente.identificacion,
            date_active: paciente.fechaActivacion,
            email: paciente.correoElectronico,
            city_id: 1,
            patient_statu_id: 1,
            neighborhood: paciente.barrio,
            address: paciente.direccion,
            via: paciente.via,
            via_detail: paciente.detallesVia,
            via_number: 18,
            inside: paciente.interior,
            inside_detail: paciente.detallesInterior,
            gender: paciente.genero,
            date_birth: paciente.fechaNacimiento,
            age: paciente.edad,
            guardian: paciente.acudiente,
            guardian_phone: paciente.telefonoAcudiente,
            document_type: "CC",
            phones: [
                paciente.telefono
            ]
        },
        treatment: {
            clain:'3 caracteres',
            number_box: '5',
            dose: tratamiento.dosis,
            dose_start: tratamiento.dosisDeInicio,
            pathological_classification: tratamiento.clasificacionPatologica,
            previous_treatment: tratamiento.tratamientoPrevio,
            consent: tratamiento.consentimiento,
            therapy_start_date: tratamiento.fechaInicioTerapia,
            regime: tratamiento.regimen,
            product_id: 2,
            insurance_id: 2,
            logistic_operator_id: 2,
            doctor_id: 1,
            last_claim_date: tratamiento.fechaUltimaReclamacion,
            other_operator: tratamiento.otrosOperadores,
            delivery_point: tratamiento.puntoDeEntrega,
            means_acquisition: tratamiento.mediosAdquisicion,
            date_next_call: tratamiento.fechaProximaLlamada,
            paramedic: tratamiento.paramedico,
            zone: tratamiento.zonaAtencion,
            code: '123',
            patient_part_PAAP: tratamiento.pacienteHaceParteDelPap,
            add_application_information: tratamiento.agregarInformacionAplicaciones,
            shipping_type: tratamiento.tipoEnvio,
            patient_statu_id: 1,
            ips_id: 1,
            city_id: 2
        }
    }

    // console.log(req_body);

    const done = await store.dispatch( tracking_post_request(url, req_name, req_body) );

    if(done){
        const modalReducer = store.getState().modalReducer;
        const {_icons} = modalReducer;
        const modal = new InfoModal('El paciente ha sido creado éxitosamente', _icons.CHECK_PRIMARY);
        store.dispatch( set_modal_on(modal) );

    }
}

export default postHandler;
