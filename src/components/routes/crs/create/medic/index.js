import { useState, useEffect } from 'react';

import CreateBtn from 'components/base/buttons/create-btn';
import ProgressIcon from 'components/base/progress-icon';
import ProgressBar from 'components/base/progress-bar';

import { Link } from 'react-router-dom';
import { tabClicked } from 'components/commons/tabs/tabClicked';

import Form1 from './form1';
import postHandler from './postHandler';
//import {formik} from './formData';

import './tabs.scss';

const CreatePatient = props => {

    const [currentTab, setCurrentTab] = useState(1);

    /* *~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~* */
    const [progress, setProgress] = useState(0);
    const [formValues1, setFormValues1] = useState(0);

    /* *~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~*~~* */
    const [progress2, setProgress2] = useState(0);




    return (
        <div className="is-flex is-flex-direction-column " style={{ height: '100%' }}>

            {/* header */}
            <section className="">
                <div className="columns is-mobile">
                    <div className="column is-3-desktop ">
                        <h1 className="subtitle has-text-horange is-3 mb-1" style={{ whiteSpace: 'nowrap' }}>
                            <strong className="has-text-horange">USUARIO NUEVO</strong>
                            <span className="is-hidden-touch"> - MEDICO</span >
                        </h1>
                        <h1 className="subtitle has-text-horange is-hidden-desktop is-6">Paciente nuevo</h1>
                    </div>
                </div>
            </section>

            <hr className="has-background-dark is-hidden-touch" />

            {/* tabs */}
            <section className="section p-0 m-0 px-1">
                <div className="columns is-vcentered">

                    <div className="column">
                        <div className="tabs is-boxed createTab">
                            <ul>
                                <li className="formTab is-active" onClick={e => { tabClicked(e); setCurrentTab(1) }} data-tab="tab1">
                                    <a>
                                        <ProgressIcon className="has-background-horange has-text-white" value={progress} color="#519EA1" icon={<i class="fas fa-user"></i>} />&nbsp;
                                        <span className="is-hidden-touch">General</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="column is-hidden-desktop">
                        <span className="title has-text-horange is-5">{currentTab == 1 ? 'General' : 'Información del tratamiento'}</span>
                    </div>

                    <div className="column is-3">
                        <ProgressBar className="has-background-horange" value={currentTab == 1 ? progress : progress2}></ProgressBar>

                    </div>
                </div>
            </section>

            {/* forms */}
            <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{ height: '1px', overflow: 'hidden', minHeight: '500px' }}>
                <div id="tab1" className="tab is-active" style={{ height: '100%' }}>
                    <div className="coolscroll horange" style={{ height: '100%', overflow: 'auto' }}>
                        <Form1
                            progress={progress} setProgress={setProgress}
                            formValues1={formValues1} setFormValues1={setFormValues1}
                            onSubmitCallback={e => { tabClicked(e); setCurrentTab(2) }}
                        />
                    </div>
                </div>
            </section>
        </div>
    );
}

export default CreatePatient;

