import { useReducer, useState } from 'react';

import FormField from 'components/form/formfield';
import Dropdown from 'components/form/dropdown';
import Radio from 'components/form/radio';
import FormField2 from 'components/form/formfield2';

import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Autocomplete from '@mui/material/Autocomplete';
import Alert from '@mui/material/Alert';

import { Link } from 'react-router-dom';


import { useFormik } from 'formik';
import * as Yup from 'yup';

import { getRequired } from 'utils/forms';

import Dependencies from './dependencies';
import LoadingComponent from 'components/base/loading-component';


import { connect } from 'react-redux';

const dependencies_initState = {
    loading: false,
    success: false,
    error: false,
    paciente: {
        id: ''
    },
    status_list: [],
    cities: [],
    departamentos: []
};

const depReducer = (state, action) => {
    switch (action.type) {
        case 'req_status':
            return { ...state, loading: action.loading, success: action.success, error: action.error };
        case 'set_paciente': return { ...state, paciente: action.payload };
        case 'set_status_list': return { ...state, status_list: action.payload };
        case 'set_form_cities': return { ...state, cities: action.payload };
        case 'set_form_departamentos': return { ...state, departamentos: action.payload }
        default: return { ...state }
    }
}

const Form = props => {

    const [isDisabled, setDisabled] = useState(true);



    const handleSubmitSearch = () => {
        console.log('El boton desactivara el disabled en los input');
        setDisabled(false);
    }


    /* *~~*~~*~~*~~*~~*~~*~~*~~* outter callbacks *~~*~~*~~*~~*~~*~~*~~*~~* */
    const { progress, setProgress } = props;
    const { formValues1, setFormValues1 } = props;
    const { onSubmitCallback } = props;

    /* *~~*~~*~~*~~*~~*~~*~~*~~* form dependencies *~~*~~*~~*~~*~~*~~*~~*~~* */
    const [dependencies, dispatchDep] = useReducer(depReducer, dependencies_initState);

    /* *~~*~~*~~*~~*~~*~~*~~*~~* form values *~~*~~*~~*~~*~~*~~*~~*~~* */
    const formSchema = Yup.object().shape({
        codigoUsuario: Yup.number().typeError('se debe especificar un valor numerico'),
        estadoPaciente: Yup.string().required('Campo obligatorio'),
        fechaActivacion: Yup.string().required('Campo obligatorio'),
        correoElectronico: Yup.string().required('Campo obligatorio').email('correoElectronico invalido'),
        nombre: Yup.string().required('Campo obligatorio'),
        apellido: Yup.string().required('Campo obligatorio'),
        identificacion: Yup.number().typeError('se debe especificar un valor numerico').required('Campo obligatorio'),
        telefono: Yup.number().typeError('se debe especificar un valor numerico').required('Campo obligatorio'),
        departamento: Yup.string().required('Campo obligatorio'),
        ciudad: Yup.string().required('Campo obligatorio'),
        barrio: Yup.string().required('Campo obligatorio'),
        direccion: Yup.string().required('Campo obligatorio'),
        via: Yup.string(),
        detallesVia: Yup.string(),
        numeroMas: Yup.string(),
        numeroCelular: Yup.number().typeError('se debe especificar un valor numerico'),
        interior: Yup.string(),
        detallesInterior: Yup.string(),
        genero: Yup.string().required('Campo obligatorio'),
        fechaNacimiento: Yup.string().required('Campo obligatorio'),
        edad: Yup.number().typeError('se debe especificar un valor numerico'),
        acudiente: Yup.string(),
        telefonoAcudiente: Yup.number().typeError('se debe especificar un valor numerico'),
        notas: Yup.string(),
    });
    const formik = useFormik({
        enableReinitialize: true,
        initialValues: {
            codigoUsuario: dependencies.paciente.id,
            estadoPaciente: '',
            fechaActivacion: '',
            correoElectronico: '',
            nombre: '',
            apellido: '',
            identificacion: '',
            telefono: '',
            departamento: '',
            ciudad: '',
            barrio: '',
            direccion: '',
            via: '',
            detallesVia: '',
            numeroMas: '',
            numeroCelular: '',
            interior: '',
            detallesInterior: '',
            genero: '',
            fechaNacimiento: '',
            edad: '',
            acudiente: '',
            telefonoAcudiente: '',
            notas: '',
            file: ''
        },
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            // alert(JSON.stringify(values, null, 2));
            // await postHandler(values);
            setFormValues1(values);
        }
    });

    const requiredFields = getRequired(formSchema);


    /* *~~*~~*~~*~~*~~*~~*~~*~~* local handlers *~~*~~*~~*~~*~~*~~*~~*~~* */
    const customHandleChange = e => {

        //handle formik change
        formik.handleChange(e);
        const form = e.target.form;
        //check if element is required and them count progress
        const count = requiredFields.filter(fieldName => form[fieldName].value != '').length;
        setProgress(count / requiredFields.length * 100);
    }

    const onContinueClicked = e => {
        if (Object.keys(formik.errors) == 0)
            onSubmitCallback(e);
    }



    return (
        <>
            <Dependencies dependencies={dependencies} dispatchDep={dispatchDep} />
            {
                dependencies.loading ?
                    <LoadingComponent loadingText="Cargando datos" />
                    :
                    dependencies.success ?
                        (


                            <form onClick={formik.handleSubmit} className="coolscroll primary pr-3 py-3 ">


                                <FormField label="Código de Médico" number="1" name="codigoUsuario" checked={formik.values.codigoUsuario != ''}>
                                    <input className="input cool-input" type="text" name="codigoUsuario" onChange={customHandleChange} value="0001" readOnly />
                                    {formik.touched.codigoUsuario && formik.errors.codigoUsuario ? <div className="help is-danger"> {formik.errors.codigoUsuario}</div> : null}
                                </FormField>

                                <FormField label="Código CRS" number="2" name="detallesInterior" checked={formik.valuesdetallesInterior != ''}>
                                    <input className="input cool-input" type="text" name="codigoUsuario" onChange={customHandleChange} value="0001"  />
                                    {formik.touched.detallesInterior && formik.errors.detallesInterior ? <div className="help is-danger"> {formik.errors.detallesInterior}</div> : null}
                                </FormField>

                                <FormField label="Nombre completo" number="3" name="nombre" checked={formik.values.nombre != ''}>
                                    <input className="input cool-input" type="text" name="nombre" onChange={customHandleChange} value="Dr. prueba"  />
                                </FormField>

                                <FormField label="Producto" number="4" name="telefonoAcudiente" checked={formik.values.telefonoAcudiente != ''}>
                                    <input className="input cool-input" type="text" name="telefonoAcudiente" onChange={customHandleChange} value={formik.values.telefonoAcudiente}  />
                                </FormField>

                                <FormField2
                                    number1="5" number2="5.1" name1="departamento" name2="ciudad" checked1={formik.values.departamento != ''} checked2={formik.values.ciudad != ''}
                                    label1="País"
                                    input1={
                                        <div>
                                            <Dropdown options={dependencies.departamentos.map(dprtm => dprtm.name)} name="departamento" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.departamento}  />
                                            {formik.touched.departamento && formik.errors.departamento ? <div className="help is-danger"> {formik.errors.departamento}</div> : null}
                                        </div>
                                    }
                                    label2="Departamento"
                                    input2={
                                        <div>
                                            <Dropdown options={dependencies.cities.map(city => city.name)} name="ciudad" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.ciudad}  />
                                            {formik.touched.ciudad && formik.errors.ciudad ? <div className="help is-danger"> {formik.errors.ciudad}</div> : null}
                                        </div>
                                    }
                                />

                                <FormField2
                                    number1="6" number2="6.1" name1="barrio" name2="direccion" checked1={formik.values.barrio != ''} checked2={formik.values.direccion != ''}
                                    label1="Ciudad"
                                    input1={
                                        <div>
                                            <input className="input cool-input" type="text" name="barrio" onChange={customHandleChange} value="Bogota D.C"  />
                                            {formik.touched.barrio && formik.errors.barrio ? <div className="help is-danger"> {formik.errors.barrio}</div> : null}
                                        </div>
                                    }
                                    label2="Dirección"
                                    input2={
                                        <div>
                                            <input className="input cool-input" type="text" name="direccion" onChange={customHandleChange} value="Calle prueba 111111"  />
                                            {formik.touched.direccion && formik.errors.direccion ? <div className="help is-danger"> {formik.errors.direccion}</div> : null}
                                        </div>
                                    }
                                />

                                <FormField2
                                    number1="7" number2="7.1" name1="barrio" name2="direccion" checked1={formik.values.barrio != ''} checked2={formik.values.direccion != ''}
                                    label1="Fecha de Solicitud*"
                                    input1={
                                        <div>
                                            <Dropdown options={dependencies.cities.map(city => city.name)} name="ciudad" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.ciudad}  />
                                            {formik.touched.ciudad && formik.errors.ciudad ? <div className="help is-danger"> {formik.errors.ciudad}</div> : null}
                                        </div>
                                    }
                                    label2="Lote"
                                    input2={
                                        <div>
                                            <Dropdown options={dependencies.cities.map(city => city.name)} name="ciudad" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.ciudad}  />
                                            {formik.touched.ciudad && formik.errors.ciudad ? <div className="help is-danger"> {formik.errors.ciudad}</div> : null}
                                        </div>
                                    }
                                />

                                <FormField2
                                    number1="8" number2="8.1" name1="barrio" name2="direccion" checked1={formik.values.barrio != ''} checked2={formik.values.direccion != ''}
                                    label1="Código Argus*"
                                    input1={
                                        <div>
                                            <input className="input cool-input" type="text" name="barrio" onChange={customHandleChange} value={formik.values.barrio}  />
                                            {formik.touched.barrio && formik.errors.barrio ? <div className="help is-danger"> {formik.errors.barrio}</div> : null}
                                        </div>
                                    }
                                    label2="Clasificación*"
                                    input2={
                                        <div>
                                            <input className="input cool-input" type="text" name="direccion" onChange={customHandleChange} value={formik.values.direccion}  />
                                            {formik.touched.direccion && formik.errors.direccion ? <div className="help is-danger"> {formik.errors.direccion}</div> : null}
                                        </div>
                                    }
                                />

                                <FormField2
                                    number1="9" number2="9.1" name1="barrio" name2="direccion" checked1={formik.values.barrio != ''} checked2={formik.values.direccion != ''}
                                    label1="Fecha de reposición"
                                    input1={
                                        <div>
                                            <Dropdown options={dependencies.cities.map(city => city.name)} name="ciudad" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.ciudad}  />
                                            {formik.touched.ciudad && formik.errors.ciudad ? <div className="help is-danger"> {formik.errors.ciudad}</div> : null}
                                        </div>
                                    }
                                    label2="Responsable"
                                    input2={
                                        <div>
                                            <input className="input cool-input" type="text" name="direccion" onChange={customHandleChange} value={formik.values.direccion}  />
                                            {formik.touched.direccion && formik.errors.direccion ? <div className="help is-danger"> {formik.errors.direccion}</div> : null}
                                        </div>
                                    }
                                />

                            <FormField2
                                number1="10" number2="10.1"
                                checked1={formik.values.reclamo} checked2={formik.values.seBrindoInformacion}
                                label1="Reposición Realizada" label2="Mes - año"
                                input1={
                                    <div>
                                        <Radio name="reclamo" onChange={customHandleChange} value={formik.values.reclamo}/>
                                        {formik.touched.reclamo && formik.errors.reclamo ? <div className="help is-danger"> {formik.errors.reclamo}</div> : null }
                                    </div>
                                }
                                input2={
                                    <div>
                                         <input name="fechaProximaLlamada" className="input cool-input" type="date"  onChange={customHandleChange} value={formik.values.fechaProximaLlamada} />
                                         {formik.touched.fechaProximaLlamada && formik.errors.fechaProximaLlamada ? <div className="help is-danger"> {formik.errors.fechaProximaLlamada}</div> : null }
                                    </div>
                                }
                            />

                                <FormField label="Correo electrónico" number="11" name="correoElectronico" checked={formik.values.correoElectronico != ''}>
                                    <input className="input cool-input" type="text" name="correoElectronico" onChange={customHandleChange} value="prueba@gmail.com"  />
                                    {formik.touched.correoElectronico && formik.errors.correoElectronico ? <div className="help is-danger"> {formik.errors.correoElectronico}</div> : null}
                                </FormField>

                                <FormField label="Fecha de evento Adverso" number="12" name="fechaNacimiento" checked1={formik.values.fechaNacimiento != ''}>
                                    <input className="input cool-input" type="date" name="fechaNacimiento" onChange={customHandleChange} value={formik.values.fechaNacimiento} />
                                            {formik.touched.fechaNacimiento && formik.errors.fechaNacimiento ? <div className="help is-danger"> {formik.errors.fechaNacimiento}</div> : null}
                                </FormField>

                                <FormField2
                                number1="13" number2="13.1"
                                checked1={formik.values.reclamo} checked2={formik.values.seBrindoInformacion}
                                label1="¿Cumple?"
                                input1={
                                    <div>
                                        <Radio name="reclamo" onChange={customHandleChange} value={formik.values.reclamo}/>
                                        {formik.touched.reclamo && formik.errors.reclamo ? <div className="help is-danger"> {formik.errors.reclamo}</div> : null }
                                    </div>
                                }
                            />

                                <FormField label="" number="14" name="file" checked={formik.values.file != ''} noline>
                                  <button className="button is-rounded is-horange">Seleccionar archivo</button>
                              </FormField>

                                <br />

                                <div className="has-text-right has-text-centered-mobile">
                                    <span className="has-text-danger has-text-right">{Object.keys(formik.errors).length > 0 ? 'Por favor revise el formulario' : ' '}</span>
                                </div>

                                <br />

                                <div className="field has-text-right has-text-centered-mobile">

                                    <div className="control">
                                        <Link to="/crs/home"
                                            className="button is-horange"
                                            style={{width: '150px'}}
                                        >
                                            Enviar
                                        </Link>
                                    </div>
                                </div>

                            </form>
                        )
                        :
                        <div>Error cargando las dependencias del formulario</div>

            }
        </>
    );
}

const mapStateToPros = state => ({
    newsReducer: state.newsReducer
});

export default connect(
    mapStateToPros,
    null
)(Form);




const top100Films = [
    { title: 'The Shawshank Redemption', year: 1994 },
    { title: 'The Godfather', year: 1972 },
    { title: 'The Godfather: Part II', year: 1974 },
    { title: 'The Dark Knight', year: 2008 },
    { title: '12 Angry Men', year: 1957 },
    { title: "Schindler's List", year: 1993 },
    { title: 'Pulp Fiction', year: 1994 },
    {
        title: 'The Lord of the Rings: The Return of the King',
        year: 2003,
    },
    { title: 'The Good, the Bad and the Ugly', year: 1966 },
    { title: 'Fight Club', year: 1999 },
    {
        title: 'The Lord of the Rings: The Fellowship of the Ring',
        year: 2001,
    },
    {
        title: 'Star Wars: Episode V - The Empire Strikes Back',
        year: 1980,
    },
    { title: 'Forrest Gump', year: 1994 },
    { title: 'Inception', year: 2010 },
    {
        title: 'The Lord of the Rings: The Two Towers',
        year: 2002,
    },
    { title: "One Flew Over the Cuckoo's Nest", year: 1975 },
    { title: 'Goodfellas', year: 1990 },
    { title: 'The Matrix', year: 1999 },
    { title: 'Seven Samurai', year: 1954 },
    {
        title: 'Star Wars: Episode IV - A New Hope',
        year: 1977,
    },
    { title: 'City of God', year: 2002 },
    { title: 'Se7en', year: 1995 },
    { title: 'The Silence of the Lambs', year: 1991 },
    { title: "It's a Wonderful Life", year: 1946 },
    { title: 'Life Is Beautiful', year: 1997 },
    { title: 'The Usual Suspects', year: 1995 },
    { title: 'Léon: The Professional', year: 1994 },
    { title: 'Spirited Away', year: 2001 },
    { title: 'Saving Private Ryan', year: 1998 },
    { title: 'Once Upon a Time in the West', year: 1968 },
    { title: 'American History X', year: 1998 },
    { title: 'Interstellar', year: 2014 },
    { title: 'Casablanca', year: 1942 },
    { title: 'City Lights', year: 1931 },
    { title: 'Psycho', year: 1960 },
    { title: 'The Green Mile', year: 1999 },
    { title: 'The Intouchables', year: 2011 },
    { title: 'Modern Times', year: 1936 },
    { title: 'Raiders of the Lost Ark', year: 1981 },
    { title: 'Rear Window', year: 1954 },
    { title: 'The Pianist', year: 2002 },
    { title: 'The Departed', year: 2006 },
    { title: 'Terminator 2: Judgment Day', year: 1991 },
    { title: 'Back to the Future', year: 1985 },
    { title: 'Whiplash', year: 2014 },
    { title: 'Gladiator', year: 2000 },
    { title: 'Memento', year: 2000 },
    { title: 'The Prestige', year: 2006 },
    { title: 'The Lion King', year: 1994 },
    { title: 'Apocalypse Now', year: 1979 },
    { title: 'Alien', year: 1979 },
    { title: 'Sunset Boulevard', year: 1950 },
    {
        title: 'Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb',
        year: 1964,
    },
    { title: 'The Great Dictator', year: 1940 },
    { title: 'Cinema Paradiso', year: 1988 },
    { title: 'The Lives of Others', year: 2006 },
    { title: 'Grave of the Fireflies', year: 1988 },
    { title: 'Paths of Glory', year: 1957 },
    { title: 'Django Unchained', year: 2012 },
    { title: 'The Shining', year: 1980 },
    { title: 'WALL·E', year: 2008 },
    { title: 'American Beauty', year: 1999 },
    { title: 'The Dark Knight Rises', year: 2012 },
    { title: 'Princess Mononoke', year: 1997 },
    { title: 'Aliens', year: 1986 },
    { title: 'Oldboy', year: 2003 },
    { title: 'Once Upon a Time in America', year: 1984 },
    { title: 'Witness for the Prosecution', year: 1957 },
    { title: 'Das Boot', year: 1981 },
    { title: 'Citizen Kane', year: 1941 },
    { title: 'North by Northwest', year: 1959 },
    { title: 'Vertigo', year: 1958 },
    {
        title: 'Star Wars: Episode VI - Return of the Jedi',
        year: 1983,
    },
    { title: 'Reservoir Dogs', year: 1992 },
    { title: 'Braveheart', year: 1995 },
    { title: 'M', year: 1931 },
    { title: 'Requiem for a Dream', year: 2000 },
    { title: 'Amélie', year: 2001 },
    { title: 'A Clockwork Orange', year: 1971 },
    { title: 'Like Stars on Earth', year: 2007 },
    { title: 'Taxi Driver', year: 1976 },
    { title: 'Lawrence of Arabia', year: 1962 },
    { title: 'Double Indemnity', year: 1944 },
    {
        title: 'Eternal Sunshine of the Spotless Mind',
        year: 2004,
    },
    { title: 'Amadeus', year: 1984 },
    { title: 'To Kill a Mockingbird', year: 1962 },
    { title: 'Toy Story 3', year: 2010 },
    { title: 'Logan', year: 2017 },
    { title: 'Full Metal Jacket', year: 1987 },
    { title: 'Dangal', year: 2016 },
    { title: 'The Sting', year: 1973 },
    { title: '2001: A Space Odyssey', year: 1968 },
    { title: "Singin' in the Rain", year: 1952 },
    { title: 'Toy Story', year: 1995 },
    { title: 'Bicycle Thieves', year: 1948 },
    { title: 'The Kid', year: 1921 },
    { title: 'Inglourious Basterds', year: 2009 },
    { title: 'Snatch', year: 2000 },
    { title: '3 Idiots', year: 2009 },
    { title: 'Monty Python and the Holy Grail', year: 1975 },
];
