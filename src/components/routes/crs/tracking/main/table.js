import {Link} from 'react-router-dom';
import DataTable from 'components/base/datatable'
const Table = props => {

    const columns = [
        { name: "codigousuario", label: "CÓD. MÉDICO", options: { filter: true, sort: true } },
        { name: "name", label: "NOMBRE", options: { filter: true, sort: true } },
        { name: "country", label: "PAÍS", options: { filter: true, sort: true } },
        { name: "state", label: "ESTADO", options: { filter: true, sort: true } },
        { name: "actions", label: "ACCIONES",  

        options: {
            customBodyRender: () => {
                return (
                 <Link to="/crs/tracking/edit" className="d-flex justify-content-center" ><i class="far fa-eye fa-3x"></i></Link>
                );
            
            }
          }
        
    }
      ];
    
      const data = [
        {
            codigousuario: "0001",
            name: "Dr. prueba",
            country: "Colombia",
            state: "Activo",
            actions: "<h3>asdadsa</h3>",
        },
        {
          remision: "0001",
          referencia: "0001",
          producto: "XARELTO",
          cantidad: "02",
          responsable: "doctor",
          bodega: "bayer",
          ubicacion: "bogota",
          ingreso: "04/11/2021",
          proveedor: "dental",
          img_producto: "Sin imagen",
          observaciones: "Ninguno",
      },
      {
        remision: "0001",
        referencia: "0001",
        producto: "XARELTO",
        cantidad: "02",
        responsable: "doctor",
        bodega: "bayer",
        ubicacion: "bogota",
        ingreso: "04/11/2021",
        proveedor: "dental",
        img_producto: "Sin imagen",
        observaciones: "Ninguno",
    },
    {
        remision: "0001",
        referencia: "0001",
        producto: "XARELTO",
        cantidad: "02",
        responsable: "doctor",
        bodega: "bayer",
        ubicacion: "bogota",
        ingreso: "04/11/2021",
        proveedor: "dental",
        img_producto: "Sin imagen",
        observaciones: "Ninguno",
    },
    {
        remision: "0001",
        referencia: "0001",
        producto: "XARELTO",
        cantidad: "02",
        responsable: "doctor",
        bodega: "bayer",
        ubicacion: "bogota",
        ingreso: "04/11/2021",
        proveedor: "dental",
        img_producto: "Sin imagen",
        observaciones: "Ninguno",
    },
      ];

    return(
        <div className="columns is-mobile">
        <div className="column is-1 is-hidden-tablet has-text-centered" style={{minWidth: '10px'}}></div>
            <div className="column">
                <div className="columns is-mobile">
                    <div className="column is-1 is-hidden-tablet has-text-centered" style={{minWidth: '10px'}}></div>
                        <div className="column">
                        <DataTable data={data} columns={columns} title={"Listado de ingresos"}/>
                        </div>
                    <div className="column is-1 is-hidden-mobile" style={{maxWidth: '10px'}}></div>
                </div>
            </div>
        <div className="column is-1 is-hidden-mobile" style={{maxWidth: '10px'}}></div>
    </div>
    );
}

export default Table;
