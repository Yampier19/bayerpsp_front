import {useState, useEffect} from 'react';

import FilterBar from '../../../../base/filter-bar';
import Table from "./table";
import {Link} from 'react-router-dom';

import axios from 'axios';


const Tracking = props => {

    const url = 'https://jsonplaceholder.typicode.com/posts';
    const [news, setNews] = useState([]);

    useEffect(
        () => {

            axios.get(url)
            .then(res => {

                setNews(res.data.splice(0, 50));

            })
            .then(err => console.log(err));

        }, []
    );

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 py-0">
                <h1 className="title has-text-hred is-2">Seguimiento</h1>
            </section>

            {/* first level - filters */}
            <section className="section px-0 py-6">

                <div className="columns">
                    <div className="column">
                        <FilterBar
                        textColorClass="has-text-hred"
                        filters={[
                            {
                                name: "Código Médico",
                                options: [{name: "op1"}, {name: "op2"}]
                            },
                            {
                                name: "Nombre",
                                options: []
                            },
                            {
                                name: "País",
                                options: []
                            },
                            {
                                name: "Estado",
                                options: []
                            },
                        ]}
                        moreFiltersOn
                        tags={null}
                    />
                    </div>
                </div>





            </section>



            <br/>

            {/* second level - table */}
            <section
        className="is-flex-grow-1 "
        style={{ minHeight: "100%", overflow: "hidden" }}
      >
        <div
          id="tab1"
          className="tab is-active py-4"
          style={{ height: "100%", overflow: "hidden" }}
        >
          <div
            className="coolscroll horange" 
             style={{ height: "100%", overflow: "auto" }}
          >
  
            <Table />
            </div>
        </div>
       
      </section>

        

        </div>
    );
}

export default Tracking;
