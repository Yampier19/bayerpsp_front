import React from 'react'
import Details from './details'
import Table from "./table";

export default function details_tracking() {
    return (
        <div className="is-flex is-flex-direction-column " style={{ height: '100%' }}>

            {/* header */}
            <section className="section px-0 py-0">
                <h1 className="title has-text-hred is-2">Seguimiento</h1>
            </section>

            <section className="section px-0 py-6">
                <div className="row">
                    <div className="col-4">
                        <div className="card ">
                            <div className="card-header" style={{ background: '#D7908F' }}>
                                <div className="row-reverse mx-auto">
                                    <div className="col text-center">
                                        <label className="text-white" style={{ fontWeight: 'bold', fontSize: 25 }}>Lydia Valencia</label>
                                    </div>
                                    <div className="col text-center">
                                        <label className="text-white" style={{ fontSize: 16 }}>Médico Ginecologa</label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <div className="card-body bg-white">
                                        <Details />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <section
                            className="is-flex-grow-1 pl-5"
                            style={{ minHeight: "100%", overflow: "hidden" }}
                        >
                            <div
                                id="tab1"
                                className="tab is-active py-4"
                                style={{ height: "100%", overflow: "hidden" }}
                            >
                                <div
                                    className="coolscroll horange"
                                    style={{ height: "100%", overflow: "auto" }}
                                >

                                    <Table />
                                </div>
                            </div>

                        </section>
                    </div>
                </div>
            </section>

        </div>
    )
}





