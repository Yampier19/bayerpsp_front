import * as React from 'react';


export default function Table() {
    return (
        <div className="row">
            <div className="col text-center">
                <ul>
                    <li style={styles.li}>Código de Médico</li>
                    <li style={styles.li} >Nombre Completo</li>
                    <li style={styles.li}>Correo Electrónico</li>
                    <li style={styles.li}>País</li>
                    <li style={styles.li}>Departamento</li>
                    <li style={styles.li}>Ciudad</li>
                    <li style={styles.li}>Dirección</li>
                    <li style={styles.li}>Fecha de creación</li>
                    <li style={styles.li}>Número de CRS Realizadas</li>
                </ul>
            </div>
            <div className="col text-center">
                <ul>
                     <li style={styles.li}>DR-35681356498</li>
                     <li style={styles.li}>Lydia Valencia</li>
                     <li style={styles.li}>correo@correo.com</li>
                     <li style={styles.li}>Colombia</li>
                     <li style={styles.li}>Cundinamarca</li>
                     <li style={styles.li}>Bogotá</li>
                     <li style={styles.li}>Calle Falsa 123</li>
                     <li style={styles.li}>01-10-2020</li>
                     <li style={styles.li}>5</li>
                </ul>
            </div>
        </div>
    )
}


const styles = {
    li: {
        marginBottom: 20
    }
}