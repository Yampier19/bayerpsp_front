import React from "react";
import DataTable from "components/base/datatable";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 800,
  bgcolor: "background.paper",
  borderRadius: 5,
  boxShadow: 24,
  p: 4,
};

const Table = (props) => {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const columns = [
    { name: "crs", label: "CRS", options: { filter: true, sort: true } },
    { name: "name", label: "PRODUCTO", options: { filter: true, sort: true } },
    {
      name: "date",
      label: "Fecha solicitud",
      options: { filter: true, sort: true },
    },
    {
      name: "responsable",
      label: "Responsable",
      options: { filter: true, sort: true },
    },
    { name: "state", label: "Estado", options: { filter: true, sort: true } },
    {
      name: "actions",
      label: "ACCIONES",

      options: {
        customBodyRender: () => {
          return (
            <div>
              <Button onClick={handleOpen}>GESTIONAR</Button>
              <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
              >
                <Box sx={style}>
                  <div className="row">
                    <div className="col-1 d-flex justify-content-center align-items-center">
                      <i
                        class="far fa-question-circle fa-3x"
                        style={{ color: "#E7967A" }}
                      ></i>
                    </div>
                    <div className="col">
                      <div className="row-reverse">
                        <div className="col">
                          <label
                            for=""
                            className="lead"
                            style={{
                              fontSize: 25,
                              color: "#E7967A",
                              fontWeight: "bold",
                            }}
                          >
                            NÚMERO CRS
                          </label>
                        </div>
                        <div className="col">
                          <label
                            className="lead"
                            style={{ fontSize: 16, color: "#E7967A" }}
                          >
                            Lydia Valencia
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row-reverse my-5">
                    <div className="col">
                      <textarea
                        placeholder="Escriba aquí su comentario..."
                        class="form-control pb-5"
                      ></textarea>
                    </div>
                    <div className="col my-3">
                      <div className="row d-flex justify-content-center">
                        <button
                          className="btn col-5 mr-3 text-white"
                          style={{
                            backgroundColor: "#E7967A",
                            borderRadius: 20,
                          }}
                        >
                          Activo
                        </button>
                        <button
                          className="btn col-5 text-white"
                          style={{
                            backgroundColor: "#BBBBBB",
                            borderRadius: 20,
                          }}
                        >
                          Inactivo
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="row-reverse">
                    <div
                      className="col coolscroll horange"
                      style={{ overflowY: "scroll", height: 250 }}
                    >
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">RESPONSABLE</th>
                            <th scope="col">FECHA</th>
                            <th scope="col">COMENTARIO</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Cliente Invitado</td>
                            <td>Resultados</td>
                            <td>
                              Lorem ipsum dolor sit amet, consectetuer
                              adipiscing elit. Aenean commodo ligula eget dolor.
                              Aenean massa. Cum sociis natoque penatibus.
                            </td>
                          </tr>
                          <tr>
                            <td>Cliente Invitado</td>
                            <td>Resultados</td>
                            <td>
                              Lorem ipsum dolor sit amet, consectetuer
                              adipiscing elit. Aenean commodo ligula eget dolor.
                              Aenean massa. Cum sociis natoque penatibus.
                            </td>
                          </tr>
                          <tr>
                            <td>Cliente Invitado</td>
                            <td>Resultados</td>
                            <td>
                              Lorem ipsum dolor sit amet, consectetuer
                              adipiscing elit. Aenean commodo ligula eget dolor.
                              Aenean massa. Cum sociis natoque penatibus.
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div className="col d-flex justify-content-end mt-5">
                      <button
                        className="btn col-4 mr-3 text-white"
                        style={{ backgroundColor: "#E7967A", borderRadius: 20 }}
                      >
                        Actualizar
                      </button>
                    </div>
                  </div>
                </Box>
              </Modal>
            </div>
          );
        },
      },
    },
  ];

  const data = [
    {
      crs: "0001",
      name: "Kyleena",
      date: "10-05-2021",
      responsable: "Asesor 1",
      state: "Activo",
    },
    {
      crs: "0001",
      name: "Kyleena",
      date: "10-05-2021",
      responsable: "Asesor 1",
      state: "Activo",
    },
    {
      crs: "0001",
      name: "Kyleena",
      date: "10-05-2021",
      responsable: "Asesor 1",
      state: "Activo",
    },
  ];

  return (
    <div className="columns is-mobile">
      <div
        className="column is-1 is-hidden-tablet has-text-centered"
        style={{ minWidth: "10px" }}
      ></div>
      <div className="column">
        <div className="columns is-mobile">
          <div
            className="column is-1 is-hidden-tablet has-text-centered"
            style={{ minWidth: "10px" }}
          ></div>
          <div className="column">
            <DataTable
              data={data}
              columns={columns}
              title={"Listado de ingresos"}
            />
          </div>
          <div
            className="column is-1 is-hidden-mobile"
            style={{ maxWidth: "10px" }}
          ></div>
        </div>
      </div>
      <div
        className="column is-1 is-hidden-mobile"
        style={{ maxWidth: "10px" }}
      ></div>
    </div>
  );
};

export default Table;
