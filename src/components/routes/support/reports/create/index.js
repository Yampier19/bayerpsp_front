import {useState, useEffect} from 'react';

import FilterBar from '../../../../base/filter-bar';

import axios from 'axios';

const ReportsCreate = props => {

    const url = 'https://jsonplaceholder.typicode.com/posts';

    const [news, setNews] = useState([]);

    useEffect(
        () => {

            axios.get(url)
            .then(res => {

                setNews(res.data.splice(0, 50));

            })
            .then(err => console.log(err));

        }, []
    );

    const [appliedFilters] = useState([
        {
            name: 'Motivo comunicación'
        },
        {
            name: 'Logro comunicación'
        }
    ]);



    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 py-0">
                <h1 className="title has-text-hblue is-2">Reportes</h1>
                <h1 className="subtitle has-text-hblue ">Crear filtro</h1>
            </section>



            <br/>
            <br/>

            {/* filter bar */}
            <section className="section p-0 pb-1 ">
                <div className="columns">
                    <div className="column">
                        <FilterBar
                        textColorClass="has-text-hblue"
                        filters={[
                            {
                                name: "Id",
                                options: [{name: "op1"}, {name: "op2"}]
                            },
                            {
                                name: "Nombre",
                                options: []
                            },
                            {
                                name: "Medicamento",
                                options: []
                            },
                            {
                                name: "Estado",
                                options: []
                            },
                        ]}
                        moreFiltersOn
                        tags={appliedFilters}
                    />
                    </div>
                    <div className="column is-2-desktop has-text-right has-text-left-mobile">
                        <button className="button is-hred">
                            <span className="icon">
                                <i class="fas fa-save"></i>
                            </span>
                            <span>
                                Guardar
                            </span>
                        </button>
                    </div>
                </div>

            </section>


            {/* second level - table */}
            <section className="is-flex-grow-1" style={{minHeight: '500px', overflow: 'hidden'}}>
                <div className="coolscroll hblue" style={{overflow: 'auto', height:"100%", width: "100%"}}>
                    <div className="box table-header has-background-hblue-light" style={{minWidth: '1450px'}}>
                        <div className="columns has-text-hblue is-mobile fila" >

                            <div className="column is-1 hblue">ID</div>
                            <div className="column is-2 hblue">Nombre</div>
                            <div className="column is-2 hblue">Medicamento</div>
                            <div className="column is-1 hblue">Estado</div>
                            <div className="column  hblue">Motivo de comunicación</div>
                            <div className="column no-border">Logro comunicación</div>


                        </div>
                    </div>
                    <br/>

                    {
                        news.map( (neww,i) =>

                            <div className="mb-6">
                                <div className="box" key={i} style={{minWidth: '1450px'}}>
                                    <div className="columns fila2 is-mobile">
                                        <div className="column is-1">ID</div>
                                        <div className="column is-2">Nombre</div>
                                        <div className="column is-2">Medicamento</div>
                                        <div className="column is-1">Estado</div>
                                        <div className="column ">Motivo de comunicación</div>
                                        <div className="column no-border">Logro comunicación</div>
                                    </div>
                                </div>

                            </div>
                        )
                    }
                </div>
            </section>

            {/* data count */}
            <section className="section px-0 pb-0">

                <h1 className="subtitle has-text-hblue">
                    se encontraron {news.length} registros
                </h1>

            </section>

        </div>

    );
}

export default ReportsCreate;
