import Chart from 'react-apexcharts'

const ChartComponent = props => {

    const options = {
        chart:{
             type: 'bar',
             stacked: true,
        },
        stroke: {
            width: 1,
            colors: ['#fff']
        },
        title:{
            text: 'Titulo de la grafica',
            align: 'center',
        },
        xaxis: {
            categories: ['junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio'],
            labels: {
                formatter: val => val.substring(0, 3)
            }
        },
        legend: {
            position: 'top',
            horizontalAlign: 'center',
            offsetX: 40,            
            markers: {
                fillColors:  ['#5167C3', '#63CAEB', '#B1E06D']
            }
        },
        fill: {
            colors: ['#5167C3', '#63CAEB', '#B1E06D']
        }
    };

    const series = [
        {
            name: 'activo',
            data: [95, 106, 99, 97, 87, 89, 78, 82, 83, 88, 94, 93, 93],

        }, {
            name: 'Interrumpido',
            data: [20, 12, 18, 11, 15, 15, 20, 21, 16, 11, 11, 16, 15]
        }, {
            name: 'Suspendido',
            data: [5, 2, 3, 8, 5, 5, 4, 1, 18, 15, 17, 14, 11]
        }
    ];

    return <div className="box"><Chart options={options} series={series} type="bar" /></div>
}

export default ChartComponent;
