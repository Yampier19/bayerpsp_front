// import img from 'temp/image.png';
import './graphics.scss';

import Chart1 from './chart1';
import Chart2 from './chart2';

import {
    banCol,
    banEcu,
    banPer,
    banN
} from './flags1';


const Graphics = props => {


    const countryBtnClick = e => {

        const newActive = e.currentTarget;

        const currentActive = document.querySelector('.country-btn.is-active');
        if(currentActive == newActive)
            return;
        currentActive.classList.remove('is-active');
        newActive.classList.add('is-active');
    }

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 py-0">

                <div className="columns is-mobile">
                    <div className="column">
                        <h1 className="title has-text-hred is-2">Reportes</h1>
                        <h1 className="subtitle has-text-hred ">Gráficas</h1>
                    </div>
                    <div className="column has-text-centered is-2">

                    </div>

                </div>

            </section>



            <br/>
            <br/>

            <section>
                <div className="level">
                    <div className="level-left">

                    </div>
                    <div className="level-right">
                        <div className="country-container has-background-light p-3 is-inline-flex" style={{width: '100%'}}>
                            <div className="country-btn has-background-white mr-3 is-active" onClick={countryBtnClick}>
                                <img src={banCol}/>
                            </div>
                            <div className="country-btn has-background-white mr-3" onClick={countryBtnClick}>
                                <img src={banEcu}/>
                            </div>
                            <div className="country-btn has-background-white mr-3" onClick={countryBtnClick}>
                                <img src={banPer}/>
                            </div>
                            <div className="country-btn has-background-white mr-3" onClick={countryBtnClick}>
                                <img src={banN}/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section className="section px-0 is-flex-grow-1 pt-0" style={{height: '300px', overflow: 'hidden'}}>
                <div className="coolscroll hred" style={{height: '100%', overflowY: 'auto', overflowX: 'hidden'}}>
                    <div className="columns">
                        <div className="column">
                            <Chart1/>
                        </div>
                        <div className="column">
                            <Chart2/>
                        </div>
                    </div>
                    <div className="columns">
                        <div className="column">
                            <Chart1/>
                        </div>
                        <div className="column">
                            <Chart1/>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    );
}

export default Graphics;
