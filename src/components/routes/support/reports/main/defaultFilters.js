export const patientFilters = [
    {
        name: 'Pacientes',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 0
    },
    {
        name: 'Paciente tratamiento',
        visibility: <div>usuarios concretos  <span className="icon"><i class="fas fa-users"></i></span></div>,
        id: 1
    },
    {
        name: 'Gestiones',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 2
    },
    {
        name: 'Clasificación patológica',
        visibility: <div>Pública <span className="icon"><i class="fas fa-globe-americas"></i></span></div>,
        id: 3
    }
];
//<div>Privada <span className="icon"><i class="fas fa-user"></i></span></div>,
