import { useState, } from 'react';

// import CreateFilter from 'components/base/buttons/create-filter';

import DraggablePanel from 'components/base/draggable-panel';

import {Link} from 'react-router-dom';


import {
    patientFilters
} from './defaultFilters';

const Reports = props => {

    const [filters1, setFilters1] = useState(patientFilters);

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="section px-0 pt-0 pb-3 ">
                <div className="columns is-mobile">
                    <div className="column is-3-desktop ">
                        <h1 className="subtitle has-text-hblue is-3 mb-1"  style={{ whiteSpace: 'nowrap'}}>
                            <strong className="has-text-hblue">Reportes</strong>
                        </h1>
                        <h1 className="subtitle has-text-hblue">Filtros</h1>
                    </div>
                    <div className="column is-3-desktop is-offset-6-desktop is-6-mobile">
                        <Link to="/support/reports/create" className="button is-hred is-fullwidth is-size-5 has-text-left btn2">
                            <span className="icon mr-1"><i className="fas fa-plus-circle"></i></span> &nbsp;
                            Crear Filtro
                        </Link>
                    </div>
                </div>
            </section>



            <br/>
            <br/>

            {/* forms */}
            <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                <div className="py-4 coolscroll" style={{height: '100%', overflowY: 'scroll'}}>

                    <DraggablePanel items={filters1} setItems={setFilters1} colorClassName="has-text-hblue"/>
                </div>
            </section>

        </div>
    );
}

export default Reports;
