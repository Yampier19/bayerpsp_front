import {useState} from 'react';

import FormField from 'components/form/formfield';
import Dropdown from 'components/form/dropdown';
import Radio from 'components/form/radio';
import RadioThree from 'components/form/radio/radioThree';
import FormField2 from 'components/form/formfield2';

import {useFormik} from 'formik';
import * as Yup from 'yup';

import {getRequired} from 'utils/forms';


import LoadingComponent from 'components/base/loading-component';
import postHandler from './postHandler';

import {connect} from 'react-redux';

const Form = props => {

    const {progress, setProgress} = props;
    const {formValues1} = props;

    const [dependencies, setDepencies] = useState({
        loading: false,
        id: '',
        productos: []
    });

    const formSchema = Yup.object().shape({
        idTratamiento: Yup.number().typeError('se debe especificar un valor numerico'),
        reclamo: Yup.string().required('Campo obligatorio'),
        seBrindoInformacion: Yup.string(),
        cajasPorUnidad: Yup.string().required('Campo obligatorio'),
        producto: Yup.string().required('Campo obligatorio'),
        dosis: Yup.string(),
        dosisDeInicio: Yup.string(),
        estadoPaciente: Yup.string(),
        clasificacionPatologica: Yup.string().required('Campo obligatorio'),
        tratamientoPrevio: Yup.string().required('Campo obligatorio'),
        consentimiento: Yup.string().required('Campo obligatorio'),
        fechaInicioTerapia: Yup.string().required('Campo obligatorio'),
        regimen: Yup.string().required('Campo obligatorio'),
        asegurador: Yup.string().required('Campo obligatorio'),
        operadorLogistico: Yup.string().required('Campo obligatorio'),
        fechaUltimaReclamacion: Yup.string(),
        otrosOperadores:  Yup.string(),
        puntoDeEntrega: Yup.string(),
        puntoQueAtiende: Yup.string().required('Campo obligatorio'),
        mediosAdquisicion: Yup.string(),
        fechaProximaLlamada: Yup.string().required('Campo obligatorio'),
        medico:  Yup.string(),
        especialidad:  Yup.string(),
        paramedico: Yup.string(),
        zonaAtencion: Yup.string(),
        codigo: Yup.string(),
        ciudadBase: Yup.string(),
        pacienteHaceParteDelPap: Yup.string(),
        agregarInformacionAplicaciones: Yup.string(),
        tipoEnvio: Yup.string()
    });

    const formik = useFormik({
        enableReinitialize: true,
        initialValues: {
            idTratamiento: '',
            reclamo: '',
            seBrindoInformacion: '',
            cajasPorUnidad: '',
            producto: '',
            dosis: '',
            dosisDeInicio: '',
            estadoPaciente: '',
            clasificacionPatologica: '',
            tratamientoPrevio: '',
            consentimiento: '',
            fechaInicioTerapia: '',
            regimen: '',
            asegurador: '',
            operadorLogistico: '',
            fechaUltimaReclamacion: '',
            otrosOperadores: '',
            puntoDeEntrega: '',
            puntoQueAtiende: '',
            mediosAdquisicion: '',
            fechaProximaLlamada: '',
            medico: '',
            especialidad: '',
            paramedico: '',
            zonaAtencion: '',
            codigo: '',
            ciudadBase: '',
            pacienteHaceParteDelPap: '',
            agregarInformacionAplicaciones: '',
            tipoEnvio: ''
        },
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            // alert(JSON.stringify(values, null, 2));
            await postHandler({
                paciente: formValues1,
                tratamiento: values
            });

        }
    });

    const requiredFields = getRequired(formSchema);

    const customHandleChange = e => {


        //handle formik change
        formik.handleChange(e);
        const form = e.target.form;
        //check if element is required and them count progress
        const count = requiredFields.filter( fieldName => form[fieldName].value != '').length;

        setProgress(count/requiredFields.length * 100);
    }

    return(
            <>
                {/*}<FormDependencies dependencies={dependencies}  setDepencies={setDepencies} />*/}
                {
                    dependencies.loading ?
                        <LoadingComponent loadingText="Cargando datos"/>
                    :
                    (
                        <form id="form2" onSubmit={formik.handleSubmit} className="pr-3 py-3">
                            <FormField label="Clasificacion Patologica" number="1" name="idTratamiento" checked={formik.values.idTratamiento != ''}>
                                <input className="input cool-input" type="text" name="idTratamiento" onChange={customHandleChange} value={formik.values.idTratamiento} />
                                {formik.touched.idTratamiento && formik.errors.idTratamiento ? <div className="help is-danger"> {formik.errors.idTratamiento}</div> : null }
                            </FormField>

                            <FormField2
                                number1="2" number2="2.1"
                                checked1={formik.values.reclamo} checked2={formik.values.seBrindoInformacion}
                                label1="Consentimiento" label2="Asegurador"
                                input1={
                                    <div>
                                        <RadioThree name="reclamo" onChange={customHandleChange} value={formik.values.reclamo}/>
                                        {formik.touched.reclamo && formik.errors.reclamo ? <div className="help is-danger"> {formik.errors.reclamo}</div> : null }
                                    </div>
                                }
                                input2={
                                    <div>
                                        <Dropdown name="seBrindoInformacion" onChange={customHandleChange} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                        {formik.touched.seBrindoInformacion && formik.errors.seBrindoInformacion ? <div className="help is-danger"> {formik.errors.seBrindoInformacion}</div> : null }
                                    </div>
                                }
                            />

                            <FormField2
                                number1="3" number2="3.1"
                                checked1={formik.values.cajasPorUnidad} checked2={formik.values.producto}
                                label1="Región" label2="IPS que atiende"
                                input1={
                                    <div>
                                        <Dropdown name="cajasPorUnidad" onChange={customHandleChange} options={["op1", "op2"]} arrowColor="has-text-primary" />
                                        {formik.touched.cajasPorUnidad && formik.errors.cajasPorUnidad ? <div className="help is-danger"> {formik.errors.cajasPorUnidad}</div> : null }
                                    </div>
                                }
                                input2={
                                    <div>
                                        <Dropdown name="producto" onChange={customHandleChange} options={["op1", "op2"]} arrowColor="has-text-primary" />
                                        {formik.touched.producto && formik.errors.producto ? <div className="help is-danger"> {formik.errors.producto}</div> : null }
                                    </div>
                                }
                            />

                            <FormField2
                                number1="4" number2="4.1"
                                checked1={formik.values.medico} checked2={formik.values.especialidad}
                                label1="Médico" label2="Especialidad"
                                input1={
                                    <div>
                                        <Dropdown name="medico" onChange={customHandleChange} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                        {formik.touched.medico && formik.errors.medico ? <div className="help is-danger"> {formik.errors.medico}</div> : null }
                                    </div>
                                }
                                input2={
                                    <div>
                                        <Dropdown name="especialidad" onChange={customHandleChange} options={["op1", "op2"]} arrowColor="has-text-primary"/>
                                        {formik.touched.especialidad && formik.errors.especialidad ? <div className="help is-danger"> {formik.errors.especialidad}</div> : null }
                                    </div>
                                }
                            />

                            <FormField label="Fecha de proxima llegada" number="5" name="idTratamiento" checked={formik.values.idTratamiento != ''}>
                                <input className="input cool-input" type="date" name="idTratamiento" onChange={customHandleChange} value={formik.values.idTratamiento} />
                                {formik.touched.idTratamiento && formik.errors.idTratamiento ? <div className="help is-danger"> {formik.errors.idTratamiento}</div> : null }
                            </FormField>

                            <FormField2
                                number1="6" number2="6.1"
                                checked1={formik.values.dosis} checked2={formik.values.dosisDeInicio}
                                label1="Seleccione los exámenes a solicitar" label2="Valor del examen"
                                input1={
                                    <div>
                                        <div class="control">
                                            <input type="radio" name={props.name} onChange={props.onChange} value={props.option1 || 'Tomografía de coherencia Óptica OCP'}/>&nbsp;
                                            {props.option1 || 'Tomografía de coherencia Óptica OCP'}
                                        </div>
                                    </div>
                                }
                                input2={
                                    <div>
                                        <input className="input cool-input" type="text" name="idTratamiento" onChange={customHandleChange} value={formik.values.idTratamiento} />
                                        {formik.touched.idTratamiento && formik.errors.idTratamiento ? <div className="help is-danger"> {formik.errors.idTratamiento}</div> : null }
                                    </div>
                                }
                            />

                            <FormField2
                                number1="7" number2="7.1"
                                checked1={formik.values.estadoPaciente} checked2={formik.values.clasificacionPatologica}
                                label1="Cantidad de examenes" label2="Número de voucher"
                                input1={
                                    <div>
                                        <input className="input cool-input" type="text" name="idTratamiento" onChange={customHandleChange} value={formik.values.idTratamiento} />
                                        {formik.touched.idTratamiento && formik.errors.idTratamiento ? <div className="help is-danger"> {formik.errors.idTratamiento}</div> : null }
                                    </div>
                                }
                                input2={
                                    <div>
                                        <input className="input cool-input" type="text" name="idTratamiento" onChange={customHandleChange} value={formik.values.idTratamiento} />
                                        {formik.touched.idTratamiento && formik.errors.idTratamiento ? <div className="help is-danger"> {formik.errors.idTratamiento}</div> : null }
                                    </div>
                                }
                            />

                            <FormField2
                                number1="8" number2="8.1"
                                checked1={formik.values.estadoPaciente} checked2={formik.values.clasificacionPatologica}
                                label1="Centro médico diagnostico" label2="Gestión"
                                input1={
                                    <div>
                                        <input className="input cool-input" type="text" name="idTratamiento" onChange={customHandleChange} value={formik.values.idTratamiento} />
                                        {formik.touched.idTratamiento && formik.errors.idTratamiento ? <div className="help is-danger"> {formik.errors.idTratamiento}</div> : null }
                                    </div>
                                }
                                input2={
                                    <div>
                                        <input className="input cool-input" type="text" name="idTratamiento" onChange={customHandleChange} value={formik.values.idTratamiento} />
                                        {formik.touched.idTratamiento && formik.errors.idTratamiento ? <div className="help is-danger"> {formik.errors.idTratamiento}</div> : null }
                                    </div>
                                }
                            />

                            

                            <br/>

                            <div className="has-text-right has-text-centered-mobile">
                                <span className={`has-text-danger has-text-right ${Object.keys(formik.errors).length > 0 ? '' : 'is-invisible'}`}>
                                    Por favor revise el formulario
                                </span>
                            </div>

                            <br/>

                            <div className="field has-text-right has-text-centered-mobile">
                                <div className="control">
                                    <button
                                        className={`button is-horange ${props.trackingReducer['CREATE_PATIENT'].loading ? 'is-loading' : ''}`}
                                            type="submit"
                                            data-tab="tab1"
                                            style={{width: '150px'}}
                                            disabled={false}
                                        >
                                            Registrar
                                    </button>
                                </div>
                            </div>
                        </form>
                    )
                }
            </>
    );
}

const mapStateToPros = state => ({
    trackingReducer: state.trackingReducer
});

export default connect(
    mapStateToPros,
    null
)(Form);
