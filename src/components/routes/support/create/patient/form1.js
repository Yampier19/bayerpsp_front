import {useReducer} from 'react';

import FormField from 'components/form/formfield';
import Dropdown from 'components/form/dropdown';
import Radio from 'components/form/radio';
import FormField2 from 'components/form/formfield2';

import {useFormik} from 'formik';
import * as Yup from 'yup';

import {getRequired} from 'utils/forms';

import Dependencies from './dependencies';
import LoadingComponent from 'components/base/loading-component';


import {connect} from 'react-redux';

const dependencies_initState = {
    loading: false,
    success: false,
    error: false,
    paciente: {
        id: ''
    },
    status_list: [],
    cities: [],
    departamentos: []
};

const depReducer = (state, action) => {
    switch (action.type) {
        case 'req_status':
            return{...state, loading: action.loading, success: action.success, error: action.error};
        case 'set_paciente': return{...state, paciente: action.payload};
        case 'set_status_list': return{...state, status_list: action.payload};
        case 'set_form_cities': return{...state, cities: action.payload};
        case 'set_form_departamentos': return {...state, departamentos: action.payload}
        default: return{...state}
    }
}

const Form = props => {

    /* *~~*~~*~~*~~*~~*~~*~~*~~* outter callbacks *~~*~~*~~*~~*~~*~~*~~*~~* */
    const {progress, setProgress} = props;
    const {formValues1, setFormValues1} = props;
    const {onSubmitCallback} = props;

    /* *~~*~~*~~*~~*~~*~~*~~*~~* form dependencies *~~*~~*~~*~~*~~*~~*~~*~~* */
    const [dependencies, dispatchDep] = useReducer(depReducer, dependencies_initState);

    /* *~~*~~*~~*~~*~~*~~*~~*~~* form values *~~*~~*~~*~~*~~*~~*~~*~~* */
    const formSchema = Yup.object().shape({
        codigoUsuario: Yup.number().typeError('se debe especificar un valor numerico'),
        estadoPaciente: Yup.string().required('Campo obligatorio'),
        fechaActivacion: Yup.string().required('Campo obligatorio'),
        correoElectronico: Yup.string().required('Campo obligatorio').email('correoElectronico invalido'),
        nombre: Yup.string().required('Campo obligatorio'),
        apellido: Yup.string().required('Campo obligatorio'),
        identificacion: Yup.number().typeError('se debe especificar un valor numerico').required('Campo obligatorio'),
        telefono: Yup.number().typeError('se debe especificar un valor numerico').required('Campo obligatorio'),
        departamento: Yup.string().required('Campo obligatorio'),
        ciudad: Yup.string().required('Campo obligatorio'),
        barrio: Yup.string().required('Campo obligatorio'),
        direccion: Yup.string().required('Campo obligatorio'),
        via: Yup.string(),
        detallesVia: Yup.string(),
        numeroMas: Yup.string(),
        numeroCelular: Yup.number().typeError('se debe especificar un valor numerico'),
        interior: Yup.string(),
        detallesInterior: Yup.string(),
        genero: Yup.string().required('Campo obligatorio'),
        fechaNacimiento: Yup.string().required('Campo obligatorio'),
        edad: Yup.number().typeError('se debe especificar un valor numerico'),
        acudiente: Yup.string(),
        telefonoAcudiente: Yup.number().typeError('se debe especificar un valor numerico'),
        notas: Yup.string(),
    });
    const formik = useFormik({
        enableReinitialize: true,
        initialValues: {
            codigoUsuario: dependencies.paciente.id,
            estadoPaciente: '',
            fechaActivacion: '',
            correoElectronico: '',
            nombre: '',
            apellido: '',
            identificacion: '',
            telefono: '',
            departamento: '',
            ciudad: '',
            barrio: '',
            direccion: '',
            via: '',
            detallesVia: '',
            numeroMas: '',
            numeroCelular: '',
            interior: '',
            detallesInterior: '',
            genero: '',
            fechaNacimiento: '',
            edad: '',
            acudiente: '',
            telefonoAcudiente: '',
            notas: '',
            file: ''
        },
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            // alert(JSON.stringify(values, null, 2));
            // await postHandler(values);
            setFormValues1(values);
        }
    });

    const requiredFields = getRequired(formSchema);


    /* *~~*~~*~~*~~*~~*~~*~~*~~* local handlers *~~*~~*~~*~~*~~*~~*~~*~~* */
    const customHandleChange = e => {

        //handle formik change
        formik.handleChange(e);
        const form = e.target.form;
        //check if element is required and them count progress
        const count = requiredFields.filter( fieldName => form[fieldName].value != '').length;
        setProgress(count/requiredFields.length * 100);
    }

    const onContinueClicked = e => {
        if(Object.keys(formik.errors) == 0)
            onSubmitCallback(e);
    }


    return(
            <>
                <Dependencies dependencies={dependencies}  dispatchDep={dispatchDep} />
                {
                    dependencies.loading ?
                        <LoadingComponent loadingText="Cargando datos"/>
                    :
                    dependencies.success ?
                        (
                            <form onSubmit={formik.handleSubmit} className="coolscroll primary pr-3 py-3 ">
                                <FormField label="Código de usuario" number="1" name="codigoUsuario" checked={formik.values.codigoUsuario != ''}>
                                    <input className="input cool-input" type="text" name="codigoUsuario" onChange={customHandleChange} value={formik.values.codigoUsuario} readOnly />
                                    {formik.touched.codigoUsuario && formik.errors.codigoUsuario ? <div className="help is-danger"> {formik.errors.codigoUsuario}</div> : null }
                                </FormField>

                                <FormField label="Estado del paciente*" number="2" name="estadoPaciente" checked={formik.values.estadoPaciente != ''}>
                                    <Dropdown options={dependencies.status_list.map(s => s.name)} name="estadoPaciente" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.estadoPaciente}/>
                                    {formik.touched.estadoPaciente && formik.errors.estadoPaciente ? <div className="help is-danger"> {formik.errors.estadoPaciente}</div> : null }
                                </FormField>

                                <FormField label="Fecha de activación*" number="3" name="fechaActivacion" checked={formik.values.fechaActivacion != ''}>
                                    <input className="input cool-input" type="date" name="fechaActivacion" onChange={customHandleChange} value={formik.values.fechaActivacion}/>
                                    {formik.touched.fechaActivacion && formik.errors.fechaActivacion ? <div className="help is-danger"> {formik.errors.fechaActivacion}</div> : null }
                                </FormField>

                                <FormField label="Correo electrónico*" number="4" name="correoElectronico" checked={formik.values.correoElectronico != ''}>
                                    <input className="input cool-input" type="text" name="correoElectronico" onChange={customHandleChange} value={formik.values.correoElectronico}/>
                                    {formik.touched.correoElectronico && formik.errors.correoElectronico ? <div className="help is-danger"> {formik.errors.correoElectronico}</div> : null }
                                </FormField>

                                <FormField2
                                    number1="5" number2="5.1" name1="nombre" name2="apellido" checked1={formik.values.nombre != ''} checked2={formik.values.apellido != ''}
                                    label1="Nombres*"
                                    input1={
                                        <div>
                                            <input className="input cool-input" type="text" name="nombre" onChange={customHandleChange} value={formik.values.nombre}/>
                                            {formik.touched.nombre && formik.errors.nombre ? <div className="help is-danger"> {formik.errors.nombre}</div> : null }
                                        </div>
                                    }
                                    label2="Apellidos*"
                                    input2={
                                        <div>
                                            <input className="input cool-input" type="text" name="apellido" onChange={customHandleChange} value={formik.values.apellido}/>
                                            {formik.touched.apellido && formik.errors.apellido ? <div className="help is-danger"> {formik.errors.apellido}</div> : null }
                                        </div>
                                    }
                                />

                                <FormField label="Identificación*" number="6" name="identificacion" checked={formik.values.identificacion != ''}>
                                    <input className="input cool-input" type="text" name="identificacion" onChange={customHandleChange} value={formik.values.identificacion}/>
                                    {formik.touched.identificacion && formik.errors.identificacion ? <div className="help is-danger"> {formik.errors.identificacion}</div> : null }
                                </FormField>

                                <FormField label="Telefono*" number="7" name="telefono" checked={formik.values.telefono != ''}>
                                    <input className="input cool-input" type="text" name="telefono" onChange={customHandleChange} value={formik.values.telefono}/>
                                    <div class="help">
                                        <div className="level is-mobile">
                                            <div className="level-right has-text-danger">{formik.touched.telefono && formik.errors.telefono ? formik.errors.telefono : ''}</div>
                                            <div className="level-left"><a><u>Agregar telefono</u></a></div>
                                        </div>
                                    </div>
                                </FormField>

                                <FormField2
                                    number1="8" number2="8.1" name1="departamento" name2="ciudad" checked1={formik.values.departamento != ''} checked2={formik.values.ciudad != ''}
                                    label1="Departamento*"
                                    input1={
                                        <div>
                                            <Dropdown options={dependencies.departamentos.map(dprtm => dprtm.name)} name="departamento" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.departamento}/>
                                            {formik.touched.departamento && formik.errors.departamento ? <div className="help is-danger"> {formik.errors.departamento}</div> : null }
                                        </div>
                                    }
                                    label2="Ciudad* "
                                    input2={
                                        <div>
                                            <Dropdown options={dependencies.cities.map(city => city.name)} name="ciudad" arrowColor="has-text-primary"  onChange={customHandleChange}  value={formik.values.ciudad}/>
                                            {formik.touched.ciudad && formik.errors.ciudad ? <div className="help is-danger"> {formik.errors.ciudad}</div> : null }
                                        </div>
                                    }
                                />

                                <FormField2
                                  number1="9" number2="9.1" name1="barrio" name2="direccion" checked1={formik.values.barrio != ''} checked2={formik.values.direccion != ''}
                                  label1="Barrio*"
                                  input1={
                                      <div>
                                          <input className="input cool-input" type="text" name="barrio" onChange={customHandleChange} value={formik.values.barrio}/>
                                          {formik.touched.barrio && formik.errors.barrio ? <div className="help is-danger"> {formik.errors.barrio}</div> : null }
                                      </div>
                                  }
                                  label2="Dirección* "
                                  input2={
                                      <div>
                                          <input className="input cool-input" type="text" name="direccion" onChange={customHandleChange}  value={formik.values.direccion}/>
                                          {formik.touched.direccion && formik.errors.direccion ? <div className="help is-danger"> {formik.errors.direccion}</div> : null }
                                      </div>
                                  }
                              />

                              <FormField2
                                  number1="10" number2="10.1" name1="via" name2="detallesVia" checked1={formik.values.via != ''} checked2={formik.values.detallesVia != ''}
                                  label1="Via"
                                  input1={
                                      <div>
                                          <Dropdown options={["op1", "op2"]} name="via" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.via}/>
                                          {formik.touched.via && formik.errors.via ? <div className="help is-danger"> {formik.errors.via}</div> : null }
                                      </div>
                                  }
                                  label2="Detalles via"
                                  input2={
                                      <div>
                                          <Dropdown options={["op1", "op2"]} name="detallesVia" arrowColor="has-text-primary"  onChange={customHandleChange}  value={formik.values.detallesVia}/>
                                          {formik.touched.detallesVia && formik.errors.detallesVia ? <div className="help is-danger"> {formik.errors.detallesVia}</div> : null }
                                      </div>
                                  }
                              />

                              <FormField2
                                  number1="11" number2="11.1" name1="numeroMas" name2="numeroCelular" checked1={formik.values.numeroMas != ''} checked2={formik.values.numeroCelular != ''}
                                  label1="Numero"
                                  input1={
                                      <div>
                                          <Dropdown options={["op1", "op2"]} name="numeroMas" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.numeroMas}/>
                                          {formik.touched.numeroMas && formik.errors.numeroMas ? <div className="help is-danger"> {formik.errors.numeroMas}</div> : null }
                                      </div>
                                  }
                                  label2=""
                                  input2={
                                      <div>
                                          <input className="input cool-input" type="text" name="numeroCelular" onChange={customHandleChange}  value={formik.values.numeroCelular}/>
                                          {formik.touched.numeroCelular && formik.errors.numeroCelular ? <div className="help is-danger"> {formik.errors.numeroCelular}</div> : null }
                                      </div>
                                  }
                              />

                              <FormField2
                                  number1="12" number2="12.1" name1="interior" name2="detallesInterior" checked1={formik.values.interior != ''} checked2={formik.values.detallesInterior != ''}
                                  label1="Interior"
                                  input1={
                                      <div>
                                          <input className="input cool-input" type="text" name="interior" onChange={customHandleChange} value={formik.values.interior}/>
                                          {formik.touched.interior && formik.errors.interior ? <div className="help is-danger"> {formik.errors.interior}</div> : null }
                                      </div>
                                  }
                                  label2="Detalles interior"
                                  input2={
                                      <div>
                                          <input className="input cool-input" type="text" name="detallesInterior" onChange={customHandleChange} value={formik.values.detallesInterior}/>
                                          <div class="help">
                                              <div className="level is-mobile">
                                                  <div className="level-right has-text-danger">{formik.touched.detallesInterior && formik.errors.detallesInterior ? formik.errors.detallesInterior : ''}</div>
                                                  <div className="level-left"><a><u>Agregar interior</u></a></div>
                                              </div>
                                          </div>
                                      </div>
                                  }
                              />

                              <FormField label="Genero*" number="13" name="genero" checked={formik.values.genero != ''}>
                                  <Dropdown options={["Masculino", "Femenino"]} name="genero" arrowColor="has-text-primary" onChange={customHandleChange} value={formik.values.genero}/>
                                  {formik.touched.genero && formik.errors.genero ? <div className="help is-danger"> {formik.errors.genero}</div> : null }
                              </FormField>

                              <FormField2
                                  number1="14" number2="14.1" name1="fechaNacimiento" name2="edad" checked1={formik.values.fechaNacimiento != ''} checked2={formik.values.edad != ''}
                                  label1="Fecha de nacimiento*"
                                  input1={
                                      <div>
                                          <input className="input cool-input" type="date" name="fechaNacimiento" onChange={customHandleChange} value={formik.values.fechaNacimiento}/>
                                          {formik.touched.fechaNacimiento && formik.errors.fechaNacimiento ? <div className="help is-danger"> {formik.errors.fechaNacimiento}</div> : null }
                                      </div>
                                  }
                                  label2="Edad"
                                  input2={
                                      <div>
                                          <input className="input cool-input" type="text" name="edad" onChange={customHandleChange} value={formik.values.edad}/>
                                          {formik.touched.edad && formik.errors.edad ? <div className="help is-danger"> {formik.errors.edad}</div> : null }
                                      </div>
                                  }
                              />

                              <FormField2
                                  number1="15" number2="15.1" name1="acudiente" name2="telefonoAcudiente" checked1={formik.values.acudiente != ''} checked2={formik.values.telefonoAcudiente != ''}
                                  label1="Acudiente"
                                  input1={
                                      <div>
                                          <input className="input cool-input" type="text" name="acudiente" onChange={customHandleChange} value={formik.values.acudiente}/>
                                          {formik.touched.acudiente && formik.errors.acudiente ? <div className="help is-danger"> {formik.errors.acudiente}</div> : null }
                                      </div>
                                  }
                                  label2="Telefono acudiente"
                                  input2={
                                      <div>
                                          <input className="input cool-input" type="text" name="telefonoAcudiente" onChange={customHandleChange} value={formik.values.telefonoAcudiente}/>
                                          {formik.touched.telefonoAcudiente && formik.errors.telefonoAcudiente ? <div className="help is-danger"> {formik.errors.telefonoAcudiente}</div> : null }
                                      </div>
                                  }
                              />

                              <FormField label="Notas" number="16" name="notas" checked={formik.values.notas != ''}>
                                  <input className="input cool-input" type="text" name="notas" onChange={customHandleChange} value={formik.values.notas} />
                                  {formik.touched.notas && formik.errors.notas ? <div className="help is-danger"> {formik.errors.notas}</div> : null }
                              </FormField>

                              <FormField label="" number="17" name="file" checked={formik.values.file != ''} noline>
                                  <button className="button is-rounded is-horange">Seleccionar archivo</button>
                              </FormField>


                              <br/>

                              <div className="has-text-right has-text-centered-mobile">
                                    <span className="has-text-danger has-text-right">{Object.keys(formik.errors).length > 0 ? 'Por favor revise el formulario' : ' '}</span>
                              </div>

                              <br/>

                              <div className="field has-text-right has-text-centered-mobile">

                                    <div className="control">
                                        <button
                                            className="button is-horange"
                                            type="submit"
                                            data-tab="tab2"
                                            style={{width: '150px'}}
                                            disabled={false}
                                            onClick={onContinueClicked}
                                        >
                                            Continuar
                                        </button>
                                    </div>
                                </div>

                            </form>
                        )
                    :
                        <div>Error cargando las dependencias del formulario</div>

                }
            </>
    );
}

const mapStateToPros = state => ({
    newsReducer: state.newsReducer
});

export default connect(
    mapStateToPros,
    null
)(Form);
