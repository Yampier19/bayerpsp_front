import {useReducer} from 'react';

import FilterBar from 'components/base/filter-bar';

import {serverURL} from 'secrets';
import {Link} from 'react-router-dom';

import Dependencies from './dependencies';
import {dependencies_initState, depReducer} from './depReducer';
import LoadingComponent from 'components/base/loading-component';
import Table from './table'

import './main.scss';

const Tracking = props => {

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>

            {/* header */}
            <section className="pb-3 mt-5" style={{marginBottom: 50}}>
                <h1 className="title has-text-hblue">Seguimiento</h1>
            </section>

            {/* DataTable */}
            <section className="is-flex-grow-1" style={{minHeight: '500px', overflowY: 'hidden'}}>
                <div className="coolscroll hblue" style={{overflowX: 'scroll', height:"100%", width: "100%"}}>
                    <Table />
                </div>
            </section>

            <br/>

        </div>
    );
}

export default Tracking;
