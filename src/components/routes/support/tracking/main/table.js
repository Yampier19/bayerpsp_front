import { useReducer } from "react";

import { Link } from "react-router-dom";

import DataTable from "components/base/datatable";

//IMAGENES PAISES
import IMGColombia from "./../../../../../utils/img/flags/colombia.png";
import IMGPeru from "./../../../../../utils/img/flags/peru.png";
import IMGEcuador from "./../../../../../utils/img/flags/ecuador.png";

const Table = (props) => {
  const columns = [
    { name: "PAP", label: "PAP", options: { filter: true, sort: true } },
    {
      name: "CODUSUARIO",
      label: "COD. USUARIO",
      options: { filter: true, sort: true },
    },
    {
      name: "IDTRAT",
      label: "ID TRAT.",
      options: { filter: true, sort: true },
    },
    {
      name: "PRODUCTO",
      label: "PRODUCTO",
      options: { filter: true, sort: true },
    },
    {
      name: "GESTION",
      label: "GESTIÓN",
      options: { filter: true, sort: true },
    },
    {
      name: "PROXCONTACTO",
      label: "PRÓX. CONTACTO",
      options: { filter: true, sort: true },
    },
    {
      name: "RESPONSABLE",
      label: "RESPONSABLE",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => (
          <div
            className="column has-no-border py-0 pr-0 p-2"
            style={{ width: "100%" }}
          >
            <div
              className="columns is-mobile my-0"
              style={{ height: "100%", background: "" }}
            >
              <div className="row">
                <div className="col">
                  <p className="text-center">Nombres y Apellidos del asesor</p>
                </div>
                <div
                  className="col-2 p-0"
                  style={{ backgroundColor: "#6992D6" }}
                >
                  <div className="row-reverse ">
                    <div className="col d-flex justify-content-center py-2">
                      <img
                        src={IMGColombia}
                        alt="img_pro
                                file"
                        style={{ width: "50%" }}
                      />
                    </div>
                    <div className="col bg-danger d-flex justify-content-center mt-5 pb-1">
                      <label className="text-white">G</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ),
      },
    },
    {
      name: "ACCIONES",
      label: "ACCIONES",
      options: {
        filter: true,
        sort: true,
        customBodyRender: (value) => (
          <div
            className="column has-no-border py-0 pr-0 p-2"
            style={{ width: "100%" }}
          >
            <div
              className="columns is-mobile my-0"
              style={{ height: "100%", background: "" }}
            >
              <div className="row">
                <div className="col ml-3">
                  <Link className="btn" to="tracking/update/0">
                    <i class="fas fa-edit"></i>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        ),
      },
    },
  ];

  const data = [
    {
      PAP: "PAP08581",
      CODUSUARIO: "123456",
      IDTRAT: "123456",
      PRODUCTO: "BETAFERON CMBP X 15 VPFS (3750 MCG) MM",
      GESTION: "2021-26-11",
      PROXCONTACTO: "2021-26-11",
      RESPONSABLE: "Nombres y Apellidos del asesor",
      ACCIONES: "Ninguno",
    },
  ];

  return (
    <div className="columns is-mobile">
      <div
        className="column is-1 is-hidden-tablet has-text-centered"
        style={{ minWidth: "10px" }}
      ></div>
      <div className="column">
        <div className="columns is-mobile">
          <div
            className="column is-1 is-hidden-tablet has-text-centered"
            style={{ minWidth: "10px" }}
          ></div>
          <div className="column">
            <DataTable
              data={data}
              columns={columns}
              title={"Listado de seguimiento"}
              color="#9CC9A3"
            />
          </div>
          <div
            className="column is-1 is-hidden-mobile"
            style={{ maxWidth: "10px" }}
          ></div>
        </div>
      </div>
      <div
        className="column is-1 is-hidden-mobile"
        style={{ maxWidth: "10px" }}
      ></div>
    </div>
  );
};

export default Table;
