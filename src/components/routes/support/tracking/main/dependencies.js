import {useEffect} from 'react';

import {endpoint} from 'secrets';

import {connect} from 'react-redux';
import {tracking_get_request} from 'redux/actions/trackingActions';

const Dependencies = props => {

    const { dispatchDep } = props;
    const {triggerReload} = props || null;

    const trackingReducer = props.trackingReducer;


    const load_dependencies = async () => {

        const dep = [
            {
                url: `${endpoint}/api/patient`,
                req_name: 'GET_ALL_PATIENTS',
                req_body: null
            }
        ];

        dep.map( async d => {
            await props.tracking_get_request(d.url, d.req_name, d.req_body);
        });
    }

    useEffect(
        () => {
            load_dependencies();
        }, []
    );

    useEffect(
        () => {
            load_dependencies();
        }, [triggerReload]
    );

    useEffect(
        () => {
            // console.log(props.trackingReducer['GET_ALL_PATIENTS']);
            const data = trackingReducer['GET_ALL_PATIENTS'].data;

            let _data = {};
            if(data != null && trackingReducer['GET_ALL_PATIENTS'].success){
                _data = {
                    patients: data.map(p => ({
                        id: p.id,
                        treatments: p.treatments.map(t => ({
                            id: t.id,
                            product: t.product,
                            dateNextCall: t.date_next_call,
                            gestion: t.last_claim_date,
                            doctor: t.doctor
                        }))
                    }))
                }

                dispatchDep({
                    type: 'set_patients',
                    payload: _data.patients
                });
            }

        }, [trackingReducer['GET_ALL_PATIENTS']]
    );

    useEffect(
        () => {

            const dep1 = trackingReducer['GET_ALL_PATIENTS'];

            dispatchDep({
                type: 'req_status',
                loading: dep1.loading,
                success: dep1.success,
                error: dep1.error
            });

        }, [trackingReducer['GET_ALL_PATIENTS']]
    );

    return null;
}

const mapStateToPros = state => ({
    trackingReducer: state.trackingReducer
});

export default connect(
    mapStateToPros,
    {
        tracking_get_request
    }
)(Dependencies);
