import {useEffect} from 'react';

import {endpoint} from 'secrets';

import {connect} from 'react-redux';
import {tracking_get_request} from 'redux/actions/trackingActions';

const Dependencies = props => {

    const {patientId} = props.inputData;

    const {dependencies, dispatchDep} = props;
    const {trackingReducer} = props;

    const load_dependencies = async () => {

        const dep = [
            {
                url: `${endpoint}/api/patient/${patientId}`,
                req_name: 'GET_SPECIFIC_PATIENT',
                req_body: null
            },{
                url: `${endpoint}/api/location/country`,
                req_name: 'GET_COUNTRIES',
                req_body: null
            },{
                url: `${endpoint}/api/location/department`,
                req_name: 'GET_DEPARTAMETS',
                req_body: null
            },{
                url: `${endpoint}/api/location/city`,
                req_name: 'GET_CITIES',
                req_body: null
            },{
                url: `${endpoint}/api/operator`,
                req_name: 'GET_LOGISTIC_OPERATOR',
                req_body: null
            },{
                url: `${endpoint}/api/insurance`,
                req_name: 'GET_DEPARTAMETS',
                req_body: null
            }
        ];

        dep.map( async d => {
            await props.tracking_get_request(d.url, d.req_name, d.req_body);
        });
    }

    useEffect(
        () => {
            load_dependencies();
        }, []
    );

    useEffect(
        () => {
            const data = trackingReducer['GET_SPECIFIC_PATIENT'].data;
            console.log(data);
            let _data = {
                paciente: {}
            };
            if(data != null && trackingReducer['GET_SPECIFIC_PATIENT'].success){
                _data = {
                    paciente: {
                        codigoUsuario: data.id,
                        estadoPaciente: data.patient_statu,
                        fechaActivacion: data.date_active,
                        solicitudCambioEstadoPaciente: '',
                        fechaRetiro: '',
                        motivoRetiro: '',
                        observacionesMotivoRetiro: '',
                        nombre: data.name,
                        apellidos: data.last_name,
                        tipoDeDocumento: '',
                        numeroIdentificacion: data.document,
                        telefono: data.phones[0].number,
                        correoElectronico: data.email,
                        departamento: data.depar,
                        ciudad: data.city,
                        barrio: data.neighborhood,
                        direccion: data.address,
                        fechaNacimiento: data.date_birth,
                        edad: data.age,
                        acudiente: data.guardian,
                        telefonoAcudiente: data.guardian_phone,
                        clasificacionPatolofica: data.pathological_classification,
                        fechaInicioTerapia: data.therapy_start_date,
                        tratamientos: data.treatments.map((t, i) => ({
                            programacionVisitaInicial: t.treatmentFollows[i].initial_visit_schedule,
                            visitaInicialEfectiva: t.treatmentFollows[i].initial_visit_schedule,
                            reclamo: t.treatmentFollows[i].claim,
                            activeParaCambio: '',
                            medicamentoHasta: t.treatmentFollows[i].medication_date,
                            seLogroComunicacion: t.treatmentFollows[i].comunication,
                            motivoComunicacion: t.treatmentFollows[i].reason_communication,
                            medioDeContacto: t.treatmentFollows[i].contact_medium,
                            tipoLlamada: t.treatmentFollows[i].call_type,
                            motivoDeNoComunicacion: t.treatmentFollows[i].reason_not_communication,
                            numeroIntentos: t.treatmentFollows[i].number_attemps,
                            asegurador: t.insurance,
                            ipsQueAtiende: t.ips,
                            medico: t.medic,
                            operadorLogistico: t.logistic_operator,
                            puntoEntrega: t.puntoEntrega,
                            ciudadEntrega: t.city,
                            numeroAutorizacion: '',
                            estadoFarmacia: '',
                            dificultadAcceso: '',
                            tipoDificultad: '',
                            autor: '',
                            product: t.product,
                            generaSolicitud: '',
                            eventoAdverso: '',
                            fechaProximaLlamada: t.date_next_call,
                            motivoProximaLlamada: '',
                            observacionesProximaLlamada: '',
                            consecutivo: '',
                            pacienteEsPartePaap: t.patient_part_PAAP,
                            numeroCajasPorUnidad: t.number_box,
                            medicamento: '',
                            dosisTratamiento: t.dose,
                            numeroLotesDeLosDispositivos: '',
                            estadoPaciente: t.patient_statu,
                            envios: '',
                            tipoEnvio: t.shipping_type,
                            descripcionDeComunicacion: '',
                            notas: '',
                            archivo: ''
                        }))
                    }
                }
            }

            // console.log(_data);
            // console.log(_data);
            dispatchDep({
                type: 'set_paciente',
                payload: _data.paciente
            });

        }, [trackingReducer['GET_SPECIFIC_PATIENT']]
    );

    useEffect(
        () => {


            const dep1 = trackingReducer['GET_SPECIFIC_PATIENT'];

            dispatchDep({
                type: 'req_status',
                loading: dep1.loading,
                success: dep1.success,
                error: dep1.error
            });

        }, [trackingReducer['GET_SPECIFIC_PATIENT']]
    );

    return null;
}

const mapStateToPros = state => ({
    trackingReducer: state.trackingReducer
});

export default connect(
    mapStateToPros,
    {
        tracking_get_request
    }
)(Dependencies);
