import React from 'react';
export const formShape = {

}
export const stepShape = {
  current: 1,
  percent: 0
}
export const fetchedDataShape = {}

export const FormUpdateTrackingContext = React.createContext({
  form: formShape,
  setForm: () => { },
  step: stepShape,
  setStep: () => { },
  country: 1,
  setCountry: () => { },
  setFetchedData: () => { },
  fetchedData: fetchedDataShape,
})