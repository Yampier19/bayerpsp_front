import {useState, useEffect, useReducer} from 'react';

import ProgressIcon from 'components/base/progress-icon';
import ProgressBar from 'components/base/progress-bar';

import {serverURL} from 'secrets';
import {Link, useParams} from 'react-router-dom';

import {tabClicked} from 'components/commons/tabs/tabClicked';

import Form1 from './form1';
import Form2 from './form2';
import Form3 from './form3';
import ApoyoD from './apoyoD/';
import GestionApoyoD from './apoyoD/table';
import GestionEdit from './apoyoD/edit';
import HistorialR from './historialR/table';
import Pedidos from './pedidos/table';

import {dependencies_initState, depReducer} from './depReducer';
import LoadingComponent from 'components/base/loading-component';
import Dependencies from './dependencies';


import styles from './trackingUpdate.scss';

const TrackingUpdatePatient = props => {

    {/* 
    const [dependencies, dispatchDep] = useReducer(depReducer, dependencies_initState);
    */}
    const [progress, setProgress] = useState(0);
    const [currentTab, setCurrentTab] = useState(1);

{/*
    const patientId = useParams().id;
    const [currentTreatment, setCurrentTreatment] = useState( 0 );
    
    const onTreatmentBtnClicked = e => {
        const target = e.currentTarget;
        const currentActive = document.querySelector('.treatment-btn.is-active');
        currentActive.classList.toggle('is-active');
        target.classList.toggle('is-active');
    }

*/}

    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>
                {/*
                    <Dependencies dependencies={dependencies} dispatchDep={dispatchDep} inputData={{patientId}}/>
                */}

                {/* header */}
                <section className="section p-0">
                    <h1 className="subtitle is-3 mb-3"><strong className="has-text-hgreen">SEGUIMIENTO</strong></h1>
                    <h2 className="subtitle has-text-hgreen">EDITAR</h2>
                </section>

                <hr/>

                {/* tabs */}
                <section className="section p-0 m-0">
                    <div className="columns is-vcentered">

                        <div className="column">
                            <div className="tabs is-boxed trackingTabs">
                                <ul>
                                    <li className={`formTab is-active ${styles.tabs}`} onClick={e => {tabClicked(e); setCurrentTab(1)}} data-tab="tab1">
                                        <a >
                                            <ProgressIcon className="has-background-hgreen has-text-white" value={90} icon={<i className="fas fa-check"></i>}></ProgressIcon> &nbsp;
                                            Paciente
                                        </a>
                                    </li>
                                    <li className={`formTab ${styles.tabs}`} onClick={e => {tabClicked(e); setCurrentTab(2)}} data-tab="tab2">
                                        <a>
                                            <ProgressIcon className="has-background-hgreen has-text-white" value={20} icon={<i className="fas fa-pills"></i>}></ProgressIcon> &nbsp;
                                            Tratamiento
                                        </a>
                                    </li>
                                    <li className={`formTab ${styles.tabs}`} onClick={e => {tabClicked(e); setCurrentTab(3)}}  data-tab="tab3">
                                        <a>
                                            <ProgressIcon className="has-background-hgreen has-text-white" value={progress} icon={<i className="far fa-comment-dots"></i>}></ProgressIcon> &nbsp;
                                            Comunicaciones
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div className="column is-3">

                            <ProgressBar
                                className="has-background-hgreen"
                                value={80}
                            ></ProgressBar>
                        </div>
                    </div>
                </section>

                <section className="my-5">
                    <button className="button is-hgreen has-text-white is-rounded" onClick={e => {tabClicked(e); setCurrentTab(6)}} data-tab="tab6">Historico reclamaciones</button>&nbsp;
                    <button className="button is-hgreen has-text-white is-rounded" onClick={e => {tabClicked(e); setCurrentTab(7)}} data-tab="tab7">Mis pedidos</button>&nbsp;
                    <button className="button is-hgreen has-text-white is-rounded" onClick={e => {tabClicked(e); setCurrentTab(7)}} data-tab="tab7">Medicamento Sin Valor Comercial</button>&nbsp;
                   

                </section>

               



                {/* forms */}
                <section className="section px-0 is-flex-grow-1 py-4 coolscroll" style={{height: '1px', overflow: 'hidden', minHeight: '500px'}}>
                    {/*
                        dependencies.loading ?
                            <LoadingComponent loadingText="Cargando datos..."/>
                        :
                    dependencies.success ?  */}
                            <>
                                <div id="tab1" className="tab is-active" style={{height: '100%'}}>
                                    <div className="coolscroll hgreen" style={{height: '100%', overflow: 'auto'}}>
                                        <Form1  />
                                    </div>
                                </div>

                                <div id="tab2" className="tab"  style={{height: '100%'}}>
                                    <div className="coolscroll hgreen" style={{height: '100%', overflow: 'auto'}}>
                                        <Form2 />
                                    </div>
                                </div>

                                <div id="tab3" className="tab"  style={{height: '100%'}}>
                                    <div className="coolscroll hgreen" style={{height: '100%', overflowY: 'auto'}}>
                                        {/* 
                                            [1,2,3,4,5,6,7,8,9,10].map((el, i) =>  */}
                                                <form className="has-background-dangera" onSubmit={null}>

                                                    <Form3  />
                                                    <hr/>
                                                </form>

                                                 {/* 
                                            )
                                        }  */}
                                    </div>

                                </div>

                                <div id="tab6" className="tab"  style={{height: '100%'}}>
                                    <div className="coolscroll hgreen" style={{height: '100%', overflow: 'auto'}}>
                                    <HistorialR  />
                                    </div>
                                </div>

                                <div id="tab7" className="tab"  style={{height: '100%'}}>
                                    <div className="coolscroll hgreen" style={{height: '100%', overflow: 'auto'}}>
                                    <Pedidos />
                                    </div>
                                </div>

                                <div id="tab4" className="tab"  style={{height: '100%'}}>
                                    <div className="coolscroll hgreen" style={{height: '100%', overflow: 'auto'}}>
                                    <ApoyoD  />
                                    </div>
                                </div>

                                <div id="tab5" className="tab"  style={{height: '100%'}}>
                                    <div className="coolscroll hgreen" style={{height: '100%', overflow: 'auto'}}>
                                    <GestionApoyoD  />
                                    </div>
                                </div>
                            </>
                   {/*     :
                        dependencies.error ?
                            <div>Error cargando las dependencias del formulario...</div>
                        :
                            null

                    }
                */}


                </section>


                 
                     



        </div>
    );
}

export default TrackingUpdatePatient;
