import React from 'react'

import Radio from 'components/form/radio';
import './formfield.scss'

function checkForm(props) {
    return (
        <div>
               <div className="field23 mb-6"  >
                <div className="columns " style={{minHeight: '100px'}}>



                    <div className="column">
                        <div className="columns  is-mobile">

                            <div className="column is-1" style={{width: '50px'}}>
                                <div className="control">
                                    <input className="def_checkbox" type="checkbox" name={`check_${props.name1}`} checked={props.checked1} readOnly/>
                                    <div className={`newcheck3 d-flex justify-content-center align-items-center ${props.noline ? 'no-line-desktop' : ''}`}>
                                        <strong className="num">2</strong>
                                    </div>
                                </div>
                            </div>

                            <div className="column">
                                <div className="columns ">
                                    <div className="column is-3">
                                        <label className="label">Seleccione los exámenes a solicitar</label>
                                    </div>
                                    <div className="column">

                                        <div className="row">
                                            <div className="col mb-3">
                                                <label class="radio">
                                                    <input type="radio" name="prueba" value="Si"/>&nbsp;
                                                </label> &nbsp;&nbsp; Gamagrafia ósea total
                                            </div>
                                            <div className="col mb-3">
                                                <div className="row">
                                                    <div className="col">
                                                        <label>Valor del examen</label>
                                                    </div>
                                                    <div className="col">
                                                        <input type="text" name="prueba" className="input cool-input"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div className="row">
                                            <div className="col mb-3">
                                                <label class="radio">
                                                    <input type="radio" name="prueba" value="Si"/>&nbsp;
                                                </label> &nbsp;&nbsp;Tomografía Axial Computarizada con Contraste (Toraco-Abdominal)
                                            </div>
                                            <div className="col mb-3">
                                                <div className="row">
                                                    <div className="col">
                                                        <label>Valor del examen</label>
                                                    </div>
                                                    <div className="col">
                                                        <input type="text" name="prueba" className="input cool-input"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col mb-3">
                                                <label class="radio">
                                                    <input type="radio" name="prueba" value="Si"/>&nbsp;
                                                </label> &nbsp;&nbsp; Cuadro Hemático
                                            </div>
                                            <div className="col mb-3">
                                                <div className="row">
                                                    <div className="col">
                                                        <label>Valor del examen</label>
                                                    </div>
                                                    <div className="col">
                                                        <input type="text" name="prueba" className="input cool-input"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col mb-3">
                                                <label class="radio">
                                                    <input type="radio" name="prueba" value="Si"/>&nbsp;
                                                </label> &nbsp;&nbsp; Fosfatasa Alcalina
                                            </div>
                                            <div className="col mb-3">
                                                <div className="row">
                                                    <div className="col">
                                                        <label>Valor del examen</label>
                                                    </div>
                                                    <div className="col">
                                                        <input type="text" name="prueba" className="input cool-input"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col mb-3">
                                                <label class="radio">
                                                    <input type="radio" name="prueba" value="Si"/>&nbsp;
                                                </label> &nbsp;&nbsp; Creatinina
                                            </div>
                                            <div className="col mb-3">
                                                <div className="row">
                                                    <div className="col">
                                                        <label>Valor del examen</label>
                                                    </div>
                                                    <div className="col">
                                                        <input type="text" name="prueba" className="input cool-input"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="col mb-3">
                                                <label class="radio">
                                                    <input type="radio" name="prueba" value="Si"/>&nbsp;
                                                </label> &nbsp;&nbsp; PSA
                                            </div>
                                            <div className="col mb-3">
                                                <div className="row">
                                                    <div className="col">
                                                        <label>Valor del examen</label>
                                                    </div>
                                                    <div className="col">
                                                        <input type="text" name="prueba" className="input cool-input"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default checkForm
