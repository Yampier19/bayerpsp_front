import {useState} from 'react'

import FormField from 'components/form/formfield';
import FormField2 from 'components/form/formfield2';
import Dropdown from 'components/form/dropdown';
import Radio from 'components/form/radio';

import * as Yup from 'yup';
import {useFormik} from 'formik';

import CheckForm from './checkForm'


import {tabClicked} from 'components/commons/tabs/tabClicked';

const Form = props => {

    const [currentTab, setCurrentTab] = useState(1);
    {/* 
   
    const {paciente} = props;

    const formSchema = Yup.object().shape({
        codigoUsuario: Yup.number().typeError('se debe especificar un valor numerico'),
        estadoPaciente: Yup.string().required('Este campo es obligatorio'),
        fechaActivacion: Yup.string().required('Este campo es obligatorio'),
        solicitudCambioEstadoPaciente: Yup.string(),
        fechaRetiro: Yup.string(),
        motivoRetiro: Yup.string(),
        observacionesMotivoRetiro: Yup.string(),
        nombre: Yup.string().required('Este campo es obligatorio'),
        apellidos: Yup.string().required('Este campo es obligatorio'),
        tipoDeDocumento: Yup.number().typeError('se debe especificar un valor numerico').required('Este campo es obligatorio'),
        numeroIdentificacion: Yup.number().typeError('se debe especificar un valor numerico').required('Este campo es obligatorio'),
        telefono: Yup.number().typeError('se debe especificar un valor numerico').required('Este campo es obligatorio'),
        correoElectronico: Yup.string().required('Campo obligatorio').email('email invalido'),
        departamento:  Yup.string().required('Este campo es obligatorio'),
        ciudad:  Yup.string().required('Este campo es obligatorio'),
        barrio:  Yup.string().required('Este campo es obligatorio'),
        direccion:  Yup.string().required('Este campo es obligatorio'),
        fechaNacimiento: Yup.string().required('Este campo es obligatorio'),
        edad: Yup.number().typeError('se debe especificar un valor numerico'),
        acudiente: Yup.string(),
        telefonoAcudiente:  Yup.number().typeError('se debe especificar un valor numerico'),
        clasificacionPatolofica: Yup.string().required('Este campo es obligatorio'),
        fechaInicioTerapia: Yup.string().required('Este campo es obligatorio'),
    });

    const formik = useFormik({
        enableReinitialize: true,
        initialValues:  {
            codigoUsuario: paciente.codigoUsuario,
            estadoPaciente: paciente.estadoPaciente,
            fechaActivacion: paciente.fechaActivacion,
            solicitudCambioEstadoPaciente: paciente.olicitudCambioEstadoPaciente,
            fechaRetiro: paciente.fechaRetiro,
            motivoRetiro: paciente.motivoRetiro,
            observacionesMotivoRetiro: paciente.observacionesMotivoRetiro,
            nombre: paciente.nombre,
            apellidos: paciente.apellidos,
            tipoDeDocumento: paciente.tipoDeDocumento,
            numeroIdentificacion: paciente.numeroIdentificacion,
            telefono: paciente.telefono,
            correoElectronico: paciente.correoElectronico,
            departamento: paciente.departamento,
            ciudad: paciente.ciudad,
            barrio: paciente.barrio,
            direccion: paciente.direccion,
            fechaNacimiento: paciente.fechaNacimiento,
            edad: paciente.edad,
            acudiente: paciente.acudiente,
            telefonoAcudiente: paciente.telefonoAcudiente,
            clasificacionPatolofica: paciente.clasificacionPatolofica,
            fechaInicioTerapia: paciente.fechaInicioTerapia
        },
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            alert(JSON.stringify(values, null, 2));
            // const {_icons} = props.modalReducer;
            // props.set_modal_on( new InfoModal('La EPS ha sido creada éxitosamente', _icons.CHECK_PRIMARY) );
        }


   
    });
*/}
    return(
        <form id="form"  className="pr-3 py-3">
              <FormField2
                number1="1" number2="1.1" name1="fechaActivacion" name2="solicitudCambioEstadoPaciente"
               
                label1="PAP" label2="Terapia"
                input1={
                    <div>
                        <input className="input cool-input" type="text" name="fechaActivacion" />
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="text" name="fechaActivacion" />
                    </div>
                }
            />

            <CheckForm />

            <FormField2
                number1="3" number2="3.1" name1="fechaActivacion" name2="solicitudCambioEstadoPaciente"
               
                label1="Cantidad de examenes" label2="Número de voucher"
                input1={
                    <div>
                        <input className="input cool-input" type="text" name="fechaActivacion" />
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="text" name="fechaActivacion" />
                    </div>
                }
            />

            <FormField2
                number1="4" number2="4.1" name1="fechaActivacion" name2="solicitudCambioEstadoPaciente"
               
                label1="Centro médico diagnostico" label2="Gestión"
                input1={
                    <div>
                        <input className="input cool-input" type="text" name="fechaActivacion" />
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="text" name="fechaActivacion" />
                    </div>
                }
            />

            <FormField label="Adjuntar archivo" number="5" name="numeroAutorizacion" >
                <a href="javascript:;" className="button is-rounded is-primary">
                Seleccionar archivo
                </a>
            </FormField>


            <br/>

            <div className="field">
                <div className="control has-text-right has-text-centered-mobile">
                    <a href="javascript:;" data-tab="tab2"
                        className="button is-hblue"
                        style={{width: '250px'}}
                    >
                        GUARDAR INFORMACIÓN
                    </a>
                </div>
            </div>

        </form>

    );
}

export default Form;
