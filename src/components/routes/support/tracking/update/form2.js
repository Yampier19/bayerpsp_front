import { useState } from 'react';

import FormField from 'components/form/formfield';
import FormField2 from 'components/form/formfield2';
import Dropdown from 'components/form/dropdown';
import Radio from 'components/form/radio';
import ModalTipoEnvio from 'components/base/modal-material/modal_tracking'

import * as Yup from 'yup';
import {useFormik} from 'formik';


import {tabClicked} from 'components/commons/tabs/tabClicked';

import './formfield.scss';
import Reclamo from './reclamo';
const Form = props => {

    const [currentTab, setCurrentTab] = useState(1);
{/*
    const {tratamiento} = props;
    // console.log(tratamiento);

    const formSchema = Yup.object().shape({
        programacionVisitaInicial:  Yup.string(),
        visitaInicialEfectiva:  Yup.string().required('Este campo es obligatorio'),
        reclamo: Yup.string().required('Este campo es obligatorio'),
        activeParaCambio:  Yup.string(),
        medicamentoHasta: Yup.string().required('Este campo es obligatorio'),
        seLogroComunicacion: Yup.string().required('Este campo es obligatorio'),
        motivoComunicacion: Yup.string(),
        medioDeContacto: Yup.string().required('Este campo es obligatorio'),
        tipoLlamada: Yup.string().required('Este campo es obligatorio'),
        motivoDeNoComunicacion: Yup.string(),
        numeroIntentos: Yup.number().typeError('se debe especificar un valor numerico').required('Este campo es obligatorio'),
        asegurador: Yup.string().required('Este campo es obligatorio'),
        ipsQueAtiende: Yup.string().required('Este campo es obligatorio'),
        medico: Yup.string().required('Este campo es obligatorio'),
        operadorLogistico: Yup.string().required('Este campo es obligatorio'),
        puntoEntrega:  Yup.string(),
        ciudadEntrega: Yup.string().required('Este campo es obligatorio'),
        numeroAutorizacion: Yup.number().typeError('se debe especificar un valor numerico').required('Este campo es obligatorio'),
        estadoFarmacia: Yup.string(),
        dificultadAcceso: Yup.string(),
        tipoDificultad: Yup.string(),
        autor: Yup.string(),
        generaSolicitud:  Yup.string().required('Este campo es obligatorio'),
        eventoAdverso:  Yup.string().required('Este campo es obligatorio'),
        fechaProximaLlamada:  Yup.string().required('Este campo es obligatorio'),
        motivoProximaLlamada:  Yup.string().required('Este campo es obligatorio'),
        observacionesProximaLlamada: Yup.string(),
        consecutivo: Yup.string(),
        pacienteEsPartePaap: Yup.string().required('Este campo es obligatorio'),
        numeroCajasPorUnidad1: Yup.string(),
        numeroCajasPorUnidad2: Yup.string(),
        medicamento: Yup.string(),
        dosisTratamiento: Yup.string().required('Este campo es obligatorio'),
        numeroLotesDeLosDispositivos: Yup.number().typeError('se debe especificar un valor numerico').required('Este campo es obligatorio'),
        estadoPaciente: Yup.string(),
        envios: Yup.string(),
        tipoEnvio: Yup.string(),
        descripcionDeComunicacion: Yup.string(),
        notas: Yup.string(),
        archivo: Yup.string()
    });


    
    const formik = useFormik({
        enableReinitialize: true,
        initialValues: tratamiento,
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            alert(JSON.stringify(values, null, 2));
            // const {_icons} = props.modalReducer;
            // props.set_modal_on( new InfoModal('La EPS ha sido creada éxitosamente', _icons.CHECK_PRIMARY) );
        }
    });

*/}

    return(
        <form id="form"  className="pr-3 py-3">
             <FormField2
                number1="1" number2="1.1" name1="motivoDeNoComunicacion" name2="numeroIntentos"
                label1="Se logro la comunicación*" label2="Motivo de la comunicación*"
                input1={
                    <div>
                        <Dropdown name="motivoDeNoComunicacion"  options={['asd']} arrowColor="has-text-hgreen"/>
                    </div>
                }

                input2={
                    <div>
                        <Dropdown name="numeroAutorizacion"  options={['asd']} arrowColor="has-text-hgreen"/>
                    </div>
                }
            />
            
            <FormField2
                number1="2" number2="2.1" name1="motivoDeNoComunicacion" name2="numeroIntentos"
                label1="Medio de contacto*" label2="Tipo de llamada*"
                input1={
                    <div>
                        <Dropdown name="motivoDeNoComunicacion"  options={['asd']} arrowColor="has-text-hgreen"/>
                    </div>
                }

                input2={
                    <div>
                        <Dropdown name="numeroAutorizacion"  options={['asd']} arrowColor="has-text-hgreen"/>
                    </div>
                }
            />

            <FormField2
                number1="3" number2="3.1" name1="motivoDeNoComunicacion" name2="numeroIntentos"
                label1="Motivo de no comunicación" label2="Número de intentos*"
                input1={
                    <div>
                        <Dropdown name="motivoDeNoComunicacion"  options={['asd']} arrowColor="has-text-hgreen"/>
                    </div>
                }

                input2={
                    <div>
                        <Dropdown name="numeroAutorizacion"  options={['asd']} arrowColor="has-text-hgreen"/>
                    </div>
                }
            />

            <FormField label="Asegurador*" number="4" name="medicamentoHasta" >
                <Dropdown name="numeroAutorizacion"  options={['asd']} arrowColor="has-text-hgreen"/>
            </FormField>


            <FormField2
                number1="5" number2="5.1" name1="motivoDeNoComunicacion" name2="numeroIntentos"
                label1="Médico*" label2="Examen*"
                input1={
                    <div>
                        <Dropdown name="motivoDeNoComunicacion"  options={['asd']} arrowColor="has-text-hgreen"/>
                    </div>
                }

                input2={
                    <div>
                        <Dropdown name="numeroAutorizacion"  options={['asd']} arrowColor="has-text-hgreen"/>
                    </div>
                }
            />

            <FormField2
                number1="6" number2="6.1" name1="motivoDeNoComunicacion" name2="numeroIntentos"
                label1="Centro diagnóstico" label2="Fecha Próxima llamada"
                input1={
                    <div>
                        <input className="input cool-input" type="number" name="medicamentoHasta"  />
                    </div>
                }

                input2={
                    <div>
                           <input className="input cool-input" type="date" name="medicamentoHasta"  />
                    </div>
                }
            />

            <FormField2
                number1="7" number2="7.1" name1="motivoDeNoComunicacion" name2="numeroIntentos"
                label1="Motivo de llamada" label2="Autor"
                input1={
                    <div>
                        <Dropdown name="motivoDeNoComunicacion"  options={['asd']} arrowColor="has-text-hgreen"/>
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="text" name="medicamentoHasta"  disabled />
                    </div>
                }
            />

            <FormField label="Descripción de comunicación" number="8" name="medicamentoHasta" >
                <input className="input cool-input" type="text" name="medicamentoHasta"/>
            </FormField>

            <FormField label="Notas" number="9" name="medicamentoHasta" >
                <input className="input cool-input" type="text" name="medicamentoHasta"/>
            </FormField>

            <FormField label="Seleccionar archivo" number="10" name="medicamentoHasta" >
                <input className="input cool-input" type="file" name="medicamentoHasta"/>
            </FormField>
         
           

            <br/>

            <div className="field">
                <div className="control has-text-right has-text-centered-mobile">
                    <a onClick={e => {tabClicked(e); setCurrentTab(3)}} data-tab="tab3"
                        className="button is-hgreen"
                        style={{width: '150px'}}
                    >
                        CONTINUAR
                    </a>
                </div>
            </div>

        </form>
    );
}

export default Form;
