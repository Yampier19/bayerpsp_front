import {Link, useParams} from 'react-router-dom';

import {tabClicked} from 'components/commons/tabs/tabClicked';
import DataTable from 'components/base/datatable'
const Table = props => {

    const columns = [
        { name: "mes", label: "MES", options: { filter: true, sort: true } },
        { name: "reclamacion", label: "RECLAMACIÓN", options: { filter: true, sort: true } },
        { name: "fechareclamacion", label: "FECHA DE RECLAMACIÓN", options: { filter: true, sort: true } },
        { name: "motivoreclamacion", label: "MOTIVO NO RECLAMACIÓN", options: { filter: true, sort: true } },
      ];
    
      const data = [
        {
            mes: "JULIO",
            reclamacion: "SI / NO",
            fechareclamacion: "02-12-2021",
            motivoreclamacion: "ABIERTO A TEXTO EXPLICATIVO",
           
        },
      
      ];

    return(
        <div className="columns is-mobile">
        <div className="column is-1 is-hidden-tablet has-text-centered" style={{minWidth: '10px'}}></div>
            <div className="column">
                <div className="columns is-mobile">
                    <div className="column is-1 is-hidden-tablet has-text-centered" style={{minWidth: '10px'}}></div>
                        <div className="column">
                        <DataTable data={data} columns={columns} title={"Historico de reclamación"}/>
                        </div>
                    <div className="column is-1 is-hidden-mobile" style={{maxWidth: '10px'}}></div>
                </div>
            </div>
        <div className="column is-1 is-hidden-mobile" style={{maxWidth: '10px'}}></div>
    </div>
    );
}

export default Table;
