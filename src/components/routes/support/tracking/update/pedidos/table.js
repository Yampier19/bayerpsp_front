import {Link, useParams} from 'react-router-dom';

import {tabClicked} from 'components/commons/tabs/tabClicked';
import DataTable from 'components/base/datatable'

const Table = props => {

    const columns = [
        { name: "guia", label: "No. DE GUÍA", options: { filter: true, sort: true } },
        { name: "serial", label: "SERIAL DE PRODUCTO", options: { filter: true, sort: true } },
        { name: "nombre", label: "NOMBRE DEL PRODUCTO", options: { filter: true, sort: true } },
        { name: "nombreM", label: "NOMBRE MEDICAMENTO", options: { filter: true, sort: true } },
        { name: "cantidad", label: "CANTIDAD", options: { filter: true, sort: true } },
        { name: "pap", label: "PAP PACIENTE", options: { filter: true, sort: true } },
        { name: "direccion", label: "DIRECCIÓN", options: { filter: true, sort: true } },
        { name: "destinatario", label: "DESTINATARIO", options: { filter: true, sort: true } },
        { name: "fechaN", label: "FECHA DE NACIMIENTO", 
        options: {
            filter: true,
            sort: true,
            customBodyRender: (value) => (
              <div
                className="column has-no-border py-0 pr-0 p-2"
                style={{ width: "100%" }}
              >
                <div
                  className="col is-mobile my-0"
                  style={{ height: "100%", background: "" }}
                >
                  <div className="row">
                    <div className="col-9 d-flex justify-content-center align-items-center">
                        <p className="text-center">02-12-2021</p>
                        </div>
                    <div className="col p-0" style={{backgroundColor: '#6992D6'}}>
                      <div className="row-reverse ">
                        <div className="col d-flex justify-content-center text-white" style={{paddingTop: 50}}> 
                           P
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ),
          }, },
       
      ];
    
      const data = [
        {
            guia: "123456",
            serial: "123456",
            nombre: "NOMBRE DEL PRODUCTO",
            nombreM: "NOMBRE MEDICAMENTO",
            cantidad: "CANTIDAD",
            pap: "PAP PACIENTE",
            direccion: "DIRECCIÓN",
            destinatario: "DESTINATARIO",
           
        },
       
      ];

    return(
        <div className="columns is-mobile">
        <div className="column is-1 is-hidden-tablet has-text-centered" style={{minWidth: '10px'}}></div>
            <div className="column">
                <div className="columns is-mobile">
                    <div className="column is-1 is-hidden-tablet has-text-centered" style={{minWidth: '10px'}}></div>
                        <div className="column">
                        <DataTable data={data} columns={columns} title={"Mis pedidos"}/>
                        </div>
                    <div className="column is-1 is-hidden-mobile" style={{maxWidth: '10px'}}></div>
                </div>
            </div>
        <div className="column is-1 is-hidden-mobile" style={{maxWidth: '10px'}}></div>
    </div>
    );
}

export default Table;
