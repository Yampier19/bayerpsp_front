import {useState} from 'react'

import FormField from 'components/form/formfield';
import FormField2 from 'components/form/formfield2';
import Dropdown from 'components/form/dropdown';
import Radio from 'components/form/radio';

import * as Yup from 'yup';
import {useFormik} from 'formik';


import {tabClicked} from 'components/commons/tabs/tabClicked';

const Form = props => {

    const [currentTab, setCurrentTab] = useState(1);
    {/* 
   
    const {paciente} = props;

    const formSchema = Yup.object().shape({
        codigoUsuario: Yup.number().typeError('se debe especificar un valor numerico'),
        estadoPaciente: Yup.string().required('Este campo es obligatorio'),
        fechaActivacion: Yup.string().required('Este campo es obligatorio'),
        solicitudCambioEstadoPaciente: Yup.string(),
        fechaRetiro: Yup.string(),
        motivoRetiro: Yup.string(),
        observacionesMotivoRetiro: Yup.string(),
        nombre: Yup.string().required('Este campo es obligatorio'),
        apellidos: Yup.string().required('Este campo es obligatorio'),
        tipoDeDocumento: Yup.number().typeError('se debe especificar un valor numerico').required('Este campo es obligatorio'),
        numeroIdentificacion: Yup.number().typeError('se debe especificar un valor numerico').required('Este campo es obligatorio'),
        telefono: Yup.number().typeError('se debe especificar un valor numerico').required('Este campo es obligatorio'),
        correoElectronico: Yup.string().required('Campo obligatorio').email('email invalido'),
        departamento:  Yup.string().required('Este campo es obligatorio'),
        ciudad:  Yup.string().required('Este campo es obligatorio'),
        barrio:  Yup.string().required('Este campo es obligatorio'),
        direccion:  Yup.string().required('Este campo es obligatorio'),
        fechaNacimiento: Yup.string().required('Este campo es obligatorio'),
        edad: Yup.number().typeError('se debe especificar un valor numerico'),
        acudiente: Yup.string(),
        telefonoAcudiente:  Yup.number().typeError('se debe especificar un valor numerico'),
        clasificacionPatolofica: Yup.string().required('Este campo es obligatorio'),
        fechaInicioTerapia: Yup.string().required('Este campo es obligatorio'),
    });

    const formik = useFormik({
        enableReinitialize: true,
        initialValues:  {
            codigoUsuario: paciente.codigoUsuario,
            estadoPaciente: paciente.estadoPaciente,
            fechaActivacion: paciente.fechaActivacion,
            solicitudCambioEstadoPaciente: paciente.olicitudCambioEstadoPaciente,
            fechaRetiro: paciente.fechaRetiro,
            motivoRetiro: paciente.motivoRetiro,
            observacionesMotivoRetiro: paciente.observacionesMotivoRetiro,
            nombre: paciente.nombre,
            apellidos: paciente.apellidos,
            tipoDeDocumento: paciente.tipoDeDocumento,
            numeroIdentificacion: paciente.numeroIdentificacion,
            telefono: paciente.telefono,
            correoElectronico: paciente.correoElectronico,
            departamento: paciente.departamento,
            ciudad: paciente.ciudad,
            barrio: paciente.barrio,
            direccion: paciente.direccion,
            fechaNacimiento: paciente.fechaNacimiento,
            edad: paciente.edad,
            acudiente: paciente.acudiente,
            telefonoAcudiente: paciente.telefonoAcudiente,
            clasificacionPatolofica: paciente.clasificacionPatolofica,
            fechaInicioTerapia: paciente.fechaInicioTerapia
        },
        validateOnChange: true,
        validationSchema: formSchema,
        onSubmit: async values => {
            alert(JSON.stringify(values, null, 2));
            // const {_icons} = props.modalReducer;
            // props.set_modal_on( new InfoModal('La EPS ha sido creada éxitosamente', _icons.CHECK_PRIMARY) );
        }


   
    });
*/}
    return(
        <form id="form"  className="pr-3 py-3">
            <FormField label="Codigo usuario" number="1" name="codigoUsuario" >
                <input className="input cool-input" type="text" name="codigoUsuario" disabled />
               
            </FormField>

            <FormField label="Estado paciente*" number="2" name="estadoPaciente" >
                <Dropdown name="estadoPaciente" options={['op1']} arrowColor="has-text-hgreen" />
               
            </FormField>

            <FormField2
                number1="3" number2="3.1" name1="fechaActivacion" name2="solicitudCambioEstadoPaciente"
               
                label1="Fecha de activación*" label2="Solicitar cambio de estado paciente"
                input1={
                    <div>
                        <input className="input cool-input" type="date" name="fechaActivacion" />
                       
                    </div>
                }

                input2={
                    <div>
                        <Radio name="solicitudCambioEstadoPaciente" />
                        
                    </div>
                }
            />

            <FormField2
                number1="4" number2="4.1" name1="fechaRetiro" name2="motivoRetiro"
               
                label1="Fecha de retiro" label2="Motivo de retiro"
                input1={
                    <div>
                        <input className="input cool-input" type="date" name="fechaRetiro" />
                        
                    </div>
                }

                input2={
                    <div>
                        <Dropdown name="motivoRetiro"  options={['op1', 'op2']} arrowColor="has-text-hgreen" />
                        
                    </div>
                }
            />

            <FormField label="Observaciones motivo de retiro" number="5" name="observacionesMotivoRetiro" >
                <input className="input cool-input" type="text" name="observacionesMotivoRetiro"  />
               
            </FormField>

            <FormField2
                number1="6" number2="6.1" name1="nombre" name2="apellidos"
                
                label1="Nombre*" label2="Apellidos*"
                input1={
                    <div>
                        <input className="input cool-input" type="text" name="nombre" />
                        
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="text" name="apellidos" />
                    </div>
                }
            />

            <FormField2
                number1="7" number2="7.1" name1="tipoDeDocumento" name2="numeroIdentificacion"

                label1="Tipo de documento*" label2="Número de identificación*"
                input1={
                    <div>
                        <input className="input cool-input" type="text" name="tipoDeDocumento" />
                      
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="text" name="numeroIdentificacion" />
                    </div>
                }
            />

            <FormField label="Teléfono" number="8" name="telefono" >
                <input className="input cool-input" type="text" name="telefono"/>
                
            </FormField>

            <FormField label="Correo electrónico" number="9" name="correoElectronico" >
                <input className="input cool-input" type="text" name="correoElectronico"  />
            </FormField>

            <FormField2
                number1="10" number2="10.1" name1="departamento" name2="ciudad"
               
                label1="Departamento*" label2="Ciudad*"
                input1={
                    <div>
                        <Dropdown name="departamento" options={['op1']} arrowColor="has-text-hgreen" />
                    </div>
                }

                input2={
                    <div>
                        <Dropdown name="ciudad" options={['op1']} arrowColor="has-text-hgreen" />
                    </div>
                }
            />

            <FormField2
                number1="11" number2="11.1" name1="departamento" name2="ciudad"
                label1="Barrio*" label2="Dirección*"
                input1={
                    <div>
                        <Dropdown name="barrio"  options={['op1']} arrowColor="has-text-hgreen" />
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="text" name="direccion"  />
                    </div>
                }
            />

            <FormField2
                number1="12" number2="12.1" name1="fechaNacimiento" name2="edad"
                label1="Fecha de nacimiento*" label2="Edad"
                input1={
                    <div>
                        <input className="input cool-input" type="date" name="fechaNacimiento"  />
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="text" name="edad" disabled />
                    </div>
                }
            />

            <FormField2
                number1="13" number2="13.1" name1="acudiente" name2="telefonoAcudiente"
                label1="Acudiente" label2="Télefono del acudiente"
                input1={
                    <div>
                        <input className="input cool-input" type="text" name="acudiente"  />
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="text" name="telefonoAcudiente"  />
                    </div>
                }
            />

            <FormField2
                noline
                number1="14" number2="14.1" name1="clasificacionPatolofica" name2="fechaInicioTerapia"
                label1="Clasificación patologica*" label2="Fecha inicio terapia*"
                input1={
                    <div>
                        <input className="input cool-input" type="text" name="clasificacionPatolofica"  />
                    </div>
                }

                input2={
                    <div>
                        <input className="input cool-input" type="date" name="fechaInicioTerapia"  />
                    </div>
                }
            />


            <br/>

            <div className="field">
                <div className="control has-text-right has-text-centered-mobile">
                    <a onClick={e => {tabClicked(e); setCurrentTab(2)}} data-tab="tab2"
                        className="button is-hgreen"
                        style={{width: '150px'}}
                    >
                        CONTINUAR
                    </a>
                </div>
            </div>

        </form>

    );
}

export default Form;
