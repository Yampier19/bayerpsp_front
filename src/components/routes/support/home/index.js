import MenuCard from 'components/cards/home/menucard';
// import CreateBtn from 'components/base/buttons/create-btn';

// import {Link} from 'react-router-dom';

import './home.scss';

const Home = props => {
    return(
        <div className="is-flex is-flex-direction-column " style={{height: '100%'}}>


            {/* header */}
            <section className="section px-0 pt-0">
                <div className="columns">
                    <div className="column is-3-desktop ">
                        <h1 className="title has-text-hpurple is-2">Apoyo Diagnóstico</h1>
                    </div>
                </div>
            </section>
           

            <br/>

            {/* second level */}
            <div className="columns">

                <div className="column mb-6">
                    <div className="notification py-2 is-hred"><h1 className="subtitle is-6">PACIENTE NUEVO</h1></div>
                    <MenuCard
                        title="Crear paciente nuevo"
                        color="hred"
                        to="/support/create/patient"
                    />
                </div>

                <div className="column mb-6">
                    <div className="notification py-2 is-hgreen"><h1 className="subtitle is-6">SEGUIMIENTO</h1></div>
                    <MenuCard
                        title="Consultar Seguimiento"
                        color="hgreen"
                        to="/support/tracking"
                    />
                </div>

            {/* 
                <div className="column mb-6">
                    <div className="notification py-2 is-hblue"><h1 className="subtitle is-6">REPORTES</h1></div>
                    <MenuCard
                        title="Todos los reportes"
                        color="hblue"
                        to="/support/reports"
                    />
                    <MenuCard
                        title="Gráficas"
                        color="hblue"
                        to="/support/reports/graphics"
                    />
                </div>
            */}
            </div>

        </div>
    );
}

export default Home;
