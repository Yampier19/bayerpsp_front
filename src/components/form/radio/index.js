const Radio = (props) => {
  return (
    <div className="control">
      <label className="radio">
        <input
          type="radio"
          name={props.name}
          onChange={props.onChange}
          value={props.option1 || "Si"}
          checked={props.value === props.option1 || props.value === "Si" || props.value === "SI"}
        />
        &nbsp;
        {props.option1 || "SI"}
      </label>{" "}
      &nbsp;&nbsp;&nbsp;
      <label className="radio">
        <input
          type="radio"
          name={props.name}
          onChange={props.onChange}
          value={props.option2 || "No"}
          checked={props.value === props.option2 || props.value === "No" || props.value === "NO"}
        />
        &nbsp;
        {props.option2 || "NO"}
      </label>
    </div>
  );
};

export default Radio;
