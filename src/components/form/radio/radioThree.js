const RadioThree = props => {
    return(
        <div class="control">
            <label class="radio">
                <input type="radio" name={props.name} onChange={props.onChange} value={props.option1 || 'Fisico'}/>&nbsp;
                {props.option1 || 'Fisico'}
            </label> &nbsp;&nbsp;&nbsp;
            <label class="radio">
                <input type="radio" name={props.name} onChange={props.onChange} value={props.option2 || 'Verbal'}/>&nbsp;
                {props.option2 || 'Verbal'}
             </label>
             <label class="radio">
                <input type="radio" name={props.name} onChange={props.onChange} value={props.option3 || 'SMS'}/>&nbsp;
                {props.option3 || 'SMS'}
             </label>
        </div>
    );
}

export default RadioThree;