import "./formfield.scss";

const FormField = (props) => {
  return (
    <div className="field mb-6 mt-2">
      <div className="columns is-mobile">
        <div className="column is-1" style={{ width: "50px" }}>
          <div className="control">
            <input
              className="def_checkbox"
              type="checkbox"
              name={`check_${props.name}`}
              checked={props.checked}
              readOnly
            />
            <div className={`newcheck ${props.noline ? "no-line" : ""}`}>
              <strong className="num">{props.number}</strong>
            </div>
          </div>
        </div>

        <div className="column">
          <div className="columns ">
            <div className="column is-2">
              <label className="label">{props.label}</label>
            </div>
            <div className="column">
              <div className="control">{props.children}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormField;
