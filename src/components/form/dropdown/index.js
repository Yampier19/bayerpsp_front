import "./dropdown.scss";

const Dropdown = ({
  options = [],
  value = null,
  arrowColor,
  id = 0,
  name = "No name",
  onChange = () => {},
  disabled = false,
}) => {
  const _options = Array.isArray(options) ? options : ["No data"]; // por si el form no sirve: || ['Este', 'Field', 'No', 'Tiene', 'Valores']
  const optIncludesValue = _options.includes(value);

  return (
    <div>
      <span className={`icon down-arrow ${arrowColor}`}>
        <i class="fas fa-chevron-down"></i>
      </span>
      <select
        id={id}
        className="input cool-input dropdown-base"
        name={name}
        onChange={onChange}
        disabled={disabled}
      >
        <option
          // disabled
          selected={value == null || !optIncludesValue}
          className=""
        >
          {" "}
        </option>
        {_options.map((op, i) => {
          if (typeof op === "object") {
            let selected = false;
            if (typeof value === "number") {
              selected = Number(value) === op.id;
            }
            if (typeof value === "string") {
              selected = value === op.name;
            }
            return (
              <option key={i} value={op.id} selected={selected}>
                {op.name}
              </option>
            );
          } else {
            return (
              <option key={i} value={op} selected={value === op}>
                {op}
              </option>
            );
          }
        })}
      </select>
    </div>
  );
};

export default Dropdown;
