import { TextField } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import React, { useState, useEffect } from "react";

export const DropdownOthers = ({
  name = "",
  options = [],
  value = "",
  onChange = () => {},
  props = {},
  renderInputProps = {},
  disabled = false,
}) => {
  return (
    <Autocomplete
      disabled={disabled}
      freeSolo
      autoSelect
      id={name}
      name={name}
      onChange={onChange}
      value={value}
      options={options}
      {...props}
      renderInput={(params) => (
        <TextField name={name} id={name} {...params} {...renderInputProps} />
      )}
    />
  );
};
