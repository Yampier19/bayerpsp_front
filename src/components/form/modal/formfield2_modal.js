import React from 'react'
import FormControlLabel from '@mui/material/FormControlLabel';
import Input from '@mui/material/Input';

const FormField_modal = (props) => {
    const ariaLabel = { 'aria-label': 'description' };

  return (
    <div className="field mb-4">

    <div className="columns is-mobile">

        <div className="column is-1" style={{width: '50px'}}>
            <div className="control">
                <input className="def_checkbox" type="checkbox" name={`check_${props.name}`} checked={props.checked} readOnly/>
                <div className={`newcheck ${props.noline ? 'no-line' : ''}`}>
                    <strong className="num">{props.number}</strong>
                </div>
            </div>
        </div>

        <div className="column">
            <div className="columns ">
                <div className="column is-12">
                     <label className="label" style={{fontSize: 14}}>{props.label}</label>
                </div>
            </div>
            <div className="columns">
                    <div className="control ml-3">
                        <Input defaultValue="Hello world" inputProps={ariaLabel} style={{width: 400}}/>
                    </div>
                </div>
        </div>

    </div>


</div>
  );
};

export default FormField_modal;
