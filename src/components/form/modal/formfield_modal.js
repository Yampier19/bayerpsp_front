import React from 'react'
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';

const FormField_modal = (props) => {
  const [selectedValue, setSelectedValue] = React.useState('a');

  const handleChange = (event) => {
    setSelectedValue(event.target.value);
  };
  return (
    <div className="field mb-4">

    <div className="columns is-mobile">

        <div className="column is-1" style={{width: '50px'}}>
            <div className="control">
                <input className="def_checkbox" type="checkbox" name={`check_${props.name}`} checked={props.checked} readOnly/>
                <div className={`newcheck ${props.noline ? 'no-line' : ''}`}>
                    <strong className="num">{props.number}</strong>
                </div>
            </div>
        </div>

        <div className="column">
            <div className="columns ">
                <div className="column is-12">
                     <label className="label" style={{fontSize: 14}}>{props.label}</label>
                </div>
            </div>
            <div className="columns">
                    <div className="control">
                      <div className="row">
                        <div className="col d-flex justify-content-center">
                          <FormControlLabel
                              value="start"
                              control={<Radio />}
                              label="Si"
                              labelPlacement="start"
                            />
                        </div>
                        <div className="col d-flex justify-content-center">
                          <FormControlLabel
                              value="start"
                              control={<Radio />}
                              label="No"
                              labelPlacement="start"
                            />
                        </div>
                      </div>
                    </div>
                </div>
        </div>

    </div>


</div>
  );
};

export default FormField_modal;
