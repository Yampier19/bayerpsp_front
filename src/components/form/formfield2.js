const FormField2 = (props) => {
  return (
    <div className="field2 mb-6 mt-2">
      <div className="columns " style={{ minHeight: "100px" }}>
        <div className="column">
          <div className="columns  is-mobile">
            <div className="column is-1" style={{ width: "50px" }}>
              <div className="control">
                <input
                  className="def_checkbox"
                  type="checkbox"
                  name={`check_${props.name1}`}
                  checked={props.checked1}
                  readOnly
                />
                <div
                  className={`newcheck ${
                    props.noline ? "no-line-desktop" : ""
                  }`}
                >
                  <strong className="num">{props.number1}</strong>
                </div>
              </div>
            </div>

            <div className="column">
              <div className="columns ">
                <div className="column is-4">
                  <label className="label">{props.label1}</label>
                </div>
                <div className="column">
                  <div className="control">{props.input1}</div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="column mt-2">
          <div className="columns is-vcentered-desktop is-mobile">
            <div
              className="column is-1 is-hidden-tablet"
              style={{ width: "50px" }}
            >
              <div className="control">
                <input
                  className="def_checkbox"
                  type="checkbox"
                  name={`check_${props.name2}`}
                  checked={props.checked2}
                  readOnly
                />
                <div
                  className={`newcheck no-line-desktop ${
                    props.noline ? "no-line" : ""
                  }`}
                >
                  <strong className="num">{props.number2}</strong>
                </div>
              </div>
            </div>

            <div className="column">
              <div className="columns">
                <div className="column is-4">
                  <label className="label">{props.label2}</label>
                </div>
                <div className="column">
                  <div className="control">{props.input2}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormField2;
