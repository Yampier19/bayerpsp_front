import { baseURL, calendar } from "../../../images";

import "./date.scss";

const Date = (props) => {
  return (
    <input
      type="date"
      placeholder={props.placeholder}
      className={`${
        props.value ? "" : "placeholderclass"
      } input cool-input dateclass`}
      name={props.name}
      value={props.value}
      onChange={props.onChange}
      data-iconURL={baseURL + calendar}
    />
  );
};

export default Date;
