import {Link} from 'react-router-dom';

import {useHistory} from 'react-router-dom';
import {connect} from 'react-redux';
import {logout_request} from '../../../../redux/actions/authenticationActions';

const LogoutBtn = props => {

    const {logout} = props.authenticationReducer;

    const history = useHistory();

    const handleLogout = async e => {
        const success = await props.logout_request();
    }

    return(
        <button
            className={`button has-text-left is-expanded is-fullwidth is-rounded has-background-transparent has-no-border has-text-danger my-0`}
            onClick={handleLogout}
            >
            <span className="icon is-size-5 has-text-centered">
                {props.children}
            </span>
            {
                props.name != null ?
                <span>&nbsp;{props.name}</span>
                :null
            }

        </button>
    );
}

const mapStateToProps = state => ({
    authenticationReducer: state.authenticationReducer
});

export default connect(
    mapStateToProps,
    {
        logout_request
    }
)(LogoutBtn);
