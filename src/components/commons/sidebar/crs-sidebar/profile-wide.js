import NotificationBtn from '../../../base/notification-btn';


import Notification from '../../../base/notification';
import DotsBtn from '../dots-btn';

import {Link} from 'react-router-dom';
import Country from "./../country";

const ProfileWide3 = props => {

    const {name, role} = props;

    return(
        <div className="px-5">

            <section className="section px-0 pt-0">
                <div className="columns is-vcentered is-mobile">
                    <div className="column">
                        <Link to="/support/home" className="has-text-black">
                            <span className="icon is-size-5 ">
                                <i class="far fa-arrow-alt-circle-left"></i>
                            </span>
                        </Link>
                    </div>

                    <div className="column has-text-right">
                        <DotsBtn/>
                    </div>
                </div>
            </section>


            {/* profile */}
            <section className="section p-0 mx-0">
                {
                    //name
                }
                <div className="columns is-mobile">
                    <div className="column is-4">
                        <figure className="image is-3by4">
                            <img src="https://bulma.io/images/placeholders/128x128.png"/>
                        </figure>
                    </div>
                    <div className="column">
                        <h1 className="title is-5" >{name}</h1>
                        <h2 className="subtitle is-6 has-text-light2">Cargo</h2>
                        <Country />
                    </div>
                </div>

                {
                    //perfil
                }
                <br/>
                <div className="mb-6">
                    <h2 className="subtitle is-6 has-text-light2">Perfil</h2>
                    <h1 className="title is-5" >{role}</h1>
                    
                </div>
            </section>

            {/* notification */}
            <section className="section p-0 m-0 is-hidden-touch">

                <a className="columns is-vcentered is-mobile has-text-black notification-btn" style={{borderTop: '1px solid black', borderBottom: '1px solid black'}}>
                    <div className="column is-1">
                        <NotificationBtn news/>
                    </div>
                    <div className="column">
                        <h1 className="subtitle is-6 has-text-right">tienes - notificaciones</h1>
                    </div>

                    <div className="notification-panel">
                        <Notification/>
                    </div>
                 </a>

            </section>
        </div>
    );
}

export default ProfileWide3;
