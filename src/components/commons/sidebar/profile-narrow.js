import DotsBtn from './dots-btn';

const ProfileNarrow = props => {

    const {name} = props;

    return(
        <div className="px-1">

            <section className="px-0 pt-0 pb-3">
                <DotsBtn/>
            </section>


            {/* profile */}
            <section className="p-0 mx-0 has-text-centered">
                {
                    //name
                }

                    <figure className="image is-32x32">
                        <img className="is-rounded" src="https://bulma.io/images/placeholders/128x128.png"/>
                    </figure>
                    <br/>

                    {/* <h1 className="title is-5 verticaltext">{name}</h1> */}


            </section>


        </div>
    );
}

export default ProfileNarrow;
