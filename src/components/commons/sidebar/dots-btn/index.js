import { Link } from "react-router-dom";
import { When } from "react-if";
import { useAuthorizationSystem } from "../../../../providers/authorization/system/authorizationSystemProvider";
const DotsBtn = () => {
  const { canSystemPSP, canSystemAD, canSystemCRS } = useAuthorizationSystem();
  return (
    <div className="dropdown is-hoverable">
      <div className="dropdown-trigger">
        <button
          className="button"
          aria-haspopup="true"
          aria-controls="dropdown-menu"
          style={{ background: "rgba(0,0,0,0)", border: "none" }}
        >
          <span className="icon is-small">
            <i className="fas fa-ellipsis-v has-text-primary"></i>
          </span>
        </button>
      </div>
      <div className="dropdown-menu" id="dropdown-menu" role="menu">
        <div className="dropdown-content">
          <When condition={canSystemPSP}>
            <Link to="/home" href="#" className="dropdown-item has-text-left">
              Programa de Seguimiento a pacientes
            </Link>
          </When>
          <When condition={canSystemCRS}>
            <Link to="/crs/home" className="dropdown-item has-text-left">
              Programa de compensacion y reembolso
            </Link>
          </When>
          <When condition={canSystemAD}>
            <Link
              to="/support/home"
              href="#"
              className="dropdown-item has-text-left"
            >
              Apoyo Diagnostico
            </Link>
          </When>
        </div>
      </div>
    </div>
  );
};

export default DotsBtn;
