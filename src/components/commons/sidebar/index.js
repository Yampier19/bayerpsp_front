import React from "react";
import { connect } from "react-redux";
import ScrollContainer from "react-indiana-drag-scroll";
import { When } from "react-if";

import ProfileWide from "./profile-wide";
import ProfileNarrow from "./profile-narrow";
import SidebarBtn from "./sidebar-btn";
import LogoutBtn from "./logout-btn";
import SubButton from "./sub-btn";
import { open_sidebar, close_sidebar } from "redux/actions/sidebarActions";
import "./sidebar.scss";
import { useAuthorizationAction } from "providers/authorization/action/authorizationActionProvider";

const Sidebar = (props) => {
  const userData = props.authenticationReducer.sessionData.user;
  const {
    canSettingUsersEdit,
    canSettingUsersView,
    canNoveltysConsultView,
    canTrackingListView,
    canProductsInventoryView,
    canReportsListView,
    canReportsCreateEdit,
  } = useAuthorizationAction();

  // *~~**~~**~~**~~**~~**~~**~~**~~* type
  const type = props.type || 1;

  // *~~**~~**~~**~~**~~**~~**~~**~~* sidebar state
  const { sidebarData } = props;

  const collapseClicked = (e) => {
    if (sidebarData.isOpen) props.close_sidebar();
    else props.open_sidebar();
  };

  return (
    <nav
      id="sidebar"
      className={`menu has-background-light is-flex is-flex-direction-column ${
        props.sidebarData.isOpen ? "open" : ""
      }`}
    >
      {/* collapse btn */}
      <div className="collapse-button">
        <button
          className="button is-primary is-rounded"
          onClick={collapseClicked}
        >
          <span className="icon">
            <i className="fas fa-bars"></i>
          </span>
        </button>
      </div>
      <br />
      <div className="sidebar-body mb-0">
        {/*<div className="has-background-hblue" style={{height: '300px'}}></div>*/}
        {sidebarData.isOpen ? (
          <ProfileWide name={userData.name} role={userData.post} />
        ) : (
          <ProfileNarrow name={userData.name} />
        )}
      </div>
      <br />
      <div
        className="sidebar-footer is-flex-grow-1"
        style={{ overflow: "hidden" }}
      >
        <div
          className="is-flex is-flex-direction-column is-justify-content-flex-end"
          style={{ overflow: "hidden", height: "100%", width: "100%" }}
        >
          <div className="coolscroll" style={{ overflow: "auto" }}>
            {type === 1 ? (
              <>
                <section
                  className={`section pb-3 pt-4 has-background-white ${
                    sidebarData.isOpen ? "px-2" : "px-0"
                  } `}
                >
                  <SidebarBtn
                    isActive={props.activeSite === "1"}
                    activeClass=""
                    activeTextClass=""
                    to="/home"
                    name={sidebarData.isOpen ? "Inicio" : null}
                  >
                    <i className="fas fa-home"></i>
                  </SidebarBtn>
                  <When condition={canNoveltysConsultView}>
                    <SidebarBtn
                      isActive={props.activeSite === "2"}
                      activeClass="is-hgreen-light"
                      activeTextClass="has-text-hgreen"
                      to={{ pathname: "/news/consult", search: "?page=1" }}
                      name={sidebarData.isOpen ? "Novedades" : null}
                    >
                      <i className="fas fa-plus-circle"></i>
                    </SidebarBtn>
                  </When>
                  <When condition={canTrackingListView}>
                    <SidebarBtn
                      isActive={props.activeSite === "3"}
                      activeClass="is-hblue-light"
                      activeTextClass="has-text-hblue"
                      to="/tracking"
                      name={sidebarData.isOpen ? "Seguimiento" : null}
                    >
                      <i className="fas fa-comment"></i>
                    </SidebarBtn>
                  </When>
                  <When condition={canProductsInventoryView}>
                    <SidebarBtn
                      isActive={props.activeSite === "4"}
                      activeClass="is-horange-light"
                      activeTextClass="has-text-horange"
                      to="/products"
                      name={sidebarData.isOpen ? "Productos" : null}
                    >
                      <i className="fas fa-box"></i>
                    </SidebarBtn>
                  </When>
                  <When condition={canReportsListView || canReportsCreateEdit}>
                    <SidebarBtn
                      isActive={props.activeSite === "5"}
                      activeClass="is-hred-light"
                      activeTextClass="has-text-hred"
                      to="/reports"
                      name={sidebarData.isOpen ? "Reportes" : null}
                    >
                      <i className="fas fa-paste"></i>
                    </SidebarBtn>
                  </When>

                  {props.activeSite === "5" ? (
                    <div className="py-1" style={{ overflow: "hidden" }}>
                      <When condition={canReportsCreateEdit}>
                        <SubButton
                          isActive={props.activeSite === "5"}
                          activeTextClass="has-text-hred"
                          linecolor="hred"
                          to="/reports/create"
                          name={sidebarData.isOpen ? "Filtros" : null}
                        >
                          <i className="fas fa-paste"></i>
                        </SubButton>
                      </When>
                      <When condition={canReportsListView}>
                        <div className="pb-1" />
                        <SubButton
                          isActive={props.activeSite === "5"}
                          activeTextClass="has-text-hred"
                          linecolor="hred"
                          to="/reports/graphics"
                          name={sidebarData.isOpen ? "Gráficas" : null}
                        >
                          <i className="fas fa-paste"></i>
                        </SubButton>
                      </When>
                    </div>
                  ) : null}
                  <When condition={canSettingUsersEdit || canSettingUsersView}>
                    <SidebarBtn
                      isActive={props.activeSite === "6"}
                      activeClass="is-light"
                      activeTextClass="has-text-hblack"
                      to="/configuration/users"
                      name={sidebarData.isOpen ? "Configuración" : null}
                    >
                      <i className="fas fa-cog"></i>
                    </SidebarBtn>
                  </When>

                  {props.activeSite === "6" ? (
                    <ScrollContainer
                      className="py-1 scroll-container coolscroll"
                      style={{ maxHeight: "100px" }}
                      horizontal={false}
                      hideScrollbars={false}
                    >
                      {/* <SubButton
                        isActive={props.activeSite === "5"}
                        activeTextClass="has-text-hblack"
                        linecolor="hblack"
                        to="/configuration/fundem"
                        name={sidebarData.isOpen ? "Gestiones" : null}
                      >
                        <i className="fas fa-paste"></i>
                      </SubButton>

                      <SubButton
                        isActive={props.activeSite === "5"}
                        activeTextClass="has-text-hblack"
                        linecolor="hblack"
                        to="/configuration/date"
                        name={sidebarData.isOpen ? "Cambio de fecha" : null}
                      >
                        <i className="fas fa-paste"></i>
                      </SubButton> */}
                      <When condition={canSettingUsersView}>
                        <SubButton
                          isActive={props.activeSite === "5"}
                          activeTextClass="has-text-hblack"
                          linecolor="hblack"
                          to="/configuration/users"
                          name={sidebarData.isOpen ? "Usuarios" : null}
                        >
                          <i className="fas fa-paste"></i>
                        </SubButton>
                      </When>
                      <When condition={canSettingUsersEdit}>
                        <SubButton
                          isActive={props.activeSite === "5"}
                          activeTextClass="has-text-hblack"
                          linecolor="hblack"
                          to="/configuration/create"
                          name={sidebarData.isOpen ? "Creación de rol" : null}
                        >
                          <i className="fas fa-paste"></i>
                        </SubButton>
                      </When>

                      {/*
                      <SubButton
                        isActive={props.activeSite === "5"}
                        activeTextClass="has-text-hblack"
                        linecolor="hblack"
                        to="/configuration/templates"
                        name={
                          sidebarData.isOpen ? "Historial plantillas" : null
                        }
                      >
                        <i className="fas fa-paste"></i>
                      </SubButton>

                      <SubButton
                        isActive={props.activeSite === "5"}
                        activeTextClass="has-text-hblack"
                        linecolor="hblack"
                        to="/configuration/users"
                        name={
                          sidebarData.isOpen ? "Historial asignación" : null
                        }
                      >
                        <i className="fas fa-paste"></i>
                      </SubButton>

                      <SubButton
                        isActive={props.activeSite === "5"}
                        activeTextClass="has-text-hblack"
                        linecolor="hblack"
                        to="/configuration/email"
                        name={sidebarData.isOpen ? "Correo electrónico" : null}
                      >
                        <i className="fas fa-paste"></i>
                      </SubButton>


                      */}
                    </ScrollContainer>
                  ) : null}
                </section>
                <div
                  className={`section py-2 has-background-white ${
                    sidebarData.isOpen ? "px-2" : "px-0"
                  }`}
                >
                  <LogoutBtn name={sidebarData.isOpen ? "Cerrar Sesión" : null}>
                    <i className="gg-log-off"></i>
                  </LogoutBtn>
                </div>
              </>
            ) : (
              <section
                className={`pb-4 ${sidebarData.isOpen ? "px-2" : "px-0"} `}
              >
                <When condition={canSettingUsersEdit || canSettingUsersView}>
                  <SidebarBtn
                    isActive
                    activeClass="has-background-light"
                    name={sidebarData.isOpen ? "Configuración" : null}
                    to="/configuration/users"
                  >
                    <i className="fas fa-cog"></i>
                  </SidebarBtn>
                </When>
                <LogoutBtn name={sidebarData.isOpen ? "Cerrar Sesión" : null}>
                  <i className="gg-log-off"></i>
                </LogoutBtn>
                <br />
                <h1
                  className={`subtitle is-6 px-4 has-text-light2 ${
                    sidebarData.isOpen ? "" : "is-hidden"
                  }`}
                >
                  By People Marketing
                </h1>
              </section>
            )}
          </div>
        </div>
      </div>
    </nav>
  );
};

const mapStateToProps = (state) => ({
  sidebarData: state.sidebarReducer,
  authenticationReducer: state.authenticationReducer,
});

export default connect(mapStateToProps, {
  close_sidebar,
  open_sidebar,
})(Sidebar);
