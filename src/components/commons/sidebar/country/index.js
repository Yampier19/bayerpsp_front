import _ from "lodash";
import { useState } from "react";
import { useEffect } from "react";
import { Switch } from "react-if";
import { Case } from "react-if";
import { Default } from "react-if";
import { Link } from "react-router-dom";

//IMAGENES PAISES
import IMGColombia from "./../../../../utils/img/flags/colombia.png";
import IMGPeru from "./../../../../utils/img/flags/peru.png";
import IMGEcuador from "./../../../../utils/img/flags/ecuador.png";

import { useListAllCountries } from "services/bayer/psp/countries/useCountries";
import { capitalizeFirstLetter } from "utils/capitalizeFirstLetter";
import { useCountry } from "hooks/useCountry";

const DotsCountry = () => {
  const { data: Countries } = useListAllCountries({
    retry: false,
    enabled: false,
  });
  const { country, set } = useCountry();
  const [countryLocal, setCountry] = useState(null);

  const selectedCountry = (id) => {
    set(id);
    setCountry(id);
  };

  useEffect(() => {}, [countryLocal]);

  const renderCuntry = (data, index) => (
    <Link
      key={`country_${index}`}
      className="dropdown-item has-text-left"
      onClick={() => selectedCountry(data.id)}
      to="/home"
    >
      <div className="row">
        <div className="col d-flex align-items-center justify-content-center p-0">
          <Switch>
            <Case condition={data.name === "COLOMBIA"}>
              <img
                src={IMGColombia}
                alt="img_colombia"
                style={{ width: "30%" }}
              />
            </Case>
            <Case condition={data.name === "PERU"}>
              <img src={IMGPeru} alt="img_peru" style={{ width: "30%" }} />
            </Case>
            <Case condition={data.name === "ECUADOR"}>
              <img
                src={IMGEcuador}
                alt="img_ecuador"
                style={{ width: "30%" }}
              />
            </Case>
            <Default>
              <img
                src={IMGColombia}
                alt="img_colombia"
                style={{ width: "30%" }}
              />
            </Default>
          </Switch>
        </div>
        <div className="col d-flex align-items-center justify-content-start p-0">
          <label
            style={{
              fontSize: 15,
              fontWeight: "bold",
              cursor: "pointer",
            }}
          >
            {capitalizeFirstLetter(data.name)}
          </label>
        </div>
      </div>
    </Link>
  );

  return (
    <div className="dropdown is-hoverable">
      <div className="dropdown-trigger">
        <Switch>
          <Case condition={country === 1}>
            <img
              src={IMGColombia}
              alt="img_profile"
              className="w-25"
              aria-controls="dropdown-menu"
              style={{ cursor: "pointer" }}
            />
          </Case>
          <Case condition={country === 2}>
            <img
              src={IMGEcuador}
              alt="img_profile"
              className="w-25"
              aria-controls="dropdown-menu"
              style={{ cursor: "pointer" }}
            />
          </Case>
          <Case condition={country === 3}>
            <img
              src={IMGPeru}
              alt="img_profile"
              className="w-25"
              aria-controls="dropdown-menu"
              style={{ cursor: "pointer" }}
            />
          </Case>
          <Default>
            <img
              src={IMGColombia}
              alt="img_profile"
              className="w-25"
              aria-controls="dropdown-menu"
              style={{ cursor: "pointer" }}
            />
          </Default>
        </Switch>
      </div>
      <div className="dropdown-menu" id="dropdown-menu" role="menu">
        <div className="dropdown-content">
          {_.map(Countries?.data, renderCuntry)}
        </div>
      </div>
    </div>
  );
};

export default DotsCountry;
