import './sub-btn.scss';

import {Link} from 'react-router-dom';

const SubButton = props => {
    return(
        <div className="">
            <Link
                className={`button has-text-left is-fullwidth is-rounded has-no-border has-background-transparent ${props.isActive ? props.activeTextClass : ''}`}
                to={props.to}
                >
                <span className={`icon is-size-7 has-text-centered sub-btn ${props.linecolor}`} style={{zIndex: '1'}}>
                    <i class="fas fa-circle"></i>
                </span>
                {
                    props.name != null ?
                    <span>&nbsp;{props.name}</span>
                    :null
                }

            </Link>
        </div>
    );
}

export default SubButton;
