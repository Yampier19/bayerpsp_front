export const tabClicked = (e) => {
  toggleTabBtn(e);
  toggleTabPanel(e);
};

const toggleTabBtn = (e) => {
  //switch off current button
  const tabButtons = document.getElementsByClassName("formTab");
  //eslint-disable-next-line
  Array.from(tabButtons).map((elm) => {
    const classes = Array.from(elm.classList);
    if (classes.includes("is-active")) elm.classList.toggle("is-active");
  });

  //switch on clicked button
  const newButton = e.currentTarget;
  newButton && newButton.classList.toggle("is-active");
};

const toggleTabPanel = (e) => {
  //switch off current tab
  const tabPanels = document.getElementsByClassName("tab");
  //eslint-disable-next-line
  Array.from(tabPanels).map((elm) => {
    const classes = Array.from(elm.classList);
    if (classes.includes("is-active")) elm.classList.toggle("is-active");
  });

  //switch on clicked tab
  if (e.currentTarget){
    const target_tab = e.currentTarget.dataset.tab;
    const tab = document.getElementById(target_tab);
    tab.classList.toggle("is-active");
  }
};
