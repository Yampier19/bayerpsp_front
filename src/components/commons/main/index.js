import React from "react";
import { connect } from "react-redux";

import NotificationBtn from "../../base/notification-btn";
import Notification from "../../base/notification";

import { Link } from "react-router-dom";

import "./main.scss";
import "./notification-hover2.scss";

const Main = (props) => {
  const { sidebarData } = props;

  const notificationClicked = (e) => {
    e.preventDefault();

    const panel = document.querySelector("#notification-panel2");
    panel.classList.toggle("is-hidden");
  };

  return (
    <div
      id="main"
      className={`block py-6 px-4 ${sidebarData.isOpen ? "main-open" : ""}`}
    >
      {/* back arrow and notification btn */}
      <div className="columns is-mobile is-hidden-desktop">
        <div className="column">
          <Link to="/home" className="has-text-black">
            <span className="icon is-size-5">
              <i
                className="far fa-arrow-alt-circle-left"
                aria-hidden="true"
              ></i>
            </span>
          </Link>
        </div>
        <div className="column has-text-right">
          <button
            className="has-text-black"
            onClick={notificationClicked}
            style={{ position: "relative" }}
          >
            <NotificationBtn news />
            <div
              id="notification-panel2"
              className="notification-panel2 is-hidden"
            >
              <Notification />
            </div>
          </button>
        </div>
      </div>

      <div
        className={`black-bg is-hidden-desktop ${
          sidebarData.isOpen ? "" : "is-transparent"
        }`}
      ></div>

      {props.children}
    </div>
  );
};

const mapStateToProps = (state) => ({
  sidebarData: state.sidebarReducer,
});

export default connect(mapStateToProps, null)(Main);
